<?php

use App\Http\Controllers\ctr_login;
use App\Http\Controllers\ctr_usuarios;
use App\Http\Controllers\ctr_activos;
use App\Http\Controllers\ctr_instrumentos;
use App\Http\Controllers\ctr_estudiantes;
use App\Http\Controllers\ctr_contratos;
use App\Http\Controllers\ctr_reportes;
use App\Http\Controllers\ctr_boletas;
use App\Http\Controllers\ctr_ayuda;
use App\Http\Controllers\ctr_registroAcciones;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ctr_login::class)->name('inicio');

Route::post('/login', [ctr_login::class, 'ingresar'])->name('ingresar');

Route::get('/logout', [ctr_login::class, 'salir'])->name('salir');

Route::get('/recoverPass/{username}', [ctr_login::class, 'recuperarContrasena'])->name('recuperarContrasena');

//  Rutas necesarias para los USUARIOS

Route::get('/usuarios', ctr_usuarios::class)->name('usuarios'); //  Ruta para mostrar los usuarios

Route::post('/usuarios/filtrar', [ctr_usuarios::class, 'filtrarUsuarios'])->name('filtrarUsuarios');   //  Ruta para ir filtrar los usuarios

Route::get('/usuarios/agregar', [ctr_usuarios::class, 'agregarUsuario'])->name('agregarUsuario');   //  Ruta para ir a la pantalla de agregar usuarios

Route::post('/usuarios/agregar/guardar', [ctr_usuarios::class, 'guardarUsuario'])->name('guardarUsuario');  //  Ruta para guardar usuarios nuevos

Route::get('/usuarios/editar/{id}', [ctr_usuarios::class, 'editarUsuario'])->name('editarUsuario'); //  Ruta para ir a la pantalla de editar usuarios

Route::put('/usuarios/editar/guardar', [ctr_usuarios::class, 'actualizarUsuario'])->name('actualizarUsuario');  //  Ruta para actualizar un usuario editado

Route::get('/usuarios/darBaja/{id}', [ctr_usuarios::class, 'bajaUsuario'])->name('bajaUsuario');    //  Ruta para dar de baja a un usuario

Route::get('/usuarios/activar/{id}', [ctr_usuarios::class, 'activarUsuario'])->name('activarUsuario');    //  Ruta para activar un usuario

Route::get('/usuarios/getPresidente', [ctr_usuarios::class, 'getPresidente']);    //  Ruta para obtener el presidente de la asociacion, si existe

Route::post('/usuarios/guardarPresidente', [ctr_usuarios::class, 'savePresidente']);    //  Ruta para guardar el presidente de la asociacion


//  Rutas necesarias para los ACTIVOS

Route::get('/activos', ctr_activos::class)->name('activos');

Route::post('/activos/page', [ctr_activos::class, 'paginarAjax']); //  Ruta para ir filtrar los estudiantes

Route::get('/activos/agregar', [ctr_activos::class, 'agregarActivo'])->name('agregarActivo');   //  Ruta para ir a la pantalla de agregar activos

Route::get('/activos/recepcion', [ctr_activos::class, 'recepcionActivo'])->name('recepcionActivo');   //  Ruta para ir a la pantalla de recepcion de activos

Route::post('/activos/agregar/guardar',[ctr_activos::class, 'guardarActivo'])->name('guardarActivo');  //Ruta para guardar activos nuevos

Route::get('/activos/editar/{id}',[ctr_activos::class, 'editarActivo'])->name('editarActivo');  //Ruta para ir a la pantalla de editar activos

Route::put('/activos/editar/guardar',[ctr_activos::class, 'actualizarActivo'])->name('actualizarActivo');  // Ruta para actualizar la informacion de un activo editado

Route::get('/activos/darBaja/{id}', [ctr_activos::class, 'bajaActivo'])->name('bajaActivo');    //  Ruta para dar de baja un activo

Route::get('/activos/activar/{id}', [ctr_activos::class, 'activarActivo'])->name('activarActivo');    //  Ruta para activar un activo

Route::post('/activos/filtrar', [ctr_activos::class, 'filtrarActivos'])->name('filtrarActivos');   //  Ruta para filtrar los activos

// Ruta especial para obtener los activos y filtrarlos en la vista crear boleta de salida
Route::get('/activos/listaActivos', [ctr_activos::class, 'listarActivos'])->name('listarActivos');

Route::post('/activos/filtrarModal', [ctr_activos::class, 'filtrarModal'])->name('filtrarModal');

//  Rutas necesarias para los INSTRUMENTOS

Route::get('/instrumentos', ctr_instrumentos::class)->name('instrumentos');

Route::get('/instrumentos/agregar', [ctr_instrumentos::class, 'agregarInstrumento'])->name('agregarInstrumento'); //  Ruta para ir a la pantalla de agregar instrumentos

Route::get('/instrumentos/recepcion', [ctr_instrumentos::class, 'recepcionInstrumento'])->name('recepcionInstrumento'); //  Ruta para ir a la pantalla de recepcion de instrumentos

Route::post('/instrumentos/agregar/guardar',[ctr_instrumentos::class, 'guardarInstrumento'])->name('guardarInstrumento');   //Ruta para guardar instrumentos nuevos

Route::get('/instrumentos/editar/{id}',[ctr_instrumentos::class, 'editarInstrumento'])->name('editarInstrumento');  //Ruta para ir a la pantalla de editar instrumentos

Route::put('/instrumentos/editar/guardar',[ctr_instrumentos::class, 'actualizarInstrumento'])->name('actualizarInstrumento');   // Ruta para actualizar la informacion de un instrumento editado

Route::get('/instrumentos/darBaja/{id}', [ctr_instrumentos::class, 'bajarInstrumento'])->name('bajarInstrumento');    //  Ruta para dar de baja un instrumento

Route::get('/instrumentos/activar/{id}', [ctr_instrumentos::class, 'activarInstrumento'])->name('activarInstrumento');    //  Ruta para activar un instrumento

Route::post('/instrumentos/filtrar', [ctr_instrumentos::class, 'filtrarInstrumentos'])->name('filtrarInstrumentos');   //  Ruta para ir filtrar los instrumentos

Route::get('/instrumentos/ultimoPorTipo/{cod}', [ctr_instrumentos::class, 'ultimoIstrumentoPortipo'])->name('ultimoIstrumentoPortipo');    //  Ruta para activar un instrumento

// Rutas necesarias para los ESTUDIANTES

Route::get('/estudiantes', ctr_estudiantes::class)->name('estudiantes'); // Ruta para la pantalla donde se muestran los estudiantes

Route::post('/estudiantes/filtrar', [ctr_estudiantes::class, 'filtrarEstudiantes'])->name('filtrarEstudiantes');   //  Ruta para ir filtrar los estudiantes

Route::get('/estudiantes/actualizar', [ctr_estudiantes::class, 'actualizarListaEstudiantes'])->name('acualizarEstudiantes');

// Rutas necesarias para los CONTRATOS  

Route::get('/contratos', ctr_contratos::class)->name('contratos'); // Ruta para la vista/pantalla donde se encuentran los contratos

Route::get('/contratos/detalle/{id}', [ctr_contratos::class, 'detalleContrato'])->name('detalleContrato'); // Ruta para la vista/pantalla donde se podran ver/generar los contratos

Route::post('/contratos/filtrar', [ctr_contratos::class, 'filtrarContratos'])->name('filtrarContratos');   //  Ruta para filtrar los contratos

Route::get('/contratos/agregar', [ctr_contratos::class, 'agregarContrato'])->name('agregarContrato');   //  Ruta para ir a la pantalla de agregar una nuevo contrato

Route::get('/contratos/detalle/cancelar/{id}',[ctr_contratos::class, 'cancelarContrato'])->name('cancelarContrato');  // Ruta para cancelar un contrato

Route::get('/contratos/detalle/terminar/{id}',[ctr_contratos::class, 'terminarContrato'])->name('terminarContrato');  // Ruta para cancelar un contrato

Route::put('/contratos/detalle/editar/guardar',[ctr_contratos::class, 'actualizarContrato'])->name('actualizarContrato');  // Ruta para editar la observación de un contrato

Route::post('/contratos/agregar/guardar',[ctr_contratos::class, 'guardarContrato'])->name('guardarContrato');  //Ruta para guardar contratos nuevos

Route::post('/contratos/agregar/buscarEstudiante', [ctr_contratos::class, 'buscarEstudiante'])->name('buscarEstudiante');   //  Ruta para buscar al estudiante por cedula en la pantalla de agregar

Route::post('/contratos/agregar/buscarInstrumento', [ctr_contratos::class, 'buscarInstrumento'])->name('buscarInstrumento');   //  Ruta para buscar al intrumento por codigo en la pantalla de agregar

Route::get('/contratos/descargar/{id}', [ctr_contratos::class, 'descargarContrato'])->name('descargarContrato'); // Ruta donde se va a cargar el contrato para descargar

Route::get('/reportes', ctr_reportes::class)->name('reportes'); // Ruta para la vista/pantalla donde se podran ver/generar los reportes

Route::post('/reportes/generar', [ctr_reportes::class, 'generar_reporte'])->name('generar_reporte');   //  Ruta para generar un contrato

Route::post('/reportes/imprimir', [ctr_reportes::class, 'reporte_imprimir'])->name('reporte_imprimir');   // Ruta para imprimir un reporte generado

// Rutas necesarias para las BOLETAS DE SALIDA

Route::get('/boletas', ctr_boletas::class)->name('boletas'); // Ruta para la vista/pantalla donde se podran ver/generar las boletas de salida

Route::get('/boletas/detalle/{id}', [ctr_boletas::class, 'detalleBoleta'])->name('detalleBoleta'); // Ruta para la vista/pantalla donde se podran ver/generar las boletas de salida

Route::post('/boletas/filtrar', [ctr_boletas::class, 'filtrarBoletas'])->name('filtrarBoletas');   //  Ruta para filtrar las boletas de salida

Route::get('/boletas/agregar', [ctr_boletas::class, 'agregarBoleta'])->name('agregarBoleta');   //  Ruta para ir a la pantalla de agregar una nueva boleta de salida

Route::post('/boletas/agregar/guardar',[ctr_boletas::class, 'guardarBoleta'])->name('guardarBoleta');  //Ruta para guardar boletas nuevos

Route::put('/boletas/editar/guardar',[ctr_boletas::class, 'actualizarBoleta'])->name('actualizarBoleta');  // Ruta para actualizar la informacion de una boleta de salida

Route::get('/boletas/detalle/activar/{id}',[ctr_boletas::class, 'activarBoleta'])->name('activarBoleta');  // Ruta para ir a la vista donde se va poder activar una boleta de salida, es decir, una boleta que pasa de "Generada" a "Activa"

Route::get('/boletas/detalle/finalizar/{id}',[ctr_boletas::class, 'finalizarBoleta'])->name('finalizarBoleta');  // Ruta para ir a la vista donde se va poder finalizar una boleta de salida, es decir, una boleta que pasa de "Activa" a "Devueltas"

Route::get('/boletas/detalle/cancelar/{id}',[ctr_boletas::class, 'cancelarBoleta'])->name('cancelarBoleta');  // Ruta para cancelar una boleta de salida, es decir, una boleta que pasa de cualquier estado (menos devuelta o ya cancelada) a "Cancelada"

Route::get('/boletas/detalle/cancelaratrasada/{id}',[ctr_boletas::class, 'cancelarBoletaAtr'])->name('cancelarBoletaAtr');  // Ruta para cancelar una boleta de salida que se encuentra atrasada

Route::get('/boletas/imprimir/{id}',[ctr_boletas::class, 'obtenerBoletaImprimir'])->name('imprimirBoleta');

Route::get('/boletas/detalle/notificar/{id}', [ctr_boletas::class, 'notificarBoletaEntregada'])->name('notificacionBoleta'); // Ruta para notificar que una boleta de salida fue entregada

// Rutas necesarias para la seccion de AYUDA

Route::get('/ayuda', ctr_ayuda::class)->name('ayuda'); // Ruta para la vista/pantalla de seccion de ayuda

// Rutas para el registro de acciones

Route::get('/registroAcciones', [ctr_registroAcciones::class, 'cargar_registro_acciones'])->name('registro_acciones'); // Ruta para la vista/pantalla de seccion del regitro de aciones

Route::post('/registroAcciones/obtenerRegistro', [ctr_registroAcciones::class, 'obtenerRegistro']);