-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 17-05-2021 a las 17:11:30
-- Versión del servidor: 10.4.18-MariaDB
-- Versión de PHP: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `db_sinfoinventarios`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `activos`
--

CREATE TABLE `activos` (
  `codActivo` varchar(12) NOT NULL COMMENT 'Codigo que esta en la placa del activo',
  `idInstitucion` int(11) DEFAULT NULL COMMENT 'Llave foranea a la instutucion a la que pertenece el activo',
  `nombre` varchar(100) DEFAULT NULL COMMENT 'Nombre del activo',
  `marca` varchar(150) DEFAULT 'Sin Marca',
  `modelo` varchar(150) DEFAULT 'Sin Modelo',
  `serie` varchar(50) DEFAULT NULL COMMENT 'numero de serie del activo',
  `observaciones` text DEFAULT 'Sin Observaciones' COMMENT 'observacion sobre el activo o su estado',
  `ubicacion` varchar(150) DEFAULT NULL,
  `valor` varchar(30) DEFAULT NULL COMMENT 'valor del activo',
  `responsable` varchar(100) DEFAULT NULL COMMENT 'Persona responsable del instrumento',
  `idEstado` int(11) NOT NULL COMMENT 'Llave foranea de la tabla de estados',
  `fotoActivo` varchar(150) DEFAULT NULL COMMENT 'Campo en el que se guardara la imagen del activo, con su path y su extension, el codigo del activo sera el nombre de la imagen para evitar que la misma se repita'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `activos`
--

INSERT INTO `activos` (`codActivo`, `idInstitucion`, `nombre`, `marca`, `modelo`, `serie`, `observaciones`, `ubicacion`, `valor`, `responsable`, `idEstado`, `fotoActivo`) VALUES
('110710', 1, 'Banco Metalico', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Bancos para tacar contrabajo', 'salon', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('110711', 1, 'Banco Metalico ', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Bancos para tacar contrabajo', 'salon', 'Sin Valor', 'LAURA MESEN', 9, NULL),
('142285', 1, 'Micrófono', 'Shure', 'Super 55', 'Sin Serie', 'Sin Oberservaciones', 'Bodega ', ' ₡108,238.00 ', 'Laura Mesen', 9, NULL),
('142286', 1, 'Micrófono', 'Shure', 'Super 55', 'No tiene', 'Sin Oberservaciones', 'Bodega ', ' ₡108,238.00 ', 'Laura Mesen', 9, NULL),
('142287', 1, 'Micrófono', 'Shure', 'SVX14US/CV-J9', '3NK1805328', 'Micrófono de solapa ', 'Bodega ', ' ₡125,000.00 ', 'Laura Mesen', 9, NULL),
('142288', 1, 'Micrófono', 'Shure', 'SVX14US/CV-J9', '3NJ3003120', 'Micrófono de solapa ', 'Bodega ', ' ₡125,000.00 ', 'Laura Mesen', 9, NULL),
('142289', 1, 'Micrófono', 'Shure', 'SVX14US/CV-J9', '3NJ3003173', 'Micrófono de solapa ', 'Bodega ', ' ₡125,000.00 ', 'Laura Mesen', 9, NULL),
('142844', 1, 'Micrófono', 'SENNHEISER', 'E614', 'Sin Serie', 'Sin Oberservaciones', 'Bodega ', ' ₡119,081.00 ', 'Laura Mesen ', 9, NULL),
('142845', 1, 'Micrófono', 'SENNHEISER', 'E614', 'Sin Serie', 'Sin Oberservaciones', 'DANILO', ' ₡119,081.00 ', 'Laura Mesen', 9, NULL),
('142846', 1, 'Micrófono', 'SENNHEISER', 'E614', 'Sin Serie', 'Sin Oberservaciones', 'Bodega', ' ₡119,081.00 ', 'Laura Mesen', 1, NULL),
('142847', 1, 'Micrófono', 'SENNHEISER', 'E614', 'Sin Serie', 'Sin Oberservaciones', 'Bodega', ' ₡119,081.00 ', 'Laura Mesen', 1, NULL),
('142848', 1, 'Micrófono', 'SENNHEISER', 'E614', 'Sin Serie', 'Sin Oberservaciones', 'Bodega', ' ₡119,081.00 ', 'Laura Mesen', 1, NULL),
('142849', 1, 'Micrófono', 'SENNHEISER', 'E614', 'Sin Serie', 'Sin Oberservaciones', 'DANILO', ' ₡119,081.00 ', 'Laura Mesen', 1, NULL),
('6543', 1, 'asd', 'qweqw', 'qweqwe', 'qweqwe', NULL, 'Mi Casa', '60005', 'Kenneth', 8, NULL),
('975660', 1, 'Switch para uso en redes', 'CISCO', 'C2960', 'FC0174DYO60', 'Sin Oberservaciones', 'Cuarto Redes ', 'Sin Valor', 'Laura Mesén', 1, NULL),
('AEMSAUV-001', 3, 'Columna o Torre', 'AVANT', '15 A', '1290140', 'Color negro con cobertor café. ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSAUV-002', 3, 'Columna o Torre', 'AVANT', '15 A', '1290076', 'Color negro. Con cobertor café acolchado.', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSAUV-003', 3, 'Woffer', 'DAS OVANT', '18 A', '20090164', 'Color negro, con cobertor café acolchado', 'TALLER', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSAUV-004', 3, 'Woffer', 'DAS OVANT', '18 A', '20090167', 'Color negro.Con cobertor café acolchado', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSAUV-005', 3, 'Amplificador', 'PEAVEY', 'TNT 115', 'Sin Serie', '-Color negro con rojo', 'Salón', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSAUV-006', 3, 'Mixer', 'XENYX', '1202FX', 'N11999', 'Pequeña ems Estuche negro con bordes de aluminio.', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSAUV-007', 3, 'Mixer', 'PEAVEY', '32FX', 'KO490961#3', 'Grande.De 32 canales.De color gris.No estuche.', 'Bodega', 'Sin Valor', 'Sin Responsable', 9, NULL),
('AEMSAUV-008', 3, 'Amplificador', 'FENDER RUMBLES', 'PR2463', 'ICTC 14014237', 'De color negro plateado.Frente pequeño', 'RAJEEHA ABUAWAD YOUSEAD', 'Sin Valor', 'Sin Responsable', 9, NULL),
('AEMSAUV-009', 3, 'RadioGrabadora', 'L.G', 'LPCLM340A', 'Sin Serie', 'De color plateado con gris.Estuche negro en mal estado.', 'Oficina Administrativa ', 'Sin Valor', 'Sin Responsable', 9, NULL),
('AEMSAUV-010', 3, 'Radio Grabadora', 'Sony', 'CFD F17CP', 'Sin Serie', 'De color plateado. Sin estuche.', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSAUV-011', 3, 'Radio Grabadora', 'Sony', 'CFD F17CP', 'Sin Serie', 'De color plateado.Sin estuche.', 'Oficina Administrativa ', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSAUV-012', 3, 'Cámara Vídeo', 'Sony', 'HDR CX700', '128659', 'Sin Oberservaciones', 'DIRECCION', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSAUV-013', 3, 'Pantalla de cámaras', 'SANKEY', 'Sin Modelo', 'Sin Serie', 'Sin Oberservaciones', 'Secretaría', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSAUV-014', 3, 'Parlante ', 'JVL', 'EON15P-1', '17438', 'Sin Oberservaciones', 'cubiculo #1', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSAUV-015', 3, 'Micrófono', 'CAD', 'GXL-1200', 'Sin Serie', 'Microfono condensado', 'Bodega', 'Sin Valor', 'Laura Mesen', 1, NULL),
('AEMSAUV-016', 3, 'Micrófono', 'CAD', 'GXL-1200', 'Sin Serie', 'Microfono condensado', 'Bodega', 'Sin Valor', 'Laura Mesen', 1, NULL),
('AEMSAUV-017', 3, 'Micrófono', 'CAD', 'GXL-1200', 'Sin Serie', 'Microfono condensado', 'Bodega', 'Sin Valor', 'Laura Mesen', 1, NULL),
('AEMSAUV-018', 3, 'Micrófono', 'CAD', 'GXL-1200', 'Sin Serie', 'Microfono condensado', 'Bodega', 'Sin Valor', 'Laura Mesen', 1, NULL),
('AEMSAUV-019', 3, 'Pedestales K-M', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color gris.Para micrófono', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSAUV-020', 3, 'Pedestales K-M', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color gris.Para micrófono', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSAUV-021', 3, 'Pedestales K-M', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color gris.Para micrófono', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSAUV-022', 3, 'Pedestales K-M', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color gris.Para micrófono', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSAUV-023', 3, 'Pedestales K-M', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color gris.Para micrófono', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSAUV-024', 3, 'Pedestales K-M', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color gris.Para micrófono', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSAUV-025', 3, 'Amplificador para  bajo electrico', 'BEHRINGER', 'GM108', '11227', 'Aplificador para bajo ', 'cubiculo #4', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSAUV-026', 3, 'Amplificador para  bajo electrico', 'PYRAMID', 'GA41', 'Sin Serie', 'Aplificador para bajo ', 'cubiculo #5', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSCCN-001', 3, 'Microondas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Sin Oberservaciones', 'Oficina Administrativa', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSCCN-002', 3, 'Cocina de gas  dos quemadores', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Industrial', 'Punto de ventas', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSCCN-003', 3, 'Tostadora', 'Cuisinart', 'Sin Modelo', 'Sin Serie', 'Sin Oberservaciones', 'Punto de ventas', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSCCN-004', 3, 'Cilintro de gas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Sin Oberservaciones', 'Punto de ventas', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSCCN-005', 3, 'Procesador de palomitas de maiz', 'West Bend', 'Sin Modelo', 'Sin Serie', 'Sin Oberservaciones', 'Punto de ventas', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSCCN-006', 3, 'Olla arrocera ', 'Sankey', 'Sin Modelo', 'Sin Serie', '45 tazas', 'Punto de ventas', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSCCN-007', 3, 'Olla arrocera ', 'sankey', 'Sin Modelo', 'Sin Serie', '23 tazas', 'Punto de vetas', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSCCN-008', 3, 'licuadora', 'Oster', 'Sin Modelo', 'Sin Serie', 'Sin Oberservaciones', 'Punto de ventas', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSCCN-009', 3, 'Olla de presion', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Sin Oberservaciones', 'Punto de ventas', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSCCN-010', 3, 'Freidora de aire', 'Telstar', 'Sin Modelo', 'Sin Serie', 'Sin Oberservaciones', 'Punto de ventas', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSCCN-011', 3, 'Pica hielo', 'saco', '350w', 'Sin Serie', 'Sin Oberservaciones', 'Punto de ventas', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSCCN-012', 3, 'Congelador ', 'Mabe', 'Sin Modelo', 'Sin Serie', 'Sin Oberservaciones', 'Punto de ventas', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSCCN-013', 3, 'refrigerador', 'Atlas', 'Sin Modelo', 'Sin Serie', 'Sin Oberservaciones', 'Punto de ventas', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSCCN-014', 3, 'Freidora', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Sin Oberservaciones', 'Punto de ventas', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSCCN-015', 3, 'Plantilla de gas', 'Lotus', 'Sin Modelo', 'Sin Serie', 'Remodelada por que se le quebro el vidrio ', 'Punto de ventas', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSCCN-016', 3, 'Hielera Grande', 'Rubbermaid', '1R41-1', 'Sin Serie', 'Roja Grande ', 'Punto de ventas', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSCCN-017', 3, 'Hielera Pequeño', 'Rubbermaid', '1R37-1', 'Sin Serie', 'Roja Pequeña', 'Punto de ventas', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSCCN-018', 3, 'Horno Tostados', 'WAVE', 'Sin Modelo', 'THTO60910DE', 'Sin Oberservaciones', 'Punto de ventas', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSCCN-019', 3, 'OLLA PARA CHICHARRONES', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Sin Oberservaciones', 'bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSCCN-020', 3, 'JUEGO DE OLLAS MEDIANAS', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Sin Oberservaciones', 'bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSCCN-021', 3, 'JUEGO DE OLLAS GRANDES', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Sin Oberservaciones', 'bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSCCN-022', 3, 'Plantilla electrica dos discos', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Se le compro a Gary', 'Estaban en el apartamento', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSCCN-023', 3, 'Microondas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Se le compro a Gary', 'Estaban en el apartamento', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSHRR-001', 3, 'Escalera de extensión', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Aluminio de 24 pies', 'Bodega ', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSHRR-002', 3, 'Escalera de abrir', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Tipo A. Aluminio, color amarillo', 'Bodega ', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSHRR-003', 3, 'Escalera de abrir ', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Tipo A Aluminio. Color roja', 'Bodega ', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSHRR-004', 3, 'Carretilla', 'COSCO', 'Sin Modelo', 'Sin Serie', 'Aluminio ( PERRA)', 'Bodega ', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSHRR-005', 3, 'Caja de Herramientas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Sin Oberservaciones', 'Bodega ', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSINF-001', 3, 'Computadora', 'H.P', 'G4-10641ª', '5CD11506SY', 'Portátil. Tapa gris. Se usa en Administración. #5', 'Oficina Administrativa ', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSINF-002', 3, 'Impresora ', 'EPSON', 'L4160', 'Sin Serie', 'Sin Oberservaciones', 'Laura', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSINF-003', 3, 'Impresora ', 'EPSON', 'L210', 'Sin Serie', 'Sin Oberservaciones', 'Wendy', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSINF-004', 3, 'Impresora ', 'EPSON', 'L210', 'Sin Serie', 'Sin Oberservaciones', 'Sin Ubicación', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSINF-005', 3, 'Impresora ', 'EPSON', 'L210', 'Sin Serie', 'Sin Oberservaciones', 'Sin Ubicación', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-001', 3, 'Sillas Plásticas con brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas con brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 9, NULL),
('AEMSMBL-002', 3, 'Sillas Plásticas con brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas con brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 9, NULL),
('AEMSMBL-003', 3, 'Sillas Plásticas con brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas con brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 9, NULL),
('AEMSMBL-004', 3, 'Sillas Plásticas con brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas con brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 9, NULL),
('AEMSMBL-005', 3, 'Sillas Plásticas con brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas con brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 9, NULL),
('AEMSMBL-006', 3, 'Sillas Plásticas con brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas con brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 9, NULL),
('AEMSMBL-007', 3, 'Sillas Plásticas con brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas con brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 9, NULL),
('AEMSMBL-008', 3, 'Sillas Plásticas con brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas con brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 9, NULL),
('AEMSMBL-009', 3, 'Sillas Plásticas con brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas con brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 9, NULL),
('AEMSMBL-010', 3, 'Sillas Plásticas con brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas con brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 9, NULL),
('AEMSMBL-011', 3, 'Sillas Plásticas con brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas con brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 9, NULL),
('AEMSMBL-012', 3, 'Sillas Plásticas con brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas con brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-013', 3, 'Sillas Plásticas con brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas con brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-014', 3, 'Sillas Plásticas con brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas con brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 9, NULL),
('AEMSMBL-015', 3, 'Sillas Plásticas con brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas con brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 9, NULL),
('AEMSMBL-016', 3, 'Sillas Plásticas con brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas con brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 9, NULL),
('AEMSMBL-017', 3, 'Sillas Plásticas con brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas con brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 9, NULL),
('AEMSMBL-018', 3, 'Sillas Plásticas con brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas con brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-019', 3, 'Sillas Plásticas con brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas con brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 9, NULL),
('AEMSMBL-020', 3, 'Sillas Plásticas con brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas con brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 9, NULL),
('AEMSMBL-021', 3, 'Sillas Plásticas con brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas con brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 9, NULL),
('AEMSMBL-022', 3, 'Sillas Plásticas con brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas con brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 9, NULL),
('AEMSMBL-023', 3, 'Sillas Plásticas con brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas con brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 9, NULL),
('AEMSMBL-024', 3, 'Sillas Plásticas con brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas con brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-025', 3, 'Sillas Plásticas con brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas con brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-026', 3, 'Sillas Plásticas con brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas con brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-027', 3, 'Sillas Plásticas con brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas con brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-028', 3, 'Sillas Plásticas con brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas con brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-029', 3, 'Sillas Plásticas con brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas con brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-030', 3, 'Sillas Plásticas con brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas con brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-031', 3, 'Sillas Plásticas con brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas con brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-032', 3, 'Sillas Plásticas con brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas con brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-033', 3, 'Sillas Plásticas con brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas con brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-034', 3, 'Sillas Plásticas con brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas con brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-035', 3, 'Sillas Plásticas con brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas con brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-036', 3, 'Sillas Plásticas con brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas con brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-037', 3, 'Sillas Plásticas con brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas con brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-038', 3, 'Sillas Plásticas con brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas con brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-039', 3, 'Sillas Plásticas con brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas con brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-040', 3, 'Sillas Plásticas con brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas con brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-041', 3, 'Sillas Plásticas con brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas con brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-042', 3, 'Sillas Plásticas con brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas con brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-043', 3, 'Sillas Plásticas con brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas con brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-044', 3, 'Sillas Plásticas con brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas con brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-045', 3, 'Sillas Plásticas con brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas con brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-046', 3, 'Sillas Plásticas con brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas con brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-047', 3, 'Sillas Plásticas con brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas con brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-048', 3, 'Sillas Plásticas con brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas con brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-049', 3, 'Sillas Plásticas con brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas con brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-050', 3, 'Sillas Plásticas con brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas con brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-051', 3, 'Sillas Plásticas sin brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas sin brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-052', 3, 'Sillas Plásticas sin brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas sin brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-053', 3, 'Sillas Plásticas sin brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas sin brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-054', 3, 'Sillas Plásticas sin brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas sin brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-055', 3, 'Sillas Plásticas sin brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas sin brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-056', 3, 'Sillas Plásticas sin brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas sin brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-057', 3, 'Sillas Plásticas sin brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas sin brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-058', 3, 'Sillas Plásticas sin brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas sin brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-059', 3, 'Sillas Plásticas sin brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas sin brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-060', 3, 'Sillas Plásticas sin brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas sin brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-061', 3, 'Sillas Plásticas sin brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas sin brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-062', 3, 'Sillas Plásticas sin brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas sin brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-063', 3, 'Sillas Plásticas sin brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas sin brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-064', 3, 'Sillas Plásticas sin brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas sin brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-065', 3, 'Sillas Plásticas sin brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas sin brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-066', 3, 'Sillas Plásticas sin brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas sin brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-067', 3, 'Sillas Plásticas sin brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas sin brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-068', 3, 'Sillas Plásticas sin brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas sin brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-069', 3, 'Sillas Plásticas sin brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas sin brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-070', 3, 'Sillas Plásticas sin brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas sin brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-071', 3, 'Sillas Plásticas sin brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas sin brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-072', 3, 'Sillas Plásticas sin brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas sin brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-073', 3, 'Sillas Plásticas sin brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas sin brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-074', 3, 'Sillas Plásticas sin brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas sin brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-075', 3, 'Sillas Plásticas sin brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas sin brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-076', 3, 'Sillas Plásticas sin brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas sin brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-077', 3, 'Sillas Plásticas sin brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas sin brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-078', 3, 'Sillas Plásticas sin brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas sin brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-079', 3, 'Sillas Plásticas sin brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas sin brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-080', 3, 'Sillas Plásticas sin brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas sin brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-081', 3, 'Sillas Plásticas sin brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas sin brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-082', 3, 'Sillas Plásticas sin brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas sin brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-083', 3, 'Sillas Plásticas sin brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas sin brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-084', 3, 'Sillas Plásticas sin brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas sin brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-085', 3, 'Sillas Plásticas sin brazo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blancas sin brazo ', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-086', 3, 'Pizarra con pentagrama', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Pizarra de los cubiculos ', 'Cubiculo #1', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-087', 3, 'Pizarra con pentagrama', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Pizarra de los cubiculos ', 'Cubiculo #2', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-088', 3, 'Pizarra con pentagrama', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Pizarra de los cubiculos ', 'Cubiculo #3', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-089', 3, 'Pizarra con pentagrama', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Pizarra de los cubiculos ', 'Cubiculo #4', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-090', 3, 'Pizarra con pentagrama', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Pizarra de los cubiculos ', 'Cubiculo #5', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-091', 3, 'Pizarra con pentagrama', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Pizarra de los cubiculos ', 'Cubiculo #6', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-092', 3, 'Pizarra con pentagrama', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Pizarra de los cubiculos ', 'Cubiculo #7', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-093', 3, 'Pizarra con pentagrama', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Pizarra de los cubiculos ', 'Cubiculo #8', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-094', 3, 'HURNA DE VIDRIO (biblioteca cub. #3)', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Hurna de vidrio, biblioteca de cubiculo #3', 'cubiculo #3', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-095', 3, 'Stent de bajos', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Estanteria para bajos cubiculo #3', 'cubiculo #4', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-096', 3, 'Achivo metalico gris', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Archivo metalico gris', 'Direccion', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-097', 3, 'Achivo metalico gris', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Archivo metalico gris', 'Secretaria', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-098', 3, 'Archivo y caja  fuerte metalico beige', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Archivo y caja  fuerte metalico beige', 'Direccion', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-099', 3, 'Bancas de madera', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Bancas de madera ', 'Pasillo', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-100', 3, 'Bancas de madera', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Bancas de madera ', 'Pasillo', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-101', 3, 'Mueble de madera con libros', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Sin Oberservaciones', 'Oficina-Dirección', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-102', 3, 'Gavetero con escritorio compu', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Escritorio de Laura', 'Oficina-Dirección', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-103', 3, 'Archivo metalico ', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris ', 'cubiculo #1', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-104', 3, 'Espejo ', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Espejo Viselado', 'cubiculo #1', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-105', 3, 'Espejo ', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Espejo Viselado', 'cubiculo #2', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-106', 3, 'Espejo ', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Espejo Viselado', 'cubiculo #3', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-107', 3, 'Espejo ', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Espejo Viselado', 'cubiculo #4', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-108', 3, 'Espejo ', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Espejo Viselado', 'cubiculo #5', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-109', 3, 'Espejo ', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Espejo Viselado', 'cubiculo #6', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-110', 3, 'Espejo ', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Espejo Viselado', 'cubiculo #7', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-111', 3, 'Espejo ', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Espejo Viselado', 'cubiculo #8', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-112', 3, 'HURNA DE VIDRIO', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'PARA VERNTA DE ASESORIOS DE INSTRUMENTOS', 'LOBBY', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-113', 3, 'HURNA DE VIDRIO', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'PARA VERNTA DE ASESORIOS DE INSTRUMENTOS', 'LOBBY', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-114', 3, 'ESCRITORIO PARA COMPUTADORA', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'DONACION ', 'LOBBY', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-115', 3, 'ESCRITORIO PARA COMPUTADORA', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'DONACION ', 'CUBICULO #2', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-116', 3, 'JUEGO DE SALA ', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'JUEGO DE SALA DONADO', 'LOBBY', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-117', 3, 'Mesa Grande ', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Mesa grande para comer', 'Pasillo', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-118', 3, 'Bancas metalicas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Bancas metalicas del pasillo', 'Pasillo', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-119', 3, 'Bancas metalicas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Bancas metalicas del pasillo', 'Pasillo', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-120', 3, 'Bancas metalicas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Bancas metalicas del pasillo', 'Pasillo', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-121', 3, 'Bancas metalicas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Bancas metalicas del pasillo', 'Pasillo', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-122', 3, 'Bancas metalicas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Bancas metalicas del pasillo', 'Pasillo', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-123', 3, 'Bancas metalicas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Bancas metalicas del pasillo', 'Pasillo', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-124', 3, 'Camarote con colchones', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Se le compraron a Gary', 'Estaban en el departamento', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-125', 3, 'Camarote con colchones', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Se le compraron a Gary', 'Estaban en el departamento', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-126', 3, 'Cama matrimonial con colchon', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Se le compraron a Gary', 'Estaban en el departamento', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-127', 3, 'Cama individual con colchon', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Se le compraron a Gary', 'Estaban en el departamento', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-128', 3, 'Lavadora ', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Se le compraron a Gary', 'Estaban en el departamento', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-129', 3, 'Banco de desayunador ', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Se le compraron a Gary', 'Estaban en el departamento', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-130', 3, 'Banco de desayunador ', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Se le compraron a Gary', 'Estaban en el departamento', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-131', 3, 'Banco de desayunador ', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Se le compraron a Gary', 'Estaban en el departamento', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-132', 3, 'Banco de desayunador ', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Se le compraron a Gary', 'Estaban en el departamento', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-133', 3, 'Mueble de madera con libros', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Mueble de libros de Percucion', 'Aula de Percusión ', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-134', 3, 'Mueble Blanco de madera', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Sin Oberservaciones', 'Aula de Percusión ', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-135', 3, 'Mueble de madera para instrumentos', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Mueble para instrumentos de percusion', 'Aula de Percusión ', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-136', 3, 'Stan de Platillos', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Stan para platillos ', 'Aula de Percusión ', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-137', 3, 'Stan de instrumenos de Percusion', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Sin Oberservaciones', 'Aula de Percusión ', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-138', 3, 'Pizarra acrilica ', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Sin Oberservaciones', 'Aula #2 ', 'Sin Valor', 'Sin Responsable', 1, NULL),
('AEMSMBL-139', 3, 'Pizarra acrilica ', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Sin Oberservaciones', 'Aula #3', 'Sin Valor', 'Sin Responsable', 1, NULL),
('kjhakshdh123', 2, 'Prueba Editada', 'Prueba', 'Prueba', 'Prueba', 'm,ahsdkaks hgasjdgasd ajsgdj', 'Prueba', '500.000', 'Prueba', 8, NULL),
('N00105954', 1, 'Proyector  ', 'EPSON', 'EMP-X3', 'GYHF6Z074L', 'Color gris.Estuche negro', 'Oficina Administrativa', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('N00115352', 1, 'COMPUTADORA INCOMPLETA Teatro de casa (parlante de computadora de desecho) MONITOR Y TECLADO', 'LOGITECH DELL', 'X-530', 'Sin Serie', 'El teatro en casa esta en la computadora de los Estudiantes tiene # de placa de computadora desecha.  El monitor y el teclado Casetilla del Guarda, para el sistema de seguridad', 'LOBI Computadora y casillero del guarda', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('N00127190', 1, 'Archivero', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Archivero # 1', 'Secretaría', ' ₡87,000.00 ', 'Laura Mesén', 1, NULL),
('N00127191', 1, 'Gavetero de metal- archivero', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Archivo #4', 'Oficina-Dirección', ' ₡87,000.00 ', 'Laura Mesén', 1, NULL),
('N00137906', 1, 'Unidad evaporadora para aire acondicionado', 'Mitsubichi', 'PLFY-P63VBN-E', '3ZU016403ZM03066(UNA)', 'Sin Oberservaciones', 'Oficina - Dirección', ' ₡1,276,573.39 ', 'Laura Mesén', 1, NULL),
('N00137907', 1, 'Unidad evaporadora  para aire acondicionado', 'Electric', 'PCFY-P40VKM-E', '3ZU01699 02A001192 (UNA)', 'Sin Oberservaciones', 'Cuarto de Redes ', ' ₡977,450.10 ', 'Laura Mesén', 1, NULL),
('N00137908', 1, 'Unidad evaporadora para aire acondicionado', 'Mitsubichi', 'PLFY-P125VBN-E', '3ZU01637', 'Sin Oberservaciones', 'Aula de Percusión ', ' ₡1,437,290.84 ', 'Laura Mesén', 1, NULL),
('N00137911', 1, 'Unidad evaporadora para aire acondicionado', 'Mitsubichi', 'PCFY-P24NKMV-E', '41N00032', 'Sin Oberservaciones', 'Cubículo #3', ' ₡1,192,339.82 ', 'Laura Mesén', 1, NULL),
('N00137912', 1, 'Unidad evaporadora para aire acondicionado', 'Mitsubichi', 'PCFY-P24NKMV-E', '24N00032', 'Sin Oberservaciones', 'Cubículo #4', ' ₡1,192,339.82 ', 'Laura Mesén', 1, NULL),
('N00137913', 1, 'Unidad evaporadora para aire acondicionado', 'Mitsubichi', 'PCFY-P24NKMV-E', '24M00029', 'Sin Oberservaciones', 'Cubículo #5', ' ₡1,192,339.82 ', 'Laura Mesén', 1, NULL),
('N00137914', 1, 'Unidad evaporadora para aire acondicionado', 'Mitsubichi', 'PCFY-P24NKMV-E', '41N00040', 'Sin Oberservaciones', 'Cubículo #6', ' ₡1,192,339.82 ', 'Laura Mesén', 1, NULL),
('N00137915', 1, 'Unidad evaporadora para aire acondicionado', 'Mitsubichi', 'PCFY-P24NKMV-E', '41N00035', 'Sin Oberservaciones', 'Cubículo #7', ' ₡1,192,339.82 ', 'Laura Mesén', 1, NULL),
('N00137916', 1, 'Unidad evaporadora para aire acondicionado', 'Mitsubichi', 'PCFY-P24NKMV-E', '41N00039', 'Sin Oberservaciones', 'Cubículo #8', ' ₡1,192,339.82 ', 'Laura Mesén', 1, NULL),
('N00137917', 1, 'Unidad evaporadora para aire acondicionado', 'Mitsubichi', 'PCFY-P24NKNV-E', '41N00038', 'Sin Oberservaciones', 'Oficina Administrativa', ' ₡1,192,339.82 ', 'Laura Mesén', 1, NULL),
('N00137918', 1, 'Unidad evaporadora para aire acondicionado', 'Mitsubichi', 'PCFY-P24NKNV-E', '41N00037', 'Sin Oberservaciones', 'Secretaría', ' ₡1,192,339.82 ', 'Laura Mesén', 1, NULL),
('N00137919', 1, 'Unidad condensadora para aire acondicionado', 'MITSUBICHI', 'PUMY-P48NHMU', '3ZU01670A', 'BUEN ESTADO', 'CUBICULO 5 Y 6', ' ₡3,173,933.47 ', 'Laura Mesen', 1, NULL),
('N00137920', 1, 'Unidad condensadora para aire acondicionado', 'MITSUBISHI', 'PUMY-P48NHMU', '3ZU01641A', 'BUEN ESTADO', 'CUBICULO 7 Y 8', ' ₡3,173,933.47 ', 'Laura Mesen', 1, NULL),
('N00137921', 1, 'Unidad condensadorapara aire acondicionado', 'MITSUBICHI', 'PUMY-P48NHMU', '3ZU01677A', 'BUEN ESTADO', 'CUBICULO 3 Y 4', ' ₡3,173,933.47 ', 'Laura Mesen', 1, NULL),
('N00137923', 1, 'Unidad condensadora para aire acondicionado', 'MITSUBISHI', 'PUMY-P48NHMU', '3ZU01675A', 'BUEN ESTADO ', 'PERCUCION', ' ₡3,173,933.47 ', 'Laura Mesen', 1, NULL),
('N00137924', 1, 'Unidad condensadora para aire acondicionado', 'MITSUBISHI', 'PUMY-P48NHMU', '2YU00036', 'BUEN ESTADO', 'DIRECCION', ' ₡3,173,933.47 ', 'Laura Mesen', 1, NULL),
('N00137925', 1, 'Sistema de protección para descargas', 'INDELEC', '86,60 MILENIUM', '349P', 'Sin Oberservaciones', 'Techo Salón', ' ￠ ', 'Laura Mesén', 1, NULL),
('N00137926', 1, 'Bomba Centrífuga', 'GOULDS', 'PS200', 'B1400211', 'Sin Oberservaciones', 'Tanque Pluviales', ' ₡3,038,960.21 ', 'Laura Mesén', 1, NULL),
('N00137927', 1, 'Bomba Centrífuga', 'GOULDS', 'WS3012D3', 'B1500323', 'Sin Oberservaciones', 'Tanque Pluviales', ' ₡1,576,286.25 ', 'Laura Mesén', 1, NULL),
('N00137928', 1, 'Hidrante', 'JONES', '4060 2013 250 PSI DI', '7L13', 'Color Rojo', 'Zona Verde Aula #1', ' ₡178,447.50 ', 'Laura Mesén', 1, NULL),
('N00138962', 1, 'Biblioteca Solida Alta tipo K3', 'Fantini', 'No tiene', 'No tiene', 'Biblioteca #14', 'Dirección', ' ₡237,858.94 ', 'Laura Mesén', 1, NULL),
('N00138963', 1, 'Biblioteca Solida Alta tipo K3', 'Fantini', 'No tiene', 'No tiene', 'Biblioteca #10', 'Oficina Administrativa', ' ₡237,858.94 ', 'Laura Mesén', 1, NULL),
('N00138964', 1, 'Biblioteca Solida Alta tipo K3', 'Fantini', 'No tiene', 'No tiene', 'Biblioteca #13', 'Oficina Administrativa', ' ₡237,858.94 ', 'Laura Mesén', 1, NULL),
('N00138965', 1, 'Biblioteca Solida Alta tipo K3', 'Fantini', 'No tiene', 'No tiene', 'Biblioteca #12', 'Oficina Administrativa', ' ₡237,858.94 ', 'Laura Mesén', 1, NULL),
('N00138966', 1, 'Biblioteca Solida Alta tipo K3', 'Fantini', 'No tiene', 'No serie', 'Biblioteca #8', 'Secretaria', ' ₡237,858.94 ', 'Laura Mesén', 1, NULL),
('N00138967', 1, 'Biblioteca Solida Alta tipo K3', 'Fantini', 'No tiene', 'No tiene', 'Biblioteca #9', 'Secretaría', ' ₡237,858.94 ', 'Laura Mesén', 1, NULL),
('N00138968', 1, 'Biblioteca Solida Alta tipo K3', 'Fantini', 'No tiene', 'No tiene', 'Biblioteca #11', 'Oficina Administrativa ', ' ₡237,858.94 ', 'Laura Mesén', 1, NULL),
('N00138969', 1, 'Biblioteca Solida Alta tipo K3', 'Fantini', 'No tiene', 'No tiene', 'Biblioteca #7', 'Cubículo #7', ' ₡237,858.94 ', 'Laura Mesén', 1, NULL),
('N00138970', 1, 'Biblioteca Solida Alta tipo K3', 'Fantini', 'No tiene', 'No tiene', 'Biblioteca #2', 'Cubículo #2', ' ₡237,858.94 ', 'Laura Mesén', 1, NULL),
('N00138971', 1, 'Biblioteca baja tipo D', 'Fantini', 'No tiene', 'No tiene', 'Biblioteca #1', 'Cubículo #1', ' ₡237,858.94 ', 'Laura Mesén', 1, NULL),
('N00138972', 1, 'Biblioteca Solida Alta tipo K3', 'Fantini', 'No tiene', 'No tiene', 'Biblioteca #6', 'Cubículo 6', ' ₡237,858.94 ', 'Laura Mesén', 1, NULL),
('N00138973', 1, 'Biblioteca solida Alta tipo K3', 'Fantini', 'No tiene', 'No tiene', '  Biblioteca #5', 'Cubículo #5', ' ₡237,858.94 ', 'Laura Mesén', 1, NULL),
('N00138974', 1, 'Biblioteca Solida Alta tipo K3', 'Fantini', 'No tiene', 'No tiene', 'Biblioteca #4', 'Cubículo #4', ' ₡237,858.94 ', 'Laura Mesén', 1, NULL),
('N00138975', 1, 'Biblioteca Solida Alta tipo K3', 'Fantini', 'No tiene', 'No tiene', 'Biblioteca #3', 'Cubículo #3', ' ₡237,858.94 ', 'Laura Mesén', 1, NULL),
('N00138976', 1, 'Biblioteca baja tipo D', 'Fantini', 'No tiene', 'No tiene', 'Biblioteca #4', 'Aula #4', ' ₡121,214.67 ', 'Laura Mesén', 1, NULL),
('N00138977', 1, 'Biblioteca baja tipo D', 'Fantini', 'No tiene', 'No tiene', 'Biblioteca # 3', 'Aula #3', ' ₡121,214.67 ', 'Laura Mesén', 1, NULL),
('N00138978', 1, 'Biblioteca baja tipo D', 'Fantini', 'No tiene', 'No tiene', 'Biblioteca #2', 'Aula #2', ' ₡121,214.67 ', 'Laura Mesén', 1, NULL),
('N00138979', 1, 'Biblioteca baja tipo D', 'Fantini', 'No tiene', 'No tiene', '  Biblioteca #1', 'Aula #1', ' ₡121,214.67 ', 'Laura Mesén', 1, NULL),
('N00138980', 1, 'Archivador Metálico Legal', 'Fantini', 'No tiene', 'No tiene', 'Sin Oberservaciones', 'bodega', ' ₡130,582.54 ', 'Laura Mesén', 1, NULL),
('N00138981', 1, 'Archivero', 'Fantini', 'No tiene', 'No tiene', 'Archivero #2', 'Secretaría', ' ₡130,582.54 ', 'Laura Mesén', 1, NULL),
('N00138984', 1, 'ARCHIVO', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'ARCHIVO #3', 'Dirección', 'Sin Valor', 'Laura MESEN', 1, NULL),
('N00138985', 1, 'Archivero', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Archivo #5', 'Oficina-Dirección', 'Sin Valor', 'Laura Mesén', 1, NULL),
('N00138986', 1, 'Casillero metálico tipo H6', 'Fantini', 'No tiene', 'No tiene', 'casillero', 'BAÑOS alumnos', ' ₡141,142.70 ', 'Laura Mesén', 1, NULL),
('N00138987', 1, 'Casillero metálico tipo H6', 'Fantini', 'No tiene', 'No tiene', 'Casillero ', 'Aula #4', ' ₡141,142.70 ', 'Laura Mesén', 1, NULL),
('N00138988', 1, 'Casillero Metálico tipo H6', 'Fantini', 'No tiene', 'No tiene', 'Sin Oberservaciones', 'Pasillo de salón', ' ₡141,142.70 ', 'Laura Mesén', 1, NULL),
('N00138989', 1, 'Casillero Metálico tipo H6', 'Fantini', 'No tiene', 'No tiene', 'Sin Oberservaciones', 'Pasillo Salón ', ' ₡141,142.70 ', 'Laura Mesén', 1, NULL),
('N00138990', 1, 'Casillero Metálico tipo H6', 'Fantini', 'No tiene', 'No tiene', 'Sin Oberservaciones', 'Pasillo Salón ', ' ₡141,142.70 ', 'Laura Mesén', 1, NULL),
('N00138991', 1, 'Casillero Metálico tipo H6', 'Fantini', 'No tiene', 'No tiene', 'Sin Oberservaciones', 'Pasillo Salón', ' ₡141,142.70 ', 'Laura Mesén', 1, NULL),
('N00138992', 1, 'Módulo alm. prod. de limpieza', 'Fantini', 'No tiene', 'No tiene', 'Sin Oberservaciones', 'Baños  hombre alum', ' ₡216,596.70 ', 'Laura Mesén', 1, NULL),
('N00138993', 1, 'Casillero de alimentos', 'Fantini', 'No tiene', 'No tiene', 'Sin Oberservaciones', 'cocina', ' ₡216,596.70 ', 'Laura Mesén', 1, NULL),
('N00138994', 1, 'Estación Básica tipo BSC150', 'Fantini', 'No tiene', 'No serie', 'Estación #11', 'Oficina Administrativa', ' ₡205,667.51 ', 'Laura Mesén', 1, NULL),
('N00138995', 1, 'Estación Básica Recta tipo ESC105', 'Fantini', 'No tiene', 'No tiene', 'Estación #8', 'Cubículo #8', ' ₡118,432.69 ', 'Laura Mesén', 1, NULL),
('N00138996', 1, 'Estación Básica Recta tipo ESC105', 'Fantini', 'No tiene', 'No tiene', 'Estación #7', 'Cubículo #7', ' ₡118,432.69 ', 'Laura Mesén', 1, NULL),
('N00138997', 1, 'Estación Básica Recta tipo ESC105', 'Fantini', 'No tiene', 'No tiene', 'Estación #16', 'Percucion', ' ₡118,432.69 ', 'Laura Mesén', 1, NULL),
('N00138998', 1, 'Estación Básica Recta tipo ESC105', 'Fantini', 'No tiene', 'No tiene', 'Estación #1', 'Cubículo #1', ' ₡118,432.69 ', 'Laura Mesén', 1, NULL),
('N00138999', 1, 'Estación Básica Recta tipo ESC105', 'Fantini', 'No tiene', 'No tiene', 'Estación #6', 'Cubículo #6', ' ₡118,432.69 ', 'Laura Mesén', 1, NULL),
('N00139000', 1, 'Estación Básica Recta Tipo ESC105', 'Fantini', 'No tiene', 'No tiene', 'Estación #5', 'Cubículo #5', ' ₡118,432.69 ', 'Laura Mesén', 1, NULL),
('N00139001', 1, 'Estación Básica Recta tipo ESC105', 'Fantini', 'No tiene', 'No tiene', 'Estación #4', 'Cubículo #4', ' ₡118,432.69 ', 'Laura Mesén', 1, NULL),
('N00139002', 1, 'Estación Básica Recta tipo ESC105', 'Fantini', 'No tiene', 'No tiene', 'Estación #3', 'Cubículo #3', ' ₡118,432.69 ', 'Laura Mesén', 1, NULL),
('N00139003', 1, 'Estación básica recta tipo ESC105', 'Fantini', 'No tiene', 'No tiene', 'Estación básica #14', 'Aula #2', ' ₡118,432.69 ', 'Laura Mesén', 1, NULL),
('N00139004', 1, 'Estación básica recta tipo ESC105', 'Fantini', 'No tiene', 'No tiene', 'Estación básica #13', 'Aula # 1', ' ₡118,432.69 ', 'Laura Mesén', 1, NULL),
('N00139005', 1, 'Estación básica recta tipo ESC105', 'Fantini', 'No tiene', 'No tiene', 'Estación # 2', 'Aula #3', ' ₡118,432.69 ', 'Laura Mesén', 1, NULL),
('N00139006', 1, 'Estación básica recta tipo ESC105', 'Fantini', 'No tiene', 'No tiene', 'Estación # 15', 'Aula #4', ' ₡118,432.69 ', 'Lara Mesén', 1, NULL),
('N00139007', 1, 'Estación básica recta tipo ESC105', 'Fantini', 'No tiene', 'No tiene', 'Estación #10', 'Oficina administra', ' ₡216,596.70 ', 'Laura Mesén', 1, NULL),
('N00139008', 1, 'Estación Secretarial tipo B5', 'Fantini', 'No tiene', 'No tiene', '  estación   #12 escrit de Lauren', 'Oficina adm, ', ' ₡327,875.74 ', 'Laura Mesén', 1, NULL),
('N00139009', 1, 'Estación Secretarial tipo B5', 'Fantini', 'No tiene', 'No tiene', 'Estación #9', 'Secretaría', ' ₡327,875.74 ', 'Laura Mesén', 1, NULL),
('N00139010', 1, 'Mesa redonda', 'Fantini', 'No tiene', 'No tiene', 'Sin Oberservaciones', 'Oficina-Dirección', ' ₡150,127.34 ', 'Laura Mesén', 1, NULL),
('N00139011', 1, 'Mesa para sala de reuniones tipo C2M', 'Fantini', 'No tiene', 'No tiene', 'Sin Oberservaciones', 'Oficina Administrativa', ' ₡254,352.09 ', 'Laura Mesén', 1, NULL),
('N00139470', 3, 'Sistema de alarma', 'Notifire', 'ONYX', 'NFS 320 SP', 'Sin Oberservaciones', 'Caseta de oficiales', ' ₡602,666.67 ', 'Laura Mesén', 1, NULL),
('N00139473', 1, 'Switch para uso en redes', 'CISCO', 'C2960', 'PC01740Y05R', 'Sin Oberservaciones', 'Sin Ubicación', ' ₡997,508.00 ', 'Laura Mesén', 1, NULL),
('N00139475', 1, 'Inyector de poder para punto de acceso inalámbrico', 'CISCO', 'Air-PNRIN75', 'SNP5TT72TDOSE', 'Sin Oberservaciones', 'Sin Ubicación', ' ₡419,513.25 ', 'Laura Mesén', 1, NULL),
('N00139476', 1, 'Inyector de poder para punto de acceso inalámbrico', 'CISCO', 'Air-PNRIN75', 'SNPSTT721DOKO', 'Sin Oberservaciones', 'Sin Ubicación', ' ₡419,513.25 ', 'Laura Mesén', 1, NULL),
('N00139477', 1, 'Inyector de poder para punto de acceso inalámbrico', 'CISCO', 'Air-PNRIN75', 'SNPSTI721DOKD', 'Sin Oberservaciones', 'Sin Ubicación', ' ₡419,513.25 ', 'Laura Mesén', 1, NULL),
('N00139480', 1, 'Secador de manos M', 'Helvex', '1010', '51100784', 'Sin Oberservaciones', 'Baño de Mujeres Administrativo ', ' ₡242,306.37 ', 'Laura Mesén Méndez', 1, NULL),
('N00139481', 1, 'Secador de manos H', 'Helvex', '1010', '511000651', 'Sin Oberservaciones', 'Baño de Hombres Administrativo ', ' ₡242,306.37 ', 'Laura Mesén Méndez', 1, NULL),
('N00139482', 1, 'Secador de Manos ', 'Helvex', '1010', '511000651', 'Sin Oberservaciones', 'Baño de Mujeres Aula #4', ' ₡242,306.37 ', 'Laura Mesén Méndez', 1, NULL),
('N00139483', 1, 'Secador de manos H', 'Helvex', '1010', '511000613', 'Sin Oberservaciones', 'Baño de Hombres Aula #4', ' ₡242,306.37 ', 'Laura Mesén Méndez', 1, NULL),
('N00139484', 1, 'Secador de manos', 'Helvex', '1010', '511000603', 'Sin Oberservaciones', '  Baño de ', ' ₡242,306.37 ', 'Laura Mesén Méndez', 1, NULL),
('N00139485', 1, 'Secador de manos', 'Helvex', '1010', '5111000597', 'Sin Oberservaciones', 'Baño de Mujeres Salón ', ' ₡242,306.37 ', 'Laura Mesén Méndez', 1, NULL),
('N00141610', 1, 'Parlantes', 'QSC', 'K8', 'GIF520172', 'Sin Oberservaciones', 'Cubículo #5 LAURA', ' ₡375,668.00 ', 'LAURA MESEN', 1, NULL),
('N00141613', 1, 'Hidrolavadora', 'Simpson HONDA', 'PS 3228', '1015584029', 'Está nueva.No funciona porque por mal transporte se dañó.', 'Bodega ', ' ₡554,082.00 ', 'LAURA MESEN', 1, NULL),
('N00141852', 1, 'Amplificador para  bajo electrico', 'FENDER', 'RUMBLE 500', 'ICTBI5020157', 'Sin Oberservaciones', 'Bodega', ' ₡326,295.00 ', 'LAURA MESEN', 1, NULL),
('N00141867', 1, 'Dispensador para agua', 'General ELECTRIC', 'GXCF04F4', 'St14062529nwg0034', 'Sin Oberservaciones', 'LOBBY', 'Sin Valor', 'Laura M', 1, NULL),
('N00141868', 1, 'Dispensador para agua', 'General Electric', 'GXCF04F4', 'St14062529nwg0263', 'Sin Oberservaciones', 'Salón', 'Sin Valor', 'Laura M', 1, NULL),
('N00141869', 1, 'Microondas', 'Panasonic ', 'NN-5T7 625', '63652 40182', 'Sin Oberservaciones', 'Pasillo Aula #2', ' ₡146,000.00 ', 'Laura Mesén Méndez', 1, NULL);
INSERT INTO `activos` (`codActivo`, `idInstitucion`, `nombre`, `marca`, `modelo`, `serie`, `observaciones`, `ubicacion`, `valor`, `responsable`, `idEstado`, `fotoActivo`) VALUES
('N00141870', 1, 'Microondas', 'Panasonic ', 'NN-5T7 625', '63652 40076', 'Sin Oberservaciones', 'Pasillo Aula #4', ' ₡146,000.00 ', 'Laura Mesén Méndez', 1, NULL),
('N00141958', 1, 'Máquina Cortadora de Césped.', 'TRUPER', 'P-622', '141958', 'Color naranja.Con bolsa para recoger.', 'Bodega “Baños”', ' ₡157,647.70 ', 'Laura Mesén', 1, NULL),
('N00141959', 1, 'Máquina de soldar', 'Licoln', 'AC-225-GLM', 'M3150906 723', 'Sin Oberservaciones', 'Bodega “Baños”', ' ₡205,000.00 ', 'Laura Mesén', 1, NULL),
('N00141960', 1, 'Motosierra', 'Oleo Mac', '952', '574154580', 'De color naranja.', 'Bodega“Baños”', ' ₡259,000.00 ', 'Laura Mesén', 1, NULL),
('N00141961', 1, 'Podadora de Altura', 'EFCO', 'PTX2700', '2344446708', 'Sin Oberservaciones', 'Bodega “Baños”', ' ₡391,000.00 ', 'Laura Mesén', 1, NULL),
('N00141962', 1, 'Sopladora', 'OLEOMAC', 'BV162', '5245179708', 'Sin Oberservaciones', 'Bodega “Baños”', ' ₡359,000.00 ', 'Laura Mesén', 1, NULL),
('N00142046', 1, 'Moto-guaraña', 'STHIL', 'FS120', '808545360', 'Sin Oberservaciones', 'Bodega “Baños”', ' ₡156,773.00 ', 'Laura Mesén', 1, NULL),
('N00142047', 1, 'Proyector de multimedia inalámbrico estándar II', 'EPSON', 'H654A', 'V6YK5501535', 'Sin Oberservaciones', 'DIRECCION', ' ₡595,000.00 ', 'LAURA MESEN', 1, NULL),
('N00142048', 1, 'Proyector de multimedia interactivo estándar IV', 'EPSON', 'H600A', 'UHWK5100020', 'Sin Oberservaciones', 'Aula #2', ' ₡1,400,000.00 ', 'LAURA MESEN', 1, NULL),
('N00142049', 1, 'Proyector de multimedia interactivo estándar IV', 'EPSON 585 WI', 'H600A', 'UHWKF460066L', 'Sin Oberservaciones', 'Aula #3', ' ₡1,400,000.00 ', 'LAURA MESEN', 1, NULL),
('N00142050', 1, 'Proyector de multimedia Full HD estándar III', 'EPSON', 'H651F', 'V88F490395L', 'Sin Oberservaciones', 'Oficina Administrativa ', ' ₡1,300,000.00 ', 'LAURA MESEN', 1, NULL),
('N00142051', 1, 'Proyector de multimedia Full HD estándar III', 'EPSON 585 WI', 'H651F', 'V88F550187L', 'Sin Oberservaciones', 'Oficina Administrativa', ' ₡1,300,000.00 ', 'LAURA MESEN', 1, NULL),
('N00142153', 1, 'Cámara fotográfica digital estándar II', 'CANON', 'RebelT5', '311034 006007', 'Sin Oberservaciones', 'Oficina de Dirección ', ' ₡703,294.06 ', 'LAURA MESEN', 1, NULL),
('N00142171', 1, 'Carrito metálico para libros', 'Fantini', 'No tiene', 'No tiene', 'Sin Oberservaciones', 'BODEGA', ' ₡178,841.31 ', 'Laura Mesén', 1, NULL),
('N00142274', 1, 'Parlantes para salida de audio', 'JBL', 'EON615', 'P1391-32026', 'Sin Oberservaciones', 'Bodega', ' ₡286,000.00 ', 'LAURA MESEN', 1, NULL),
('N00142275', 1, 'Parlantes para salida de audio', 'JBL', 'EON615', 'P1391-32030', 'Sin Oberservaciones', 'Bodega', ' ₡286,000.00 ', 'LAURA MESEN', 1, NULL),
('N00142276', 1, 'Parlantes para salida de audio', 'JBL', 'EON515', 'P1391-32031', 'Sin Oberservaciones', 'Bodega', ' ₡286,000.00 ', 'LAURA MESEN', 1, NULL),
('N00142277', 1, 'Parlantes para salida de audio', 'JBL', 'EON615', 'P1391-32032', 'Sin Oberservaciones', 'Bodega', ' ₡286,000.00 ', 'LAURA MESEN', 1, NULL),
('N00142278', 1, 'Parlantes para salida de audio', 'QSC', 'KW153', '61P650193', 'Sin Oberservaciones', 'Bodega', ' ₡825,000.00 ', 'LAURA MESEN', 1, NULL),
('N00142279', 1, 'Parlantes para salida de audio', 'QSC', 'KW153', '61P650199', 'Sin Oberservaciones', 'Bodega ', ' ₡825,000.00 ', 'LAURA MESEN', 1, NULL),
('N00142280', 1, 'Parlantes para salida de audio', 'QSC', 'KW153', '61P650197', 'Sin Oberservaciones', 'Bodega', ' ₡825,000.00 ', 'LAURA MESEN', 1, NULL),
('N00142281', 1, 'Parlantes  para salida de audio  ', 'QSC', 'KW153', '61P650195', 'Sin Oberservaciones', 'Bodega', ' ₡825,000.00 ', 'LAURA MESEN', 1, NULL),
('N00142282', 1, 'Parlantes para salida de audio', 'Peavey', 'KB4-12046', '0DBDF170487', 'Sin Oberservaciones', 'Salón', ' ₡234,000.00 ', 'LAURA MESEN', 1, NULL),
('N00142283', 1, 'Parlantes para salida de audio', 'Peavey', 'KB4-120UG', 'ODBDF170490', 'Sin Oberservaciones', 'bodega', ' ₡234,000.00 ', 'LAURA MESEN', 1, NULL),
('N00142284', 1, 'Mezcladora de sonido', 'YAMAHA', 'MGP24x', 'ECULO1149', 'Sin Oberservaciones', 'Bodega ', ' ₡689,400.00 ', 'LAURA MESEN', 1, NULL),
('N00142334', 1, 'Pizza interactiva estándar III con Proyector', 'TRIUMPH', '89” MULTITOUCHproyectos PJ 3000', '1508030260136', 'Sin Oberservaciones', 'Aula #4', ' ₡1,859,343.00 ', 'LAURA MESEN', 1, NULL),
('N00142335', 1, 'Pizza interactiva estándar III con Proyector', 'TRIUMPH', '89” MULTITOUCHproyector PJ 3000', '1508300260139', 'Sin Oberservaciones', 'Aula #1', ' ₡1,859,343.00 ', 'LAURA MESEN', 1, NULL),
('N00142709', 1, 'Sistema de Iluminación', 'Chauvet DJ', 'OBBY40', '09080233-0915017359', 'Sin Oberservaciones', 'Bodega ', ' ₡1,245,684.10 ', 'Laura Mesén', 1, NULL),
('N00143924', 1, 'CPU, Monitor y Teclado', 'HP', 'Prodesk 600 G2', 'MXL 64120 P9', ' ( Computadora incompleta solo monitor y teclado)( el cpu esta en mal estado)', 'SECRETARIA', ' ₡480,075.63 ', 'Laura Mesén', 1, NULL),
('N00143925', 1, 'Monitor y Teclado', 'HP', 'Prodesk 600 G2', 'MXL 64 120Q1', ' ( Computadora incompleta solo monitor y teclado)( el cpu esta en mal estado, la UNA retiro el CPU el 18 de enero 2021)', 'Oficina Administrativa', '₡480,075.63 ', 'Laura Mesén', 1, NULL),
('N00143926', 1, 'CPU, Monitor y Teclado', 'HP', 'Prodesk600G2', 'MXL64120PY', ' ( Computadora incompleta solo monitor y teclado)( el cpu esta en mal estado)', 'Wendy', ' ₡480,075.63 ', 'Laura Mesén', 1, NULL),
('N00143927', 1, 'Monitor y Teclado', 'HP', 'Prodesk 600G2', 'MXL 64120 PQ', 'JERRIKA ( Computadora incompleta solo monitor y teclado)  ( el CPU esta en mal estado, la UNA lo retiro el 18 de enero 2021)', 'Oficina Administrativa', ' ₡480,075.63 ', 'Laura Mesén', 1, NULL),
('N00143928', 1, 'Computadora de escritorio estándar I', 'DELL', 'Prodesk600G2', 'MXL64120PS', 'LOBBY', 'LOBBY ', ' ₡480,075.63 ', 'Laura Mesén', 1, NULL),
('N00143929', 1, 'Portatil', 'HP', 'BCM943228Z', '5CD6060N1J', 'NÚMERO 4', 'Wendy', ' ₡427,502.77 ', 'Laura Mesén', 1, NULL),
('N00143930', 1, 'Portatil', 'HP', 'BCM943228Z', '5CD6060MZO', 'NÚMERO 2', 'Wendy', ' ₡427,502.77 ', 'Laura Mesén', 1, NULL),
('N00143931', 1, 'Portatil', 'HP', 'BCM943228Z', '5CD6060NOP', 'NÚMERO 3', 'Laura', ' ₡427,502.77 ', 'Laura Mesén', 1, NULL),
('N00143932', 1, 'Portatil', 'HP', 'BCM943228Z', '5CD6060MZ8', 'NÚMERO 1', 'Oficina Administrativa ', ' ₡427,502.77 ', 'Laura Mesén', 1, NULL),
('N00144458', 1, 'Compresor de aire', 'Cambell Hausfeld', 'HX510000', 'Jll1515-150105', 'BODEGA Y BAÑOS', 'Bodega ', ' ₡119,460.00 ', 'Laura Mesén', 1, NULL),
('N00144808', 1, 'PUNTO DE ACCESO', 'CISCO', 'AIR-CAP27021-AK9', 'SFCW2028NR1G', 'Sin Oberservaciones', 'SALON DE ENSAYO', ' ₡430,725.97 ', 'Laura Mesén', 1, NULL),
('N00144824', 1, 'PUNTO DE ACCESO', 'CISCO', 'AIR-CAP27021-AK9', 'SFCW2028NRPL', 'Sin Oberservaciones', 'LOBBY', ' ₡430,725.97 ', 'Laura Mesén', 1, NULL),
('N00144825', 1, 'PUNTO DE ACCESO', 'CISCO', 'AIR-CAP27021-AK9', 'SFCW2028NRPR', 'Sin Oberservaciones', 'AULA2', ' ₡430,725.97 ', 'Laura Mesén', 1, NULL),
('N00153895', 1, 'DESHUMEDECEDOR', 'FRIDGIDARE', '70 PINTAS', 'in81803221', 'Sin Oberservaciones', 'Bodega ', ' ₡174,745.00 ', 'Laura Mesén', 1, NULL),
('N00153896', 1, 'DESHUMEDECEDOR', 'FRIDGIDARE', '70 PINTAS', 'in81901199', 'Sin Oberservaciones', 'Bodega ', ' ₡174,745.00 ', 'Laura Mesén', 1, NULL),
('N00155615', 1, 'FOTOCOPIADORA', 'XEROX', 'AL-B8045', 'Y4X836824', 'Sin Oberservaciones', 'SECRETARIA', 'Sin Valor', 'Laura MESEN', 1, NULL),
('N00162502', 1, 'Unidad evaporadora y condensadora para aire acondicionado', 'TROPICOOL', 'TRMEP-12K TRMCP-12K', '120059       120040', 'NUEVO Remplaza unidad dañada del cubiculo1 Este activo consta de dos articulos Unudad evaporadora y Unidad Condensadora', 'Cubículo 1 ', 'Sin Valor', 'Laura Mesen', 1, NULL),
('N00162503', 1, 'Unidad evaporadora y condensadora para aire acondicionado', 'TROPICOOLL', 'TRMEP-12K TRMCP-12K', '120097      120068', 'NUEVO Remplaza unidad dañada del cubiculo2 Este activo consta de dos articulos Unudad evaporadora y Unidad Condensadora', 'CUBICULO 2 ', 'Sin Valor', 'Laura Mesen', 1, NULL),
('N00163894', 1, 'Computadoras Portatiles', 'HP ', '427 G7', '5CDO11F52P', 'Sin Oberservaciones', 'Lauren', 'Sin Valor', 'Sin Responsable', 1, NULL),
('N00163895', 1, 'Computadoras Portatiles', 'HP ', '428 G7', '5CDO11F52X', 'Sin Oberservaciones', 'DAVID GRANADOS', 'Sin Valor', 'Sin Responsable', 1, NULL),
('N00163896', 1, 'Computadoras Portatiles', 'HP ', '429 G7', '5CDO11F4VH', 'Sin Oberservaciones', 'Laura', 'Sin Valor', 'Sin Responsable', 1, NULL),
('N00163897', 1, 'Computadoras Portatiles', 'HP ', '430 G7', '5CDO11F4TZ', 'Sin Oberservaciones', 'MAYDA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('N00164899', 1, 'Computadora ', 'DELL', 'Optiplex 3070', 'Sin Serie', '2020, incluye CPU, monitor, teclado.', 'Laura', 'Sin Valor', 'Laura Mesén', 1, NULL),
('N00164900', 1, 'Computadora ', 'DELL', 'Optiplex 3070', 'Sin Serie', '2020, incluye CPU, monitor, teclado.', 'Laura', 'Sin Valor', 'Laura Mesén', 1, NULL),
('N00164901', 1, 'Computadora ', 'DELL', 'Optiplex 3070', 'Sin Serie', '2020, incluye CPU, monitor, teclado.', 'Laura', 'Sin Valor', 'Laura Mesén', 1, NULL),
('UNAACT-001', 1, 'Basureros', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Sin Oberservaciones', 'Pasillos', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-002', 1, 'Basureros', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Sin Oberservaciones', 'Pasillos', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-003', 1, 'Basureros', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Sin Oberservaciones', 'Pasillos', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-004', 1, 'Basureros', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Sin Oberservaciones', 'Pasillos', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-005', 1, 'Basureros', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Sin Oberservaciones', 'Pasillos', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-006', 1, 'Basureros', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Sin Oberservaciones', 'Pasillos', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-007', 1, 'Taladro Eléctrico', 'MIlWAUKEE', '5374-20', 'E62AD12401322', 'Sin Oberservaciones', 'Bodega “Baños”', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-008', 1, 'Pistola para pintar', 'Sagola', 'Alta D.F 4001G', 'Sin Serie', 'Sin Oberservaciones', 'Bodega “Baños”', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-009', 1, 'Juego de 12 llaves. Coro fijas.', 'STANLEY', 'Sin Modelo', 'Sin Serie', 'Sin Oberservaciones', 'Bodega “Baños”', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-010', 1, 'Prensa para Banco', 'TRUPER', 'Sin Modelo', 'Sin Serie', 'Sin Oberservaciones', 'Bodega “Baños”', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-011', 1, 'Esmeriladora  pequeña', 'MIlWAUKEE', '1630-33', 'C27DD151404837', 'Para usar con disco de 4” ½', 'Bodega “Baños”', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-012', 1, 'Esmeriladora  grande', 'MIlWAUKEE', '6086-30', 'E74A914470437', 'Para usar con disco de 7” ½', 'Bodega “Baños”', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-013', 1, 'Alicate de presión punta larga.', 'STANLEY', 'Sin Modelo', '84-396', 'Dos alicates con las mismas características.', 'Bodega “Baños”', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-014', 1, 'Alicate Combinación', 'STANLEY', 'Sin Modelo', '84-098', 'Sin Oberservaciones', 'Bodega “Baños”', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-015', 1, 'Llave para Cañería', 'STANLEY', '87-625', 'Sin Serie', 'Sin Oberservaciones', 'Bodega “Baños”', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-016', 1, 'Llave Francesa', 'STANLEY', '87-434', 'Sin Serie', 'Sin Oberservaciones', 'Bodega “Baños”', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-017', 1, 'Cajas para Herramientas', 'RIMAX', '24”', 'Sin Serie', '-4 unidades', 'Bodega “Baños”', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-018', 1, 'Cajas para Herramientas', 'RIMAX', '20”', 'Sin Serie', '-3 unidades', 'Bodega “Baños”', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-019', 1, 'Taladro Eléctrico', 'MILWAUKEE', '5375-20', 'C50AD15231589', 'Sin Oberservaciones', 'Bodega “Baños”', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-020', 1, ' Palas', 'TRUPER', 'Sin Modelo', 'Sin Serie', 'Mango de madera.Puño color naranja.', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-021', 1, ' Palas', 'TRUPER', 'Sin Modelo', 'Sin Serie', 'Mango de madera.Puño color naranja.', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-022', 1, ' Palas', 'TRUPER', 'Sin Modelo', 'Sin Serie', 'Mango de madera.Puño color naranja', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-023', 1, 'Macana', 'Forjado', '10820C', 'Sin Serie', 'De color gris.', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-024', 1, 'Tijera Podadora', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color gris.', 'Bodega “Baños”', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-025', 1, 'Carretillo', 'Saco', 'Sin Modelo', 'Sin Serie', 'Color negro.Llanta inflable.Batea grande.', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-026', 1, 'Micrófono ', 'CVD', 'M179', 'Sin Serie', 'Micrófono 360 MAL ESTADO', 'Bodega ', 'Sin Valor', 'Laura Mesen', 1, NULL),
('UNAACT-027', 1, 'Micrófono ', 'CVD', 'M129', 'Sin Serie', 'Micrófono 360 MAL ESTADO', 'Bodega ', 'Sin Valor', 'Laura Mesen', 1, NULL),
('UNAACT-028', 1, ' micrófonos', 'Samson', 'C51-CS2', 'Sin Serie', 'Dos cabezas (cs1 para vocales cs2 para instrumento)Con estuche duro, color negro, marca samson', 'Bodega', 'Sin Valor', 'Laura Mesen', 1, NULL),
('UNAACT-029', 1, ' micrófonos', 'Samson', 'C51-CS2', 'Sin Serie', 'Dos cabezas (cs1 para vocales cs2 para instrumento)Con estuche duro, color negro, marca samson', 'Bodega', 'Sin Valor', 'Laura Mesen', 1, NULL),
('UNAACT-030', 1, ' micrófonos', 'Samson', 'C51-CS2', 'Sin Serie', 'Dos cabezas (cs1 para vocales cs2 para instrumento)Con estuche duro, color negro, marca samson', 'Bodega', 'Sin Valor', 'Laura Mesen', 1, NULL),
('UNAACT-031', 1, ' micrófonos', 'Samson', 'C51-CS2', 'Sin Serie', 'Dos cabezas (cs1 para vocales cs2 para instrumento)Con estuche duro, color negro, marca samson', 'Bodega', 'Sin Valor', 'Laura Mesen', 1, NULL),
('UNAACT-032', 1, ' micrófonos', 'Samson', 'C51-CS2', 'Sin Serie', 'Dos cabezas (cs1 para vocales cs2 para instrumento)Con estuche duro, color negro, marca samson', 'Bodega', 'Sin Valor', 'Laura Mesen', 1, NULL),
('UNAACT-033', 1, 'Mesa metálica con rodines', 'NO TIENE', 'NO TIENE', 'NO TIENE', '    Mesa  #1', 'INDEFINIDA', 'Sin Valor', 'Laura Mesen', 1, NULL),
('UNAACT-034', 1, 'Mesa metálica con rodines', 'NO TIENE', 'NO TIENE', 'NO TIENE', '  MESA #2', 'INDEFINIDA', 'Sin Valor', 'Laura Mesen', 1, NULL),
('UNAACT-035', 1, 'Mesa metálica con rodines', 'NO TIENE', 'NO TIENE', 'NO TIENE', 'MESA  #3', 'INDEFINIDA', 'Sin Valor', 'Laura Mesen', 1, NULL),
('UNAACT-036', 1, 'Mesa metálica con rodines', 'NO TIENE', 'NO TIENE', 'NO TIENE', 'MESA  #4', 'INDEFINIDA', 'Sin Valor', 'Laura Mesen', 1, NULL),
('UNAACT-037', 1, 'Sillas para escritorio.', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color negro. Con rodines', 'Lobby', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-038', 1, 'Sillas para escritorio.', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color negro.Con rodines', 'Laura', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-039', 1, 'Sillas para escritorio.', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color negro.Con rodines', 'Laura', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-040', 1, 'Sillas para escritorio.', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color negro. Con rodines', 'Wendy', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-041', 1, 'Sillas para escritorio.', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color negro. Con rodines', ' Jerryka', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-042', 1, 'Sillas para escritorio.Roja', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. Con rodines color rojo.', 'Secretaria', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-043', 1, 'Sillas para escritorio.Roja', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. Con rodines color rojo.', 'Cubículos aulas', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-044', 1, 'Sillas para escritorio.Roja', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. Con rodines color rojo.', 'Cubículos aulas', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-045', 1, 'Sillas para escritorio.Roja', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. Con rodines color rojo.', 'Cubículos aulas', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-046', 1, 'Sillas para escritorio.Roja', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. Con rodines color rojo.', 'Cubículos aulas', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-047', 1, 'Sillas para escritorio.Roja', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. Con rodines color rojo.', 'Cubículos aulas', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-048', 1, 'Sillas para escritorio.Roja', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. Con rodines color rojo.', 'Cubículos aulas', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-049', 1, 'Sillas para escritorio.Roja', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. Con rodines color rojo.', 'Cubículos aulas', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-050', 1, 'Sillas para escritorio.Roja', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. Con rodines color rojo.', 'Cubículos aulas', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-051', 1, 'Sillas para escritorio.Roja', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. Con rodines color rojo.', 'Cubículos aulas', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-052', 1, 'Sillas para escritorio.Roja', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. Con rodines color rojo.', 'Cubículos aulas', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-053', 1, 'Sillas para escritorio.Roja', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. Con rodines color rojo.', 'Cubículos aulas', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-054', 1, 'Sillas para escritorio.Roja', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. Con rodines color rojo.', 'Cubículos aulas', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-055', 1, 'Sillas para escritorio.Roja', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. Con rodines color rojo.', 'Cubículos aulas', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-056', 1, 'Sillas para escritorio.Roja', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. Con rodines color rojo.', 'Cubículos aulas', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-057', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negra.', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-058', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negra.', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-059', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negra.', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-060', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negra.', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-061', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negra.', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-062', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negra.', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-063', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negra.', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-064', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negra.', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-065', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negra.', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-066', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negra.', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-067', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negra.', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-068', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negra.', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-069', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negra.', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-070', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negra.', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-071', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negra.', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-072', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negra.', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-073', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negra.', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-074', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negra.', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-075', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negra.', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-076', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negra.', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-077', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negra.', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-078', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negra.', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-079', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negra.', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-080', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negra.', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-081', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negra.', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-082', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negra.', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-083', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negra.', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-084', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negra.', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-085', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negra.', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-086', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negra.', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-087', 1, 'Pedestal para micrófono.', 'Sto-gg', 'Mis-1022 BK', 'Sin Serie', 'Color negro.', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-088', 1, 'Pedestal para micrófono.', 'Sto-gg', 'Mis-1022 BK', 'Sin Serie', 'Color negro.', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-089', 1, 'Pedestal para micrófono.', 'Sto-gg', 'Mis-1022 BK', 'Sin Serie', 'Color negro.', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-090', 1, 'Pedestal para micrófono.', 'Sto-gg', 'Mis-1022 BK', 'Sin Serie', 'Color negro.', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-091', 1, 'Pedestal para micrófono.', 'Sto-gg', 'Mis-1022 BK', 'Sin Serie', 'Color negro.', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-092', 1, 'Pedestal para micrófono.', 'Sto-gg', 'Mis-1022 BK', 'Sin Serie', 'Color negro.', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-093', 1, 'Pedestal para micrófono.', 'Sto-gg', 'Mis-1022 BK', 'Sin Serie', 'Color negro.', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-094', 1, 'Pedestal para micrófono.', 'Sto-gg', 'Mis-1022 BK', 'Sin Serie', 'Color negro.', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-095', 1, 'Pedestal para micrófono.', 'Sto-gg', 'Mis-1022 BK', 'Sin Serie', 'Color negro.', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-096', 1, 'Pedestal para micrófono.', 'Sto-gg', 'Mis-1022 BK', 'Sin Serie', 'Color negro.', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-097', 1, 'Pedestal para micrófono.', 'Sto-gg', 'Mis-1022 BK', 'Sin Serie', 'Color negro.', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-098', 1, 'Pedestal para micrófono.', 'Sto-gg', 'Mis-1022 BK', 'Sin Serie', 'Color negro.', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-099', 1, 'Pedestal para micrófono.', 'Sto-gg', 'Mis-1022 BK', 'Sin Serie', 'Color negro.', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-100', 1, 'Pedestal para micrófono.', 'Sto-gg', 'Mis-1022 BK', 'Sin Serie', 'Color negro.', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-101', 1, 'Pedestal para micrófono.', 'Sto-gg', 'Mis-1022 BK', 'Sin Serie', 'Color negro.', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-102', 1, 'Pedestal para micrófono.', 'Sto-gg', 'Mis-1022 BK', 'Sin Serie', 'Color negro.', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-103', 1, 'Pedestal para micrófono.', 'Sto-gg', 'Mis-1022 BK', 'Sin Serie', 'Color negro.', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-104', 1, 'Pedestal para micrófono.', 'ON -STAGE', 'Sin Modelo', 'Sin Serie', 'Color negro.', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-105', 1, 'Pedestal para micrófono.', 'ON -STAGE', 'Sin Modelo', 'Sin Serie', 'Color negro.', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-106', 1, 'Pedestal para micrófono.', 'ON -STAGE', 'Sin Modelo', 'Sin Serie', 'Color negro.', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-107', 1, 'Culebra de 24 canales.', 'Sound barrier', 'ST244XLR/150', 'Sin Serie', 'Nueva.', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-108', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-109', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-110', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-111', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-112', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-113', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-114', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-115', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-116', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-117', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-118', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-119', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-120', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-121', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-122', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-123', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-124', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-125', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-126', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-127', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-128', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-129', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-130', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-131', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-132', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-133', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-134', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-135', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-136', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-137', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-138', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-139', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-140', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-141', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-142', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-143', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-144', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-145', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-146', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-147', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-148', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-149', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-150', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-151', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-152', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-153', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-154', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-155', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-156', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-157', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-158', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-159', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-160', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-161', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-162', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-163', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-164', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-165', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-166', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-167', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-168', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-169', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-170', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-171', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-172', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-173', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-174', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-175', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-176', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-177', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-178', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-179', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-180', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-181', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-182', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-183', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-184', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-185', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-186', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-187', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-188', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-189', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-190', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-191', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-192', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-193', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-194', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-195', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-196', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-197', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-198', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-199', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-200', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-201', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-202', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-203', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-204', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-205', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-206', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-207', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-208', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-209', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-210', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-211', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-212', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-213', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-214', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-215', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-216', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-217', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-218', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-219', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-220', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-221', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-222', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-223', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-224', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-225', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-226', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-227', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-228', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-229', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-230', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-231', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-232', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-233', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-234', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-235', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-236', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-237', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-238', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-239', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-240', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-241', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-242', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-243', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-244', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-245', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-246', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-247', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-248', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-249', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-250', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-251', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-252', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-253', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-254', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-255', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL);
INSERT INTO `activos` (`codActivo`, `idInstitucion`, `nombre`, `marca`, `modelo`, `serie`, `observaciones`, `ubicacion`, `valor`, `responsable`, `idEstado`, `fotoActivo`) VALUES
('UNAACT-256', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-257', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color gris metal. Plástico bige ', 'Salón Y BODEGA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-258', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-259', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-260', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-261', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-262', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-263', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-264', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-265', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-266', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-267', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-268', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-269', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-270', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-271', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-272', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-273', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-274', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-275', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-276', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-277', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-278', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-279', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-280', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-281', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-282', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-283', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-284', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-285', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-286', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-287', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-288', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-289', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-290', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-291', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-292', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-293', 1, 'Sillas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras', 'Aulas Y Salon', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-294', 1, 'Mesas Plásticas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blanco', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-295', 1, 'Mesas Plásticas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blanco', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-296', 1, 'Mesas Plásticas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blanco', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-297', 1, 'Mesas Plásticas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blanco', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-298', 1, 'Mesas Plásticas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blanco', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-299', 1, 'Mesas Plásticas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blanco', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-300', 1, 'Mesas Plásticas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blanco', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-301', 1, 'Mesas Plásticas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blanco', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-302', 1, 'Mesas Plásticas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blanco', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-303', 1, 'Mesas Plásticas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blanco', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-304', 1, 'Mesas Plásticas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blanco', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-305', 1, 'Mesas Plásticas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blanco', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-306', 1, 'Mesas Plásticas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blanco', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-307', 1, 'Mesas Plásticas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blanco', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-308', 1, 'Mesas Plásticas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'De color blanco', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-309', 1, 'Atriles Cubiculo #7', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Atriles p/director aluminio negros', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-310', 1, 'Sillas  Acolchadas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. De color rojo', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-311', 1, 'Sillas  Acolchadas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. De color rojo', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-312', 1, 'Sillas  Acolchadas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. De color rojo', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-313', 1, 'Sillas  Acolchadas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. De color rojo', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-314', 1, 'Sillas  Acolchadas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. De color rojo', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-315', 1, 'Sillas  Acolchadas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. De color rojo', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-316', 1, 'Sillas  Acolchadas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. De color rojo', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-317', 1, 'Sillas  Acolchadas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. De color rojo', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-318', 1, 'Sillas  Acolchadas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. De color rojo', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-319', 1, 'Sillas  Acolchadas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. De color rojo', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-320', 1, 'Sillas  Acolchadas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. De color rojo', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-321', 1, 'Sillas  Acolchadas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. De color rojo', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-322', 1, 'Sillas  Acolchadas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. De color rojo', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-323', 1, 'Sillas  Acolchadas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. De color rojo', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-324', 1, 'Sillas  Acolchadas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. De color rojo', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-325', 1, 'Sillas  Acolchadas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. De color rojo', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-326', 1, 'Sillas  Acolchadas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. De color rojo', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-327', 1, 'Sillas  Acolchadas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. De color rojo', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-328', 1, 'Sillas  Acolchadas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. De color rojo', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-329', 1, 'Sillas  Acolchadas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. De color rojo', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-330', 1, 'Sillas  Acolchadas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. De color rojo', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-331', 1, 'Sillas  Acolchadas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. De color rojo', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-332', 1, 'Sillas  Acolchadas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. De color rojo', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-333', 1, 'Sillas  Acolchadas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. De color rojo', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-334', 1, 'Sillas  Acolchadas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. De color rojo', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-335', 1, 'Sillas  Acolchadas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. De color rojo', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-336', 1, 'Sillas  Acolchadas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. De color rojo', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-337', 1, 'Sillas  Acolchadas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. De color rojo', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-338', 1, 'Sillas  Acolchadas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. De color rojo', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-339', 1, 'Sillas  Acolchadas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. De color rojo', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-340', 1, 'Sillas  Acolchadas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. De color rojo', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-341', 1, 'Sillas  Acolchadas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. De color rojo', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-342', 1, 'Sillas  Acolchadas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. De color rojo', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-343', 1, 'Sillas  Acolchadas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. De color rojo', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-344', 1, 'Sillas  Acolchadas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. De color rojo', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-345', 1, 'Sillas  Acolchadas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. De color rojo', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-346', 1, 'Sillas  Acolchadas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. De color rojo', 'Bodega', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-347', 1, 'Sillas  Acolchadas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. De color rojo', ' Aulas, cubículos Dirección', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-348', 1, 'Sillas  Acolchadas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. De color rojo', ' Aulas, cubículos Dirección', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-349', 1, 'Sillas  Acolchadas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. De color rojo', ' Aulas, cubículos Dirección', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-350', 1, 'Sillas  Acolchadas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. De color rojo', ' Aulas, cubículos Dirección', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-351', 1, 'Sillas  Acolchadas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. De color rojo', ' Aulas, cubículos Dirección', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-352', 1, 'Sillas  Acolchadas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. De color rojo', ' Aulas, cubículos Dirección', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-353', 1, 'Sillas  Acolchadas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. De color rojo', ' Aulas, cubículos Dirección', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-354', 1, 'Sillas  Acolchadas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. De color rojo', ' Aulas, cubículos Dirección', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-355', 1, 'Sillas  Acolchadas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. De color rojo', ' Aulas, cubículos Dirección', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-356', 1, 'Sillas  Acolchadas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. De color rojo', ' Aulas, cubículos Dirección', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-357', 1, 'Sillas  Acolchadas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. De color rojo', ' Aulas, cubículos Dirección', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-358', 1, 'Sillas  Acolchadas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. De color rojo', ' Aulas, cubículos Dirección', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-359', 1, 'Sillas  Acolchadas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. De color rojo', ' Aulas, cubículos Dirección', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-360', 1, 'Sillas  Acolchadas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. De color rojo', ' Aulas, cubículos Dirección', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-361', 1, 'Pro-Lok', 'Sin Marca', 'Sin Modelo', 'PKS-555', 'Stent para piano ', ' Cubículo #6', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-362', 1, 'Pro-Lok', 'Sin Marca', 'Sin Modelo', 'PKS-556', 'Stent para piano ', ' ( en contrato)', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-363', 1, 'Pro-Lok', 'Sin Marca', 'Sin Modelo', 'PKS-557', 'Stent para piano ', '( en contrato)', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-364', 1, 'Pro-Lok', 'Sin Marca', 'Sin Modelo', 'PKS-558', 'Stent para piano ', ' (en  contrato)', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-365', 1, 'Cajas para Herramientas', 'RIMAX', '24”', 'Sin Serie', '4 unidades', 'Bodega “Baños”', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-366', 1, 'Cajas para Herramientas', 'RIMAX', '24”', 'Sin Serie', '4 unidades', 'Bodega “Baños”', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-367', 1, 'Cajas para Herramientas', 'RIMAX', '24”', 'Sin Serie', '4 unidades', 'Bodega “Baños”', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-368', 1, 'Cajas para Herramientas', 'RIMAX', '20”', 'Sin Serie', '3 unidades', 'Bodega “Baños”', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-369', 1, 'Cajas para Herramientas', 'RIMAX', '20”', 'Sin Serie', '3 unidades', 'Bodega “Baños”', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-370', 1, 'Cajas para Herramientas', 'RIMAX', '20”', 'Sin Serie', '3 unidades', 'Bodega “Baños”', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-371', 1, 'A.P.C', 'Sin Marca', 'BR1000G', '3B1572 X34480', 'Battery Backup (REPARACIÓN UNA)MALA', 'BODEGA ', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-372', 1, 'A.P.C', 'Sin Marca', 'BR1500G', '4B152 7P04035', 'Battery Backup (REPARACIÓN UNA)MALA', 'BODEGA ', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-373', 1, 'A.P.C', 'Sin Marca', 'BR1000G', '3B1512 X34484', 'Battery Backup', 'CASETILLA DEL  GUARDA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-374', 1, 'A.P.C', 'Sin Marca', 'BR1000G', '3B1512 X34460', 'Battery Backup', 'CUARTO DE REDES', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-375', 1, 'A.P.C', 'Sin Marca', 'BR1000G', '3B1512 X34489', 'Battery Backup', 'LAURA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-376', 1, 'A.P.C', 'Sin Marca', 'BR1000G', '3B1512 X3464', 'Battery Backup', 'LAURA', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-377', 1, 'A.P.C', 'Sin Marca', 'BR1000G', '3B1512 X34467', 'Battery Backup (REPARACIÓN UNA)MALA', 'BODEGA ', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-378', 1, 'A.P.C', 'Sin Marca', 'BR1000G', '4B1527 PO2269', 'Battery Backup (REPARACIÓN UNA)MALA', 'BODEGA ', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-379', 1, 'Atriles', 'MANHASSET', 'Sin Modelo', 'Sin Serie', 'Atriles p/director aluminio negros', 'Bodega', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-380', 1, 'Atriles', 'MANHASSET', 'Sin Modelo', 'Sin Serie', 'Atriles p/director aluminio negros', 'Bodega', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-381', 1, 'Atriles', 'MANHASSET', 'Sin Modelo', 'Sin Serie', 'Atriles p/director aluminio negros', 'Bodega', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-382', 1, 'Atriles', 'MANHASSET', 'Sin Modelo', 'Sin Serie', 'Atriles p/director aluminio negros', 'Bodega', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-383', 1, 'Atriles', 'MANHASSET', 'Sin Modelo', 'Sin Serie', 'Atriles p/director aluminio negros', 'Bodega', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-384', 1, 'Atriles', 'MANHASSET', 'Sin Modelo', 'Sin Serie', 'Atriles p/director aluminio negros', 'Bodega', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-385', 1, 'Atriles', 'MANHASSET', 'Sin Modelo', 'Sin Serie', 'Atriles p/director aluminio negros', 'Bodega', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-386', 1, 'Atriles', 'MANHASSET', 'Sin Modelo', 'Sin Serie', 'Atriles p/director aluminio negros', 'Bodega', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-387', 1, 'Atriles', 'MANHASSET', 'Sin Modelo', 'Sin Serie', 'Atriles p/director aluminio negros', 'Bodega', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-388', 1, 'Atriles', 'MANHASSET', 'Sin Modelo', 'Sin Serie', 'Atriles p/director aluminio negros', 'Bodega', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-389', 1, 'Atriles', 'MANHASSET', 'Sin Modelo', 'Sin Serie', 'Atriles p/director aluminio negros', 'Bodega', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-390', 1, 'Atriles', 'MANHASSET', 'Sin Modelo', 'Sin Serie', 'Atriles p/director aluminio negros', 'Bodega', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-391', 1, 'Atriles', 'MANHASSET', 'Sin Modelo', 'Sin Serie', 'Atriles p/director aluminio negros', 'Bodega', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-392', 1, 'Atriles', 'MANHASSET', 'Sin Modelo', 'Sin Serie', 'Atriles p/director aluminio negros', 'Bodega', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-393', 1, 'Atriles', 'MANHASSET', 'Sin Modelo', 'Sin Serie', 'Atriles p/director aluminio negros', 'Bodega', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-394', 1, 'Atriles', 'MANHASSET', 'Sin Modelo', 'Sin Serie', 'Atriles p/director aluminio negros', 'Bodega', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-395', 1, 'Atriles', 'MANHASSET', 'Sin Modelo', 'Sin Serie', 'Atriles p/director aluminio negros', 'Bodega', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-396', 1, 'Atriles', 'MANHASSET', 'Sin Modelo', 'Sin Serie', 'Atriles p/director aluminio negros', 'Bodega', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-397', 1, 'Atriles', 'MANHASSET', 'Sin Modelo', 'Sin Serie', 'Atriles p/director aluminio negros', 'Bodega', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-398', 1, 'Atriles', 'MANHASSET', 'Sin Modelo', 'Sin Serie', 'Atriles p/director aluminio negros', 'Bodega', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-399', 1, 'Atriles', 'MANHASSET', 'Sin Modelo', 'Sin Serie', 'Atriles p/director aluminio negros', 'Bodega', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-400', 1, 'Atriles', 'MANHASSET', 'Sin Modelo', 'Sin Serie', 'Atriles p/director aluminio negros', 'Bodega', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-401', 1, 'Atriles', 'MANHASSET', 'Sin Modelo', 'Sin Serie', 'Atriles p/director aluminio negros', 'Bodega', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-402', 1, 'Atriles', 'MANHASSET', 'Sin Modelo', 'Sin Serie', 'Atriles p/director aluminio negros', 'Bodega', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-403', 1, 'Atriles', 'ONSTAGE', 'Sin Modelo', 'Sin Serie', 'Atriles p/director aluminio negros', 'Bodega', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-404', 1, 'Atriles', 'ONSTAGE', 'Sin Modelo', 'Sin Serie', 'Atriles p/director aluminio negros', 'Bodega', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-405', 1, 'Atriles', 'ONSTAGE', 'Sin Modelo', 'Sin Serie', 'Atriles p/director aluminio negros', 'Bodega', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-406', 1, 'Atriles', 'ONSTAGE', 'Sin Modelo', 'Sin Serie', 'Atriles p/director aluminio negros', 'Bodega', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-407', 1, 'Atriles', 'ONSTAGE', 'Sin Modelo', 'Sin Serie', 'Atriles p/director aluminio negros', 'Bodega', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-408', 1, 'Atriles', 'ONSTAGE', 'Sin Modelo', 'Sin Serie', 'Atriles p/director aluminio negros', 'Bodega', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-409', 1, 'Atriles', 'ONSTAGE', 'Sin Modelo', 'Sin Serie', 'Atriles p/director aluminio negros', 'Bodega', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-410', 1, 'Atriles', 'ONSTAGE', 'Sin Modelo', 'Sin Serie', 'Atriles p/director aluminio negros', 'Bodega', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-411', 1, 'Atriles', 'ONSTAGE', 'Sin Modelo', 'Sin Serie', 'Atriles p/director aluminio negros', 'Bodega', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-412', 1, 'Atriles', 'ONSTAGE', 'Sin Modelo', 'Sin Serie', 'Atriles p/director aluminio negros', 'Bodega', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-413', 1, 'Atriles', 'ONSTAGE', 'Sin Modelo', 'Sin Serie', 'Atriles p/director aluminio negros', 'Bodega', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-414', 1, 'Atriles', 'ONSTAGE', 'Sin Modelo', 'Sin Serie', 'Atriles p/director aluminio negros', 'Bodega', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-415', 1, 'Atriles', 'ONSTAGE', 'Sin Modelo', 'Sin Serie', 'Atriles p/director aluminio negros', 'Bodega', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-416', 1, 'Atriles', 'ONSTAGE', 'Sin Modelo', 'Sin Serie', 'Atriles p/director aluminio negros', 'Bodega', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-417', 1, 'Atriles', 'ONSTAGE', 'Sin Modelo', 'Sin Serie', 'Atriles p/director aluminio negros', 'Bodega', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-418', 1, 'Atriles', 'ONSTAGE', 'Sin Modelo', 'Sin Serie', 'Atriles p/director aluminio negros', 'Bodega', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-419', 1, 'Atriles', 'ONSTAGE', 'Sin Modelo', 'Sin Serie', 'Atriles p/director aluminio negros', 'Bodega', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-420', 1, 'Atriles', 'ONSTAGE', 'Sin Modelo', 'Sin Serie', 'Atriles p/director aluminio negros', 'Bodega', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-421', 1, 'Atriles', 'ONSTAGE', 'Sin Modelo', 'Sin Serie', 'Atriles p/director aluminio negros', 'Bodega', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-422', 1, 'Atriles', 'ONSTAGE', 'Sin Modelo', 'Sin Serie', 'Atriles p/director aluminio negros', 'Bodega', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-423', 1, 'Atriles', 'ONSTAGE', 'Sin Modelo', 'Sin Serie', 'Atriles p/director aluminio negros', 'Bodega', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-424', 1, 'REGLETAS', 'FORZA', 'Sin Modelo', 'Sin Serie', 'Regleta grande de de 12 entradas', 'Laura', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-425', 1, 'REGLETAS', 'FORZA', 'Sin Modelo', 'Sin Serie', 'Regleta grande de de 12 entradas', 'Laura', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-426', 1, 'REGLETAS', 'FORZA', 'Sin Modelo', 'Sin Serie', 'Regleta grande de de 12 entradas', 'Laura', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-427', 1, 'Mezcladora de sonido( MIXER)', 'Sin Marca', 'Sin Modelo', 'RW 5735- 352645', 'De Violey', 'Danilo', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-428', 1, 'Sillas  Acolchadas', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Acolchadas. De color rojo', ' Aulas, cubículos Dirección', 'Sin Valor', 'Sin Responsable', 1, NULL),
('UNAACT-429', 1, 'Sillas uni-personal', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras uni-personal', 'Aulas #1 y #2', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-430', 1, ' micrófonos', 'Samson', 'CS', 'Sin Serie', 'Dos cabezas (cs1 para vocales cs2 para instrumento)Con estuche duro, color negro, marca samson', 'Bodega', 'Sin Valor', 'Laura Mesen', 1, NULL),
('UNAACT-431', 1, ' micrófonos', 'Samson', 'CS', 'Sin Serie', 'Dos cabezas (cs1 para vocales cs2 para instrumento)Con estuche duro, color negro, marca samson', 'Bodega', 'Sin Valor', 'Laura Mesen', 1, NULL),
('UNAACT-432', 1, ' micrófonos', 'Samson', 'CS', 'Sin Serie', 'Dos cabezas (cs1 para vocales cs2 para instrumento)Con estuche duro, color negro, marca samson', 'Bodega', 'Sin Valor', 'Laura Mesen', 1, NULL),
('UNAACT-433', 1, ' micrófonos', 'Samson', 'CS', 'Sin Serie', 'Dos cabezas (cs1 para vocales cs2 para instrumento)Con estuche duro, color negro, marca samson', 'Bodega', 'Sin Valor', 'Laura Mesen', 1, NULL),
('UNAACT-434', 1, ' micrófonos', 'SENNHEISER', 'e835', 'Sin Serie', 'Microfono para voz color gris', 'Bodega', 'Sin Valor', 'Laura Mesen', 1, NULL),
('UNAACT-435', 1, ' micrófonos', 'SENNHEISER', 'e835', 'Sin Serie', 'Microfono para voz color gris', 'Bodega', 'Sin Valor', 'Laura Mesen', 1, NULL),
('UNAACT-436', 1, ' micrófonos', 'SENNHEISER', 'e835', 'Sin Serie', 'Microfono para voz color gris', 'Bodega', 'Sin Valor', 'Laura Mesen', 1, NULL),
('UNAACT-437', 1, ' micrófonos', 'SENNHEISER', 'e835', 'Sin Serie', 'Microfono para voz color gris', 'Bodega', 'Sin Valor', 'Laura Mesen', 1, NULL),
('UNAACT-438', 1, ' micrófonos', 'SENNHEISER', 'e835', 'Sin Serie', 'Microfono para voz color gris', 'Bodega', 'Sin Valor', 'Laura Mesen', 1, NULL),
('UNAACT-439', 1, ' micrófonos', 'JTS', 'TX-9', 'Sin Serie', 'Microfono condensado', 'Bodega', 'Sin Valor', 'Laura Mesen', 1, NULL),
('UNAACT-440', 1, ' micrófonos', 'JTS', 'TX-10', 'Sin Serie', 'Microfono condensado', 'Bodega', 'Sin Valor', 'Laura Mesen', 1, NULL),
('UNAACT-441', 1, 'micrófonos', 'JTS', 'TK-600', 'Sin Serie', 'Micrófono para voz ', 'Bodega', 'Sin Valor', 'Laura Mesen', 1, NULL),
('UNAACT-442', 1, 'Sillas uni-personal', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras uni-personal', 'Aulas #1 y #2', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-443', 1, 'Sillas uni-personal', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras uni-personal', 'Aulas #1 y #2', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-444', 1, 'Sillas uni-personal', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras uni-personal', 'Aulas #1 y #2', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-445', 1, 'Sillas uni-personal', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras uni-personal', 'Aulas #1 y #2', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-446', 1, 'Sillas uni-personal', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras uni-personal', 'Aulas #1 y #2', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-447', 1, 'Sillas uni-personal', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras uni-personal', 'Aulas #1 y #2', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-448', 1, 'Sillas uni-personal', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras uni-personal', 'Aulas #1 y #2', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-449', 1, 'Sillas uni-personal', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras uni-personal', 'Aulas #1 y #2', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-450', 1, 'Sillas uni-personal', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras uni-personal', 'Aulas #1 y #2', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-451', 1, 'Sillas uni-personal', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras uni-personal', 'Aulas #1 y #2', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-452', 1, 'Sillas uni-personal', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras uni-personal', 'Aulas #1 y #2', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-453', 1, 'Sillas uni-personal', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras uni-personal', 'Aulas #1 y #2', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-454', 1, 'Sillas uni-personal', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras uni-personal', 'Aulas #1 y #2', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-455', 1, 'Sillas uni-personal', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras uni-personal', 'Aulas #1 y #2', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-456', 1, 'Sillas uni-personal', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras uni-personal', 'Aulas #1 y #2', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-457', 1, 'Sillas uni-personal', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras uni-personal', 'Aulas #1 y #2', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-458', 1, 'Sillas uni-personal', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras uni-personal', 'Aulas #1 y #2', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-459', 1, 'Sillas uni-personal', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras uni-personal', 'Aulas #1 y #2', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-460', 1, 'Sillas uni-personal', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras uni-personal', 'Aulas #1 y #2', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-461', 1, 'Sillas uni-personal', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras uni-personal', 'Aulas #1 y #2', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-462', 1, 'Sillas uni-personal', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras uni-personal', 'Aulas #1 y #2', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-463', 1, 'Sillas uni-personal', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras uni-personal', 'Aulas #1 y #2', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-464', 1, 'Sillas uni-personal', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras uni-personal', 'Aulas #1 y #2', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-465', 1, 'Sillas uni-personal', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras uni-personal', 'Aulas #1 y #2', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-466', 1, 'Sillas uni-personal', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras uni-personal', 'Aulas #1 y #2', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-467', 1, 'Sillas uni-personal', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras uni-personal', 'Aulas #1 y #2', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-468', 1, 'Sillas uni-personal', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras uni-personal', 'Aulas #1 y #2', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-469', 1, 'Sillas uni-personal', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras uni-personal', 'Aulas #1 y #2', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-470', 1, 'Sillas uni-personal', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color negras uni-personal', 'Aulas #1 y #2', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-471', 1, 'Mesa pupitre', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Mesa pupitre para estudiante color gris', 'Aulas #3 y #4', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-472', 1, 'Mesa pupitre', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Mesa pupitre para estudiante color gris', 'Aulas #3 y #4', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-473', 1, 'Mesa pupitre', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Mesa pupitre para estudiante color gris', 'Aulas #3 y #4', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-474', 1, 'Mesa pupitre', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Mesa pupitre para estudiante color gris', 'Aulas #3 y #4', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-475', 1, 'Mesa pupitre', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Mesa pupitre para estudiante color gris', 'Aulas #3 y #4', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-476', 1, 'Mesa pupitre', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Mesa pupitre para estudiante color gris', 'Aulas #3 y #4', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-477', 1, 'Mesa pupitre', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Mesa pupitre para estudiante color gris', 'Aulas #3 y #4', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-478', 1, 'Mesa pupitre', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Mesa pupitre para estudiante color gris', 'Aulas #3 y #4', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-479', 1, 'Mesa pupitre', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Mesa pupitre para estudiante color gris', 'Aulas #3 y #4', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-480', 1, 'Mesa pupitre', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Mesa pupitre para estudiante color gris', 'Aulas #3 y #4', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-481', 1, 'Mesa pupitre', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Mesa pupitre para estudiante color gris', 'Aulas #3 y #4', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-482', 1, 'Mesa pupitre', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Mesa pupitre para estudiante color gris', 'Aulas #3 y #4', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-483', 1, 'Mesa pupitre', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Mesa pupitre para estudiante color gris', 'Aulas #3 y #4', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-484', 1, 'Mesa pupitre', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Mesa pupitre para estudiante color gris', 'Aulas #3 y #4', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-485', 1, 'Mesa pupitre', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Mesa pupitre para estudiante color gris', 'Aulas #3 y #4', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-486', 1, 'Mesa pupitre', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Mesa pupitre para estudiante color gris', 'Aulas #3 y #4', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-487', 1, 'Mesa pupitre', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Mesa pupitre para estudiante color gris', 'Aulas #3 y #4', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-488', 1, 'Mesa pupitre', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Mesa pupitre para estudiante color gris', 'Aulas #3 y #4', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-489', 1, 'Mesa pupitre', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Mesa pupitre para estudiante color gris', 'Aulas #3 y #4', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-490', 1, 'Mesa pupitre', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Mesa pupitre para estudiante color gris', 'Aulas #3 y #4', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-491', 1, 'Mesa pupitre', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Mesa pupitre para estudiante color gris', 'Aulas #3 y #4', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-492', 1, 'Mesa pupitre', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Mesa pupitre para estudiante color gris', 'Aulas #3 y #4', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-493', 1, 'Mesa pupitre', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Mesa pupitre para estudiante color gris', 'Aulas #3 y #4', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-494', 1, 'Mesa pupitre', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Mesa pupitre para estudiante color gris', 'Aulas #3 y #4', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-495', 1, 'Mesa pupitre', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Mesa pupitre para estudiante color gris', 'Aulas #3 y #4', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-496', 1, 'Mesa pupitre', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Mesa pupitre para estudiante color gris', 'Aulas #3 y #4', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-497', 1, 'Mesa pupitre', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Mesa pupitre para estudiante color gris', 'Aulas #3 y #4', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-498', 1, 'Mesa pupitre', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Mesa pupitre para estudiante color gris', 'Aulas #3 y #4', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-499', 1, 'Mesa pupitre', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Mesa pupitre para estudiante color gris', 'Aulas #3 y #4', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-500', 1, 'Mesa pupitre', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Mesa pupitre para estudiante color gris', 'Aulas #3 y #4', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-501', 1, 'Amplificador p/ Bajo Eléctrico', 'Orange ', 'CRUSH BASS 25', '56651117', 'Color Naranja nuevo', 'JUAN DAVID VARGAS GAMBOA', 'Sin Valor', 'LAURA MESEN', 1, NULL),
('UNAACT-502', 1, 'Amplificador p/ guitarra', 'Orange', 'CRUSH 20', '264761117', 'Color Naranja nuevo', 'SARA ARGUEDAS', 'Sin Valor', 'LAURA MESEN', 1, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `boletassalida`
--

CREATE TABLE `boletassalida` (
  `idBoleta` int(11) NOT NULL COMMENT 'llave primaria de la tabla',
  `idUsuario` int(11) NOT NULL,
  `fechaEntrega` datetime DEFAULT NULL COMMENT 'Fecha en la que se entrego el activo al responsable',
  `fechaDevolucion` date NOT NULL COMMENT 'fecha planeada en la que se devolvera el activo solicitado ',
  `fechaRealDevolucion` datetime DEFAULT NULL COMMENT 'Fecha en la que realmente se devolvieron los activos\r\n',
  `nombreResponsable` varchar(100) NOT NULL COMMENT 'Nombre completo de la persona a la que se le presta el activo',
  `emailResponsable` varchar(100) NOT NULL COMMENT 'Correo electronico de la persona a la que se le prestan los activos',
  `estado` varchar(3) NOT NULL COMMENT 'estado de la boleta de salida, gen = generada, act = activa, dev = devuelta, atr = atrasada.',
  `observaciones` text DEFAULT NULL COMMENT 'Observaciones que se le pueden hacer a la boleta,y al estado de los activos al ser devueltos'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contratos`
--

CREATE TABLE `contratos` (
  `idContrato` int(11) NOT NULL COMMENT 'identificador del contrato',
  `cedulaEstudiante` varchar(15) DEFAULT NULL COMMENT 'Estudiante al que se le prestara el instrumento',
  `cedulaEncargado` varchar(15) DEFAULT NULL COMMENT 'Encargado del estudiante',
  `codInstrumento` varchar(20) DEFAULT NULL COMMENT 'Instrumento que sera prestado al estudiante',
  `PresidenteAsociacion` varchar(15) DEFAULT NULL COMMENT 'Nombre y cedula del presidente de la asociacion',
  `fechaInicio` date NOT NULL COMMENT 'Fecha de Inicio del contrato',
  `fechaFinalizacion` date NOT NULL COMMENT 'Fecha de finalizacion del contrato',
  `estado` varchar(1) NOT NULL COMMENT 'Estado del contrato, A = activo, T = terminado, R = Retrasado(Atrasado), C = cancelado (por fallos o daños al instrumento)',
  `observaciones` text DEFAULT NULL COMMENT 'espacio para observaciones agregadas al contrato, o para el estado del instrumento al ser devuelto',
  `cedulaFiador` varchar(15) DEFAULT NULL,
  `emailFiador` varchar(150) DEFAULT NULL,
  `nombreCompletoFiador` varchar(200) DEFAULT NULL,
  `telefonoFiador` varchar(16) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalleboleta`
--

CREATE TABLE `detalleboleta` (
  `pk_cod_boleta` int(11) NOT NULL,
  `pk_cod_activo` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `encargadosestudiantes`
--

CREATE TABLE `encargadosestudiantes` (
  `cedulaEstudiante` varchar(15) NOT NULL,
  `cedulaEncargado` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estados`
--

CREATE TABLE `estados` (
  `idEstado` int(11) NOT NULL COMMENT 'Llave primaria de la tabla estados',
  `estado` varchar(100) NOT NULL COMMENT 'El estado como tal, es decir, si en uso, dado de baja, en contrato, etc'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `estados`
--

INSERT INTO `estados` (`idEstado`, `estado`) VALUES
(1, 'Nuevo Ingreso'),
(2, 'En uso'),
(3, 'En bodega, buen estado'),
(4, 'En bodega, mal estado'),
(5, 'En contrato'),
(6, 'En boleta de salida'),
(7, 'En reparacion'),
(8, 'Dado de baja'),
(9, 'Libre');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `instituciones`
--

CREATE TABLE `instituciones` (
  `idInstitucion` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL COMMENT 'Nombre de la institucion',
  `estdInstitucion` char(20) NOT NULL COMMENT 'codigo estandar de la institucion para formar los codigos estandar, UNA, AEMS, SINEM, MPZ'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `instituciones`
--

INSERT INTO `instituciones` (`idInstitucion`, `nombre`, `estdInstitucion`) VALUES
(1, 'Universidad Nacional de Costa Rica', 'UNA'),
(2, 'Municipalidad de Perez Zeledon', 'MPZ'),
(3, 'Asociacion Escuela Musica Sinfonica', 'AEMS'),
(4, 'Sinem', 'SINEM');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `instrumentos`
--

CREATE TABLE `instrumentos` (
  `codInstrumento` varchar(20) NOT NULL COMMENT 'Llave primaria de la tabla',
  `codTipoInstrumento` varchar(5) NOT NULL COMMENT 'llave foranea al nombre de instrumento',
  `numeroInstrumento` int(11) NOT NULL COMMENT 'numero del instrumento, varia segun el tipo. ejem, clarinete 1',
  `nombre` varchar(100) NOT NULL,
  `marca` varchar(100) DEFAULT NULL,
  `modelo` varchar(100) DEFAULT NULL,
  `serie` varchar(100) DEFAULT NULL,
  `observaciones` varchar(500) DEFAULT 'Sin Observaciones',
  `valor` varchar(50) DEFAULT NULL,
  `ubicacion` varchar(100) DEFAULT NULL,
  `idInstitucion` int(11) NOT NULL,
  `responsable` varchar(150) DEFAULT NULL,
  `idEstado` int(11) NOT NULL,
  `fotoInstrumento` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `instrumentos`
--

INSERT INTO `instrumentos` (`codInstrumento`, `codTipoInstrumento`, `numeroInstrumento`, `nombre`, `marca`, `modelo`, `serie`, `observaciones`, `valor`, `ubicacion`, `idInstitucion`, `responsable`, `idEstado`, `fotoInstrumento`) VALUES
('1000128', 'CLV', 2, 'CLAVINOVA', 'Yamaha', 'CLP 240', '1102', 'Instrumento en uso. Buen estado.Se le pega una tecla.', ' ₡817,483.00 ', 'En bodega', 1, 'LAURA MESEN', 9, NULL),
('1000129', 'CLV', 1, 'Piano Clavinova', 'Yamaha', 'CLP 240', '1198', 'Instrumento en uso.Muy buen estado.No le suena la tecla D central.', ' ₡817,483.00 ', 'En bodega', 1, 'LAURA MESEN', 9, NULL),
('117239', 'FGT', 1, 'Fagot ', 'Selmer USA', '1432B', 'ROOO5385', 'Color negro, con llaves plateadas. Estuche negro de vinil con interior acolchado negro.  Tiene la llave “Fa” mala.', ' ₡2,888,812.00 ', 'En bodega', 1, 'LAURA MESEN', 9, NULL),
('2661816', 'PER', 31, 'PLATILLO', 'Sin Marca', 'Sin Modelo', 'JH06305152', 'Sin Oberservaciones', ' ₡150,000.00 ', 'Aula de Percusión', 4, 'LAURA MESEN', 9, NULL),
('2661817', 'PER', 32, 'PLATILLO', 'Sin Marca', 'Sin Modelo', 'JH06305157', 'Sin Oberservaciones', ' ₡150,000.00 ', 'BRYAN JOSE PIEDRA RODRIGUEZ ', 4, 'LAURA MESEN', 5, NULL),
('2661818', 'PER', 33, 'CAMPANA TUBULAR', 'Sin Marca', 'Sin Modelo', 'Modelo  6105', 'Sin Oberservaciones', '', 'Aula de Percusión ', 4, 'LAURA MESEN', 9, NULL),
('2661819', 'CLL', 8, 'CELLO 1/2', 'OXFORD', 'OXF-14112', 'SINEM- 3CH025', 'Color rojo, en buen estado, estuche negro y buen estado. ', ' ₡357,600.00 ', 'BODEGA ', 4, 'LAURA MESEN', 9, NULL),
('2661820', 'CLL', 5, 'CELLO 1/2', 'OXFORD', 'OXF-14112', 'SINEM-3CH026', 'Color rojo, buen estad, estuche negro, sin arco', ' ₡357,600.00 ', 'BODEGA', 4, 'LAURA MESEN', 9, NULL),
('2661821', 'CLL', 9, 'CELLO 4/4', 'OXFORD', 'OXF-14431', 'SINEM-5CH053', 'Color rojo, en buen estado, estucho negro, con arco.', ' ₡596,000.00 ', 'GARITA PEREZ VALERY JIMENA', 4, 'LAURA MESEN', 9, NULL),
('32194', 'PER', 20, 'XILOFONOS DE 13 TECLAS Y  NOTE\'S MAP', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Sin Oberservaciones', ' ₡24,000.00 ', 'FABIANA CASCANTE ARIAS', 1, 'LAURA MESEN', 9, NULL),
('32195', 'PER', 21, 'XILOFONOS DE 13 TECLAS Y  NOTE\'S MAP', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Sin Oberservaciones', ' ₡24,000.00 ', 'ANA VICTORIA FERNANDES VILLALOVOS', 1, 'LAURA MESEN', 9, NULL),
('32196', 'PER', 22, 'XILOFONOS DE 13 TECLAS Y  NOTE\'S MAP', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Sin Oberservaciones', ' ₡24,000.00 ', 'SEBASTIAN Y KEILA LEIVA BARRANTES', 1, 'LAURA MESEN', 9, NULL),
('32197', 'PER', 23, 'XILOFONOS DE 13 TECLAS Y  NOTE\'S MAP', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Sin Oberservaciones', ' ₡24,000.00 ', 'SEBASTIAN CASTRO VALVERDE', 1, 'LAURA MESEN', 9, NULL),
('32198', 'PER', 24, 'XILOFONOS DE 13 TECLAS Y  NOTE\'S MAP', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Sin Oberservaciones', ' ₡24,000.00 ', 'MARIANGEL MATA VASQUEZ', 1, 'LAURA MESEN', 9, NULL),
('32199', 'PER', 25, 'XILOFONOS DE 13 TECLAS Y  NOTE\'S MAP', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Sin Oberservaciones', ' ₡24,000.00 ', 'ANA MARIA CORRALES CORRALES', 1, 'LAURA MESEN', 9, NULL),
('32200', 'PER', 26, 'XILOFONOS DE 13 TECLAS Y  NOTE\'S MAP', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Sin Oberservaciones', ' ₡24,000.00 ', 'SEBASTIAN Y KEILA LEIVA BARRANTES', 1, 'LAURA MESEN', 9, NULL),
('32201', 'PER', 27, 'XILOFONOS DE 13 TECLAS Y  NOTE\'S MAP', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Sin Oberservaciones', ' ₡24,000.00 ', 'JORGE QUESADA HERNANDEZ', 1, 'LAURA MESEN', 9, NULL),
('32202', 'PER', 28, 'XILOFONOS DE 13 TECLAS Y  NOTE\'S MAP', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Sin Oberservaciones', ' ₡24,000.00 ', 'EMMA ISABELA CORDERO OBANDO', 1, 'LAURA MESEN', 9, NULL),
('32203', 'PER', 29, 'XILOFONOS DE 13 TECLAS Y  NOTE\'S MAP', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Sin Oberservaciones', ' ₡24,000.00 ', 'BODEGA ', 1, 'LAURA MESEN', 9, NULL),
('534000573', 'PER', 38, 'Xilófono', 'YAMAHA', 'YX-35G', '11006', 'Sin Oberservaciones', ' ₡1,072,800.00 ', 'TAMARA GUIDO', 4, 'LAURA MESEN', 9, NULL),
('534004664', 'PER', 34, 'REDOBLANTE ', 'Sin Marca', 'Sin Modelo', '859408', 'Sin Oberservaciones', ' ₡60,000.00 ', 'Aula de Percusión', 4, 'LAURA MESEN', 9, NULL),
('534004665', 'PER', 35, 'TIMBAL 26”', 'Sin Marca', 'Sin Modelo', '41123', 'Sin Oberservaciones', ' ₡715,200.00 ', 'GABRIEL CHACON', 4, 'LAURA MESEN', 9, NULL),
('534004666', 'PER', 36, 'TIMBAL 29”', 'Sin Marca', 'Sin Modelo', '40986', 'Sin Oberservaciones', ' ₡715,200.00 ', 'GABRIEL CHACON', 4, 'LAURA MESEN', 9, NULL),
('534004668', 'VLN', 37, 'Violín ¼ ', 'Oxford', 'Sin Modelo', '33221', 'Estuche azul por afuera y negro por adentro.Con arco.', ' ₡56,000.00 ', 'LEIVA ACUÑA SCARLTH BEATRIZ', 4, 'LAURA MESEN', 9, NULL),
('534004669', 'VLN', 28, 'Violín ¼ ', 'Oxford', 'Sin Modelo', '33179', 'Estuche azul por afuera y negro por adentro.Con arco.', ' ₡56,000.00 ', 'MORA MURCIA NOELIA', 4, 'LAURA MESEN', 9, NULL),
('534004670', 'VLN', 45, 'Violín ¼  ', 'Oxford', 'Sin Modelo', '33272', 'Estuche azul por afuera y negro por adentro.Con arco.Sin cuerda MI', ' ₡56,000.00 ', 'Daniela Leiva ', 4, 'LAURA MESEN', 9, NULL),
('534004671', 'VLN', 3, 'Violín ¼', 'Oxford', 'Sin Modelo', '33262', 'Estuche de color azul por afuera y negro por adentro.sin arco.', ' ₡56,000.00 ', 'FERNANDEZ VILLALOBOS ANA VICTORIA', 4, 'LAURA MESEN', 9, NULL),
('534004672', 'VLN', 43, 'Violín ¼  ', 'Oxford', 'Sin Modelo', '33263', 'Estuche azul por afuera y negro por adentro.Sin arco.Le falta cuerda RE -Tiene un golpe en uno de los bordes de las tapas', ' ₡56,000.00 ', 'PADILLA PEREZ AMERICA', 4, 'LAURA MESEN', 9, NULL),
('534004673', 'VLN', 36, 'Violín ¼  ', 'Oxford', 'Sin Modelo', '33266', 'Estuche azul por afuera y negro por adentro.Con arco.', ' ₡56,000.00 ', 'Bodega ', 4, 'LAURA MESEN', 9, NULL),
('534004674', 'VLN', 35, 'Violín ¼  ', 'Oxford', 'Sin Modelo', '332652', 'Estuche azul por afuera y negro por adentro.Sin arco.', ' ₡56,000.00 ', 'Bodega', 4, 'LAURA MESEN', 9, NULL),
('534004675', 'VLN', 2, 'Violín ¼', 'Oxford', '2', '33264', 'Estuche de color azul por afuera y negro por adentro.Sin arco.', ' ₡56,000.00 ', 'CASCANTE ARIAS FABIANA', 4, 'LAURA MESEN', 9, NULL),
('534004676', 'VLN', 5, 'Violín ¾', 'Oxford', 'Sin Modelo', '39812', 'Estuche de color azul por afuera y negro por adentro.Con arco.', ' ₡56,000.00 ', 'Danna Naranjo ', 4, 'LAURA MESEN', 9, NULL),
('534004677', 'VLN', 21, 'Violín ¾', 'Oxford', 'Sin Modelo', '29875', 'Estuche azul por afuera y negro por adentro.Con arco.', ' ₡56,000.00 ', 'BODEGA', 4, 'LAURA MESEN', 9, NULL),
('534004679', 'VLN', 20, 'Violín ¾', 'Oxford', 'Sin Modelo', '31644', 'Estuche azul por afuera y negro por adentro.Con arco.', ' ₡56,000.00 ', 'PRISCILLA ABARCA MONTEALEGRE ', 4, 'LAURA MESEN', 9, NULL),
('534004680', 'VLN', 24, 'Violín 3/4  ', 'Oxford', 'Sin Modelo', '31641', 'Estuche azul por afuera y negro por adentro.Con arco.', ' ₡56,000.00 ', 'Bodega', 4, 'LAURA MESEN', 9, NULL),
('534004681', 'VLN', 44, 'Violín ¾', 'Oxford', 'Sin Modelo', '31640', 'Estuche azul por afuera y negro por adentro.Sin arco.', ' ₡56,000.00 ', 'Bodega  ', 4, 'LAURA MESEN', 9, NULL),
('534004682', 'VLN', 40, 'Violín 3/4  ', 'Oxford', 'Sin Modelo', '31642', 'Estuche azul por afuera y negro por adentro.Sin arco.', ' ₡56,000.00 ', 'BODEGA', 4, 'LAURA MESEN', 9, NULL),
('534004683', 'VLN', 10, 'Violín ¾,', 'Oxford', 'Sin Modelo', '31639', 'Estuche de color azul por afuera y negro por adentro.Con arco.', ' ₡56,000.00 ', 'BODEGA', 4, 'LAURA MESEN', 9, NULL),
('534004684', 'VLN', 11, 'Violín 4/4', 'Oxford', 'Sin Modelo', '31579', 'Estuche de color azul por afuera y negro por adentro.Con arco.', ' ₡56,000.00 ', 'BODEGA', 4, 'LAURA MESEN', 9, NULL),
('534004685', 'VLN', 19, 'Violín 4/4', 'Oxford', 'Sin Modelo', '31582', 'Estuche azul por afuera y negro por adentro.Sin arco.Le falta una cuerda.', ' ₡56,000.00 ', 'ARIANA ROJAS SANTAMARIA', 4, 'LAURA MESEN', 9, NULL),
('534004686', 'VLN', 17, 'Violín 4/4', 'Oxford', 'Sin Modelo', '31486', 'Estuche azul por afuera y negro por adentro.Sin arco.', ' ₡56,000.00 ', 'Bodega', 4, 'LAURA MESEN', 9, NULL),
('534004687', 'VLN', 14, 'Violín 4/4', 'Oxford', 'Sin Modelo', '31487', 'Estuche azul por afuera y negro por adentro.Con arco.', ' ₡56,000.00 ', 'Fatima Elizondo ', 4, 'LAURA MESEN', 9, NULL),
('534004688', 'VLN', 13, 'Violín 4/4', 'Oxford', 'Sin Modelo', '31485', 'Estuche azul por fuera y negro por adentro.Con arco.', ' ₡56,000.00 ', 'BODEGA', 4, 'LAURA MESEN', 9, NULL),
('534004689', 'VLN', 12, 'Violín 4/4', 'Oxford', 'Sin Modelo', '31483', 'Estuche de color azul por afuera y negro poradentro.Con arco.', ' ₡56,000.00 ', 'ANGIE BONILLA RAMIREZ', 4, 'LAURA MESEN', 9, NULL),
('534004690', 'VLN', 18, 'Violín 4/4', 'Oxford', 'Sin Modelo', '31484', 'Estuche azul por afuera y negro por adentro.Con arco.', ' ₡56,000.00 ', 'BODEGA', 4, 'LAURA MESEN', 9, NULL),
('534004691', 'VLN', 15, 'Violín 4/4', 'Oxford', 'Sin Modelo', 'ss', 'Estuche azul por afuera y negro por adentro.Con arco.', ' ₡56,000.00 ', 'Bodega', 4, 'LAURA MESEN', 9, NULL),
('534004692', 'VLA', 13, 'Viola 16”', 'Oxford', 'Sin Modelo', '25941', 'Estuche de color azul por afuera y negro por adentro.Último contrato Natanael Fonseca', ' ₡82,000.00 ', 'BODEGA', 4, 'LAURA MESEN', 9, NULL),
('534004693', 'VLA', 12, 'Viola 16”', 'Oxford', 'Sin Modelo', '25940', 'Estuche negro por afuera y azul por adentro.', ' ₡82,000.00 ', 'Bodega', 4, 'LAURA MESEN', 9, NULL),
('534004694', 'VLA', 4, 'Viola 15”', 'Oxford', 'Sin Modelo', '28689', 'Estuche de color negro por afuera y azul por adentro.', ' ₡82,000.00 ', 'Bodega', 4, 'LAURA MESEN', 9, NULL),
('534004695', 'VLA', 3, 'Viola 16”', 'Oxford', 'Sin Modelo', '28693', 'Estuche de color azul por afuera y negro por adentro.Con dos arcos.', ' ₡82,000.00 ', 'BODEGA', 4, 'LAURA MESEN', 9, NULL),
('534004696', 'VLA', 1, 'VIOLA 15\"', 'Oxford', 'Sin Modelo', '28690', 'Estuche de color negro por afuera y por dentro.Con arco.', ' ₡82,000.00 ', 'BODEGA', 4, 'LAURA MESEN', 9, NULL),
('534004698', 'VLA', 10, 'Viola 15”', 'Oxford', 'Sin Modelo', '28703', 'Estuche de color azul por afuera y negro por adentro.', ' ₡82,000.00 ', 'BODEGA', 4, 'LAURA MESEN', 9, NULL),
('534004699', 'VLA', 5, 'Viola 15”', 'Oxford', 'Sin Modelo', '28702', 'Estuche de color negro por afuera y azul por adentro. Sin puente.', ' ₡82,000.00 ', 'Bodega', 4, 'LAURA MESEN', 9, NULL),
('534004700', 'VLA', 9, 'Viola 15”', 'Oxford', 'Sin Modelo', '28704', 'Estuche de color negro por afuera y azul por adentro.Le falta la cuerda la y el arco.', ' ₡82,000.00 ', 'Bodega ', 4, 'LAURA MESEN', 9, NULL),
('534004701', 'VLA', 6, 'Viola 15”', 'Oxford', 'Sin Modelo', '28706', 'Estuche de color negro por afuera y azul por adentro.', ' ₡82,000.00 ', 'Bodega', 4, 'LAURA MESEN', 9, NULL),
('534004702', 'VLA', 8, 'Viola 15”', 'Oxford', 'Sin Modelo', '28705', 'Estuche de color negro por afuera y azul por adentro.', ' ₡82,000.00 ', 'Bodega ', 4, 'LAURA MESEN', 9, NULL),
('534004703', 'PER', 37, 'REDOBLANTE', 'Sin Marca', 'Sin Modelo', '859397', 'Sin Oberservaciones', ' ₡60,000.00 ', 'Aula de Percusión', 4, 'LAURA MESEN', 9, NULL),
('534004704', 'CLT', 20, 'Clarinete bajo', 'Hunter', '6407', 'HTBC 700230', 'En buen estado.Con estuche.', ' ₡1,045,254.00 ', 'Salón ', 4, 'LAURA MESEN', 9, NULL),
('534005677', 'VLA', 7, 'Viola 15”', 'Oxford', 'Sin Modelo', '28701', 'Estuche de color negro por afuera y azul por adentro.Arco quebrado.', ' ₡82,000.00 ', 'BODEGA', 4, 'LAURA MESEN', 9, NULL),
('914001351', 'VLN', 51, 'Violín ¾', 'No marca', 'CH2VOO134', 'Sin Serie', 'Nuevo es el reemplazo del #22', ' ₡66,000.00 ', 'VEGA MORA DANIELA', 4, 'LAURA MESEN', 9, NULL),
('914001923', 'VLN', 30, 'violin 1/4', 'Valencia', 'V160', 'sin serie', 'Reemplazo #30estuche negro por fuera y gris por dentro', ' ₡56,000.00 ', 'Bodega', 4, 'LAURA MESEN', 9, NULL),
('92372', 'GTR', 17, 'Guitarra', 'Fabio Prado', 'Sin Modelo', '60954', 'Sin Oberservaciones', ' ₡1.00 ', 'Bodega', 1, 'LAURA MESEN', 9, NULL),
('92373', 'CLL', 6, 'CELLO1/2', 'BELLAFINA', '510', '98809', 'Color rojo, 2007 , buen estado, estuche verde musgo en buen estado, con arco en buen estado', ' ₡208,250.00 ', 'Salma Quesada Rojas', 1, 'LAURA MESEN', 9, NULL),
('92375', 'CLT', 17, 'Clarinete', 'Selmer USA', '1400', '1598063', 'Clarinete con boquilla y abrazadera. Estuche duro, negro por fuera, acolchado y azul por dentro.', ' ₡1.00 ', 'TALLER', 1, 'LAURA MESEN', 9, NULL),
('92376', 'TRN', 2, 'Trombón', 'Getzen', '400 Series', '451-3239', 'Instrumento en buen estado, de color dorado simple. Boquilla vinvent n°12C, Vincent Bach. Algunos golpes en la parte interior de la vara y otro golpe en la campana. ', ' ₡692,400.00 ', 'Bodega', 1, 'Laura Mesén', 9, NULL),
('92377', 'PER', 3, 'XILOFONO', 'YAMAHA', 'YX-135', '1617', '13 TECLAS EN MAL ESTADO', ' ₡1,080,000.00 ', 'EIDA MONGE FONSECA', 1, 'LAURA MESEN', 9, NULL),
('92378', 'PER', 6, 'BATERIA LUDWING VERDE', 'LUDWING', 'ROCKER', '9607885', 'EN ESTADO REGULAR. BOMBO, TOM GRANDE, TOM MEDIANO, TOM PEQUEÑO Y REDOBLANTE ', '', 'AULA DE PERCUSION', 1, 'LAURA MESEN', 9, NULL),
('92379', 'PER', 4, 'PLATILLO CHOQUE 18\"', 'ZILDJIAN', 'Sin Modelo', 'Sin Serie', 'Sin Oberservaciones', '', 'AULA DE PERCUSION', 1, 'LAURA MESEN', 9, NULL),
('92429', 'PER', 12, 'BONGOE', 'LP', 'Sin Modelo', 'Sin Serie', 'NUEVOS DE BIOLEY', ' ₡108,000.00 ', 'ISAAC QUESADA BARTES', 1, 'LAURA MESEN', 9, NULL),
('92431', 'PER', 16, 'MARACAS LP', 'LP', 'LP394', 'Sin Serie', 'Estan en una caja de Herramientas de la UNA', '', 'Caja          UNAACT-366   PERCU MENOR', 1, 'LAURA MESEN', 9, NULL),
('92434', 'CLV', 4, 'Piano Clavinova', 'Yamaha', 'CLP 120', '662610', 'Instrumento en uso. Muy buen estado.', ' ₡600,000.00 ', 'Susana Delgado Varela', 1, 'LAURA MESEN', 9, NULL),
('92480', 'TRA', 16, 'Trompeta92480', 'Yamaha', 'YTR 43356', '772167', 'Instrumento en buenas condiciones. Estuche marca Yamaha de color café por fuera y negro por dentro.', ' ₡720,000.00 ', 'ALEXIA FLORES', 1, 'LAURA MESEN', 9, NULL),
('92483', 'TRN', 6, 'Trombón', 'Getzen', 'Sin Modelo', '400', 'Instrumento en buen estado dorado. Estuche de color azul, en regular estado.', ' ₡692,400.00 ', 'Bodega ', 1, 'Laura Mesén', 9, NULL),
('92484', 'TRN', 1, 'Trombón', 'Getzen', 'Eterna II 72', '725-2561', 'Instrumento en buen estado. De color dorado, con llave.Estuche de color azul, en buen estado de tela, por dentro negro, marca Getzen.Con boquilla N° 6.5 y con serie en vara L84.Muy rayado en vara y campana', ' ₡692,400.00 ', 'Alex Santiago FLORES ARAYA ', 1, 'Laura Mesén', 9, NULL),
('AEMSBJO-001', 'BJO', 3, 'Bajo Eléctrico', 'Ibanez', 'GSRM201P-02', 'I150516054', 'Pequeño, 4 cuerdas, negro', ' ₡200,000.00 ', 'SARA ARGUEDAS', 3, 'Sin Responsable', 9, NULL),
('AEMSBJO-002', 'BJO', 4, 'Bajo Eléctrico', 'Squier Fender', 'Broco Bass', 's/nICS13206127', '4 cuerdas, mediano, negro y blanco', ' ₡200,000.00 ', 'Cubículo #4', 3, 'Sin Responsable', 9, NULL),
('AEMSBJO-003', 'BJO', 5, 'Bajo Eléctrico', 'Ibanez', 'GSRM201P-02', 'I150701390', '4 cuerdas, Pequeño, negro', ' ₡200,000.00 ', ' Danilo Castro', 3, 'Sin Responsable', 9, NULL),
('AEMSBJO-004', 'BJO', 6, 'Bajo Eléctrico', 'Ibanez', 'No tiene', 'GS120904407', '5 cuerdas, rojo', ' ₡200,000.00 ', 'RAJEEHA ABUAWAD', 3, 'Sin Responsable', 9, NULL),
('AEMSBJO-005', 'BJO', 7, 'Cuatro', 'Guzmán y Álvarez', 'Sin Modelo', 'Sin Serie', 'Bajo para niño, color madera, cuatro cuerdas.', '', 'DANILO CASTRO', 3, 'Sin Responsable', 9, NULL),
('AEMSBJO-006', 'BJO', 8, 'Bajo Electrico', 'IBANEZ', 'N427', 'A06073757', 'COLOR NEGRO 5 CUERDAS', ' ₡200,000.00 ', 'CUBÍCULO #4', 3, 'Sin Responsable', 9, NULL),
('AEMSCLL-001', 'CLL', 1, 'CELLO 3/4', 'BELLAFINA', '510', '98809', 'Color rojo, en buen estado, estuche café y por dentro acolchado buen estado, sin arco. ', ' ₡595,000.00 ', 'Jorge Sánchez Marin ', 3, 'ANA HILDA MARIN FERNANDEZ', 9, NULL),
('AEMSCLL-002', 'CLL', 2, 'CELLO 4/4', 'CREMONA', 'Sin Modelo', 'SE-175101381', 'Color rojo, en exelente estado, estuche negro buen estado, con arco. ', ' ₡595,000.00 ', 'GONZALES ACUÑA CAMILA', 3, 'Sin Responsable', 9, NULL),
('AEMSCLL-003', 'CLL', 3, 'CELLO 4/4', 'CREMONA', 'SE-175', '101-389', 'Color rojo, en buen estado, solo con algunos rapones, estucho negro en buen estado, sintetico. Con arco en buen estado.', ' ₡476,000.00 ', 'Maria Paula Leiva', 3, 'MARIBEL ELIZONDO ESPINOZA', 9, NULL),
('AEMSCLL-004', 'CLL', 4, 'CELLO 4/4', 'PRAGA', 'PPC244', 'Sin Serie', 'Color rojo, en buen estado, estuche negro y buen estado, con arco. ', ' ₡357,000.00 ', 'CALEB Garro ', 3, 'EIDANIA ARIAS LOPEZ', 9, NULL),
('AEMSCLL-005', 'CLL', 10, 'CELLO 4/4', 'HOFFER', 'Sin Modelo', 'Sin Serie', 'Color rojo opaco, en buen estado, estuche negro por fuera y verde por dentro, sin arco  ', ' ₡595,000.00 ', 'Josue Asaf Garro Arias', 3, 'EIDANIA ARIAS LOPEZ', 9, NULL),
('AEMSCLT-001', 'CLT', 1, 'Clarinete', 'Jupiter', 'JCL 631', 'L73816', 'En buen estado, con boquilla, abrazadera, articulos de limpieza y estuche duro, color negro por afuera y vino por dentro (Donado por Lynbrook)', ' ₡357,000.00 ', 'TALLER', 3, 'Sin Responsable', 9, NULL),
('AEMSCLT-002', 'CLT', 2, 'Clarinete', 'Buffet', 'E11', '512945', 'De madera, con boquilla, abrazadera y estuche duro, color negro (Donado por Lynbrook)', ' ₡595,000.00 ', 'CAROLINA FALLAS MORA ', 3, 'PATRICIA MORA ROBLES', 9, NULL),
('AEMSCLT-003', 'CLT', 3, 'Clarinete', 'Yamaha', 'YCL 26 II', '6981A', 'En mal estado, con boquilla, abrazadera y estuche café acolchado por adentro', ' ₡535,500.00 ', 'EFREN FLORS AGUILAR', 3, 'EFREN FLORES QUESADA', 9, NULL),
('AEMSCLT-004', 'CLT', 4, 'Clarinete', 'Buffet', 'E11', '805539', 'De madera, en buen estado, con boquilla, cubre boquilla, abrazadera, paño de limpieza y llaves. Estuche de vinil café y por dentro vino. ', ' ₡595,000.00 ', 'BODEGA', 3, 'LIGIA BARRANTES SIBAJA', 9, NULL),
('AEMSCLT-005', 'CLT', 5, 'Clarinete Profesional', 'Yamaha', 'YCL 650', '119817', 'Clarinete profesional,con boquilla y cubre boquillas, en buen estado. Estuche ligeramente dañado, color negro por fuera y vino por dentro', ' ₡1,130,500.00 ', 'BODEGA', 3, 'Sin Responsable', 9, NULL),
('AEMSCLT-006', 'CLT', 6, 'Clarinete', 'Jupiter', 'JCL 631', 'L73759', 'En buen estado, con boquilla, con tapa boquilla, correa, abrazadera, articulos de limpieza y estuche plastico duro, color negro marca Buffet (Donado por Lynbrook)', ' ₡357,000.00 ', 'BODEGA', 3, 'Sin Responsable', 9, NULL),
('AEMSCLT-007', 'CLT', 7, 'Clarinete', 'Artley', 'Sin Modelo', '3927002', 'De madera, con boquilla, abrazadera y estuche plastico, color negro por afuera y acolchado azul por dentro, la agarradera del estuche esta en mal estado', ' ₡178,500.00 ', 'BODEGA', 3, 'Sin Responsable', 9, NULL),
('AEMSCLT-008', 'CLT', 8, 'Clarinete', 'Yamaha', 'Sin Modelo', '63462', 'En regular estado, estuche duro envuelto en tela, acolchado por adentro y de color negro. Sin marca. ', '', 'Bodega', 3, 'Sin Responsable', 9, NULL),
('AEMSCLT-009', 'CLT', 9, 'Clarinete', 'Bundy', 'Sin Modelo', '1001545', 'Buen estado. Sólo la aguja de la llave 3 está quebrada, con boquilla, abrazadera, estuche duro, de color gris y negro en regular estado. Sin marca. Dos llaves atoradas.', ' ₡59,500.00 ', 'Bodega. ', 3, 'Sin Responsable', 9, NULL),
('AEMSCLT-011', 'CLT', 11, 'Clarinete', 'Sin marca', 'Sin Modelo', '158137', 'Con abrazadera bonade, estuche duro, café por afuera y morado por dentro. Sin marca', ' ₡252,875.00 ', 'FRANCELA SARAY FRUTOS UMAÑA', 3, 'RUTH UMAÑA APU', 9, NULL),
('AEMSCLT-012', 'CLT', 13, 'Clarinete Plástico', 'Artley', 'Prelude', '23672', 'De plástico, sin boquín.Estuche duro, negro, por afuera y por adentro también, acolchado Necesita limpieza de llaves, madera un poco sucia, corcho superior de la parte central superior en mal estado.Llave 1 pegada y fuga en llave 4', '', 'Bodega', 3, 'Sin Responsable', 9, NULL),
('AEMSCLT-013', 'CLT', 14, 'Clarinete', 'Kenosha WIS', '7214', 'C42880', 'Sin Oberservaciones', '', 'Bodega', 3, 'Sin Responsable', 9, NULL),
('AEMSCLT-014', 'CLT', 15, 'Clarinete', 'Selmer USA', 'Signet 100', '200604', 'Buen estado. Sólo no tiene corcho inferior.De madera.Con estuche duro, negro por fuera y acolchado y negro por adentro.', '', 'Bodega ', 3, 'Sin Responsable', 9, NULL),
('AEMSCLT-015', 'CLT', 16, 'Clarinete', 'Bundy', 'Sin Modelo', '965747', 'Con boquilla, abrazadera y cubre boquín.Estuche Bundy duro, de color verde por fuera y acolchado y de color negro por adentro.Necesita limpieza de llaves, corcho de ajuste y reparación de la llave 2.', '', 'Bodega', 3, 'Sin Responsable', 9, NULL),
('AEMSCLT-016', 'CLT', 18, 'Clarinete', 'Lazer', 'Sin Modelo', '9291', 'Sin boquilla.Estuche duro y de color negro por fuera, acolchado y de color negro por adentro', ' ₡59,500.00 ', 'Bodega ', 3, 'Sin Responsable', 9, NULL),
('AEMSCLT-017', 'CLT', 19, 'Clarinete Requinto de madera', 'Bundy', 'Sin Modelo', '4715', 'De madera.Sin abrazadera.Estuche duro y acolchado por adentro. Presenta fugas, necesita reparación.', '', 'Bodega', 3, 'Sin Responsable', 9, NULL),
('AEMSCLT-018', 'CLT', 21, 'Clarinete Bajo', 'Bundy', '2290', '2290', '-Con estuche duro de color negro.', ' ₡892,500.00 ', 'BODEGA', 3, 'Sin Responsable', 9, NULL),
('AEMSCLT-019', 'CLT', 22, 'Clarinete', 'Weimar', 'Sin Modelo', 'Sin Serie', 'No tiene corcho (Donado por Hillsdale)', ' ₡148,750.00 ', 'Bodega ', 3, 'Sin Responsable', 9, NULL),
('AEMSCLT-020', 'CLT', 23, 'Normandy', 'Sin Marca', 'Sin Modelo', '88182', '#¿NOMBRE?', ' ₡119,000.00 ', 'Bodega', 3, 'Sin Responsable', 9, NULL),
('AEMSCLV-001', 'CLV', 6, 'Clavinova', 'Kawai', 'CN21R', '6-000009', 'De madera.De color café.88 teclas con sensibilidad.Banqueta color café.Cable de corriente de color negro 120w.( donado X  doc. Rincón)', ' ₡892,500.00 ', 'VALERIA GRANADOS', 3, 'LAURA MESEN', 9, NULL),
('AEMSCRF-001', 'CRF', 1, 'Corno', 'Yamaha', 'YHR 567', '34648', 'Dorado.En muy buen estado.Estuche negro en muy buen estado', ' ₡2,677,500.00 ', 'Marin Carrilla Leonardo', 3, 'Sin Responsable', 9, NULL),
('AEMSCRF-002', 'CRF', 2, 'Corno', 'Conn', 'USMA', 'Sin serie', 'Plateado.En regular estado.Estuche negro en buen estado.Boquilla Yamaha 32C4 del Corno 1', ' ₡297,500.00 ', 'Bodega', 3, 'Sin Responsable', 9, NULL),
('AEMSCRF-003', 'CRF', 3, 'Corno', 'Yamaha', 'YHL 567', '34649', 'Dorado.En muy buen estado.', ' ₡2,677,500.00 ', 'Bodega', 3, 'Sin Responsable', 9, NULL),
('AEMSCRF-004', 'CRF', 4, 'Corno', 'Jupiter', '****', '****', 'Dorado.En buen estado.Estuche negro en regular estado.Con boquilla.', '', 'Bodega', 3, 'Sin Responsable', 9, NULL),
('AEMSCRI-001', 'CRI', 1, 'Corno Inglés', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Sin Oberservaciones', '', 'Bodega', 3, 'Sin Responsable', 9, NULL),
('AEMSCTB-001', 'CTB', 1, 'Contrabajo', 'OXFORD Cafry bass', 'Sin Modelo', 'Sin Serie', 'Contrabajo 4/4, buen estado, con arco, estuche color negro por afuera y acolchado vino por dentro', ' ₡700,000.00 ', 'GUSTAVO BLANCO COTO', 3, 'Sin Responsable', 9, NULL),
('AEMSCTB-002', 'CTB', 2, 'Contrabajo', 'OXFORD Cafry bass', 'Sin Modelo', 'Sin Serie', 'Contrabajo 4/4, buen estado, con arco, estuche color negro por afuera y acolchado vino por dentro', ' ₡700,000.00 ', 'RICHARD AGÜERO CHAVEZ', 3, 'VALERIE CHAVEZ VARGAS', 9, NULL),
('AEMSCTB-003', 'CTB', 3, 'Contrabajo', 'OXFORD Cafry bass', 'Sin Modelo', 'Sin Serie', 'Contrabajo 4/4, buen estado, con arco, estuche color negro por afuera y acolchado vino por dentro', ' ₡700,000.00 ', 'Bodega', 3, 'Sin Responsable', 9, NULL),
('AEMSCTB-004', 'CTB', 5, 'Contrabajo', 'Paganine', 'Sin Modelo', 'Sin Serie', 'Contrabajo ¼ , buen estado, estuche suave negro.', ' ₡400,000.00 ', 'Bodega', 3, 'Sin Responsable', 9, NULL),
('AEMSCTB-005', 'CTB', 6, 'Contrabajo', 'Paganine', 'Sin Modelo', 'Sin Serie', 'Contrabajo ¼, buen estado, estuche suave negro', ' ₡400,000.00 ', 'Bodega', 3, 'Sin Responsable', 9, NULL),
('AEMSEUF-001', 'EUF', 1, 'Euphonium', 'Euphonium Yamaha', 'YEP-321', '470738', 'Sin Oberservaciones', ' ₡1,487,500.00 ', 'BODEGA', 3, 'Sin Responsable', 9, NULL),
('AEMSFGT-001', 'FGT', 2, 'Fagot', 'Renad Fox', 'B7000', '34498', 'Color madera café.Estuche negro por afuera y azul por adentro.', ' ₡4,165,000.00 ', 'Monge Madrigal Nancy Mercedes ', 3, 'Sin Responsable', 9, NULL),
('AEMSFGT-002', 'FGT', 3, 'Fagot', 'Maestro', 'EMS1994', 'Sin Serie', 'Color café claro.De tubo largo con dos tudeles.Sin faja, con paño, y doble estuche .Primer estuche es marca Altieri negro por afuera y blanco por adentro. El segundo estuche es de color negro por afuera y morado por adentro. ', ' ₡1,190,000.00 ', 'TALLER', 3, 'Sin Responsable', 9, NULL),
('AEMSFTR-001', 'FTR', 1, 'Flauta', 'Yamaha', '211 N LL', '303029', 'En buen estado.Estuche de color negro por fuera y rojo por dentro. Necesita una limpieza.Manilla del estuche despegada.', ' ₡476,000.00 ', 'ASHLY MARIANA GRANADO ELIZONDO', 3, 'Sin Responsable', 9, NULL),
('AEMSFTR-002', 'FTR', 2, 'Flauta', 'Armstrong', '303', '6157640', 'En buen estado.Estuche color negro por fuera.Necesita limpieza.(Donada por Cornell)', ' ₡595,000.00 ', 'BODEGA', 3, 'Sin Responsable', 9, NULL),
('AEMSFTR-003', 'FTR', 3, 'Flauta', 'Selmer USA', 'Sin Modelo', '75336', 'En buen estado.Con baño de plata. Estuche plástico, negro por fuera y azul por dentro.Con palillo de limpieza.Necesita limpieza ', ' ₡357,000.00 ', 'BODEGA', 3, 'Sin Responsable', 9, NULL),
('AEMSFTR-004', 'FTR', 4, 'Flauta', 'Yamaha', '361', '305408', 'En buen estado.Con baño de plata. Estuche Yamaha, por fuera duro y de color negro. Con cabeza Yamaha de plata, serie 925.Buen estado.Solamente necesita limpieza de cuerpo y llaves.', ' ₡595,000.00 ', 'BODEGA', 3, 'Sin Responsable', 9, NULL),
('AEMSFTR-005', 'FTR', 5, 'Flauta', 'Selmer USA', 'Sin Modelo', '7498', 'Con baño de plata. Estuche de madera, por afuera de color negro, por dentro acolchado de color café.Buen estado', ' ₡178,500.00 ', 'BODEGA', 3, 'Sin Responsable', 9, NULL),
('AEMSFTR-007', 'FTR', 7, 'Flauta', 'Armstrong', '104', '2523117', 'Bañada en plata.Sin cabeza.Estuche por afuera de color negro y duro, por adentro azul y acolchado.Necesita limpieza.   ', ' ₡148,750.00 ', 'BODEGA', 3, 'Sin Responsable', 9, NULL),
('AEMSFTR-008', 'FTR', 9, 'Flauta', 'Bundy', 'Sin Modelo', '610201', 'Bañada en plata. Estuche de color negro y duro por afuera, acolchado y azul por adentro. Buen estado.Solo necesita una limpieza.Último contrato MF Pérez.', ' ₡208,250.00 ', 'ERICK LARA SANCHEZ', 3, 'Sin Responsable', 9, NULL),
('AEMSFTR-009', 'FTR', 10, 'Flauta', 'Buffet', 'BC6010', '767392', 'Bañada en plata. Estuche de color negro y duro por afuera, acolchado y azul por adentro. Buffet. Buen estado.(donada por Cornell)', ' ₡208,250.00 ', 'Priscila Bonilla', 3, 'WENDY RAMIREZ CARRANZA', 9, NULL),
('AEMSFTR-010', 'FTR', 11, 'Flauta', 'Artley', '18-0', '496457', 'Bañada en plata. Estuche por afuera duro y de color negro, por adentro. acolchado y de color morado.Regular estado.', ' ₡150,000.00 ', 'Bodega', 3, 'Sin Responsable', 9, NULL),
('AEMSFTR-011', 'FTR', 13, 'Flauta', 'Yamaha', '225 SII', '346831', 'En buen estado. Estuche marca Yamaha, de color negro por adentro y por afuera.', ' ₡297,500.00 ', 'BODEGA', 3, 'Sin Responsable', 9, NULL),
('AEMSFTR-012', 'FTR', 14, 'Flauta', 'Jupiter', 'CFS', 'K76334', 'De color plateada.En excelente estado. Estuche de color negro por afuera y color vino por adentro.(donada por Cornell)', ' ₡595,000.00 ', 'Bodega', 3, 'Sin Responsable', 9, NULL),
('AEMSFTR-013', 'FTR', 15, 'Flauta', 'Bundy Selmer', 'Sin Modelo', '183452', 'Buen estado.Con estuche negro por afuera y rojo por adentro.(donada por Cornell)', ' ₡208,250.00 ', 'FALLAS FONSECA KEMBLY', 3, 'Sin Responsable', 9, NULL),
('AEMSFTR-014', 'FTR', 18, 'Flauta', 'WT Armstrong', 'Cabeza 56', '104m0956', '(Donada por Hillsdale)', '', 'Taller', 3, 'Sin Responsable', 9, NULL),
('AEMSFTR-016', 'FTR', 22, 'Flauta', 'Bundy Selmer ', 'Sin Modelo', '329085', 'En buen estado. Estuche duro color verde oscuro por fuera y azul por dentro ', ' ₡297,500.00 ', 'VALVERDE ZUÑIGA EMMANUEL', 3, 'Sin Responsable', 9, NULL),
('AEMSGTR-001', 'GTR', 16, 'Guitarra Electrica', 'Behringer', '908', '901012395', 'Mal estado.Guitarra Eléctrica.De color blanco y negro.Sin estuche.', ' ₡150,000.00 ', 'Bodega', 3, 'Sin Responsable', 9, NULL),
('AEMSOBE-001', 'OBE', 1, 'Oboe', 'Buffer', 'Sin Modelo', '25917', 'En buen estado.Estuche color negro de vinil, marca buffet.El interior del estuche es acolchado, de color vino.EN MAL ESTADO, DAÑADO 3/09/2020', ' ₡1,666,000.00 ', 'Taller', 3, 'Sin Responsable', 9, NULL),
('AEMSOBE-002', 'OBE', 2, 'Oboe', 'Barrington', 'Sin Modelo', 'Sin Serie', 'De madera con baño de plata.Estuche de color negro por fuera y duro, y por adentro acolchado de color vino.', '', 'Taller', 3, 'Sin Responsable', 9, NULL),
('AEMSOBE-003', 'OBE', 3, 'Oboe', 'Marca Irreconocible', '118', '2485', 'En buen estado. Estuche duro, de color café por fuera y acolchado por dentro. (Abuelito).', ' ₡267,750.00 ', 'Taller', 3, 'Sin Responsable', 9, NULL),
('AEMSOBE-004', 'OBE', 4, 'Oboe', 'Hoffer', 'Sin Modelo', 'Sin Serie', 'De madera y llaves bañadas en níquel.Estuche duro y de color negro por fuera, por adentro es acolchado y de color vino.No cerradura.', ' ₡892,500.00 ', 'Taller', 3, 'Sin Responsable', 9, NULL),
('AEMSOBE-005', 'OBE', 5, 'Oboe', 'Theomarkaruf', '118', '102776', 'En regular estado.De madera con baño de plata. Estuche duro y café por afuera, por adentro es acolchado y de color azul', ' ₡892,500.00 ', 'Taller', 3, 'Sin Responsable', 9, NULL),
('AEMSOBE-006', 'OBE', 6, 'Oboe', 'Selmer', '123 F', 'B70606', 'De pasta.Estuche duro y negro por afuera y por adentro de color azul.', ' ₡892,500.00 ', 'Taller', 3, 'Sin Responsable', 9, NULL),
('AEMSOBE-007', 'OBE', 7, 'Oboe', 'Probens', 'YOB 441', '34053', 'En buen estado.Estuche negro porafuera y azul poradentro.Estuche Marca Yamaha. DAÑADO', ' ₡892,500.00 ', 'TALLER', 3, 'Sin Responsable', 9, NULL),
('AEMSORG-001', 'ORG', 8, 'ORGANETAS', 'Yamaha', 'PSR 550', 'TC 001', 'Teclado electrónico.Buen Estado. Con 5 8vas. Teclas sensibles.', ' ₡100,000.00 ', 'AHMED VEGA GAMBOA', 3, 'LAURA MESEN', 9, NULL),
('AEMSORG-002', 'ORG', 9, 'ORGANETAS', 'Yamaha', 'PSR 550', 'TC 002', 'Teclado electrónico.Buen estado.Con 5 8vas.Teclas sensibles.', ' ₡100,000.00 ', 'ALVARES MORA ISAAC MATIAS', 3, 'LAURA MESEN', 9, NULL),
('AEMSORG-003', 'ORG', 10, 'Organeta', 'Yamaha', 'PSR 170', '61728', '(Donación) Buen estado.', ' ₡100,000.00 ', 'TAMARA GUIDO', 3, 'LAURA MESEN', 9, NULL),
('AEMSPCL-001', 'PCL', 1, 'Piccolo', 'Yamaha ', 'YPC32', '40974', 'En buen estado.Llaves de metal, con baño de plata. Estuche negro con interior rojo. Necesita Overhaul(Donada por Cornell)', ' ₡297,500.00 ', 'Bodega ', 3, 'Sin Responsable', 9, NULL),
('AEMSPCL-002', 'PCL', 2, 'Piccolo ', 'Rejona by Bukart', 'Sin Modelo', '13221', 'De madera .Con llaves bañadas en plata. Estuche duro, de color negro.Con estuche exterior de tela.', ' ₡1,487,500.00 ', 'Bodega ', 3, 'Sin Responsable', 9, NULL),
('AEMSPER-001 ', 'PER', 41, 'MARIMBA', 'PREMIER', 'Sin Modelo', 'Sin Serie', 'Sin Oberservaciones', '', 'EMMANUEL BONILLA RAMIREZ', 3, 'Sin Responsable', 9, NULL),
('AEMSPER-002 ', 'PER', 42, 'VIBRÁFONO', 'Sin Marca', 'YV520', '102843', 'Sin Oberservaciones', '', 'JOSE DAVID FLORES', 3, 'Sin Responsable', 9, NULL),
('AEMSPER-003 ', 'PER', 43, 'XILOFONO', 'PREMIER', 'Sin Modelo', 'Sin Serie', 'Sin Oberservaciones', '', 'MAYTE RODRIGUEZ ', 3, 'Sin Responsable', 9, NULL),
('AEMSPER-004 ', 'PER', 44, 'BOMBO', 'LUDWIG', 'Sin Modelo', 'Sin Serie', 'EN BUEN ESTADO', '', 'Aula De Percusión', 3, 'Sin Responsable', 9, NULL),
('AEMSPER-005 ', 'PER', 45, 'GLOCKESPIEL (CAMPANIÑAS)', 'PREMIER', 'Sin Modelo', 'PER-018', 'Sin Oberservaciones', '', 'Aula de percucion', 3, 'Sin Responsable', 9, NULL),
('AEMSPER-006 ', 'PER', 46, 'GLOCKESPIEL (CAMPANIÑAS)', 'MUSSER', 'M656', 'W1316', 'Sin Oberservaciones', '', 'BODEGA ', 3, 'Sin Responsable', 9, NULL),
('AEMSPER-007 ', 'PER', 47, 'PLATILLO SUSP 20\"', 'SABIAN', 'Sin Modelo', 'Sin Serie', 'Sin Oberservaciones', '', 'Aula De Percusión', 3, 'Sin Responsable', 9, NULL),
('AEMSPER-008 ', 'PER', 48, 'PLATILLO CRASH', 'SABIAN', 'XS', 'Sin Serie', 'Sin Oberservaciones', '', 'Aula De Percusión', 3, 'Sin Responsable', 9, NULL),
('AEMSPER-009 ', 'PER', 49, 'BATERIA SONOR MADERA', 'SONOR', 'Sin Modelo', 'Sin Serie', 'REGULAR ESTADO. BOMBO, TOM GRANDE, MEDIANO Y PEQUEÑO Y REDOBLANTE', '', 'Aula De Percusión', 3, 'Sin Responsable', 9, NULL),
('AEMSPER-010 ', 'PER', 50, 'BATERIA DE ESTUDIO PEQUEÑA', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'DONADA POR OSCAR VALVERDE. BOMBO, TOM GRANDE, MEDIANO Y PEQUEÑO', '', 'Aula De Percusión', 3, 'Sin Responsable', 9, NULL),
('AEMSPER-011 ', 'PER', 51, 'PANDERETE', 'BLACK SWAMP PERCUSSION', 'Sin Modelo', 'Sin Serie', 'Sin Oberservaciones', '', 'Aula De Percusión', 3, 'Sin Responsable', 9, NULL),
('AEMSPER-012 ', 'PER', 52, 'WIND CHIMES', 'Sin Marca', 'Sin Modelo', 'EMS- PER 025', 'Sin Oberservaciones', '', 'Aula De Percusión', 3, 'Sin Responsable', 9, NULL),
('AEMSPER-013 ', 'PER', 53, 'TEMPLE BLOCKS', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Sin Oberservaciones', '', 'Aula De Percusión', 3, 'Sin Responsable', 9, NULL),
('AEMSPER-014 ', 'PER', 54, 'CASTAÑIELAS ', 'LP', 'Sin Modelo', 'Sin Serie', 'Sin Oberservaciones', '', 'Aula De Percusión', 3, 'Sin Responsable', 9, NULL),
('AEMSPER-015 ', 'PER', 55, 'CABAZA', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Esta en una caja celeste plastica ', '', ' CAJA       AEMS    Percusion Menor', 3, 'Sin Responsable', 9, NULL),
('AEMSPER-016 ', 'PER', 56, '3 CONGAS COLOR MADERA', 'LP MATADOR', 'Sin Modelo', 'Sin Serie', 'Sin Oberservaciones', '', 'TAMARA GUIDO', 3, 'Sin Responsable', 9, NULL),
('AEMSPER-017 ', 'PER', 57, 'REDOBLANTE ', 'PULSE', 'PICCOLO', 'Sin Serie', 'Sin Oberservaciones', '', 'AULA PERCUSIÓN', 3, 'Sin Responsable', 9, NULL),
('AEMSPER-018 ', 'PER', 58, 'GÜIRA METALICA', 'LP', 'Sin Modelo', 'Sin Serie', 'Sin Oberservaciones', '', 'AULA PERCUSIÓN', 3, 'Sin Responsable', 9, NULL),
('AEMSPER-019 ', 'PER', 79, 'Practicador ', 'Vic Firth', 'Sin Modelo', 'Sin Serie', 'Sin Oberservaciones', '', 'Aula de percusion', 3, 'Sin Responsable', 9, NULL),
('AEMSPER-020 ', 'PER', 80, 'Practicador ', 'Evans', 'Sin Modelo', 'Sin Serie', 'Sin Oberservaciones', '', 'Aula de percusion', 3, 'Sin Responsable', 9, NULL),
('AEMSPER-021 ', 'PER', 61, ' LATIGO', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Sin Oberservaciones', '', 'AULA DE PERCUSION', 3, 'Sin Responsable', 9, NULL),
('AEMSPER-023 ', 'PER', 64, 'PANDERETA AMARILLA', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Sin Oberservaciones', '', 'AULA DE PERCUSION', 3, 'Sin Responsable', 9, NULL),
('AEMSPER-024 ', 'PER', 65, 'CAMPANA DE SALSA', 'LP', 'Sin Modelo', 'Sin Serie', 'Sin Oberservaciones', '', 'AULA DE PERCUSION', 3, 'Sin Responsable', 9, NULL),
('AEMSPER-028 ', 'PER', 69, 'GUIRO', 'LP', 'LP243', 'Sin Serie', 'Sin Oberservaciones', ' ₡90,000.00 ', 'Aula de percusion', 3, 'Sin Responsable', 9, NULL),
('AEMSPER-030 ', 'PER', 71, 'WOODS BLOCKS ', 'LP', 'Sin Modelo', 'Sin Serie', 'Sin Oberservaciones', '', 'Aula de percusion ', 3, 'Sin Responsable', 9, NULL),
('AEMSPER-031 ', 'PER', 72, 'SET DE TIMBALES ', 'MATADOR', 'Sin Modelo', 'Sin Serie', 'SET DE TIMBALES COLOR PLATEADO', '', 'Aula de percusion ', 3, 'Sin Responsable', 9, NULL),
('AEMSPER-032 ', 'PER', 39, 'BONGÓE', 'MATADOR', 'Sin Modelo', 'Sin Serie', 'Sin Oberservaciones', '', 'Aula De Percusión', 3, 'Sin Responsable', 9, NULL),
('AEMSPER-033 ', 'PER', 40, 'CAJÓN ACÚSTICO', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Sin Oberservaciones', '', 'Aula De Percusión', 3, 'Sin Responsable', 9, NULL),
('AEMSPER-034', 'PER', 73, ' LATIGO', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Sin Oberservaciones', '', 'AULA DE PERCUSION', 3, 'Sin Responsable', 9, NULL),
('AEMSPIN-001', 'PIN', 5, 'Piano Portátil', 'Yamaha', 'A7DGX 620', 'CL 005', 'Buen Estado Color Gris', ' ₡350,000.00 ', 'PRISCILLA BONILLA RAMIREZ  ', 3, 'LAURA MESEN', 9, NULL),
('AEMSPIN-002', 'PIN', 6, 'Piano Acústico ', 'Yamaha', 'U1JPE', 'J126174431', 'Muy buen estado color negro', '', 'Cubículo #3', 3, 'LAURA MESEN', 9, NULL),
('AEMSPIN-003', 'PIN', 7, 'Piano Acústico ', 'Baldwin', 'Hamilton', 'No tiene', 'De madera, color café', '', 'Cubiculo #2', 3, 'LAURA MESEN', 9, NULL),
('AEMSPIN-004', 'PIN', 8, 'Piano Acústico ', 'Yahama', 'U1JPM', 'J6173012', 'Buen estado, color cafe', '', 'Cubículo #8', 3, 'LAURA MESEN', 9, NULL),
('AEMSSXF-001', 'SXF', 1, 'Saxofón Alto', 'YAMAHA', 'YAS 62', 'C93492', 'Dorado.En buen estado.El estuche es de color negro por dentro y por afuera con fajas, tapa boquín, paño de limpieza. Vaselina, correa y sin boquilla en buen estado.', ' ₡1,487,500.00 ', 'MARIANGEL VILLEGAS VARGAS', 3, 'LIGIA PICADO VARGAS ', 9, NULL),
('AEMSSXF-002', 'SXF', 2, 'Saxofón Alto', 'YAMAHA', 'YAS 62', 'C91281', 'Dorado.En buen estado.El estuche es de color negro por dentro y por afuera con fajas, tapa boquín, paño de limpieza. Vaselina, correa y sin boquilla en buen estado.', ' ₡1,487,500.00 ', 'ANDREW UMAÑA DELGADO', 3, 'Sin Responsable', 9, NULL),
('AEMSSXF-003', 'SXF', 3, 'Saxofón Alto', 'YAMAHA', 'YAS 25', '48967', 'Dorado.En perfecto estado.Estuche en perfecto estado, de color negro por afuera y rojo por adentro. Sin correa', ' ₡1,487,500.00 ', 'BODEGA', 3, 'Sin Responsable', 9, NULL),
('AEMSSXF-004', 'SXF', 4, 'Saxofón Alto', 'P. Mauriat', 'Custom class Big Band global series', '829410', 'Dorado.Excelente estado.Con tudel.Estuche color negro por adentro y gris por afuera, marca Mauriat .Con colgante', ' ₡101,150.00 ', 'BODEGA', 3, 'Sin Responsable', 9, NULL),
('AEMSSXF-005', 'SXF', 5, '', 'P.Mauriat', 'Custom class Big Band global series', '1026710', 'Dorado. Excelente estado. Con tudel.Estuche color negro por adentro y por afuera marca Mauriat Con colgante.', ' ₡101,150.00 ', 'Bodega', 3, 'Sin Responsable', 9, NULL),
('AEMSSXF-006', 'SXF', 6, 'Saxofón Alto', 'P. Mauriat', 'Custom class Big Band global series', '828310', 'Dorado.Excelente estado.  Con tudel.Estuche color negro por adentro y por afuera, marca Mauriat.Con colgante.', ' ₡101,150.00 ', 'Bodega', 3, 'Sin Responsable', 9, NULL),
('AEMSSXF-007', 'SXF', 7, 'Saxofón Alto', 'YAMAHA', 'YAS 23', '376538', 'Dorado.En buen estado.Sin boquilla, sin faja ni abrazadera.El estuche está en buen estado, es de color negro, de vinil, marca Yamaha. ', ' ₡714,000.00 ', 'Bodega', 3, 'Sin Responsable', 9, NULL),
('AEMSSXF-008', 'SXF', 8, 'Saxofón Alto', 'YAMAHA', 'YAS 23', '293280', 'Dorado.En buen estado.Sin boquilla, sin faja ni abrazadera.El estuche está en buen estado, es de color negro de vinil, marca Yamaha.', ' ₡357,000.00 ', 'BODEGA', 3, 'Sin Responsable', 9, NULL),
('AEMSSXF-009', 'SXF', 9, 'Saxofón Alto', 'YAMAHA', 'YAS 23', '296697', 'Dorado.En buen estado.Sin boquilla, sin faja ni abrazadera.El estuche  YAMAHA está en buen estado, es de color cafe, de vinil,  marca Yamaha.', ' ₡357,000.00 ', 'ABIGAIL BONILLA', 3, 'Sin Responsable', 9, NULL),
('AEMSSXF-010', 'SXF', 10, 'Saxofón Alto', 'YAMAHA', 'Sin Modelo', '121000', 'Dorado.Con llaves plateadas en buen estado.El estuche no tiene marca, es de color negro por adentro y por afuera, está en regular estado.No tiene boquilla, abrazadera ni correa.Tiene dos roturas grandes en las esquinas.', ' ₡714,000.00 ', 'Bodega', 3, 'Sin Responsable', 9, NULL),
('AEMSSXF-011', 'SXF', 11, 'Saxofón Alto', 'Vito', 'Sin Modelo', '554979', 'Dorado.En buen estado, con tudel.Estuche marca Leblanc, de color negro por adentro y por afuera.Sin boquilla ni abrazadera.', ' ₡892,500.00 ', 'BODEGA', 3, 'Sin Responsable', 9, NULL),
('AEMSSXF-012', 'SXF', 12, 'Saxofón Alto', 'IMAGE', 'Sin Modelo', '92308', 'Dorado.En buen estado, con tudel.Tiene una flor en la campana.EL estuche es negro (por afuera y por adentro) de fibra plástica, sin marca. ', ' ₡892,500.00 ', 'Bodega', 3, 'Sin Responsable', 9, NULL),
('AEMSSXF-013', 'SXF', 13, 'Saxofón Alto', 'Alpine', 'Sin Modelo', 'A00842', 'Dorado.En buen estado, con tudel.El estuche es negro (por afuera y por adentro) de fibra plástica, sin marca.', ' ₡357,000.00 ', 'BODEGA', 3, 'Sin Responsable', 9, NULL),
('AEMSSXF-014', 'SXF', 14, 'Saxofón Alto', 'Bundy', 'Sin Modelo', '773052', 'Dorado.En buen estado, con tudel.El estuche es de color negro por adentro y por afuera.Sin boquilla.', ' ₡178,500.00 ', 'Bodega ', 3, 'Sin Responsable', 9, NULL),
('AEMSSXF-015', 'SXF', 15, 'Saxofón Alto', 'Bundy', 'Sin Modelo', '1060611', 'Dorado.En buen estado, con tudel.El estuche color negro por adentro y por afuera.Sin boquilla. ', ' ₡178,500.00 ', 'Bodega', 3, 'Sin Responsable', 9, NULL),
('AEMSSXF-016', 'SXF', 17, 'Saxofón Soprano', 'P.Mauritat', 'Custom Class System 76', '660809', 'Con acabado Mate.Con dos tudeles, correa, boquilla y abrazadera.El estuche de color negro por adentro y afuera', ' ₡2,261,000.00 ', 'Bodega', 3, 'Sin Responsable', 9, NULL),
('AEMSSXF-017', 'SXF', 19, 'Saxofón Tenor', 'YAMAHA', 'YTS 25', '15887', 'Dorado.Con llaves plateadas.Con tudel, boquilla y abrazadera.En buen estado.Con estuche Yamaha, de color negro por adentro y por afuera.', ' ₡1,487,500.00 ', 'BODEGA', 3, 'Sin Responsable', 9, NULL),
('AEMSSXF-018', 'SXF', 21, 'Saxofón Tenor', 'VITO', 'Sin Modelo', '802060', 'Dorado.Con llaves plateadas.Con tudel, boquilla y abrazadera.En buen estado. Con estuche Vito, de color negro (por adentro y afuera)', ' ₡357,000.00 ', 'Bodega', 3, 'Sin Responsable', 9, NULL),
('AEMSSXF-019', 'SXF', 22, 'Saxofón Tenor', 'CLEVELAND', 'H.N.White', 'C 188791', 'Dorado.En buen estado.Con tudel, boquilla, tapa boquín, abrazadera y correa.Con estuche PRO TEC, de color negro por adentro y por afuera.', ' ₡238,000.00 ', 'Bodega', 3, 'Sin Responsable', 9, NULL),
('AEMSSXF-020', 'SXF', 23, 'Saxofón Tenor', 'KING CLEVELAND', '615 USA', '679574', 'Dorado.En regular estado.Con tudel.Con estuche negro por adentro y por afuera.Sin boquilla ni abrazadera.', ' ₡178,500.00 ', 'Bodega', 3, 'Sin Responsable', 9, NULL),
('AEMSSXF-021', 'SXF', 25, 'Saxofón Barítono', 'YAMAHA', 'YBS 52', '25482', 'Dorado.En buen estado.Con estuche Yamaha, de color rojo por adentro y negro por afuera. Con tudel, boquilla, abrazadera y arnés.', ' ₡2,380,000.00 ', 'BODEGA', 3, 'Sin Responsable', 9, NULL),
('AEMSSXF-022', 'SXF', 30, 'Saxofon Tenor ', 'Prelude', 'T8710', 'AD17911002', 'Saxofon Dorado, estuche negro de telan negro por dentro y por fuera (*EMS estaban en el salon)', '', 'Bodega', 3, 'LAURA MESEN', 9, NULL),
('AEMSSXF-023', 'SXF', 31, 'Saxofon Alto', 'H. Hoffer', 'HAS100', 'Sin Serie', 'Saxofon Dorado, en buen estadp estuche negro de telan negro por dentro y por fuera . Con Boquilla y Abrasadera (*EMS estaban en el salon)', '', 'Bodega', 3, 'LAURA MESEN', 9, NULL),
('AEMSTRA-001', 'TRA', 1, 'Trompeta', 'Conn', 'Director', 'S65012 1813', 'Dorado.En regular estado.Estuche café en buen estado.Con boquilla. Golpe en la bomba del primer pistón.Tiene boquilla 7C de Trompeta N° 5', ' ₡178,500.00 ', 'Yeudy Barrantes Ureña', 3, 'WILSON LEIVA MARTINEZ', 9, NULL),
('AEMSTRA-002', 'TRA', 2, 'Trompeta', 'Yamaha', 'YTR 2335', '415767', 'Dorado, corroído.En regular estado.Estuche morado en regular estado. Boquilla vincent Bach 5C.Banda principal aterrada', ' ₡238,000.00 ', 'JULIAN CALEB CHAVARRIA CECILIANO', 3, 'Sin Responsable', 9, NULL),
('AEMSTRA-003', 'TRA', 3, 'Trompeta', 'Bundy', 'Sin serie', '92007', 'Dorado corroído.En regular estado.Estuche gris en buen estado, marca bundy. -Con boquilla 7C.Tiene manchas y muchos rayones.Bombas aterrada', ' ₡119,000.00 ', 'Bodega', 3, 'Sin Responsable', 9, NULL),
('AEMSTRA-004', 'TRA', 4, 'Trompeta', 'Eastman', 'ETR 420', '908087', 'Dorado.En muy buen estado.Con kit de limpieza y correa.Estuche negro en buen estado.Boquilla Eastman 7C', ' ₡357,000.00 ', 'Bodega', 3, 'Sin Responsable', 9, NULL),
('AEMSTRA-005', 'TRA', 5, 'Trompeta', 'Eastman', 'Sin Modelo', '908105', 'Dorado.En mal estado.Estuche negro en buen estado.(donado por Lynbrook)', ' ₡297,500.00 ', 'Bodega', 3, 'Sin Responsable', 9, NULL),
('AEMSTRA-006', 'TRA', 6, 'Trompeta', 'Bach', 'TR300', 'C72315', 'Dorado, muy corroído.En buen estado. Estuche negro en buen estado.Con boquilla', ' ₡59,500.00 ', 'Bodega', 3, 'Sin Responsable', 9, NULL),
('AEMSTRA-007', 'TRA', 7, 'Trompeta', 'King', 'Cleveland', '145597', 'Dorado, corroído.En mal estado por golpe en camisa del primer pistón.Estuche negro en regular estado.Marca CONN', '', 'Bodega', 3, 'Sin Responsable', 9, NULL),
('AEMSTRA-008', 'TRA', 8, 'Trompeta', 'Yamaha', 'YTR 6345G', 'C20000', 'Dorado.En muy buen estado.Estuche color cafe.', ' ₡1,249,500.00 ', 'FABIAN SALAS', 3, 'Sin Responsable', 9, NULL),
('AEMSTRA-009', 'TRA', 10, 'Trompeta', 'Conn', '16B', 'GC722228', 'Dorado, un poco corroído.En buen estado. Estuche negro en regular estado CONN .', ' ₡59,500.00 ', 'Bodega', 3, 'Sin Responsable', 9, NULL),
('AEMSTRA-011', 'TRA', 14, 'Trompeta', 'Blessing', 'Sin Modelo', '575248', 'Dorada. En regular estado.Con boquilla 7C. Estuche negro por afuera y por adentro.  Estuche quebrado en la esquina.Marca KING (Donado por Lynbrook)', ' ₡59,500.00 ', 'BODEGA', 3, 'Sin Responsable', 9, NULL),
('AEMSTRA-012', 'TRA', 15, 'Trompeta', 'Sin marca', 'Sin Modelo', '166309', 'En buen estado.Con boquilla. Estuche en mal estado, de color negro por afuera y anaranjado por adentro.  La bomba de afinación cuesta sacarla.Faltan dos anillos de afinación.', ' ₡119,000.00 ', 'Bodega', 3, 'Sin Responsable', 9, NULL),
('AEMSTRA-013', 'TRA', 17, 'Trompeta', 'Bundy', 'Bundy', '978215', 'De color Dorado.En buen estado.Con golpes, rayones y corrosión. Estuche negro. Con boquilla.', ' ₡119,000.00 ', 'BODEGA', 3, 'Sin Responsable', 9, NULL),
('AEMSTRN-001', 'TRN', 3, 'Trombón', 'King', 'Concert', '348194', 'De color dorado, simple, corroído.En regular estado.Estuche café con café claro en buen estado. Boquilla Vincent Bach 11C. (Donado por Cornell)', ' ₡119,000.00 ', 'Bodega', 3, 'Sin Responsable', 9, NULL),
('AEMSTRN-002', 'TRN', 4, 'Trombón', 'Getzen', 'Sin Modelo', 'KT7652', 'Dorado, simple.En regular estado.Estuche duro café por afuera y acolchado y de color verde por dentro.Boquilla vincent bach 11C. Con algunos rayones (Donado por Cornell)', ' ₡119,000.00 ', 'Bodega', 3, 'Sin Responsable', 9, NULL),
('AEMSTRN-003', 'TRN', 5, 'Trombón', 'Bach', 'Sin Modelo', 'E11690', 'Dorado.En buen estado.Estuche en buen estado, duro, negro, marca Bach.Boquilla Benge 12C.  -Con varios rayones y pequeños golpes en la campana.', ' ₡89,250.00 ', 'Bodega', 3, 'Sin Responsable', 9, NULL),
('AEMSTRN-004', 'TRN', 7, 'Trombón', 'King', 'Cleveland 605', '530693', 'Amarillo.En buen estado. Estuche negro en buen estado. Boquilla Vincent Bach 12C.', ' ₡89,250.00 ', 'Bodega', 3, 'Sin Responsable', 9, NULL),
('AEMSTRN-005', 'TRN', 8, 'Trombón', 'Bundy', 'ML', '231692', 'Dorado.  En buen estado.Estuche negro en buen estado, marca Bundy.Con boquilla Yamaha 45C2-12C.  -Quebradura en el salivero, golpes en la campana y raspones.(Donado por Cornell)', ' ₡59,500.00 ', 'BODEGA ', 3, 'Sin Responsable', 9, NULL),
('AEMSTRN-006', 'TRN', 9, 'Trombón', 'Bundy', 'ML', '553032', 'Dorado.En buen estado.Estuche gris en buen estado, marca bundy. Con boquilla', ' ₡59,500.00 ', 'Bodega', 3, 'Sin Responsable', 9, NULL),
('AEMSTRN-007', 'TRN', 10, 'Trombón', 'Bundy Selmer', 'Bu-8', '982', 'Dorado.En buen estado. Estuche de color café Buescher, en buen estado.Sin boquilla. ', ' ₡59,500.00 ', 'Bodega', 3, 'Sin Responsable', 9, NULL),
('AEMSTRN-008', 'TRN', 11, 'Trombón', 'Getzen', 'Eterna', '1047 F-2126', 'Dorado.En muy buen estado.Con boquilla Getzen 6.5.Estuche duro de color negro en buen estado, marca Getzen.', ' ₡1,785,000.00 ', 'BODEGA', 3, 'Sin Responsable', 9, NULL),
('AEMSTRN-009', 'TRN', 12, 'Trombón', 'Bundy', 'ML', '552952', 'Dorado.En regular estado.Estuche suave color negro, en regular estado.Salibero en mal estado.', ' ₡59,500.00 ', 'BODEGA', 3, 'Sin Responsable', 9, NULL),
('AEMSTRN-010', 'TRN', 14, 'Trombón', 'Getzen', 'Sin Modelo', 'E3093', 'Dorado.En regular estado.Sin estuche.Trombón de embolos.Mal estado (donado por Cornell)', ' ₡59,500.00 ', 'Bodega', 3, 'Sin Responsable', 9, NULL),
('AEMSTRN-011', 'TRN', 15, 'Trombón', 'Bach', 'Sin Modelo', 'F66316', 'Dorado.Estuche duro de color negro.Tiene seis pequeños golpes.', ' ₡178,500.00 ', '', 3, 'Sin Responsable', 9, NULL),
('AEMSTRN-012', 'TRN', 17, 'Trombón Tenor', 'Bach', 'TB650', 'AD20417003', 'Trombon nuevo 2019. Tenor p/niño. Estuche nuevo bach Negro por dentro y fuera. Con llave alen. Con boquilla vicent bach 12', ' ₡564,800.00 ', 'BODEGA', 3, 'Sin Responsable', 9, NULL),
('AEMSTUB-001', 'TUB', 1, 'Tuba', 'Yamaha', 'YBB-321', '424910', 'Dorado.En muy buen estado.Estuche negro en muy buen estado.', ' ₡1,487,500.00 ', 'BODEGA', 3, 'Sin Responsable', 9, NULL),
('AEMSTUB-002', 'TUB', 2, 'Tuba', 'Yamaha', 'YBB-321', '390507', 'Dorado.En muy buen estado.Estuche negro en muy buen estado.', ' ₡1,487,500.00 ', 'Bodega', 3, 'Sin Responsable', 9, NULL),
('AEMSVLA-001', 'VLA', 2, 'Viola 16”', 'Mapple Lea Wei yin music', '1996', 'Sin Serie', 'Estuche azul por fuera y negro por dentro.Con arco.', ' ₡178,500.00 ', 'Bodega', 3, 'Sin Responsable', 9, NULL),
('AEMSVLA-002', 'VLA', 11, 'Viola 15.5”,', 'Cecilia', '200043736', 'Sin Serie', 'Estuche negro por fuera y beige por dentro.Con arco.', ' ₡119,000.00 ', 'Bodega', 3, 'Sin Responsable', 9, NULL),
('AEMSVLA-003', 'VLA', 14, 'Viola 16.5”', 'Verhoved', 'Sin Modelo', 'Sin Serie', 'Estuche negro por fuera y gris por dentro.Le falta la cuerda la y el mástil está despegado.', ' ₡178,500.00 ', 'Bodega', 3, 'Sin Responsable', 9, NULL),
('AEMSVLN-001', 'VLN', 1, 'Violín 4/4', 'Samuel Eastman', '2010', '805104', 'Arco K. Holtz. Estuche rectangular negro.(Donado por Lynbrook)', ' ₡120,000.00 ', 'BODEGA', 3, 'Sin Responsable', 9, NULL),
('AEMSVLN-002', 'VLN', 4, 'Violín 4/4', 'Samuel Eastman', '2010', '805040', 'Arco K. Holtz, Estuche rectangular negro.sin arco.(Donado por Lynbrook)', ' ₡120,000.00 ', 'DUARTE SOLANO IHAN', 3, 'Sin Responsable', 9, NULL),
('AEMSVLN-003', 'VLN', 6, 'Violín 4/4', 'Praga', '7', 'Sin Serie', 'Con arco. Estuche color negro.', ' ₡120,000.00 ', 'Bodega', 3, 'Sin Responsable', 9, NULL),
('AEMSVLN-004', 'VLN', 7, 'Violín ¾', 'Sin marca', 'Sin Modelo', 'Sin serie', 'Estuche negro por fuera y verde por dentro.Mal estado.No arco.', ' ₡60,000.00 ', 'Bodega', 3, 'Sin Responsable', 9, NULL);
INSERT INTO `instrumentos` (`codInstrumento`, `codTipoInstrumento`, `numeroInstrumento`, `nombre`, `marca`, `modelo`, `serie`, `observaciones`, `valor`, `ubicacion`, `idInstitucion`, `responsable`, `idEstado`, `fotoInstrumento`) VALUES
('AEMSVLN-005', 'VLN', 8, 'Violín 1/2 ', 'Lark', 'Sin Modelo', 'Sin serie', 'Estuche negro por fuera y verde por dentro.', ' ₡60,000.00 ', 'Bodega', 3, 'Sin Responsable', 9, NULL),
('AEMSVLN-006', 'VLN', 9, 'Violín ½', 'Valencia', 'Sin Modelo', 'Sin serie', 'Estuche negro por fuera y celeste por dentro. Con arco. Donado por Alena', ' ₡120,000.00 ', 'GARCIA WEBER AMANDA', 3, 'Sin Responsable', 9, NULL),
('AEMSVLN-007', 'VLN', 16, 'Violín 4/4', 'Praga', '16', '200053457', 'Estuche negro por fuera y café por dentro. Con arco', ' ₡120,000.00 ', 'BODEGA ', 3, 'Sin Responsable', 9, NULL),
('AEMSVLN-008', 'VLN', 23, 'Violín ¾', 'Mapple Lea Wei yin Musical Co', 'V 160', 'Sin Serie', 'Estuche café por fuera y verde oscuro por dentro.Con arco', ' ₡150,000.00 ', 'HILA GEVA', 3, 'Sin Responsable', 9, NULL),
('AEMSVLN-009', 'VLN', 25, 'Violín ¾', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Estuche negro por fuera y azul por dentro.', ' ₡60,000.00 ', 'Bodega', 3, 'Sin Responsable', 9, NULL),
('AEMSVLN-010', 'VLN', 26, 'Violín ½', 'Valencia', 'Sin Modelo', 'Sin Serie', 'Estuche negro por fuera y gris por dentro.', ' ₡120,000.00 ', 'Bodega', 3, 'Sin Responsable', 9, NULL),
('AEMSVLN-011', 'VLN', 27, 'Violín ½', 'Valencia', 'Sin Modelo', 'Sin Serie', 'Estuche negro por fuera y gris por dentro.Con arco.', ' ₡60,000.00 ', 'YULISA CASTRO BARRANTES', 3, 'MARIA LUISA BARRABTES SIBAJA', 9, NULL),
('AEMSVLN-012', 'VLN', 29, 'Violín ¼', 'Di Palo', 'Sin Modelo', '22558', 'Estuche negro por fuera y verde oscuro por dentro.Con arco.', ' ₡120,000.00 ', 'ORTEGA GAMBOA SOHE', 3, 'LAURA VARGAS ACOSTA', 9, NULL),
('AEMSVLN-013', 'VLN', 31, 'Violín ¼', 'Di Palo', 'Sin Modelo', '22559', 'Estuche negro por fuera y verde por dentro.Sin arco', ' ₡120,000.00 ', 'FERNANDEZ ARGUEDAS GENESIS', 3, 'Sin Responsable', 9, NULL),
('AEMSVLN-014', 'VLN', 32, 'Violín ¼', 'Anton Breton', 'Sin Modelo', '10374', 'Estuche negro por fuera y verde oscuro por dentro.Con arco', ' ₡120,000.00 ', 'KADY ROMERO', 3, 'Sin Responsable', 9, NULL),
('AEMSVLN-015', 'VLN', 33, 'Violín ¼', 'Anton Breton', 'Sin Modelo', '10420', 'Estuche negro por fuera y verde oscuro por dentro.Sin arco.', ' ₡120,000.00 ', 'QUESADA HERNADEZ JORGE', 3, 'Sin Responsable', 9, NULL),
('AEMSVLN-016', 'VLN', 34, 'Violín ¼', 'Anton Breton', 'Sin Modelo', '10372', 'Estuche negro por fuera y verde oscuro por dentro.Con arco', ' ₡120,000.00 ', 'BODEGA', 3, 'Sin Responsable', 9, NULL),
('AEMSVLN-017', 'VLN', 38, 'Violín ½', 'Pablo', 'Sin Modelo', 'Sin Serie', 'Estuche negro por fuera y verde claro por dentro.Le falta cuerda RE. Sin arco.', ' ₡120,000.00 ', 'SOLIS SEGURA KRISBELL LUCIANA', 3, 'Sin Responsable', 9, NULL),
('AEMSVLN-018', 'VLN', 39, 'Violín ¾', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Estuche negro por fuera y verde claro por dentro.Con arco.', ' ₡60,000.00 ', 'Bodega', 3, 'Sin Responsable', 9, NULL),
('AEMSVLN-019', 'VLN', 46, 'Violín3/4', 'Cecilia', 'Sin Modelo', '200053457', 'Estuche negro por fuera crema por DENTRO arco', ' ₡120,000.00 ', 'BODEGA', 3, 'Sin Responsable', 9, NULL),
('AEMSVLN-020', 'VLN', 47, 'Violín 1/8', 'Chino', 'MY-010', 'Sin Serie', 'Estuche duro negro por fuera y verde por dentro.Sin arco.Le falta una cuerda.', ' ₡60,000.00 ', 'ARROYO FERNANDEZ ZOHE', 3, 'Sin Responsable', 9, NULL),
('AEMSVLN-021', 'VLN', 48, 'Violín ½', 'Masakichi Suzuki', 'Sin Modelo', '220', 'Estuche donado por Hillsdale. Sin arco (Donado por Hillsdale)', ' ₡120,000.00 ', 'BODEGA', 3, 'Sin Responsable', 9, NULL),
('AEMSVLN-022', 'VLN', 49, 'Violín ¾', 'Sherl 8 ruth INC', 'Mod. 301', '278', '-Estuche duro, café por fuera y azul por dentro.', ' ₡60,000.00 ', 'VARGAS NARANJO ANA PAULA', 3, 'Sin Responsable', 9, NULL),
('AEMSVLN-023', 'VLN', 52, 'Violín ¼', 'Volencia ', 'V160', 'No', 'Estuche negro Porfum, de color gris por dentro. (Donación)', ' ₡120,000.00 ', 'RIVERA MORA NOELIA', 3, 'CORNELIO GONZALEZ SALAS', 9, NULL),
('AEMSVLN-025', 'VLN', 53, 'Violín ¾', 'No tiene ', 'No tiene', 'Sin Serie', 'Con estuche.Con arco.', ' ₡120,000.00 ', 'BODEGA', 3, 'Sin Responsable', 9, NULL),
('AEMSVLN-026', 'VLN', 54, 'Violín ¾', 'Mendini', 'Mv300', 'S/n201201428', 'Estuche negro por fuera y gris por dentro. Buen estado ', ' ₡60,000.00 ', 'JUAN IGNACIO SANCHEZ MORA', 3, 'GABRIELA MORA ELIZONDO', 9, NULL),
('MPZSFN-001', 'SFN', 1, 'Sousáfono', 'Selmer', 'Signet', '728451', 'En regular estado.De color blanco.Sin estuche.Está en apartamento de profes. (MPZ)', '', 'Bodega', 3, 'LAURA MESEN', 9, NULL),
('MPZSXF-001', 'SXF', 16, 'Saxofón Alto', 'Schankelanrs', 'Sin Modelo', '3446788', 'Saxofón alto. De color plateado.Con tudel.Sin boquilla.Estuche de color negro por adentro y por afuera.En mal estado. (MPZ)', ' ₡90,000.00 ', 'Bodega', 3, 'LAURA MESEN', 9, NULL),
('MPZSXF-002', 'SXF', 20, 'Saxofón Tenor', 'YAMAHA', 'TS 100', '3989', 'Saxofón tenor.De color dorado, con llaves plateadas.En buen estado.Con estuche YAMAHA.Estuche de color negro por adentro y por afuera.Con tudel.Con boquilla.Con abrazadera.(MPZ)', ' ₡1,080,000.00 ', 'BODEGA', 3, 'LAURA MESEN', 9, NULL),
('MPZSXF-003', 'SXF', 24, 'Saxofón Tenor', 'HUTTL', 'BERUF Musiker', '3857', 'Saxofón tenor.De color plata.En mal estado.Con tudel.Estuche de color negro por adentro y por afuera.Sin boquilla y abrazadera.(MPZ)', '', 'Bodega', 3, 'LAURA MESEN', 9, NULL),
('MPZSXF-004', 'SXF', 26, 'Saxofón Barítono', 'COUESNON', 'Paris', '942', 'Color Plateado en mal estado con tudel , sin boquilla ni abrazadera. (MPZ)', ' $300 ', 'BODEGA', 3, 'Sin Responsable', 9, NULL),
('N00141609', 'PIN', 2, 'PIANO Portátil', 'Casio', 'PX-350MBK', '92L ADC23K024979', 'Nuevo   Piano 7/8  Buen Estado ', ' ₡513,955.00 ', 'Josue Asaf Garro Arias', 1, 'Laura Mesén', 9, NULL),
('N00141874', 'BJO', 2, 'Bajo Eléctrico', 'YAMAHA', 'TRB1006J', 'HL0193023', '6 cuerdas, color madera', ' ₡763,000.00 ', 'GUSTABO BLACON', 1, 'Laura Mesén', 9, NULL),
('N00141875', 'SXF', 29, 'Saxofón Soprano', 'Conn SELMER', 'SS 650', 'AD16015024', 'Estuche negro VERIFICAR # DE SERIE Y MODELO', ' ₡828,226.00 ', 'ALVAREZ SOLANO SEBASTIAN', 1, 'LAURA MESEN', 9, NULL),
('N00141876', 'SXF', 28, 'Saxofón Soprano Recto', 'Conn SELMER', 'SS 650', 'AD16015039', 'Saxofón soprano nuevo.VERIFICAR # DE SERIE Y MODELO ', ' ₡828,226.00 ', 'NARNAJO RAMIREZ GABRIEL', 1, 'LAURA MESEN', 9, NULL),
('N00142290', 'PER', 7, 'BATERIA YAMAHA ROJA', 'YAMAHA', 'STAGER CUSTON SCBO', 'YCSBOF51C', 'BUEN ESTADO. BOMBO, TOM GRANDE, MEDIANO Y PEQUEÑO Y REDOBLANTE', ' ₡310,000.00 ', 'AULA DE PERCUSION', 1, 'LAURA MESEN', 9, NULL),
('N00144181', 'REQ', 1, 'Requinto', 'Guzmán y Albares', 'No tiene', 'No tiene', 'Color madera . Con estuche', ' ₡238,000.00 ', 'Bodega', 1, 'LAURA MESEN', 9, NULL),
('N00144182', 'MND', 4, 'Mandolina', 'Guzmán Álvarez', 'Sin Modelo', 'Sin Serie', 'De color madera.Con estuche.', ' ₡140,000.00 ', 'Bodega', 1, 'Laura Mesén', 9, NULL),
('N00144183', 'MND', 3, 'Mandolina', 'Guzmán Álvarez', 'Sin Modelo', 'Sin Serie', 'De color madera.Con estuche colgante y púa color negro.', ' ₡140,000.00 ', 'Bodega', 1, 'Laura Mesén', 9, NULL),
('N00144184', 'MND', 2, 'Mandolina', 'Guzmán Álvarez', 'Sin Modelo', 'Sin Serie', 'De color madera.Con estuche colgante y púa color negro.', ' ₡140,000.00 ', 'Bodega', 1, 'Laura Mesén', 9, NULL),
('N00144185', 'MND', 1, 'Mandolina', 'Guzmán y Albares', 'No tiene', 'No tiene', 'De color madera.Con estuche colgante y púa color negra.', ' ₡140,000.00 ', 'Bodega', 1, 'Laura Mesén', 9, NULL),
('N00144186', 'BAC', 2, 'Bajo Electro  acústico', 'Oscar Schmidt', '328924 OB100/N', '15070377', 'Sin Oberservaciones', ' ₡139,939.00 ', 'Cubículo 4', 1, 'LAURA MESEN', 9, NULL),
('N00144187', 'BAC', 1, 'Bajo Electro acustico ', 'Oscar Schmidt', '328924 OB100/N', '15070372', 'Sin Oberservaciones', ' ₡139,939.00 ', 'Bodega', 1, 'LAURA MESEN', 9, NULL),
('N00144188', 'ACD', 2, 'Acordeón', 'JMEISTER', 'BM2353', 'Sin Serie', 'Color negro ', ' ₡398,000.00 ', 'Bodega', 1, 'Laura Mesén', 9, NULL),
('N00144189', 'ACD', 1, 'Acordeón', 'J.MEISTER', 'BM2353', 'Sin Serie', 'Color Negro', ' ₡398,000.00 ', 'Bodega', 1, 'Laura Mesén', 9, NULL),
('N00144924', 'VHU', 1, 'Vihuela ', 'Paganini', 'Vpg-01 C/estuche', 'Sin serie', 'Nuevo color madera. Con estuche color negro', '', 'Bodega', 1, 'LAURA MESEN', 9, NULL),
('N00144928', 'PER', 5, 'SET DE PLATILLOS', 'ZILDJIAN', 'K1250K CUSTOM', 'Sin Serie', 'PLATILLO SUSP. 22\", 20\"ZHT Y HIT HAT 16\" Y 14\" SCIMITAR', '', 'AULA DE PERCUSION', 1, 'LAURA MESEN', 9, NULL),
('N00144933', 'PER', 10, 'CONGAS', 'LP', 'LP559T-WD 11 1/2', 'Sin Serie', 'Sin Oberservaciones', ' ₡299,750.00 ', 'JOSE DAVID FLORES', 1, 'LAURA MESEN', 9, NULL),
('N00144940', 'PER', 9, 'CONGAS', 'LP', 'LP552T-DT 12', 'Sin Serie', 'Sin Oberservaciones', ' ₡321,550.00 ', 'JOSE DAVID FLORES', 1, 'LAURA MESEN', 9, NULL),
('N00144941', 'CLL', 13, 'CELLO4/4', 'Cremono-ceruini', 'HC-100', '1405080', 'Nuevo 2016, estuche negro suave ', ' ₡207,965.00 ', 'JIMENA MENDEZ ASCA', 1, 'LAURA MESEN', 9, NULL),
('N00144942', 'PER', 8, 'TIMBALETAS TITO PUENTE', 'LP MATADOR', 'LP257-B', 'Sin Serie', 'Sin Oberservaciones', ' ₡423,000.00 ', 'ISAAC QUESADA BARTES', 1, 'LAURA MESEN', 9, NULL),
('N00144944', 'PER', 14, 'TAMBORA', 'LP', 'LP271-WD', 'Sin Serie', 'Sin Oberservaciones', ' ₡239,646.00 ', 'AULA DE PERCUSION', 1, 'LAURA MESEN', 9, NULL),
('N00144945', 'CLL', 11, 'CELLO3/4', 'Oxford', 'OXF14341', 'No tiene', 'Nuevo.2016, color rojo, estuche negro suave', ' ₡278,053.00 ', 'Bodega', 1, 'LAURA MESEN', 9, NULL),
('N00146006', 'TRA', 18, 'Trompeta', 'Bach', 'TR500', 'AD23611020', 'Nueva 2019.De color plateada.Estuche nuevo de color negro.', ' ₡260,000.00 ', 'BODEGA', 1, 'LAURA MESEN', 9, NULL),
('N00146587', 'TRN', 16, 'Trombón', 'Yamaha', 'YSL620', '629182', '-Instrumento nuevo.', ' ₡1,862,000.00 ', 'PABLO SOLANO PICADO', 1, 'Laura Mesén', 9, NULL),
('N00147027', 'OBE', 9, 'Oboe', 'Selmer', 'YOB 121', '16824', 'Nuevo.Con estuche negro.Con cuña y aceite de vaselina.', ' ₡2,145,945.00 ', 'TALLER', 1, 'LAURA MESEN', 9, NULL),
('N00147028', 'CLL', 12, 'CELLO1/8', 'PAGANNI', 'HC1/8', 'Sin Serie', 'Nuevo.2017, estuche negro suave', ' ₡241,816.00 ', 'Fiorela Castro Mesen', 1, 'LAURA MESEN', 9, NULL),
('N00147029', 'TUB', 4, 'Tuba', 'Holton', '88450', 'BC21916024', 'Nuevo 2019.Estuche de color negro.', ' ₡2,255,160.00 ', 'JERMIA BERD LIZANO', 1, 'Laura Mesén', 9, NULL),
('N00155991', 'PIC', 1, 'Piano Cola ', 'YAMAHA ', 'YCGC1PE   ( CGI)', 'No tiene ', 'Piano Nuevo ', ' ₡7,786,447.00 ', 'Salón ', 1, 'LAURA MESEN', 9, NULL),
('N00155992', 'PER', 1, 'TIMPANI 23\" 26\" 29\" 32\"', 'YAMAHA', 'TP7300', 'Sin Serie', 'Sin Oberservaciones', ' ₡2,840,000.00 ', 'SALON', 1, 'LAURA MESEN', 9, NULL),
('N00155993', 'PER', 11, 'GONG', 'ZILDJIAN', 'ZIP0501', 'Sin Serie', 'Sin Oberservaciones', ' ₡519,498.00 ', 'Aula de percucion', 1, 'LAURA MESEN', 9, NULL),
('N00156123', 'CLT', 25, 'Clarinete plástico', 'Buffet Grampon', 'Sin Modelo', 'C034195', 'Nuevo 2019', ' ₡496,481.00 ', ' Bodega', 1, 'LAURA MESEN', 9, NULL),
('N00156124', 'PIN', 4, 'Piano Portátil', 'Casio Privio', 'PX160  BKK2', 'ACD-389068290', 'Nuevo', ' ₡308,672.00 ', 'JOSAN ABARCA CHAVEZ', 1, 'LAURA MESEN', 9, NULL),
('N00156125', 'PIN', 3, 'Piano Portátil', 'Casio Privio', 'PX-160  BKK2', 'ADC-389068291', 'Nuevo', ' ₡308,672.00 ', 'MARIANGEL VILLEGAS', 1, 'LAURA MESEN', 9, NULL),
('N00156126', 'CLT', 24, 'Clarinete', 'Buffet Grampon', 'Sin Modelo', '723172', 'Nuevo 2019 necesita reparacion', ' ₡2,163,391.00 ', 'TALLER', 1, 'LAURA MESEN', 9, NULL),
('N00156356', 'CTB', 8, 'Contrabajo', 'BUFFET', 'BC2541-2 OGB PRODIGE', 'NC', 'Llegaron el 2019', ' ₡953,919.00 ', 'JUAN IGNACIO RODRIGUEZ', 1, 'LAURA MESEN', 9, NULL),
('N00156696', 'CTB', 7, 'Contrabajo', 'SCHERL AND ROTH', 'SR46E3', 'ARIETA 3/4', 'Llegaron el 2019', ' ₡953,919.00 ', 'SALON', 1, 'LAURA MESEN', 9, NULL),
('N00157729', 'OBE', 10, 'Oboe ', 'BUUFFET CRAMPO', 'BC406220', '33668', 'Nuevo. Estuche negro por fuera y vino por dentro ', ' ₡3,000,000.00 ', 'MONSERRATH LEIVA ACUÑA', 1, 'LAURA MESEN', 9, NULL),
('N00162309', 'PER', 2, 'MARIMBA', 'MARIMBA ONE', '3100', 'Sin Serie', 'Sin Oberservaciones', '', 'SALON', 1, 'LAURA MESEN', 9, NULL),
('PLACA 92374', 'CLL', 7, 'CELLO4/4', 'MAPLE LEAF', 'Sin Modelo', 'Sin Serie', 'Color rojo,2007 , en buen estado, estuche negro en mal estado, con arco en regular estado ', ' ₡595,000.00 ', 'LEONELA ZUÑIGA ROJAS', 1, 'LAURA MESEN', 9, NULL),
('Placa 92435', 'PIN', 1, 'PIANO Portátil', 'Yamaha', 'P-80', '18458', 'Sin Oberservaciones', ' ₡100,000.00 ', 'ARTURO CAMPOS ', 1, 'LAURA MESEN', 9, NULL),
('Placa 92482', 'CTB', 4, 'Contrabajo', 'Sin Marca', 'Sin Modelo', 'Sin Serie', 'Color rojo, 4/4, buen estado, estuche negro. ', ' ₡300,000.00 ', 'Bodega', 1, 'LAURA MESEN', 9, NULL),
('pru-001', 'FGT', 4, 'prueba editado', 'pruea', 'prueba', 'prueba', 'asnakosbdkla asndkansdasd', '500.000', 'prueba', 2, 'prueba', 2, 'rsc/instrumentosPics/pru-001.png'),
('UNAGTR-001', 'GTR', 1, 'Guitarra', 'Oscar Schmidt', '313948 0C11CE', '15030146', 'Guitarra eléctro acustica de color madera.Con estuche, faja, tornillos y cejilla.', ' ₡150,000.00 ', 'Bodega', 1, 'LAURA MESEN', 9, NULL),
('UNAGTR-002', 'GTR', 2, 'Guitarra', 'Oscar Schmidt', '313948 0C11CE', '15030147', 'Guitarra eléctro acustica de color madera.Con estuche, faja, tornillos y cejilla.', ' ₡150,000.00 ', 'Bodega', 1, 'LAURA MESEN', 9, NULL),
('UNAGTR-003', 'GTR', 3, 'Guitarra', 'Oscar Schmidt', '328924 OC11CE', '15070396', 'Guitarra eléctro acustica de color madera.Con estuche, faja, tornillos y cejilla.', ' ₡150,000.00 ', 'ERICK BONILLA BOLAÑOS', 1, 'LAURA MESEN', 9, NULL),
('UNAGTR-004', 'GTR', 4, 'Guitarra', 'Oscar Schmidt', '313948 OC11CE', '15030148', 'Guitarra eléctro acustica de color madera.Con estuche, faja, tornillos y cejilla.', ' ₡150,000.00 ', 'Bodega', 1, 'LAURA MESEN', 9, NULL),
('UNAGTR-005', 'GTR', 5, 'Guitarra', 'Oscar Schmidt', '328924 OC11CE', '15070398', 'Guitarra eléctro acustica de color madera.Con estuche, faja, tornillos y cejilla.', ' ₡150,000.00 ', 'Bodega DANILO', 1, 'LAURA MESEN', 9, NULL),
('UNAGTR-006', 'GTR', 6, 'Guitarra', 'Oscar Schmidt', '313948 OC11CE', '15030149', 'Guitarra eléctro acustica de color madera.Con estuche, faja, tornillos y cejilla.', ' ₡150,000.00 ', 'Bodega', 1, 'LAURA MESEN', 9, NULL),
('UNAGTR-007', 'GTR', 7, 'Guitarra', 'Oscar Schmidt', '313948 OC11CE', '15030157', 'Guitarra eléctro acustica de color madera.Con estuche, faja, tornillos y cejilla.', ' ₡150,000.00 ', 'Bodega', 1, 'LAURA MESEN', 9, NULL),
('UNAGTR-008', 'GTR', 8, 'Guitarra', 'Oscar Schmidt', '313948 OC11CE', '15030150', 'Guitarra eléctro acustica de color madera.Con estuche, faja, tornillos y cejilla.', ' ₡150,000.00 ', 'Bodega', 1, 'LAURA MESEN', 9, NULL),
('UNAGTR-009', 'GTR', 9, 'Guitarra', 'Oscar Schmidt', '328924 OC11CE', '15070400', 'Guitarra eléctro acustica de color madera.Con estuche, faja, tornillos y cejilla.', ' ₡150,000.00 ', 'Bodega', 1, 'LAURA MESEN', 9, NULL),
('UNAGTR-010', 'GTR', 10, 'Guitarra', 'Oscar Schmidt', '328924 OC11CE', '15070399', 'Guitarra eléctro acustica de color madera.Con estuche, faja, tornillos y cejilla.', ' ₡150,000.00 ', 'Bodega', 1, 'LAURA MESEN', 9, NULL),
('UNAGTR-011', 'GTR', 11, 'Guitarra', 'Oscar Schmidt', '313948 OC11CE', '15030115', 'Guitarra eléctro acustica de color madera.Con estuche, faja, tornillos y cejilla.', ' ₡150,000.00 ', 'Bodega', 1, 'LAURA MESEN', 9, NULL),
('UNAGTR-012', 'GTR', 12, 'Guitarra', 'Oscar Schmidt', '328924 OC11CE', '15070395', 'Guitarra eléctro acustica de color madera.Con estuche, faja, tornillos y cejilla.', ' ₡150,000.00 ', 'Bodega', 1, 'LAURA MESEN', 9, NULL),
('UNAGTR-013', 'GTR', 13, 'Guitarra', 'Oscar Schmidt', '328924 OC11CE', '15070397', 'Guitarra eléctro acustica de color madera.Con estuche, faja, tornillos y cejilla.', ' ₡150,000.00 ', 'Bodega', 1, 'LAURA MESEN', 9, NULL),
('UNAGTR-014', 'GTR', 14, 'Guitarra', 'Oscar Schmidt', '328924 OC11CE', '15070436', 'Guitarra eléctro acustica de color madera.Con estuche, faja, tornillos y cejilla.', ' ₡150,000.00 ', 'Bodega', 1, 'LAURA MESEN', 9, NULL),
('UNAGTR-015', 'GTR', 15, 'Guitarra', 'Oscar Schmidt', '313948 OC11CE', '15030405', 'Guitarra electroacústica. Con cuerdas de metal y estuche.', ' ₡150,000.00 ', 'Bodega', 1, 'LAURA MESEN', 9, NULL),
('UNAGTR-016', 'GTR', 18, 'Guitarra', 'SEVILLA', 'LC-18 4/4 NAT', 'Sin Serie', 'INGRESO 2020', '', 'Bodega', 1, 'Sin Responsable', 9, NULL),
('UNAORG-001', 'ORG', 1, 'Organeta Electrónica ', 'Casio ', 'CTK 5200', 'No tiene ', 'Nuevo 5/8', ' ₡250,000.00 ', 'DIEGO VARGAS ', 1, 'LAURA MESEN', 9, NULL),
('UNAORG-002', 'ORG', 2, 'Organeta Electrónica ', 'Casio ', 'CTK 5200', 'No tiene ', 'Nuevo 5/8', ' ₡250,000.00 ', 'DAVID GRANADOS', 1, 'LAURA MESEN', 9, NULL),
('UNAORG-003', 'ORG', 3, 'Organeta Electrónica ', 'Casio ', 'CTK 5200', 'No tiene ', 'Nuevo 5/8', ' ₡250,000.00 ', 'GUSTAVO BLANCO COTO', 1, 'LAURA MESEN', 9, NULL),
('UNAORG-004', 'ORG', 4, 'Organeta Electrónica', 'Casio ', 'CTK 5200', 'No tiene ', 'Nuevo 5/8', ' ₡250,000.00 ', 'ADRIAN CASCANTE ', 1, 'LAURA MESEN', 9, NULL),
('UNAORG-005', 'ORG', 5, 'Organeta Electrónica ', 'Casio ', 'CTK 5200', 'No tiene ', 'Nuevo 5/8', ' ₡250,000.00 ', 'HERMANOS FLORES ARAYA ', 1, 'LAURA MESEN', 9, NULL),
('UNAORG-006', 'ORG', 6, 'Organeta Electrónica ', 'Casio ', 'CTK 5200', 'No tiene ', 'Nuevo 5/8', ' ₡250,000.00 ', 'ESTEBAN MENA', 1, 'LAURA MESEN', 9, NULL),
('UNAORG-007', 'ORG', 7, 'ORGANETA ', 'CASIO', 'ADC37K', 'CTK3500', 'VIOLEY', ' ₡100,000.00 ', 'CRISTIAN PROYECTO DE LA UNA', 1, 'LAURA MESEN', 9, NULL),
('UNAPER-001', 'PER', 15, 'CAJON PERUANO', 'MEINL', 'HCAJIMH-M', 'Sin Serie', 'Sin Oberservaciones', ' ₡120,000.00 ', 'AULA DE PERCUSION', 1, 'LAURA MESEN', 9, NULL),
('UNAPER-002', 'PER', 13, 'BONGOE', 'SINERGY', 'SOM SAK', '27481', 'Sin Oberservaciones', '', 'Aula de percucion', 1, 'LAURA MESEN', 9, NULL),
('UNASXF-001', 'SXF', 18, 'Saxofón Soprano Curvo', 'PALMER', 'Sin Modelo', '769671265', 'Saxofón soprano curvo negro, con llaves doradas en buen estado.Con estuche color negro por dentro y café por fuera.Con tudel, boquilla y abrazadera. ', ' ₡240,000.00 ', 'Bodega', 1, 'LAURA MESEN', 9, NULL),
('UNASXF-002', 'SXF', 27, 'Saxofón Soprano', 'Wisemann', 'Dss-500', '1087120089', 'Curvo profesional nuevo dorado. No aparese registro en la UNA', ' ₡600,000.00 ', 'BRANDON STEVEN VILLALOBOS QUIROS', 1, 'LAURA MESEN', 9, NULL),
('v-1', 'CLV', 3, 'Piano Clavinova ', 'YAMAHA ', 'CLP 240', 'WE04210', 'Buen Estado. Color café', ' ₡700,000.00 ', 'Siany Ugalde Quesada', 1, 'LAURA MESEN', 9, NULL),
('v-10', 'PER', 67, 'CAMPANA DE MANO', 'LP', 'Sin Modelo', 'Sin Serie', 'Sin Oberservaciones', '', 'AULA DE PERCUSION', 3, 'Sin Responsable', 9, NULL),
('v-11', 'PER', 68, 'MARACAS LP', 'LP', 'Sin Modelo', 'Sin Serie', ' Esta en una caja celeste plastica ', '', ' CAJA       AEMS    Percusion Menor', 3, 'Sin Responsable', 9, NULL),
('v-12', 'PER', 70, 'AGOGO  Metalico', 'Sin Marca', 'Sin Modelo', 'Sin Serie', ' Esta en una caja celeste plastica ', '', ' CAJA       AEMS    Percusion Menor', 3, 'Sin Responsable', 9, NULL),
('v-13', 'PER', 74, 'AGOGO de madera', 'LAZER', 'Sin Modelo', 'Sin Serie', ' Esta en una caja celeste plastica ', '', ' CAJA       AEMS    Percusion Menor', 3, 'Sin Responsable', 9, NULL),
('v-14', 'PER', 75, 'Castanielas induvidual', 'Sin Marca', 'Sin Modelo', 'Sin Serie', ' Esta en una caja celeste plastica ', '', ' CAJA       AEMS    Percusion Menor', 3, 'Sin Responsable', 9, NULL),
('v-15', 'PER', 76, 'Cascabel individuales', 'Sin Marca', 'Sin Modelo', 'Sin Serie', ' Esta en una caja celeste plastica ', '', ' CAJA       AEMS    Percusion Menor', 3, 'Sin Responsable', 9, NULL),
('v-16', 'PER', 77, 'WOOD BLOCK', 'Sin Marca', 'Sin Modelo', 'Sin Serie', ' Esta en una caja celeste plastica ', '', ' CAJA       AEMS    Percusion Menor', 3, 'Sin Responsable', 9, NULL),
('v-17', 'PER', 78, 'TAMBOURINE', 'Sin Marca', 'Sin Modelo', 'Sin Serie', ' Esta en una caja celeste plastica ', '', ' CAJA       AEMS    Percusion Menor', 3, 'Sin Responsable', 9, NULL),
('v-2', 'CLV', 5, 'clavinova', 'Yamaha', 'CLP 645R', 'YCY 081881', 'NUEVO COLOR CAFÉ 2019', ' ₡1,700,000.00 ', 'Melissa Rojas Estrada ', 1, 'LAURA MESEN', 9, NULL),
('v-3', 'PER', 18, 'TRIÁNGULO 6”', 'LP', 'LP311C6”', 'SIN SERIE', 'Estan en una caja de Herramientas de la UNA', '', 'Caja          UNAACT-366   PERCU MENOR', 1, 'LAURA MESEN', 9, NULL),
('v-4', 'PER', 30, 'CABAZA', 'LP', 'Sin Modelo', 'Sin Serie', 'Cabaza pequeña negra. Esta en una caja celeste plastica ', '', ' CAJA       AEMS    Percusion Menor', 3, 'Sin Responsable', 9, NULL),
('v-5', 'PER', 59, 'CLAVES', 'HUERTAR', 'Sin Modelo', 'Sin Serie', ' Esta en una caja celeste plastica ', '', ' CAJA       AEMS    Percusion Menor', 3, 'Sin Responsable', 9, NULL),
('v-6', 'PER', 60, 'CASCABELES', 'MEINL', 'Sin Modelo', 'Sin Serie', ' Esta en una caja celeste plastica ', '', ' CAJA       AEMS    Percusion Menor', 3, 'Sin Responsable', 9, NULL),
('v-7', 'PER', 62, 'TRIANGULO', 'ALAN ABEL', 'LP311C6\"', 'Sin Serie', ' Esta en una caja celeste plastica ', '', ' CAJA       AEMS    Percusion Menor', 3, 'Sin Responsable', 9, NULL),
('v-8', 'PER', 63, 'Juego 2 TRIANGULOS', 'Sin Marca', 'Sin Modelo', 'Sin Serie', ' Esta en una caja celeste plastica ', '', ' CAJA       AEMS    Percusion Menor', 3, 'Sin Responsable', 9, NULL),
('v-9', 'PER', 66, 'CAMPANA CHA CHA', 'Sin Marca', 'Sin Modelo', 'Sin Serie', ' Esta en una caja celeste plastica ', '', ' CAJA       AEMS    Percusion Menor', 3, 'Sin Responsable', 9, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personas`
--

CREATE TABLE `personas` (
  `cedula` varchar(15) NOT NULL COMMENT 'Cedula que identifica a la persona',
  `email` varchar(150) DEFAULT NULL,
  `telefono` varchar(16) DEFAULT NULL COMMENT 'numero de contacto de la persona',
  `nombreCompleto` varchar(200) DEFAULT NULL,
  `nombre` varchar(50) DEFAULT NULL COMMENT 'Nombre de la persona',
  `segundoNombre` varchar(50) DEFAULT NULL COMMENT 'Segundo nombre de la persona',
  `apellido` varchar(20) DEFAULT NULL COMMENT 'Primer Apellido de la persona\r\n',
  `segundoApellido` varchar(50) DEFAULT NULL COMMENT 'Segundo apellido de la persona',
  `genero` varchar(1) DEFAULT NULL COMMENT 'Genero del Estudiante, F femenino M masculino',
  `tipoPersona` varchar(3) NOT NULL COMMENT 'Tipo de la persona. Est = estudiante, Enc = encargado, Us = usuario, Pre = presidente'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `personas`
--

INSERT INTO `personas` (`cedula`, `email`, `telefono`, `nombreCompleto`, `nombre`, `segundoNombre`, `apellido`, `segundoApellido`, `genero`, `tipoPersona`) VALUES
('000000000', 'dev.sinfoinv@gmail.com', NULL, 'desarrolladores', NULL, NULL, NULL, NULL, NULL, 'Us');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `registroacciones`
--

CREATE TABLE `registroacciones` (
  `idRegistro` int(11) NOT NULL COMMENT 'Llave primaria de la tabla',
  `idReferencial` varchar(12) DEFAULT NULL COMMENT 'id refencial del activo, contrato o boleta al que se le realizo la accion',
  `idTipoAccion` int(11) NOT NULL COMMENT 'Llave foranea al tipo de accion',
  `idUsuario` int(11) NOT NULL COMMENT 'Llave foranea al usuario que realizo la accion',
  `fechaAccion` date NOT NULL COMMENT 'fecha y hora en la que se realizo la accion'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reparaciones`
--

CREATE TABLE `reparaciones` (
  `idReparacion` int(11) NOT NULL COMMENT 'Llave primaria de la tabla',
  `codigoActivo` varchar(12) DEFAULT NULL COMMENT 'Llave foranea al activo que se reparo',
  `detalleReparacion` text NOT NULL COMMENT 'Detalles de la reparacion que se le hace al activo',
  `fechaReparacion` date NOT NULL COMMENT 'Fecha en la que se hizo la reparacion\r\n'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoaccion`
--

CREATE TABLE `tipoaccion` (
  `idTipoAccion` int(11) NOT NULL COMMENT 'Identificador del tipo de activo',
  `tipoAccion` varchar(30) NOT NULL COMMENT 'Tipo de acciones:\r\ncreacion contrato, \r\ncreacion boleta, \r\nentrega instrumentos, \r\nentraga activos, \r\nrecepcion instrumentos, \r\nrecepcion activos,  \r\nfinalizacion contrato,  \r\nfinalizacion boleta, \r\ncancelacion boleta, \r\ncancelacion contrato, \r\ningreso de activo, \r\ningreso instrumento, \r\nedicion activo, \r\nedicion instrumento,\r\nbaja activo, \r\nbaja intrumento,\r\nactualizar estudiantes,\r\nagregar usuario,\r\neditar usuario,\r\nbaja usuario\r\n\r\n\r\n'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tipoaccion`
--

INSERT INTO `tipoaccion` (`idTipoAccion`, `tipoAccion`) VALUES
(1, 'creacion contrato'),
(2, 'entrega instrumentos'),
(3, 'recepcion instrumentos'),
(4, 'finalizacion contrato'),
(5, 'cancelacion contrato'),
(6, 'creacion boleta'),
(7, 'entraga activos'),
(8, 'recepcion activos'),
(9, 'finalizacion boleta'),
(10, 'cancelacion boleta'),
(11, 'ingreso de activo'),
(12, 'edicion activo'),
(13, 'baja activo'),
(14, 'ingreso instrumento'),
(15, 'edicion instrumento'),
(16, 'baja instrumento'),
(17, 'agregar usuario'),
(18, 'editar usuario'),
(19, 'baja usuario'),
(20, 'actualizar estudiantes'),
(21, 'reactivar instrumento'),
(22, 'reactivar activo'),
(23, 'reactivar usuario');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoinstrumento`
--

CREATE TABLE `tipoinstrumento` (
  `codTipoInstrumento` varchar(5) NOT NULL COMMENT 'Codigo estandar del tipo de instrumento para formar los codigos estandarizados, ejem clarinete clt saxofon sxf',
  `tipoInstrumento` varchar(20) NOT NULL COMMENT 'Nombre de instrumento, ejem clarinete o violin'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tipoinstrumento`
--

INSERT INTO `tipoinstrumento` (`codTipoInstrumento`, `tipoInstrumento`) VALUES
('ACD', 'Acordeón'),
('BAC', 'Bajo E. Acustico'),
('BJO', 'Bajo Electrico'),
('CLL', 'Cello'),
('CLT', 'Clarinete'),
('CLV', 'Clavinova'),
('CRF', 'Corno Frances'),
('CRI', 'Corno Ingles'),
('CTB', 'Contrabajo'),
('EUF', 'Eufonio'),
('FGT', 'Fagot'),
('FTR', 'Flauta Traversa'),
('GTR', 'Guitarra'),
('MND', 'Mandolina'),
('OBE', 'Oboe'),
('ORG', 'Organeta'),
('PCL', 'Picollo'),
('PER', 'Percusion'),
('PIC', 'Piano de Cola'),
('PIN', 'Piano Portatil'),
('REQ', 'Requinto'),
('SFN', 'Sausofono'),
('SXF', 'Saxofon'),
('TRA', 'Trompeta'),
('TRN', 'Trombon'),
('TUB', 'Tuba'),
('VHU', 'Vihula'),
('VLA', 'Viola'),
('VLN', 'Violin');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `idUsuario` int(11) NOT NULL COMMENT 'id del usuario\r\n',
  `cedulaPersona` varchar(15) DEFAULT NULL,
  `nombreUsuario` varchar(50) NOT NULL COMMENT 'Nombre de usuario',
  `contraseña` varchar(300) NOT NULL COMMENT 'Contraseña del usuario',
  `rolAcceso` varchar(1) NOT NULL COMMENT 'Rol de acceso del usuario A = administrador, R = regular, M = mantenimiento',
  `fotoUsuario` varchar(100) DEFAULT NULL COMMENT 'Direccion completa de la foto de perfil del usuario, de existir una, sino sera null, sino el nombre de la imagen será el nombre de usuario.',
  `estado` varchar(1) NOT NULL COMMENT 'estado del usuario A activo I = inactivo(Dado Baja)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`idUsuario`, `cedulaPersona`, `nombreUsuario`, `contraseña`, `rolAcceso`, `fotoUsuario`, `estado`) VALUES
(25, '000000000', 'dev', 'eyJpdiI6IkxyeFhKQWNXM2xjOWQ2MW95TmdrdVE9PSIsInZhbHVlIjoicWVjSHFVVDFzVko5YjNVZkN4TFNtdz09IiwibWFjIjoiOTg0MTExMmZjOTRmNTc2NzZhMmRlMTFkZWJiZmQ4ZjVmMDc3OTg0Y2E2YmY5ZTE4YWY4MDU1ZDRkNjkzOWNmYiJ9', 'A', NULL, 'A');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `activos`
--
ALTER TABLE `activos`
  ADD PRIMARY KEY (`codActivo`),
  ADD KEY `propietario_activo` (`idInstitucion`),
  ADD KEY `estado_activo` (`idEstado`);

--
-- Indices de la tabla `boletassalida`
--
ALTER TABLE `boletassalida`
  ADD PRIMARY KEY (`idBoleta`),
  ADD KEY `usuario_boleta` (`idUsuario`);

--
-- Indices de la tabla `contratos`
--
ALTER TABLE `contratos`
  ADD PRIMARY KEY (`idContrato`),
  ADD KEY `estudiante_contrato` (`cedulaEstudiante`),
  ADD KEY `encargado_contrato` (`cedulaEncargado`),
  ADD KEY `presidente_contrato` (`PresidenteAsociacion`),
  ADD KEY `instrumento_contrato` (`codInstrumento`);

--
-- Indices de la tabla `detalleboleta`
--
ALTER TABLE `detalleboleta`
  ADD PRIMARY KEY (`pk_cod_boleta`,`pk_cod_activo`),
  ADD KEY `activo_detalle` (`pk_cod_activo`);

--
-- Indices de la tabla `encargadosestudiantes`
--
ALTER TABLE `encargadosestudiantes`
  ADD PRIMARY KEY (`cedulaEstudiante`,`cedulaEncargado`),
  ADD KEY `encargado_estudiante` (`cedulaEncargado`);

--
-- Indices de la tabla `estados`
--
ALTER TABLE `estados`
  ADD PRIMARY KEY (`idEstado`);

--
-- Indices de la tabla `instituciones`
--
ALTER TABLE `instituciones`
  ADD PRIMARY KEY (`idInstitucion`);

--
-- Indices de la tabla `instrumentos`
--
ALTER TABLE `instrumentos`
  ADD PRIMARY KEY (`codInstrumento`),
  ADD KEY `tipoInstrumento_instrumento` (`codTipoInstrumento`),
  ADD KEY `instituciones_instrumentos` (`idInstitucion`),
  ADD KEY `estado_instrumento` (`idEstado`);

--
-- Indices de la tabla `personas`
--
ALTER TABLE `personas`
  ADD PRIMARY KEY (`cedula`);

--
-- Indices de la tabla `registroacciones`
--
ALTER TABLE `registroacciones`
  ADD PRIMARY KEY (`idRegistro`),
  ADD KEY `tipo_accion_registro` (`idTipoAccion`),
  ADD KEY `usuario_registro` (`idUsuario`);

--
-- Indices de la tabla `reparaciones`
--
ALTER TABLE `reparaciones`
  ADD PRIMARY KEY (`idReparacion`),
  ADD KEY `activo_reparacion` (`codigoActivo`);

--
-- Indices de la tabla `tipoaccion`
--
ALTER TABLE `tipoaccion`
  ADD PRIMARY KEY (`idTipoAccion`);

--
-- Indices de la tabla `tipoinstrumento`
--
ALTER TABLE `tipoinstrumento`
  ADD PRIMARY KEY (`codTipoInstrumento`) USING BTREE;

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`idUsuario`),
  ADD UNIQUE KEY `us_username` (`nombreUsuario`),
  ADD KEY `persona_usuario` (`cedulaPersona`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `boletassalida`
--
ALTER TABLE `boletassalida`
  MODIFY `idBoleta` int(11) NOT NULL AUTO_INCREMENT COMMENT 'llave primaria de la tabla';

--
-- AUTO_INCREMENT de la tabla `contratos`
--
ALTER TABLE `contratos`
  MODIFY `idContrato` int(11) NOT NULL AUTO_INCREMENT COMMENT 'identificador del contrato', AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `estados`
--
ALTER TABLE `estados`
  MODIFY `idEstado` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Llave primaria de la tabla estados', AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `instituciones`
--
ALTER TABLE `instituciones`
  MODIFY `idInstitucion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `registroacciones`
--
ALTER TABLE `registroacciones`
  MODIFY `idRegistro` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Llave primaria de la tabla';

--
-- AUTO_INCREMENT de la tabla `tipoaccion`
--
ALTER TABLE `tipoaccion`
  MODIFY `idTipoAccion` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador del tipo de activo', AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `idUsuario` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id del usuario\r\n', AUTO_INCREMENT=26;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `activos`
--
ALTER TABLE `activos`
  ADD CONSTRAINT `estado_activo` FOREIGN KEY (`idEstado`) REFERENCES `estados` (`idEstado`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `propietario_activo` FOREIGN KEY (`idInstitucion`) REFERENCES `instituciones` (`idInstitucion`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Filtros para la tabla `boletassalida`
--
ALTER TABLE `boletassalida`
  ADD CONSTRAINT `usuario_boleta` FOREIGN KEY (`idUsuario`) REFERENCES `usuarios` (`idUsuario`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Filtros para la tabla `contratos`
--
ALTER TABLE `contratos`
  ADD CONSTRAINT `encargado_contrato` FOREIGN KEY (`cedulaEncargado`) REFERENCES `personas` (`cedula`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `estudiante_contrato` FOREIGN KEY (`cedulaEstudiante`) REFERENCES `personas` (`cedula`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `instrumento_contrato` FOREIGN KEY (`codInstrumento`) REFERENCES `instrumentos` (`codInstrumento`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `presidente_contrato` FOREIGN KEY (`PresidenteAsociacion`) REFERENCES `personas` (`cedula`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Filtros para la tabla `detalleboleta`
--
ALTER TABLE `detalleboleta`
  ADD CONSTRAINT `activo_detalle` FOREIGN KEY (`pk_cod_activo`) REFERENCES `activos` (`codActivo`),
  ADD CONSTRAINT `boleta_detalle` FOREIGN KEY (`pk_cod_boleta`) REFERENCES `boletassalida` (`idBoleta`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `encargadosestudiantes`
--
ALTER TABLE `encargadosestudiantes`
  ADD CONSTRAINT `encargado_estudiante` FOREIGN KEY (`cedulaEncargado`) REFERENCES `personas` (`cedula`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `esrudiante_encargado` FOREIGN KEY (`cedulaEstudiante`) REFERENCES `personas` (`cedula`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `instrumentos`
--
ALTER TABLE `instrumentos`
  ADD CONSTRAINT `estado_instrumento` FOREIGN KEY (`idEstado`) REFERENCES `estados` (`idEstado`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `instituciones_instrumentos` FOREIGN KEY (`idInstitucion`) REFERENCES `instituciones` (`idInstitucion`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `tipoInstrumento_instrumento` FOREIGN KEY (`codTipoInstrumento`) REFERENCES `tipoinstrumento` (`codTipoInstrumento`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Filtros para la tabla `registroacciones`
--
ALTER TABLE `registroacciones`
  ADD CONSTRAINT `tipo_accion_registro` FOREIGN KEY (`idTipoAccion`) REFERENCES `tipoaccion` (`idTipoAccion`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `usuario_registro` FOREIGN KEY (`idUsuario`) REFERENCES `usuarios` (`idUsuario`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Filtros para la tabla `reparaciones`
--
ALTER TABLE `reparaciones`
  ADD CONSTRAINT `activo_reparacion` FOREIGN KEY (`codigoActivo`) REFERENCES `activos` (`codActivo`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `persona_usuario` FOREIGN KEY (`cedulaPersona`) REFERENCES `personas` (`cedula`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
