<?php

namespace App\Mail;

use App\Models\Contrato;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class notificarAdminsContrato extends Mailable
{
    use Queueable, SerializesModels;

    protected $contrato;
    protected $instrumento;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Contrato $contrato, $accion)
    {
        $this->contrato = $contrato;
        $this->accion = $accion;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('correos.notificarAdminsContrato', ['contrato' => $this->contrato,'accion' => $this->accion]);
    }
}
