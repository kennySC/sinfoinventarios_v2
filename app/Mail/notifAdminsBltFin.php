<?php

namespace App\Mail;

use App\Models\BoletaSalida;
use App\Models\DetalleBoleta;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class notifAdminsBltFin extends Mailable
{
    use Queueable, SerializesModels;

    protected $boleta;
    protected $activos;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(BoletaSalida $boleta, $activos)
    {
        $this->boleta = $boleta;
        $this->activos = $activos;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('correos.notifAdminsBltFin', ['boleta' => $this->boleta, 'activos' => $this->activos]);
    }
}
