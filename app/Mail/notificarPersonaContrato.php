<?php

namespace App\Mail;

use App\Models\Contrato;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class notificarPersonaContrato extends Mailable
{
    use Queueable, SerializesModels;

    protected $contrato;
    protected $instrumento;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Contrato $contrato, $instrumento)
    {
        $this->contrato = $contrato;
        $this->instrumento = $instrumento;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('view.name');
    }
}
