<?php

namespace App\Http\Controllers;

use App\Models\Contrato;
use App\Models\Persona;
use App\Models\Usuario;
use App\Models\Instrumento;
use App\Models\Estado;
use App\Mail\notificarAdminsContrato;
use App\Mail\notificarPersonaContrato;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Carbon\Carbon;
use DateTime;
use Illuminate\Support\Facades\Mail;

class ctr_contratos extends Controller
{
    
    public function __invoke()
    {
        $contratos = Contrato::with('estudiante')->get();
        return view('contratos.contratos',compact('contratos'));
    }

    public function detalleContrato($id){
        try{
            $contrato= Contrato::where('idContrato', '=', $id)->with('estudiante','encargado','instrumento')->first();
            $contrato->instrumento->tipoInstrumento;            
            $contrato->instrumento->institucion;
            $contrato->instrumento->estado;
            return view('contratos.detalleContrato', compact('contrato'));            
        }
        catch (\Throwable $th) {            
            return back()->with('error', 'Ocurrió un error interno, por favor intente de nuevo');
        }
        
    }

    public function filtrarContratos(Request $req){
        try{
            $contratos = Contrato::leftJoin('personas as estudiante','estudiante.cedula','=','contratos.cedulaEstudiante')
            ->leftjoin('personas as encargado','encargado.cedula','=','contratos.cedulaEncargado')

            ->where('cedulaEstudiante', 'like', '%'.$req->txt_ced_estudiante.'%')

            ->where('estudiante.nombreCompleto', 'like', '%'.$req->txt_nom_estudiante.'%')  

            ->where(function ($query) use ($req) {
                $query->where('encargado.nombreCompleto', 'like', '%'.$req->txt_nom_encargado.'%')
                ->orWhere('nombreCompletoFiador', 'like', '%'.$req->txt_nom_encargado.'%');
            })

            ->where(function ($query) use ($req) {
                $query->where('cedulaFiador', 'like', '%'.$req->txt_ced_encargado.'%')
                ->orWhere('cedulaEncargado', 'like', '%'.$req->txt_ced_encargado.'%');
            }) 

            ->where('idContrato','like','%'.$req->txt_cod_contrato.'%')->with('encargado')->get();
    
            if($req->cmb_estado !='To'){
                $contratos= $contratos->where('estado','=',$req->cmb_estado);
            }
            return view('contratos.contratos',compact('contratos'));
        }
        catch (\Throwable $th) {
            throw $th;
        }
    }

    public function cancelarContrato($id){
        try{
            $contrato = Contrato::where('idContrato', '=', $id)->first();
            $contrato->estado = 'C';
            $contrato->save();
            $estado =  Estado::where('estado','like','Libre')->first();

            $instrumento = Instrumento::where('codInstrumento', '=',  $contrato->codInstrumento)->first();
            $instrumento->idEstado = $estado->idEstado;
            $instrumento->ubicacion = "En bodega";
            $instrumento->save();
            $this->notificarContrato($contrato,'CANCELAR');//se notifica a los admins

            // return redirect('/contratos')->with('success', '¡Contrato cancelado con éxito!');
            return true;
        }
        catch (\Throwable $th) {
            throw $th;            
        }
    }
    public function terminarContrato($id){
        try{
            $contrato = Contrato::where('idContrato', '=', $id)->first();
            $contrato->estado = 'T';
            $contrato->save();
            $estado =  Estado::where('estado','like','Libre')->first();

            $instrumento = Instrumento::where('codInstrumento', '=',  $contrato->codInstrumento)->first();
            $instrumento->idEstado = $estado->idEstado;
            $instrumento->ubicacion = "En bodega";
            $instrumento->save();
            $this->notificarContrato($contrato,'DAR POR TERMINADO');//se notifica a los admins
            return redirect('/contratos')->with('success', '¡Contrato cancelado con éxito!');
        }
        catch (\Throwable $th) {
            throw $th;
        }
    }

    public function actualizarContrato(Request $req){
        try{
            $contrato = Contrato::where('idContrato', '=', $req->idContrato)->first();
            $contrato->observaciones = $req->txt_observaciones;
            $contrato->save();
            return redirect('/contratos/detalle/'.$req->idContrato)->with('success', '¡Información editada con éxito!');
        }
        catch (\Throwable $th) {
            //throw $th;
            return back()->with('error', '¡Ha ocurrido un error interno!');
        }
    }

    public function agregarContrato(){
        try{
        return view('contratos.agregarContrato');
    }
    catch (\Throwable $th) {
        //throw $th;
        return back()->with('error', '¡Ha ocurrido un error interno!');
    }
    }

    public function guardarContrato(Request $req){
        try{
            $contrato = new Contrato;
            
            $req->validate([
                'txt_fechadeinicio' => 'required',
                'txt_fechafinalizacion' => 'required',
                'txt_cod_instrumento' => 'required',
                'txt_cedula_estudiante' => 'required',
                'cmb_encargado_fiador' => 'required', 
            ]);                                

            $contrato->cedulaEstudiante = $req->txt_cedula_estudiante;
                
            // Borramos los posibles espacios en blanco de la cedula del estudiante
            $contrato->cedulaEstudiante = preg_replace('/\s+/', '', $contrato->cedulaEstudiante);
            
            $contrato->codInstrumento = $req->txt_cod_instrumento;
            $contrato->fechaInicio = $req->txt_fechadeinicio;
            $contrato->fechaFinalizacion = $req->txt_fechafinalizacion;

            $contrato->PresidenteAsociacion = Persona::where('tipoPersona','=','Pre')->first()->cedula;

            $contrato->estado = 'A';

            
        if($req->cmb_encargado_fiador=="F"){
            $req->validate([
                'txt_cedula_fiador' => 'required',
                'txt_nombre_fiador' => 'required',
                'txt_email_fiador' => 'required',
                'txt_telefono_fiador' => 'required', 
            ]);
            $contrato->cedulaFiador = $req->txt_cedula_fiador;
            $contrato->emailFiador = $req->txt_email_fiador;
            $contrato->nombreCompletoFiador = $req->txt_nombre_fiador;
            $contrato->telefonoFiador = $req->txt_telefono_fiador;

        }else{
            $contrato->cedulaEncargado = $req->cmb_encargado_fiador;
        }

        $contrato->save();
        
        $estado =  Estado::where('estado','like','En contrato')->first();
        
        
        $instrumento = Instrumento::where('codInstrumento', '=',  $req->txt_cod_instrumento)->first();
        
        $instrumento->idEstado = $estado->idEstado;
        
        $instrumento->ubicacion = $contrato->estudiante->nombreCompleto;

        $instrumento->save();

        $this->notificarContrato($contrato,'CREAR');//se notifica a los admins

        return redirect('/contratos/detalle/'.$contrato->idContrato)->with('success', '¡Contrato agregado correctamente!');

       }
       catch (\Throwable $th) {
            throw $th;
            // return back()->with('error', '¡Ha ocurrido un error interno!');
        }
    }

    public function buscarEstudiante(Request $req){
        try{
            return Persona::where('cedula', '=', $req->txt_cedula_estudiante)->where('tipoPersona','Est')->with('encargados')->first();
            // $estudiante = null;
            // $contrato = Contrato::where('cedulaEstudiante',$req->txt_cedula_estudiante)->where('estado','A')->first();
            // if($contrato == null){
            //     $estudiante = Persona::where('cedula', '=', $req->txt_cedula_estudiante)->where('tipoPersona','Est')->with('encargados')->first();
            // }
            // return $estudiante;
            
        }
        catch (\Throwable $th) {
            throw $th;
        }
    }
    public function buscarInstrumento(Request $req){
        try{
            $libre = Estado::where('estado','like','Libre')->first()->idEstado;
            $nuevo = Estado::where('estado','like','Nuevo Ingreso')->first()->idEstado;
            $bodega = Estado::where('estado','like','En bodega, buen estado')->first()->idEstado;
            
            $instrumento = Instrumento::where('codInstrumento', $req->txt_cod_instrumento)->where(function ($query) use ($libre,$nuevo,$bodega) {
                $query->where('idEstado',$libre)
                ->orWhere('idEstado',$nuevo)
                ->orWhere('idEstado',$bodega);
            })->first();
            
            return $instrumento;
        }
        catch (\Throwable $th) {
            throw $th;
        }
    }

    // Metodo para notificar a la persona responsable y adminsitradores de su respectivo contrato por email
    public function notificarContrato($contrato,$accion){
        try {
            
           
            $admins = Usuario::where('rolAcceso','A')->where('estado','A')->with('persona')->get(); //Admins de Sinfoinventarios            

            foreach ($admins as $user) {
                if($user->persona->email != null){
                    Mail::to($user->persona->email)->send(new notificarAdminsContrato($contrato,$accion)); // Notificamos a cada admin del sistema
                }
            }   
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function descargarContrato($id){
        try{
            $contrato= Contrato::where('idContrato', '=', $id)->with('estudiante','encargado','instrumento')->first();
           
            $numeros = ['1','2','3','4','5','6','7','8','9','0'];
            $remplazo_numero =[' uno',' dos',' tres',' cuatro',' cinco',' seis',' siete',' ocho',' nueve',' cero'];
            $mes = ['-01-','-02-','-03-','-04-','-05-','-06-','-07-','-08-','-09-','-10-','-11-','-12-'];
            $remplazo_mes =[' DE ENERO DEL ',' DE FEBRERO DEL ',' DE MARZO DEL ',' DE ABRIL DEL ',' DE MAYO DEL ',' DE JUNIO DEL ',' DE JULIO DEL ',' DE AGOSTO DEL ',' DE SEPTIEMBRE DEL ',' DE OCTUBRE DEL ',' DE NOVIEMBRE DEL ',' DE DICIEMBRE DEL '];
            //cambia el número de cédula a prosa
            for($j=0; $j<15; $j++){//recorre las cedulas
            for($i=0;$i<10;$i++){// recorre los array con los dato para remplazar
                $contrato->estudiante->cedula = Str::replaceFirst($numeros[$i],$remplazo_numero[$i],$contrato->estudiante->cedula);

                if($contrato->cedulaEncargado)
                $contrato->encargado->cedula = Str::replaceFirst($numeros[$i],$remplazo_numero[$i],$contrato->encargado->cedula);

                if($contrato->cedulaFiador)
                $contrato->cedulaFiador = Str::replaceFirst($numeros[$i],$remplazo_numero[$i],$contrato->cedulaFiador);

                if($contrato->PresidenteAsociacion)
               $contrato->presidente->cedula = Str::replaceFirst($numeros[$i],$remplazo_numero[$i],$contrato->presidente->cedula);
          }
        }
        $anno = Str::substr($contrato->fechaInicio,0,4);//obtiene el año
        //le da formato a las fechas
        $contrato->fechaFinalizacion = Carbon::parse($contrato->fechaFinalizacion)->format('d-m-Y');
        $contrato->fechaInicio = Carbon::parse($contrato->fechaInicio)->format('d-m-Y');
        //cambia el mes n las fechas a prosa
        for($i=0;$i<12;$i++){
            $contrato->fechaFinalizacion = Str::replaceFirst($mes[$i],$remplazo_mes[$i],$contrato->fechaFinalizacion);

            
            $contrato->fechaInicio = Str::replaceFirst($mes[$i],$remplazo_mes[$i],$contrato->fechaInicio);

      }

            return view('contratos.plantillaContrato', compact('contrato','anno'));
        }
        catch (\Throwable $th) {
           throw $th;            
        }
        
    }
}
