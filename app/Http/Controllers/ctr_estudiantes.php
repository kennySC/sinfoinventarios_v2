<?php

namespace App\Http\Controllers;

use App\Models\EstudiantesEncargados;
use Illuminate\Http\Request;
use App\Models\Persona;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

use function PHPUnit\Framework\isNull;


class ctr_estudiantes extends Controller
{
    public function __invoke()
    {
        try {
            $estudiantes = Persona::where('tipoPersona', 'Est')->where('estadoPersona', 'A')->with('encargados')->paginate(20);
            $paginar = true;
            return view('estudiantes.estudiantes', compact('estudiantes', 'paginar'));
            // return $estudiantes;
        } catch (\Throwable $th) {
            // throw $th;
            return back()->with('error', '¡Ha ocurrido un error interno!');
        }        
    }

    // Metodo para filtrar los estudiantes por nombre completo, por rol de acceso, por estado y por cualquier combinacion de los mismos!!
    public function filtrarEstudiantes(Request $req) {
        try {
            $paginar = false;
            $estudiantes = Persona::where('nombreCompleto', 'like', '%' . $req->txt_nombre . '%')
                                ->where('cedula', 'like', '%' . $req->txt_cedula . '%')
                                ->where('tipoPersona', 'Est')
                                ->where('estadoPersona', 'A')
                                ->with('encargados')->paginate(20);

            return view('estudiantes.estudiantes', compact('estudiantes', 'paginar'))->render();
        } catch (\Throwable $th) {
            throw $th;
        }                
    }

    public function actualizarListaEstudiantes()
    {
        try {
            $page = 1;
            $elements = 50;
            $flag = true;
            $periodo = date("Y");

            $affected = DB::table('personas')
                                    ->where('tipoPersona', 'Est')
                                    ->update(['estadoPersona' => 'I']);

            while ($flag) {

                $query = <<<GQL
                query {
                    estudiantes(first: $elements, page: $page, yearPeriodo: $periodo) {
                        data{
                            id
                            identificacion
                            nombre_completo
                            email
                            movil    
                            padre {
                                id
                                identificacion
                                nombre
                                email
                                movil
                            }
                            madre {
                                id
                                identificacion
                                nombre
                                email
                                movil
                            }
                        }
                    }
                }
                GQL;

                $response = Http::withHeaders([
                    'Content-Type' => 'application/json',
                    "Authorization" => "Bearer 4aDLYslXEjUOyKG3apIxRnngyhw5fSeqc2bX8Al1TZxzYytP7I65fEgaikJdXkW3rVdjbsoHkeOCKKz6eBFpywpl6NqxRs75LRx5JldGg1j8T0C5O5hqNjzXo5lL27R2"
                ])->post('https://sinfonicapz.developancrixcampus.site/graphql', [
                    'query' => $query
                ]);

                // dump($response->json());

                

                $estudiantes = $response->json()['data']['estudiantes']['data'];
                
                if ($estudiantes != null) {                                                      
                    
                    foreach ($estudiantes as $est) {
                        // Procedemos a guardar la informaicon de los estudiantes de esta paginacion en la bd
                        $estudiante = new Persona();

                        $cedEstudiante = $est['identificacion'];

                        $this->setDatosPersona($estudiante, $est, 'Est');                    

                        if ($est['padre'] != null && $est['padre']['identificacion'] != null) {
                            $encargado = new Persona();

                            $this->setDatosPersona($encargado, $est['padre'], 'Enc');     
                            
                            $this->guardarEncargado($cedEstudiante, $est['padre']['identificacion']);

                        }

                        if ($est['madre'] != null && $est['madre']['identificacion'] != null) {
                            $encargado = new Persona();

                            $this->setDatosPersona($encargado, $est['madre'], 'Enc');                                            

                            $this->guardarEncargado($cedEstudiante, $est['madre']['identificacion']);

                        }

                    }           
                    $page = $page + 1;   
                } else {
                    // Si no tenemos estudiantes presentes significa que llegamos a la ultima pagina
                    // Entonces nos detenemos
                    $flag = false;
                }                               

            }
            return true;
        } catch (\Throwable $th) {
            
            $affected = DB::table('personas')
                                    ->where('tipoPersona', 'Est')
                                    ->update(['estadoPersona' => 'A']);

            throw $th;                    
        }        
    }


    public function setDatosPersona($persona, $datos, $tipoPers) {
        $persExiste = Persona::where('cedula', '=', $datos['identificacion'])->first();        
        if($persExiste == null) {

            if ($tipoPers == 'Est') {
                $persona->nombreCompleto = $datos['nombre_completo'];
            } else {
                $persona->nombreCompleto = $datos['nombre'];
            }   

            $persona->cedula = $datos['identificacion'];

            // hacemos un trim de la cedula, para que solo sean los numeros
            $persona->cedula = str_replace(' ', '', $persona->cedula);
            $persona->cedula = str_replace('-', '', $persona->cedula);
            $persona->cedula = preg_replace('/[^A-Za-z0-9\-]/', '', $persona->cedula); // Removes special chars.

            $persona->email = $datos['email'];
            if (strlen($datos['movil']) > 16) {
                $persona->telefono = substr($datos['movil'], 0, 16);
            } else {
                $persona->telefono = $datos['movil'];
            }
            $persona->tipoPersona = $tipoPers;      
            
            $persona->estadoPersona = 'A';
    
            try {
                $persona->save();
            } catch (\Throwable $th) {
                //throw $th;
            }
        } else {
            $persExiste->estadoPersona = 'A';
            $persExiste->save();
        }
    }

    public function guardarEncargado($cedEstudiante, $cedEncargado) {

        $cedEstudiante = str_replace(' ', '', $cedEstudiante);
        $cedEstudiante = str_replace('-', '', $cedEstudiante);
        $cedEstudiante = preg_replace('/[^A-Za-z0-9\-]/', '', $cedEstudiante); // Removes special chars.

        $cedEncargado = str_replace(' ', '', $cedEncargado);
        $cedEncargado = str_replace('-', '', $cedEncargado);
        $cedEncargado = preg_replace('/[^A-Za-z0-9\-]/', '', $cedEncargado); // Removes special chars.

        if (!EstudiantesEncargados::where('cedulaEstudiante', $cedEstudiante)
                        ->where('cedulaEncargado', $cedEncargado)->exists()) {

            $EstEnc = new EstudiantesEncargados();
            $EstEnc->cedulaEstudiante = $cedEstudiante;
            $EstEnc->cedulaEncargado = $cedEncargado;
            $EstEnc->save();

        }
    }

}
