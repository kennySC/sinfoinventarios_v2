<?php

namespace App\Http\Controllers;

use App\Models\Instrumento;
use App\Models\Activo;
use App\Models\FamiliaInstrumento;
use App\Models\TipoInstrumento;
use App\Models\Institucion;
use App\Models\TipoActivo;
use App\Models\Estado;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use function PHPUnit\Framework\isNull;

class ctr_instrumentos extends Controller
{
    public function __invoke(){
        $instrumentos = Instrumento::with('tipoInstrumento')->with('estado')->with('institucion')->paginate(10);
        $paginar = true;
        $institucion = Institucion::All();
        $tipo = TipoInstrumento::All();
        $estado = Estado::All();
        return view('instrumentos.instrumentos', compact('instrumentos','tipo','institucion','estado','paginar'));
    }

    // Metodo para abrir la pantalla de agregar instrumentos
    public function agregarInstrumento() {
        try{
            $institucion = Institucion::All();
            $tipo = TipoInstrumento::All();
            $estado_activo = Estado::All();
            return view('instrumentos.agregarInstrumento', compact('tipo','institucion','estado_activo'));
        }
        catch (\Throwable $th) {
            return back()->with('error', '¡Ha ocurrido un error interno!');
        }
    }

    // Metodo para guardar un nuevo instrumento
    public function guardarInstrumento(Request $req) {
        try{            
        
            $req->validate([
                'txt_codigo' => 'required',
                'cmb_tipo' => 'required',
                'txt_numero' => 'required',
                'txt_nombre' => 'required',
                'cmb_institucion' => 'required',
                'cmb_estado' => 'required',
            ]);
            
            $instrumento = new Instrumento;
            
            $instrumento->codInstrumento = $req->txt_codigo;
            $instrumento->codTipoInstrumento = $req->cmb_tipo;
            $instrumento->numeroInstrumento = $req->txt_numero; 
            $instrumento->nombre = $req->txt_nombre;
            $instrumento->marca = $req->txt_marca;
            $instrumento->modelo = $req->txt_modelo;
            $instrumento->serie = $req->txt_serie;
            $instrumento->observaciones = $req->txt_observaciones;
            $instrumento->valor = $req->txt_valor;
            $instrumento->ubicacion = $req->txt_ubicacion;
            $instrumento->idInstitucion = $req->cmb_institucion;
            $instrumento->responsable = $req->txt_responsable;
            $instrumento->idEstado = $req->cmb_estado;

            $instrumento->fotoInstrumento = $this->guardarIMG($req);

            $instrumento->save();

            return redirect('/instrumentos')->with('success', '¡Instrumento agregado correctamente!');
        }
        catch (\Throwable $th) {
            return back()->with('error', '¡Ha ocurrido un error interno!');
        }
    }

    // Metodo para pasar a la vista donde se edita/actualiza la informacion de un instrumento
    public function editarInstrumento($id) {
        try{
            $instrumento = Instrumento::where('codInstrumento', $id)->first();
            $estado_activo = Estado::All();
            $institucion = Institucion::All();
            $tipo = TipoInstrumento::All();
            
            if(is_null($instrumento)) {                
                return redirect('/instrumentos')->with('error', 'Instrumento no encontrado');
            }
            else{
                return view('instrumentos.editarInstrumento', compact('instrumento','tipo','institucion','estado_activo'));
            }
        }
        catch (\Throwable $th) {
            return back()->with('error', '¡Ha ocurrido un error interno!');
        }
    }

    // Metodo para actualizar la informacion de un instrumento
    public function actualizarInstrumento(Request $req) {
        try {

            $req->validate([
                'txt_codigo' => 'required',
                'cmb_tipo' => 'required',
                'txt_nombre' => 'required',
                'cmb_institucion' => 'required',
                'cmb_estado' => 'required',
            ]);
        
            $instrumento = Instrumento::where('codInstrumento',$req->txt_codOriginal)->first();                

            $instrumento->codInstrumento = $req->txt_codigo;
            $instrumento->codTipoInstrumento = $req->cmb_tipo;
            $instrumento->numeroInstrumento = $req->txt_numero; 
            $instrumento->nombre = $req->txt_nombre;
            $instrumento->marca = $req->txt_marca;
            $instrumento->modelo = $req->txt_modelo;
            $instrumento->serie = $req->txt_serie;
            $instrumento->observaciones = $req->txt_observaciones;
            $instrumento->valor = $req->txt_valor;
            $instrumento->ubicacion = $req->txt_ubicacion;
            $instrumento->idInstitucion = $req->cmb_institucion;
            $instrumento->responsable = $req->txt_responsable;
            $instrumento->idEstado = $req->cmb_estado;
    
            if ($req->file_imgInstrumento != null || $req->borrarOriginal == 1) {                
                Storage::disk('public')->delete($instrumento->fotoInstrumento);
                $instrumento->fotoInstrumento = $this->guardarIMG($req);
            }

            $instrumento->save();
    
            return redirect ('/instrumentos')->with('success', '¡Instrumento editado correctamente!');
        } 
        catch (\Throwable $th) {
            return back()->with('error', '¡Ha ocurrido un error interno!');
        }
    }

    public function bajarInstrumento($codigo_instrumento) {
        try {
            
            
            $estado =  Estado::where('estado','like','Dado de baja')->first();
        
            $instrumento = Instrumento::where('codInstrumento', '=',  $codigo_instrumento)->first();
            $instrumento->idEstado = $estado->idEstado;
            
            $instrumento->save();
            
            return redirect ('/instrumentos')->with('success', '¡Instrumento dado de baja con éxito!');
        } 
        catch (\Throwable $th) {
            throw $th;
        }
    }
    public function activarInstrumento($codigo_instrumento) {
        try{
            $estado =  Estado::where('estado','like','En uso')->first();

            $instrumento = Instrumento::where('codInstrumento', '=',  $codigo_instrumento)->first();
            $instrumento->idEstado = $estado->idEstado;
            
            $instrumento->save();

            return redirect('/instrumentos')->with('success', '¡El instrumento fue activado correctamente!');
        }
        catch (\Throwable $th) {
            throw $th;
        }
    }

    // Metodo para pasar a la vista/pantalla para recibir instrumentos
    public function recepcionInstrumento(){
        try{
            $instrumentos = Instrumento::with('activo')->get();
 
            $familia = FamiliaInstrumento::All();
            $institucion = Institucion::All();
            $tipo = TipoInstrumento::All();
            $estado_activo = Estado::All();
            return view('instrumentos.recepcionInstrumento', compact('instrumentos','familia','tipo','institucion','estado_activo'));
        }
        catch (\Throwable $th) {
            throw $th;
        }
    }

    public function filtrarInstrumentos(Request $req) {
        
        try{
            $instrumentos = Instrumento::where('codInstrumento','like','%'.$req->txt_cod_instrumento.'%')
                                            ->when($req->cmb_tipo_instrumentos !='T', function($query) use($req) {
                                                return $query->where('codTipoInstrumento',$req->cmb_tipo_instrumentos);
                                            })  
                                            ->when($req->cmb_estado !='T', function($query) use($req) {
                                                return $query->where('idEstado',$req->cmb_estado);
                                            })
                                            ->when($req->cmb_institucion_instrumentos !='T', function($query) use($req) {
                                                return $query->where('idInstitucion',$req->cmb_institucion_instrumentos);
                                            })
                                            ->with('tipoInstrumento')->with('estado')->with('institucion')->paginate(10);

            $institucion = Institucion::All();
            $tipo = TipoInstrumento::All();
            $estado = Estado::All();
            return view('instrumentos.instrumentos', compact('instrumentos','tipo','institucion', 'estado'));
        }
        catch (\Throwable $th) {
            throw $th;
        }
    }

    public function ultimoIstrumentoPortipo($tipoInst) {
        try {
            
            $Instrumento = Instrumento::where('codTipoInstrumento', $tipoInst)->max('numeroInstrumento');

            return $Instrumento + 1;

        } catch (\Throwable $th) {
            throw $th;
        }
    }

    // Metodo para abrir un input file y seleccionar una imagen para un activo   
    private function guardarIMG(Request $req) {        
        if ($req->hasFile('file_imgInstrumento')) {            
            if ($req->file('file_imgInstrumento')->isValid()) {                
                $extension = $req->file_imgInstrumento->extension();
                $pathIMG = $req->file_imgInstrumento->storeAs(FOTOS_INSTRUMENTOS, $req->txt_codigo . '.' . $extension, 'public');
                return $pathIMG;
            }
        } else {
            return null;
        }
    }
}
