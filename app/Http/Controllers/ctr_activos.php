<?php

namespace App\Http\Controllers;

use App\Models\Activo;
use App\Models\Institucion;
use App\Models\Estado;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use function PHPUnit\Framework\isNull;

class ctr_activos extends Controller
{
    public function __invoke()
    {
        try {
            $estado_activo = Estado::All();
            $activos = Activo::with('institucion')->paginate(10);
            $paginar = true;
            $instituciones = Institucion::All();
            return view('activos.activos', compact('activos', 'instituciones', 'estado_activo','paginar'));
        } 
        catch (\Throwable $th) {
            return back()->with('error', 'Ocurrió un error interno, porfavor intente de nuevo');
        }
    }

    // Metodo para abrir la pantalla de agregar activos
    public function agregarActivo() {
        try {
            $instituciones = Institucion::All();
            $estado_activo = Estado::All();
            return view('activos.agregarActivo', compact('instituciones','estado_activo'));
        } 
        catch (\Throwable $th) {
            // throw $th;
            return back()->with('error', 'Ocurrió un error interno, porfavor intente de nuevo');
        }
    }
    
    /* Metodo para guardar un nuevo activo */
    public function guardarActivo(Request $req) {
        try{
            // Solo validamos que el activo por agregar lleve un codigo, los demas campos pueden ir vacios (Cosas de la Sinfonica no principios o ideales nuestros XD)
            $req->validate([
                'txt_cod_activo' => 'required',  
                'txt_nom_activo' => 'required'              
            ]);
    
            $activo = new Activo;
            $activo->codActivo = $req->txt_cod_activo;
            $activo->nombre = $req->txt_nom_activo;
            $activo->marca = $req->txt_marca_activo; 
            $activo->modelo = $req->txt_modelo_activo;
            $activo->serie = $req->txt_serie_activo;
            $activo->ubicacion = $req->txt_ubicacion_activo;
            $activo->valor = $req->txt_valor_activo;
            $activo->responsable = $req->txt_resp_activo;
            $activo->idInstitucion = $req->cmb_institucion;
            $activo->idEstado = $req->cmb_estado;
            $activo->observaciones = $req->txt_observ_activo;
    
            $activo->fotoActivo = $this->guardarIMG($req);
                
            $activo->save();

            // Registramos la accion de guardar un nuevo activo, la cual es la accion 11
            ctr_registroAcciones::registrarAccion($activo->codActivo, 11);

            return redirect ('/activos')->with('success', '¡Activo guardado con éxito!');
        }
        catch (\Throwable $th) {
            // throw $th;
            return back()->with('error', 'Ocurrió un error interno, porfavor intente de nuevo');
        }
    }

    // Metodo al dar al boton de EDITAR en un activo, es decir, con el cual se pasa a la vista de editar un activo //
    public function editarActivo($id) {
        try{
            $activo = Activo::where('codActivo', '=', $id)->first();
            $instituciones = Institucion::All();
            $estado_activo = Estado::All();
            if(is_null($activo)) {
                return view('activos.activos')->with('error', '¡Activo no encontrado!');
            }
            else{
                return view('activos.editarActivo', compact('activo', 'instituciones', 'estado_activo'));
            }
        }
        catch (\Throwable $th) {
            // throw $th;
            return back()->with('error', 'Ocurrió un error interno, porfavor intente de nuevo');
        }
    }

    // Metodo para ACTUALIZAR/EDITAR la informacion del activo como tal, es decir un update de la informacion del activo correspondiente //
    public function actualizarActivo(Request $req) {                
        try{

            // return $req;
    
            $activo = Activo::where('codActivo', '=', $req->txt_cod_activo)->first();
            
            $activo->nombre = $req->txt_nom_activo;
            $activo->marca = $req->txt_marca_activo; 
            $activo->modelo = $req->txt_modelo_activo;
            $activo->serie = $req->txt_serie_activo;
            $activo->ubicacion = $req->txt_ubicacion_activo;
            $activo->valor = $req->txt_valor_activo;
            $activo->responsable = $req->txt_resp_activo;
            $activo->idInstitucion = $req->cmb_institucion;
            $activo->idEstado = $req->cmb_estado;
            $activo->observaciones = $req->txt_observ_activo;
    
            if ($req->file_imgActivo != null || $req->borrarOriginal == 1) {                
                Storage::disk('public')->delete($activo->fotoActivo);
                $activo->fotoActivo = $this->guardarIMG($req);
            }
    
            $activo->save();

            // Registramos la accion de editar un activo, la cual es la accion 12
            ctr_registroAcciones::registrarAccion($activo->codActivo, 12);
    
            return redirect ('/activos')->with('success', '¡Activo actulizado con éxito!');
        }
        catch (\Throwable $th) {
            // throw $th;
            return back()->with('error', 'Ocurrió un error interno, porfavor intente de nuevo');
        }
    }

    // Metodo para dar una activo de baja, es decir, cambiar  su estado a dado de baja
    public function bajaActivo($codigo_activo) {
        try{
            $activo = Activo::where('codActivo', '=', $codigo_activo)->first();
            $activo->idEstado = 8;
            
            $activo->save();

            // Registramos la accion de dar de baja un activo, la cual es la accion 13
            ctr_registroAcciones::registrarAccion($activo->codActivo, 13);

            return redirect ('/activos')->with('success', '¡Activo dado de baja con éxito!');
        }
        catch (\Throwable $th) {
            throw $th;            
        }
    }

    // Metodo para activar un activo que se encontraba dado de baja, procede a quedar con un ESTADO LIBRE
    public function activarActivo($codigo_activo) {

        try{
            $activo = Activo::where('codActivo', '=', $codigo_activo)->first();
            $activo->idEstado = 1; // Le ponemos un estado libre al activo
            $activo->save();

            // Registramos la accion de re activar un activo, la cual es la accion 22
            ctr_registroAcciones::registrarAccion($activo->codActivo, 22);
            
            return redirect('/activos')->with('success', '¡El activo fue activado correctamente!');
        }
        catch (\Throwable $th) {
            throw $th;            
        }

    }

    // Metodo para abrir un input file y seleccionar una imagen para un activo   
    private function guardarIMG(Request $req) {        
        if ($req->hasFile('file_imgActivo')) {            
            if ($req->file('file_imgActivo')->isValid()) {                
                $extension = $req->file_imgActivo->extension();
                $pathIMG = $req->file_imgActivo->storeAs(FOTOS_ACTIVOS, $req->txt_cod_activo . '.' . $extension, 'public');
                return $pathIMG;
            }
        } else {
            return null;
        }
    }

    public function filtrarActivos(Request $req){        
        try{
            $paginar = false;
            $activos = Activo::where('nombre','like','%'.$req->txt_nom_activo.'%')
                                ->where('codActivo','like','%'.$req->txt_cod_activo.'%')
                                ->when($req->cmb_institucion != 'T', function($query) use ($req) {
                                    return $query->where('idInstitucion','=',$req->cmb_institucion);
                                })
                                ->when($req->cmb_estado != 'T', function($query) use ($req) {
                                    return $query->where('idEstado','=',$req->cmb_estado);
                                })
                                ->paginate(10);

            $instituciones = Institucion::All();
            $estado_activo = Estado::All();
            return view('activos.activos', compact('activos', 'instituciones', 'estado_activo','paginar'));
        }
        catch (\Throwable $th) {
            throw $th;            
        }
    }
        
    // Metodo para popular la tabla de los activos a seleccionar
    public function listarActivos () {
        try {
            $id_estado = Estado::where('estado', '=', 'En boleta de salida')->first()->idEstado;
            $activos = Activo::where('idEstado', '<>', $id_estado)->get();
            return $activos;
        } catch (\Throwable $th) {
            throw $th;
        }
    }

}
