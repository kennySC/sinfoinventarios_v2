<?php


namespace App\Http\Controllers;


use App\Models\Persona;
use App\Models\Usuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Crypt;

require('constantes.php');

use function PHPUnit\Framework\isNull;

class ctr_usuarios extends Controller
{
    public function __invoke() {
        try {
            $usuarios = Usuario::with('persona')->paginate(10);         
            return view('usuarios.usuarios', compact('usuarios'));
        } catch (\Throwable $th) {
            // throw $th;
            return back()->with('error', '¡Ha ocurrido un error interno!');
        }
    }

    // Metodo para abrir la pantalla de agregar usuarios
    public function agregarUsuario() {
        return view('usuarios.agregarUsuario');
    }

    // Metodo para guardar un usuario nuevo
    public function guardarUsuario(Request $req) {        

        $req->validate([
            'txt_ced' => 'required',
            'txt_nombreUs' => 'required',
            'txt_password' => 'required',
            'txt_nombre' => 'required',
            'txt_email' => 'required'
        ]);

        try {
            $persona = new Persona();
            $usuario = new Usuario();

            $persona->cedula = $req->txt_ced;
            $persona->nombreCompleto = $req->txt_nombre;
            $persona->tipoPersona = 'Us';        
            $persona->email = $req->txt_email;         
            
            $usuario->cedulaPersona = $req->txt_ced;
            $usuario->nombreUsuario = $req->txt_nombreUs;
            $usuario->contraseña = Crypt::encryptString($req->txt_password);
            $usuario->rolAcceso = $req->cmb_rol;
            $usuario->estado = 'A';
            
            $usuario->fotoUsuario=$this->guardarIMG($req);

            $persona->save();   
            $usuario->save();

            // Registramos la accion de añadir usuario, la cual es la accion 17
            ctr_registroAcciones::registrarAccion($usuario->idUsuario, 17);
            
            return redirect('/usuarios')->with('success', '¡Usuario agregado correctamente!');
        } catch (\Throwable $th) {
            // throw $th;
            return back()->with('error', '¡Ha ocurrido un error interno!');
        }

    }

    private function guardarIMG(Request $req) {        
        if ($req->hasFile('file_imgUsuario')) {            
            if ($req->file('file_imgUsuario')->isValid()) {                
                $extension = $req->file_imgUsuario->extension();
                $pathIMG = $req->file_imgUsuario->storeAs(FOTOS_USUARIOS, $req->txt_nombreUs . '.' . $extension, 'public');
                return $pathIMG;
            }
        } else {
            return null;
        }
    }

    // Metodo para filtrar los usuarios por nombre completo, por rol de acceso, por estado y por cualquier combinacion de los mismos!!
    public function filtrarUsuarios(Request $req) {

        try {
            $rol = $req->cmb_rol;
            $estado = $req->cmb_estado;        

            $usuarios = Usuario::where(function ($query) {
                                        $query->select('nombreCompleto')
                                        ->from('personas')
                                        ->whereColumn('personas.cedula', 'usuarios.cedulaPersona')
                                        ->OrderByDesc('personas.cedula')
                                        ->limit(1);
                                }, 'like', '%'. $req->txt_buscarNom .'%')

                                ->when($req->cmb_rol != 'T', function($query) use ($rol) {
                                    return $query->where('rolAcceso', '=', $rol);
                                })
                                ->when($req->cmb_estado != 'T', function($query) use ($estado) {
                                    return $query->where('estado', '=', $estado);
                                })        

                                ->with('persona')->simplePaginate(10);

            $paginar = false;            

            return view('usuarios.usuarios', compact('usuarios', 'paginar'))->render();

        } catch (\Throwable $th) {
            throw $th;
            // return back()->with('error', '¡Ha ocurrido un error interno!');
        }

    }

    // Metodo para abrir la pantalla de editar usuarios
    public function editarUsuario($id) {

        try {
            $usuario = Usuario::with('persona')->where('idUsuario', '=', $id)->first();
            
            $usuario->contraseña = Crypt::decryptString($usuario->contraseña);

            if(is_null($usuario)) {
                return view('usuarios.usuarios')->with('error', '¡Usuario no encontrado!');
            } else {
                return view('usuarios.editarUsuario', compact('usuario'));
            }
        } catch (\Throwable $th) {
            // throw $th;
            return back()->with('error', '¡Ha ocurrido un error interno!');
        }
        
    }

    // metodo para actualizar la informacion del usuario editado
    public function actualizarUsuario(Request $req) {        

        if ($req->idUsuario == session('userdata')['userID']) {
            $req->validate([
                'txt_ced' => 'required',
                'txt_nombreUs' => 'required',
                'txt_password' => 'required',
                'txt_nombre' => 'required',
            ]);   
        } else {
            $req->validate([
                'txt_ced' => 'required',
                'txt_nombreUs' => 'required',
                'txt_nombre' => 'required',
                'txt_email' => 'required'
            ]);
        }  


        try {
            $usuario = Usuario::with('persona')->where('idUsuario', '=', $req->idUsuario)->first();        
            $persona = Persona::where('cedula', '=', $usuario->cedulaPersona)->first();  

            $usuario->nombreUsuario = $req->txt_nombreUs;

            if ($req->idUsuario == session('userdata')['userID']) { 
                $usuario->contraseña = Crypt::encryptString($req->txt_password);
            }
            
            $usuario->rolAcceso = $req->cmb_rol;
                                                                        
            $persona->nombreCompleto = $req->txt_nombre;    

            $persona->email = $req->txt_email;
            
            if ($req->file_imgUsuario != null || $req->borrarOriginal == 1) {                
                Storage::disk('public')->delete($usuario->fotoUsuario);
                $usuario->fotoUsuario=$this->guardarIMG($req);
            }

            $usuario->save();
            $persona->save();
            
            return redirect('/usuarios')->with('success', '¡Usuario editado correctamente!');
        } catch (\Throwable $th) {
            // throw $th;
            return back()->with('error', '¡Ha ocurrido un error interno!');
        }
    }    

    // Metodo para dar de baja un usuario
    public function bajaUsuario($id) {

        try {
            if ($id != session('userdata')['userID']) {

                $usuario = Usuario::with('persona')->where('idUsuario', '=', $id)->first();
                
                $usuario->estado = 'I';
                $usuario->save();
    
                // Registramos la accion de dar de baja el usuario, la cual es la accion 19
                ctr_registroAcciones::registrarAccion($usuario->idUsuario, 19);
    
                return redirect('/usuarios')->with('success', '¡Usuario dado de baja correctamente!');            
    
            } else {
                die(header('HTTP/1.0 404 Not Found'));
            }
        } catch (\Throwable $th) {
            throw $th;
        }

    }

    // Metodo para activar un usuario
    public function activarUsuario($id) {

        try {
            $usuario = Usuario::with('persona')->where('idUsuario', '=', $id)->first();
            $usuario->estado = 'A';
            $usuario->save();

            // Registramos la accion de reactivar el usuario, la cual es la accion 23
            ctr_registroAcciones::registrarAccion($usuario->idUsuario, 23);

            return redirect('/usuarios')->with('success', '¡Usuario activado correctamente!');
        } catch (\Throwable $th) {
            throw $th;
        }

    }

    //  Metodo para obtener la informacion del presidente de la asociacion
    public function getPresidente () {
        try {
            // Obtenemos y retornamos al presidente, en js veremos si es null o no
            $presidente = Persona::where('tipoPersona', '=', 'Pre')->first();
            return $presidente;
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    //  Metodo para guardar la informacion del presidente de la asociacion
    public function savePresidente (Request $req) {
        try {
            
            $presidente = Persona::where('tipoPersona', '=', 'Pre')->first();
            if (!is_null($presidente)) {
                $presidente->nombreCompleto = $req->nombrePre;
                $presidente->cedula = $req->cedulaPre;
                $presidente->save();
            } else {
                $presidente = new Persona();
                $presidente->nombreCompleto = $req->nombrePre;
                $presidente->cedula = $req->cedulaPre;
                $presidente->genero = $req->generoPre;
                $presidente->tipoPersona = 'Pre';
                $presidente->email = $req->emailPre;
                $presidente->telefono = $req->telefonoPre;
                $presidente->save();
            }         

        } catch (\Throwable $th) {
            throw $th;
        }
    }

}




