<?php

namespace App\Http\Controllers;

use App\Mail\notificarAdminsBoleta;
use App\Mail\notificarPersonaBoleta;
use App\Mail\notifPersonaBltFin;
use App\Mail\notifAdminsBltFin;
use App\Models\Activo;
use App\Models\BoletaSalida;
use App\Models\DetalleBoleta;
use App\Models\Estado;
use App\Models\Usuario;
use Illuminate\Http\Request;
use DateTime;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class ctr_boletas extends Controller
{
    public function __invoke()
    {
        try {
            $boletas = BoletaSalida::with('usuario')->get();
            return view('boletas.boletas', compact('boletas'));
        } catch (\Throwable $th) {
            // throw $th;
            return view('inicio.inicio')->with('error', 'Ocurrió un error interno, porfavor intente de nuevo');
        }
    }

    // Metodo para acceder a la vista/pantalla donde se crearan las boletas de salida
    public function agregarBoleta(){
        try {
            return view('boletas.agregarBoleta');
        } catch (\Throwable $th) {
            // throw $th;
            return back()->with('error', '¡Ha ocurrido un error interno!');
        }
    }

    // Metodo para guardar una nueva boleta de salida
    public function guardarBoleta(Request $req){

        $boleta = new BoletaSalida;

        $req->validate([
            'txt_responsable' => 'required',  
            'txt_fechadevolucion' => 'required',            
            'txt_correoresponsable' => 'required'
        ]);

        try {
            $boleta->idUsuario =session('userdata')['userID'];
            $boleta->fechaDevolucion = $req->txt_fechadevolucion; // La fecha esperada en que sea devuelto el activo
            $boleta->nombreResponsable = $req->txt_responsable;
            $boleta->emailResponsable = $req->txt_correoresponsable;
            $boleta->estado = 'gen'; // Al crear una nueva boleta de salida le mandamos de estado gen porque acaba de ser generada
            $boleta->observaciones = $req->txt_observaciones;

            $boleta->save();

            $id_estado = Estado::where('estado', '=', 'En boleta de salida')->first()->idEstado;
        
            //Hay que recorrer todos los activos que nos lleguen y obtener el codigo de cada uno y guardarlo junto el id de la boleta recien creada
            foreach ($req->activos as $act) {
                $idAct = $act['codActivo'];
                $detalle = new DetalleBoleta;
                $detalle->pk_cod_boleta = $boleta->idBoleta;
                $detalle->pk_cod_activo = $idAct;
                $detalle->save();
                // Obtenemos el activo de la base de datos para cambiarl el estado
                $activo = Activo::where('codActivo', '=', $idAct)->first();
                $activo->idEstado = $id_estado;
                $activo->save();
            }
            
            // return redirect('/boletas/detalle/' . $boleta->idBoleta)->with('success', '¡Boleta creada con éxito!');  
            ctr_registroAcciones::registrarAccion($boleta->idBoleta, 10);
            return $boleta->idBoleta;

        } catch (\Throwable $th) {
            $boleta->delete();            
            // throw $th;
            return back()->with('error', '¡Ha ocurrido un error interno!');
        }

    }

    // Metodo para pasar a la vista del detalle respectivo de la boleta de salida
    public function detalleBoleta($id){
        try {
            $boleta = BoletaSalida::where('idBoleta', '=', $id)->first();
            $detalles = DetalleBoleta::where('pk_cod_boleta', '=', $id)->with('activo')->get(); // Con esto obtenemos del detalle de boleta los activos relacionados a una boleta de salida
            
            return view('boletas.detalleBoleta', compact('boleta', 'detalles'));
        } catch (\Throwable $th) {
            // throw $th;
            return back()->with('error', '¡Ha ocurrido un error interno!');
        }
    }

    // Metodo para cambiar el estado de una boleta de generada a activa es decir que la boleta fue entregada
    public function activarBoleta($id){        
        try {

            $date = new DateTime();
            $boleta = BoletaSalida::where('idBoleta', '=', $id)->first();
            $boleta->fechaEntrega = $date;
            $boleta->estado = 'act'; //Le seteamos el estado a la boleta como activa, dado que ya fue entregada

            $this->notificarBoletaEntregada($id); // Llamamos al metodo para notificar a la persona de la boleta y admins del sistema que se entrego su respectiva boleta

            $boleta->save();
            ctr_registroAcciones::registrarAccion($boleta->idBoleta, 7);
            return true;

        } catch (\Throwable $th) {
            throw $th;
        }
    }

    // Metodo para cambiar el estado de una boleta de activa a devuelta
    public function finalizarBoleta($id){
        try {
            $date = new DateTime();        
            $boleta = BoletaSalida::where('idBoleta', '=', $id)->first();
            $boleta->estado = 'dev';
            $boleta->fechaRealDevolucion = $date;
            $boleta->save();
        
            $detalles = DetalleBoleta::where('pk_cod_boleta', '=', $id)->get();

            $id_estado = Estado::where('estado', '=', 'Libre')->first()->idEstado;

            $this->cambiarEstadoActivos($detalles, $id_estado);
            $this->notificarBoletaFinalizada($id);

            ctr_registroAcciones::registrarAccion($boleta->idBoleta, 9);

            return true;
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    // Metodo para cambiar el estado (puede ser cualquier estado) de una boleta a cancelada
    public function cancelarBoleta($id){
        try {
            $boleta = BoletaSalida::where('idBoleta', '=', $id)->first();
            $boleta->estado = 'can';
            $boleta->save();

            $detalles = DetalleBoleta::where('pk_cod_boleta', '=', $id)->get();

            $id_estado = Estado::where('estado', '=', 'Libre')->first()->idEstado;

            $this->cambiarEstadoActivos($detalles, $id_estado);

            ctr_registroAcciones::registrarAccion($boleta->idBoleta, 10);

            return true;
        } catch (\Throwable $th) {
            throw $th;            
        }
    }

    // Metodo para cambiar cancelar una boleta que se encontraba atrasada, pasa a tener estado entregada atrasada
    public function cancelarBoletaAtr($id){
        try {
            $boleta = BoletaSalida::where('idBoleta', '=', $id)->first();
            $boleta->estado = 'eat';
            $boleta->save();

            $detalles = DetalleBoleta::where('pk_cod_boleta', '=', $id)->get();
            
            $id_estado = Estado::where('estado', '=', 'Libre')->first()->idEstado;

            $this->cambiarEstadoActivos($detalles, $id_estado); //Hay que cambiar el parametro porque ahora es un id lo del estado del activo

            return true;
        } catch (\Throwable $th) {
            throw $th;        
        }
    }

    public function filtrarBoletas(Request $req){
        try {
            $boletas = BoletaSalida::with('usuario')->get();

            $boletas = BoletaSalida::where('idBoleta','like','%'.$req->txt_cod_boleta.'%')->where('nombreResponsable','like','%'.$req->txt_responsable.'%')->get();
            
            if($req->cmb_estado !='T'){
                $boletas= $boletas->where('estado','=',$req->cmb_estado);
            }
            
            return view('boletas.boletas', compact('boletas'));
        } catch (\Throwable $th) {
            throw $th;            
        }
    }

    public function cambiarEstadoActivos($detalles, $estado) {
        try {
            foreach ($detalles as $det) {
                $activo = Activo::where('codActivo', '=', $det->pk_cod_activo)->first();
                $activo->idEstado = $estado;
                $activo->save();
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    // Metodo para imprimir la boleta de salida solicitada
    public function obtenerBoletaImprimir($id) {
        try {
            $boleta = BoletaSalida::where('idBoleta', $id)->with('usuario')->first();
            if (!is_null($boleta)) {
                $activos = DB::table('activos')->join('detalleBoleta', 'pk_cod_activo', '=', 'codActivo')
                                                ->join('boletassalida', 'pk_cod_boleta', '=', 'idBoleta')
                                                ->where('idBoleta', '=', $boleta->idBoleta)
                                                ->get();
                $mpdf = new \Mpdf\Mpdf();
                $mpdf->WriteHTML(view('boletas.boletaImpresa', compact('boleta', 'activos')));
                $mpdf->SetTitle('');            
                $mpdf->Output('EMSPZ-BLT-' . $boleta->idBoleta . '.pdf', 'I');
                exit;
            }
        } catch (\Throwable $th) {
            throw $th;            
        }
    }

    // Metodo para llamar al mail y mandar por correo una notificacion de una boleta de salida entregada, por eso mandamos el id
    public function notificarBoletaEntregada($idBoleta){
        try {
            $boleta = BoletaSalida::where('idBoleta', $idBoleta)->with('usuario')->first();
            $admins = Usuario::where('rolAcceso','A')->where('estado','A')->with('persona')->get(); //Admins de Sinfoinventarios            
            if (!is_null($boleta)){
                $activos = DetalleBoleta::where('pk_cod_boleta', '=', $idBoleta)->with('activo')->get(); //Activos respectivos de la boleta de salida
                $emailPersona = $boleta->emailResponsable; //Correo electronico de la persona a quien se le prestan el(los) activos

                Mail::to($emailPersona)->send(new notificarPersonaBoleta($boleta, $activos)); //Enviamos la notificacion a la persona responsable de la boleta de salida
                foreach ($admins as $user) {
                    if($user->persona->email != null){
                        Mail::to($user->persona->email)->send(new notificarAdminsBoleta($boleta, $activos)); // Notificamos a cada admin del sistema 
                    }
                }           
                return "success";
            }
            else{
                return false;
            }
        } catch (\Throwable $th) {
            throw $th;            
        }
    }

    //Metodo para notificar a la persona y admins, como manera de comprobante que una boleta ya fue entregada/cancelada
    public function notificarBoletaFinalizada($idBoleta){
        try {
            $boleta = BoletaSalida::where('idBoleta', $idBoleta)->with('usuario')->first();
            $admins = Usuario::where('rolAcceso','A')->where('estado','A')->with('persona')->get(); //Admins de Sinfoinventarios            
            if (!is_null($boleta)){
                $activos = DetalleBoleta::where('pk_cod_boleta', '=', $idBoleta)->with('activo')->get(); //Activos respectivos de la boleta de salida
                $emailPersona = $boleta->emailResponsable; //Correo electronico de la persona a quien se le prestan el(los) activos

                Mail::to($emailPersona)->send(new notifPersonaBltFin($boleta, $activos)); //Enviamos la notificacion a la persona responsable de la boleta de salida
                foreach ($admins as $user) {
                    if($user->persona->email != null){
                        Mail::to($user->persona->email)->send(new notifAdminsBltFin($boleta, $activos)); // Notificamos a cada admin del sistema
                    }
                }           
                return "success";
            }
            else{
                return false;
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
