<?php

namespace App\Http\Controllers;

use App\Mail\recuperacionContresena;
use App\Models\Persona;
use App\Models\Usuario;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailer;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Symfony\Contracts\Service\Attribute\Required;
use Illuminate\Support\Facades\Crypt;

class ctr_login extends Controller
{

    public function __invoke()
    {
        try {
            if(!is_null(session('userdata')) && (session('userdata')['logged_in'] == true)) {
                return view('inicio.inicio');
            }        
            return view('login.login');
        } catch (\Throwable $th) {
            return view('login.login');
        }
    }

    public function ingresar(Request $req) {

        $req->validate([
            'username' => 'required',
            'password' => 'required'
        ]);
        
        try {
            $usuario = Usuario::with('persona')->where('nombreUsuario', '=', $req->username)->first();
            if(is_null($usuario)) {
                return redirect('/')->with('error', '¡Usuario Incorrecto!');   
            } else {      
                if (Crypt::decryptString($usuario->contraseña) == $req->password) {
                    if ($usuario->estado == 'A') {                        
                        $user_data = array(
                            'logged_in' => TRUE,
                            'userID'    => $usuario->idUsuario,
                            'username'  => $usuario->nombreUsuario,
                            'realname'  => $usuario->persona->nombreCompleto,
                            'userPic'   => $usuario->fotoUsuario,
                            'role'  => $usuario->rolAcceso,
                            'gender' => $usuario->persona->genero                    
                        );
                        session(['userdata' => $user_data]);
                        return view('inicio.inicio')->with('success', 'Bienvenido');
                    } else {                
                        return back()->with('error', '¡Este usuario fue DADO DE BAJA y se encuentra INACTIVO!'); 
                    }
                } else {
                    return back()->with('error', '¡Contraseña Incorrecta!'); 
                }                
            }
        } catch (\Throwable $th) {
            throw $th;
            return back()->with('error', '¡Ha ocurrido un error interno!');
        }
    }

    public function salir(){
        session()->forget('userdata');
        session()->forget('c_error');
        return view('login.login')->with('info', '¡Hasta la próxima!');
    }

    public function recuperarContrasena($username) {        
        try {

            $usuario = Usuario::where('nombreUsuario', $username)->with('persona')->first();                
            if (is_null($usuario)) {            
                return 'empty';
            } else {            
                $email = $usuario->persona->email;

                $usuario->contraseña = Crypt::decryptString($usuario->contraseña);
                // Aqui va el codigo para enviar el email con la contraseña                
                Mail::to($email)->send(new recuperacionContresena($usuario));

                return 'success';
            }

        } catch (\Throwable $th) {
            throw $th;
        }        
    }
    

}