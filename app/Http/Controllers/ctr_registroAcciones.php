<?php

namespace App\Http\Controllers;

use App\Models\RegistroAcciones;
use DateTime;
use Illuminate\Http\Request;

class ctr_registroAcciones extends Controller
{
    public function __invoke() {
        // $usuarios = Usuario::with('persona')->get();

        // return view('usuarios.usuarios', compact('usuarios'));
    }

    // Metodo estatico para guardar una accion en el registro de acciones que realizan los usuarios
    public static function registrarAccion ($idReferencial, $idTipoAccion) {
        try{
            $accion = new RegistroAcciones();

            $accion->idUsuario = session('userdata')['userID'];

            $accion->idReferencial = $idReferencial;

            $accion->idTipoAccion = $idTipoAccion;

            $accion->fechaAccion = date('y-m-d h:i:s');

            $accion->save();
        }
        catch (\Throwable $th) {
            throw $th;
        }
    }

    public function cargar_registro_acciones(){
        try{
            $fecha = New DateTime();
            return view('registroacciones.registroacciones', compact('fecha'));
        }
        catch (\Throwable $th) {
            return back()->with('error', '¡Ha ocurrido un error interno!');
        }
    }

    public function obtenerRegistro(Request $req) {
        try {
            
            if ($req->usar_rango == 'true') {
                $registro = RegistroAcciones::whereDate('fechaAccion', '>=', $req->dp_inicio)
                                            ->whereDate('fechaAccion', '<=', $req->dp_final)
                                            ->with('tipo_accion')->with('usuario')->get();    
            } else {
                $registro = RegistroAcciones::with('tipo_accion')->with('usuario')->get();
            }            

            return $registro;
        } catch (\Throwable $th) {
            throw $th;
        }
    }

}
