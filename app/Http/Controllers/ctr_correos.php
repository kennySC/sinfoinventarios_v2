<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Mail\Mailer;
use Mail;

class ctr_correos extends Controller
{
    public function create()
    {
        return view('email');
    }

    public function sendEmail(/*Request req*/)
    {
        /*$request->validate([
          'email' => 'required|email',
          'subject' => 'required',
          'name' => 'required',
          'content' => 'required',
        ]);*/

        $data = [
          'subject' => 'prueba'/*$request->subject*/,
          'name' => 'Prueba'/*$request->name*/,
          'email' => 'kennysc97@gmail.com'/*$request->email*/,
          'content' => '$request->content'
        ];
        
        Mail::send('correos.plantillaCorreos', $data, function($message) use ($data) {
          $message->to($data['email'])
          ->subject($data['subject']);
        });

        return back()->with(['message' => 'Email successfully sent!']);
    }
}