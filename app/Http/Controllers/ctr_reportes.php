<?php

namespace App\Http\Controllers;

use App\Models\Institucion;
use App\Models\Instrumento;
use App\Models\Activo;
use App\Models\TipoActivo;
use App\Models\Usuario;
use DateTime;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class ctr_reportes extends Controller
{
    public function __invoke()
    {
        $instituciones = Institucion::All();
        return view('reportes.reportes', compact('instituciones'));
    }

    public function generar_reporte(Request $req){    
        try {
            // Consultamos que tipo de reporte desea generar/crear
            switch($req->cmb_tipo_rep){
                // Consultamos si se quiere generar un reporte solo de ACTIVOS
                case('A'):                    
                    // para despues llamar unicamente a los activos, es decir, todo lo que sea diferente a instrumento
                    $activos = Activo::with('institucion')
                                        ->with('estado_activo')
                                        ->when($req->cmb_institucion != 'T', function ($query) use ($req) {
                                            return $query->where('idInstitucion','=', intval($req->cmb_institucion));
                                        })
                                        ->get();                    
                    return $activos;                    
                    break;
                // Consultamos si se quiere generar un reporte solo de INSTRUMENTOS
                case('I'):                    
                    // para despues llamar unicamente a los activos, es decir, todo lo que sea diferente a instrumento
                    $instrumentos = Instrumento::with('institucion')
                                                ->with('estado')
                                                ->with('tipoInstrumento')
                                                ->when($req->cmb_institucion != 'T', function ($query) use ($req) {
                                                    return $query->where('idInstitucion','=', intval($req->cmb_institucion));
                                                })
                                                ->get();                    
                    return $instrumentos;                  
                    break;
                // Consultamos si se quiere generar un reporte de ACTIVOS E INSTRUMENTOS
                case('T'):                
                    $activos = Activo::join('instrumentos')
                                        ->get();                    
                    return $activos;                    
                    break;
            }   
        } catch (\Throwable $th) {
            throw $th;
        }
    }  
    
}

