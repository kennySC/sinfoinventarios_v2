<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EstudiantesEncargados extends Model
{

    protected $table = "encargadosestudiantes";
    protected $primaryKey = 'cedulaEstudiante';
    public $timestamps = false;

    use HasFactory;

    public function persona(){
        return $this->belongsTo(Persona::class, 'cedulaEncargado', 'cedula');
    }
}
