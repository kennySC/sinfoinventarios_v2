<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Instrumento extends Model
{
    use HasFactory;

    protected $table = "instrumentos";
    protected $primaryKey = 'codInstrumento';
    public $incrementing = false;
    public $timestamps = false;

    public function institucion() {        
        return $this->belongsTo(Institucion::class, 'idInstitucion', 'idInstitucion');
    } 
    public function tipoInstrumento(){
        
        return $this->belongsTo(TipoInstrumento::class, 'codTipoInstrumento', 'codTipoInstrumento');
    }
    public function estado() {
        return $this->belongsTo(Estado::class, 'idEstado', 'idEstado');
    }
}
