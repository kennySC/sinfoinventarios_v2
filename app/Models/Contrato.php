<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contrato extends Model
{
    use HasFactory;

    protected $table = "contratos";
    protected $primaryKey = 'idContrato';
    public $timestamps = false;

    public function estudiante(){
        return $this->belongsTo(Persona::class, 'cedulaEstudiante', 'cedula');
    }
    public function encargado(){
        return $this->belongsTo(Persona::class, 'cedulaEncargado', 'cedula');
    }
    public function instrumento(){
        return $this->belongsTo(Instrumento::class, 'codInstrumento', 'codInstrumento');
        // return Instrumento::where('codInstrumento', '=', $this->codIstrumento)->with('tipoInstrumento');
    }
    public function presidente(){
        return $this->belongsTo(Persona::class, 'PresidenteAsociacion', 'cedula');
    }
}
