<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RegistroAcciones extends Model
{
    use HasFactory;

    protected $table = "registroacciones";
    protected $primaryKey = 'idRegistro';
    public $timestamps = false;

    public function tipo_accion() {
        return $this->belongsTo(TipoAccion::class, 'idTipoAccion', 'idTipoAccion');
    }

    public function usuario() {
        return $this->belongsTo(Usuario::class, 'idUsuario', 'idUsuario');
    }

}
