<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Activo extends Model
{
    use HasFactory;

    protected $table = "activos";
    protected $primaryKey = 'codActivo';
    public $timestamps = false;
    public $incrementing = false;

    public function institucion() {        
        return $this->belongsTo(Institucion::class, 'idInstitucion', 'idInstitucion');
    }

    public function estado_activo() {
        return $this->belongsTo(Estado::class, 'idEstado', 'idEstado');
    }

}
