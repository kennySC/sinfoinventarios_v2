<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BoletaSalida extends Model
{
    use HasFactory;

    protected $table = "boletassalida";
    protected $primaryKey = 'idBoleta';
    public $timestamps = false;

    public function usuario(){
        return $this->belongsTo(Usuario::class, 'idUsuario', 'idUsuario');
    }

}
