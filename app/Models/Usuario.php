<?php

namespace App\Models;

use Faker\Provider\ar_JO\Person;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Usuario extends Model
{
    use HasFactory;

    protected $table = "usuarios";
    protected $primaryKey = 'idUsuario';
    public $timestamps = false;

    public function persona(){
        return $this->belongsTo(Persona::class, 'cedulaPersona', 'cedula');
    }
}
