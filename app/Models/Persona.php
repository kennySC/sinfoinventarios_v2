<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    use HasFactory;

    protected $table = "personas";
    protected $primaryKey = "cedula";
    public $timestamps = false;
    public $incrementing = false;
    protected $keyType = 'string';


    // Obtener los encargados de los estudiantes
    public function encargados () {
        return $this->hasMany(EstudiantesEncargados::class, 'cedulaEstudiante', 'cedula')->with('persona');
    }

}
