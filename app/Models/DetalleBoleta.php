<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetalleBoleta extends Model
{
    use HasFactory;

    protected $table = "detalleboleta";
    // protected $primaryKey = 'pk_cod_boleta';
    // protected $primary = 'pk_cod_activo';
    public $timestamps = false;

    public function boleta(){
        return $this->belongsTo(Boleta::class, 'pk_cod_boleta', 'idBoleta');
    }

    public function activo(){
        return $this->belongsTo(Activo::class, 'pk_cod_activo', 'codActivo');
    }
}
