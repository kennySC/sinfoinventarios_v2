<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TipoInstrumento extends Model
{
    use HasFactory;
    protected $table = "tipoinstrumento";
    protected $primaryKey = 'codTipoInstrumento';
    public $incrementing = false;
    public $timestamps = false;
}

