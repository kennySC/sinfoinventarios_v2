<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FamiliaInstrumento extends Model
{
    use HasFactory;
    protected $table = "familiasinstrumentos";
    protected $primaryKey = 'idFamilia';
    public $timestamps = false;
}
