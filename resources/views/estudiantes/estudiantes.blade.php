@extends('layouts.master')


@section('title', 'Sinfoinventarios | Estudiantes')

@section('seccionActiva', 'Estudiantes')

@section('css_js')
    <link rel="stylesheet" href="{{asset('assets/css/estudiantes.css')}}">
    <script src="{{asset('assets/js/estudiantes.js')}}"></script>    
@endsection

@section('content')

@if (session('userdata') != null)

<div class="div_contenido">
    <div id="div_izq">        
        <form id="form_filtrar" action="{{route('filtrarEstudiantes')}}" method="POST">
            @csrf
            <h1>Opciones <small class="small_class">de filtrado</small></h1>
            <hr class="hr_class">

            <label class="lb_buscar" for="txt_nombre">
                Nombre:
                <input type="text" class="txt" name="txt_nombre" id="txt_nombre">
            </label>

            <label class="lb_buscar" for="txt_cedula">
                Cedula:
                <input type="text" class="txt" name="txt_cedula" id="txt_cedula">
            </label>        

            <div class="div_btnsfiltrar">
                <input type="submit" class="boton" id="btn_filtrar" value="Filtrar">
                <input type="button" class="boton" id="btn_limpiar" value="Limpiar" onclick="limpiarFiltros()">
            </div>
        </form>

        <input type="button" class="boton" id="btn_actualizar" value="Actualizar lista de Estudiantes" onclick="actualizarEstudiantes()">
        <a hidden id="acualizarEstudiantes" href="{{route('acualizarEstudiantes')}}"></a>
    </div>
    <div id="div_der">
        
        <div id="div_estudiantes">
            @if ($estudiantes->count() <= 0)
                <h1 class='h1_sinResultado'> ¡Sin resultados para mostrar!</h1>
            @else                 

                @foreach ($estudiantes ?? '' as $est)
                    <div class="div_contenedorEstudiante">            
                        <div class="div_infoEstudiante">
                            {{-- <h1>Nombre: <small> {{$est->nombre}} {{$est->segundoNombre}} {{$est->apellido}} {{$est->segundoApellido}} </small></h1> --}}
                            <h1>Nombre: <small> {{$est->nombreCompleto}} </small></h1>

                            <h1>Ced: <small> {{substr($est->cedula, 0, 1)}} {{substr($est->cedula, 1, 4)}} {{substr($est->cedula, 5, 4)}} </small></h1>
                            
                            <h1>Correo: <small> {{$est->email}} </small></h1>
                        </div>
                        <hr>
                        <div class="div_encargados">
                            <strong>Encargados: </strong>
                            @if (count($est->encargados) > 0)
                                @foreach ($est->encargados ?? '' as $enc)
                                    <div class="div_infoEnc">
                                        {{-- <h1>Nombre: <small> {{$enc->persona->nombre}} {{$enc->persona->segundoNombre}} {{$enc->persona->apellido}} {{$enc->persona->segundoApellido}} </small></h1> --}}
                                        <h1>Nombre: <small> {{$enc->persona->nombreCompleto}} </small> </h1>

                                        <h1>Ced: <small> {{substr($enc->persona->cedula, 0, 1)}} {{substr($enc->persona->cedula, 1, 4)}} {{substr($enc->persona->cedula, 5, 4)}} </small></h1>
                                    </div>
                                @endforeach
                            @endif
                            {{-- {{json_encode($est->encargados)}} --}}
                        </div>
                    </div>        
                @endforeach

            @endif
        </div>
        
        <div id="div_paginacion">
            {{ $estudiantes->links() }}
        </div>

    </div>
</div>

@else
    <script>window.location = "/";</script>
@endif

@endsection