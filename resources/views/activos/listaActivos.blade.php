
<style>
    .div_activos {
        position: relative;
        width: 100%;
        height: 650px;        
        color: white;
        background-color: #201d1d;
    }

    .div_busqueda {
        margin: 5px;
        width: 98%;
        height: 50px;
        display: flex;
        flex-direction: row;
        align-items: center;
        justify-content: center;
    }

    #div_tabla {
        height: 590px;
        width: 98%;
        margin: auto;
        flex-direction: row;
        align-items: center;
        justify-content: center;
        overflow: auto;
    }

    #txt_buscar {
        width: 40%;    
    }

    #btn_buscar, #btn_limpiar {
        margin: 5px;
        width: 80px;
        height: 34px;
        border-radius: 5px;
    }

    .tabla td, .tabla th {
        border: 1px solid rgb(19, 9, 9);
        max-height: 20px;
        overflow: hidden;
    }

    .tabla tr:nth-child(even){background-color: #201d1d;}

    .tabla tr:hover {
        background-color: rgb(71, 71, 71);
        color: #e9a825;
    }

    .tabla th {
        text-align: left;
        background-color: cadetblue;
        color: white;
    }

</style>

<div class="div_activos">
    <form class="div_busqueda">
        @csrf
        <input type="text" class="txt" id="txt_buscarActivo" name="txt_buscarActivo" placeholder="Nombre del Activo a buscar">
        <input type="button" class="btn_guardar" id="btn_buscar" value="Buscar!" onclick="filtrarListaActivos()">
        <input type="button" class="btn_guardar" id="btn_limpiar" value="Limpiar!">
    </form>
    <hr style="width: 95%">
    <div id="div_tabla">
        <table class="tabla" id="tabla_activos">
            <tr>
                <th>Código</th>
                <th># Serie</th>
                <th>Nombre</th>
                <th><input type="checkbox" name="cb_select" id="cb_select"> Seleccionar Todos</th>
            </tr>
            @foreach ($activos as $act)
                <tr>
                    <td> {{$act->codActivo}} </td>
                    <td> {{$act->serie}} </td>
                    <td> {{$act->nombre}} </td>
                    <td> <input type="checkbox" name="cb_select_{{$act->codActivo}}" id="cb_select_{{$act->codActivo}}"> </td>
                </tr>                        
            @endforeach
        </table>
    </div>
</div>