@extends('layouts.master')

@section('title', 'Sinfoinventarios | Recepción de Activos')

@section('seccionActiva', 'Recepción de Activos')

@section('css_js')
    <link rel="stylesheet" href="css/activos.css">
    <script src="js/activos.js"></script>
@endsection

@section('content')

    <div class="div_contenido">
        <div id="div_izq">
            <h1>Simon div izquierdo pase el activo aqui lo recibo</h1>
        </div>
        <div id="div_der">
            <h1>Simon div derecho</h1>
        </div>
    </div>

@endsection