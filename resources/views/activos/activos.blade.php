@extends('layouts.master')

@section('title', 'Sinfoinventarios | Activos')

@section('seccionActiva', 'Activos')

@section('css_js')
    <link rel="stylesheet" href="{{asset('assets/css/activos.css')}}">
    <script src="{{asset('assets/js/activos.js')}}"></script>
@endsection

@section('content')

@if (session('userdata') != null)

    <div class="div_contenido">
    <!--Div del contenido del LADO IZQUIERDO de la vista, es decir, lo que respecta lo de filtrar y el boton para agregar un nuevo activo-->
        <div id="div_izq">
            <!--FORM donde vendran todas las opciones posibles de filtrado-->
            <form id="form_filtrar" action="{{route('filtrarActivos')}}" method="POST">
            @csrf
                <h1 id="header_filtrado">Opciones <small class="small_class">de filtrado</small></h1><br>
                <hr>
                <label id="lb_buscar" for="txt_nom_activo">
                    Nombre del Activo:
                    <input type="text" name="txt_nom_activo" id="txt_nom_activo" class="txt_datos" maxlength="100">
                </label>
                <label id="lb_buscar" for="txt_cod_activo">
                    Código del Activo:
                    <input type="text" name="txt_cod_activo" id="txt_cod_activo" class="txt_datos" maxlength="12">
                </label>

                <div id="div_opciones">
                <label id="lb_buscar" for="cmb_datos">Institución:</label> <br>
                        <select name="cmb_institucion" id="cmb_institucion" class="cmb_datos">
                            <option selected value="T">Todos</option>
                            @foreach ($instituciones ?? '' as $inst)
                                <option value='{{$inst->idInstitucion}}'>{{$inst->nombre}}</option>
                            @endforeach
                        </select>

                        <label id="lb_buscar" for="cmb_datos">Estado:</label><br>
                        <select name="cmb_estado" id="cmb_estado" class="cmb_datos">
                        <option selected value="T">Todos</option>
                                @foreach ($estado_activo ?? '' as $est_act)
                                    <option value='{{$est_act->idEstado}}'>{{$est_act->estado}}</option>
                                @endforeach
                        </select>
                
                    <div class="div_btnsfiltrar">
                        <input type="submit" class="boton" id="btn_filtrar" value="Filtrar">
                        <input type="button" class="boton" id="btn_limpiar" value="Limpiar" onclick="limpiarFiltros()">
                    </div>
                </div>
            </form>

            @if(session('userdata')['role'] == 'A')
                <div class="div_btns_agregar">
                    <!--Boton para agregar un nuevo activo--> 
                    <input type="button" id="btn_nuevoAct" class="boton" value="Agregar nuevo activo" onclick="irNuevoActivo()">
                    <a id="a_nuevoActivo" href="{{route('agregarActivo')}}"></a>
                </div>
            @endif
        </div>

        <!--Div del contenido del LADO DERECHO de la vista, es decir, lo que respecta los activos como tal, junto con sus botones de editar y dar de baja-->
        <div id="div_der">
            <div class="div_activos">
                @if ($activos->count() <= 0)
                    <h1 class='h1_sinResultado'> ¡Sin resultados para mostrar!</h1>
                @else   
                <!--Aqui viene el FOREACH para mostrar todos los activos en el sistema-->    
                    @foreach ($activos ?? '' as $act) 
                        <div @if ($act->idEstado != 8) class="div_contenedorActivo"
                            @else class="div_contenedorActivo dadoBaja"
                            @endif> <!--Div contenedor del activo, si tiene estado de baja es div_contenedorActivo_b, si no el otro class; div_ contenedorActivo_n-->
                            <h1>{{$act->nombre}}</h1>
                                <div class="div_datos_activo"><!--Div donde iran los datos del activo, la info y foto en caso de tener-->
                                    <div class="div_foto_activo">
                                        <!--Consultamos si el activo cuenta con foto, de ser asi, se carga-->
                                        @if ($act->fotoActivo != null)                         
                                            <img class="img_fotoAct" src="{{asset('storage/'.$act->fotoActivo)}}" alt="">
                                        <!--De no tener, cargamos un icono predeterminado-->
                                        @else
                                            <!--Si el activo no cuenta con foto cargamos una imagen por defecto-->
                                            <img class="img_fotoAct" src="{{asset('assets/rsc/pngs/activo_default.png')}}" alt="">
                                        @endif
                                    </div> 
                                    <!--Div con la informacion respectiva del activo-->   
                                    <div class="div_info_activo">
                                        <div class="rowInfo">
                                            <p>Código Placa: <span id="placa">{{$act->codActivo}}</span></p>
                                            <p>Institución: <span>{{$act->institucion->nombre}}</span></p>
                                        </div>
                                        <div class="rowInfo">
                                            <p>Ubicación: <span>{{$act->ubicacion}}</span></p>
                                            <p>Estado: <span>{{$act->estado_activo->estado}}</span></p>
                                        </div>
                                        <div class="rowInfo">
                                            <input type="button" id="{{$act->codActivo}}" class="btnVerMas" value="Ver más" onclick="verMas({{$act}})">
                                        </div>
                                    </div>
                                </div>
                                <!--Consultamos si la persona usuario en session es administrador entonces creamos el Div para los botones de EDITAR Y DAR DE BAJA para cada activo -->
                                @if(session('userdata')['role'] == 'A')
                                    <div class="div_botones">
                                        <!-- A continuacion declaramos los botones y los "a" necesarios para acceder a las rutas usando javascript  -->
                                        <input id="{{$act->codActivo}}" type="button" class="btnEditar" value="Editar" onclick="editarActivo(this)">
                                        <a id="a_editar_{{$act->codActivo}}" href="{{route('editarActivo', $act->codActivo)}}"></a>
                                        @if ($act->idEstado != 8) <!--CONSULTAMOS SI EL ACTIVO TIENE UN ESTADO DIFERENTE A DADO DE BAJA-->
                                            <input type="button" class="btnBaja" id="{{$act->codActivo}}" name='{{$act->nombre}}' value="Dar de Baja" onclick="confirmarBaja(this)">
                                        @else <!--SI TIENE ESTADO DADO DE BAJA ENTONCES MOSTRAMOS UN BOTON PARA ACTIVARLO-->
                                            <input type="button" class="btnActivar" id="{{$act->codActivo}}" name='{{$act->nombre}}' value="Activar" onclick="activarActivo(this)">
                                        @endif
                                    </div>
                                @endif
                        </div>
                    @endforeach
                @endif
            </div>
            
            <div id="div_paginacion">
                {{ $activos->links() }}
            </div>
            
         </div> <!-- Cierra el div_der -->

        <div id="div_fondoOscuro">
            <div id="div_contMensaje">
                
            </div>
        </div>
    </div>

@else 
    <script>window.location = "/";</script>
@endif
@endsection
