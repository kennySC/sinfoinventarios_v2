@extends('layouts.master')

@section('title', 'Sinfoinventarios | Agregando Activo')

@section('seccionActiva', 'Agregando Activo')

@section('css_js')
    <link rel="stylesheet" href="{{asset('assets/css/editarAgregarActivo.css')}}">
    <script src="{{asset('assets/js/agregarActivo.js')}}"></script>
@endsection

@section('content')

@if (session('userdata') != null)

    <div class="div_contenido">
        <form id="form_activo" action="{{route('guardarActivo')}}" method ="post" enctype="multipart/form-data">
            @csrf
            @method('post')
            <div id="div_datos">
                <label for="txt_cod_activo">
                    Código del activo: <br>
                    <input class="txt" type="text" name="txt_cod_activo" id="txt_cod_activo" value="{{old('txt_cod_activo')}}" maxlength="12">
                    @error('txt_cod_activo')
                        <small class="txtError">*{{$message}}</small>
                    @enderror
                </label>
                <label for="txt_nom_activo">
                    Nombre del activo: <br>
                    <input class="txt" type="text" name="txt_nom_activo" id="txt_nom_activo" value="{{old('txt_nom_activo')}}" maxlength="100">
                    @error('txt_nom_activo')
                        <small class="txtError">*{{$message}}</small>
                    @enderror
                </label>
                <label for="txt_marca_activo">
                    Marca del activo: <br>
                    <input class="txt" type="text" name="txt_marca_activo" id="txt_marca_activo" value="{{old('txt_marca_activo')}}" maxlength="150">
                    @error('txt_marca_activo')
                        <small class="txtError">*{{$message}}</small>
                    @enderror
                </label>
                <label for="txt_modelo_activo">
                    Modelo del activo: <br>
                    <input class="txt" type="text" name="txt_modelo_activo" id="txt_modelo_activo" value="{{old('txt_modelo_activo')}}" maxlength="150">
                    @error('txt_modelo_activo')
                        <small class="txtError">*{{$message}}</small>
                    @enderror
                </label>
                <label for="txt_serie_activo">
                    # Serie del activo: <br>
                    <input class="txt" type="text" name="txt_serie_activo" id="txt_serie_activo" value="{{old('txt_serie_activo')}}" maxlength="50">
                    @error('txt_serie_activo')
                        <small class="txtError">*{{$message}}</small>
                    @enderror
                </label>
                <label for="txt_ubicacion_activo">
                    Ubicacion del activo: <br>
                    <input class="txt" type="text" name="txt_ubicacion_activo" id="txt_ubicacion_activo" value="{{old('txt_ubicacion_activo')}}" maxlength="150">
                    @error('txt_ubicacion_activo')
                        <small class="txtError">*{{$message}}</small>
                    @enderror
                </label>
                <label for="txt_valor_activo">
                    Valor ₡ del activo: <br>
                    <input class="txt" type="text" name="txt_valor_activo" id="txt_valor_activo" value="{{old('txt_valor_activo')}}" maxlength="30">
                    @error('txt_valor_activo')
                        <small class="txtError">*{{$message}}</small>
                    @enderror
                </label>
                <label for="txt_resp_activo">
                    Responsable del activo: <br>
                    <input class="txt" type="text" name="txt_resp_activo" id="txt_resp_activo" value="{{old('txt_resp_activo')}}" maxlength="100">
                    @error('txt_resp_activo')
                        <small class="txtError">*{{$message}}</small>
                    @enderror
                </label>
                <!--COMBOBOX para la INSTITUCION-->
                <label for="cmb_institucion">
                Institución: <br>
                    <select class="cmb" name="cmb_institucion" id="cmb_institucion">
                        @foreach ($instituciones ?? '' as $inst)
                            <option value='{{$inst->idInstitucion}}'>{{$inst->nombre}}</option>
                        @endforeach
                    </select>
                    @error('cmb_institucion')
                        <small class="txtError">*{{$message}}</small>
                    @enderror
                </label>
                <!--COMBOBOX para el ESTADO DEL ACTIVO-->
                <label for="cmb_estado">
                Estado: <br>
                    <select class="cmb" name="cmb_estado" id="cmb_estado">
                        @foreach ($estado_activo ?? '' as $est_act)
                            <option value='{{$est_act->idEstado}}'>{{$est_act->estado}}</option>
                        @endforeach
                    </select>
                    @error('cmb_estado')
                        <small class="txtError">*{{$message}}</small>
                    @enderror
                </label>
                <label for="txt_observ_activo">
                    Observaciones: <br>
                    <textarea class="txt" name="txt_observ_activo" id="txt_observ_activo" cols="30" rows="50" value="{{old('txt_observ_activo')}}"></textarea>
                </label>
                @error('txt_observ_activo')
                    <small class="txtError">*{{$message}}</small>
                @enderror
                <div id="div_img">
                    <div id="btnsImg">
                        <button type="button" id="btn_cargarImg" class="btn_cargar" onclick="presionarInputFile()"><i class="far fa-images fa-2x"></i></button>                    
                    </div>
                    <img id="img_activo" name="img_activo" src="{{asset('assets/rsc/pngs/activo_default.png')}}" alt="">
                    <input type="file" accept="image/x-png, image/jpeg" name="file_imgActivo" id="file_imgActivo" onclick="cargarIMG()">
                </div>
            </div>
            <div id="div_btns">
                <input class="btn" type="submit" id="btn_guardar" value="Guardar">
                <input class="btn" type="button" id="btn_cancelar" value="Cancelar" onclick="cancelar()">
                <a href="{{route('activos')}}" id="a_cancelar"></a>
            </div>
        </form>
    </div>

@else 
    <script>window.location = "/";</script>
@endif

@endsection