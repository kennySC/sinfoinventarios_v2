@extends('layouts.master')

@section('title', 'Sinfoinventarios | Editando Activo')

@section('seccionActiva', 'Editando Activo')

@section('css_js')
    <link rel="stylesheet" href="{{asset('assets/css/editarAgregarActivo.css')}}">
    <script src="{{asset('assets/js/editarActivo.js')}}"></script>
@endsection

@section('content')

@if (session('userdata') != null)

    <div class="div_contenido">
            <form id="form_activo" action="{{route('actualizarActivo')}}" method ="post" enctype="multipart/form-data">
                @csrf
                @method('put')
                <div id="div_datos">
                    <label for="txt_cod_activo">
                        Código del activo: <br>
                        <input class="txt" type="text" name="txt_cod_activo" id="txt_cod_activo" value="{{$activo->codActivo}}" readonly>
                        @error('txt_cod_activo')
                            <small class="txtError">*{{$message}}</small>
                        @enderror
                    </label>
                    <label for="txt_nom_activo">
                        Nombre del activo: <br>
                        <input class="txt" type="text" name="txt_nom_activo" id="txt_nom_activo" value="{{$activo->nombre}}" maxlength="100">
                        @error('txt_nom_activo')
                            <small class="txtError">*{{$message}}</small>
                        @enderror
                    </label>
                    <label for="txt_marca_activo">
                        Marca del activo: <br>
                        <input class="txt" type="text" name="txt_marca_activo" id="txt_marca_activo" value="{{$activo->marca}}" maxlength="150">
                        @error('txt_marca_activo')
                            <small class="txtError">*{{$message}}</small>
                        @enderror
                    </label>
                    <label for="txt_modelo_activo">
                        Modelo del activo: <br>
                        <input class="txt" type="text" name="txt_modelo_activo" id="txt_modelo_activo" value="{{$activo->modelo}}" maxlength="150">
                        @error('txt_modelo_activo')
                            <small class="txtError">*{{$message}}</small>
                        @enderror
                    </label>
                    <label for="txt_serie_activo">
                        # Serie del activo: <br>
                        <input class="txt" type="text" name="txt_serie_activo" id="txt_serie_activo" value="{{$activo->serie}}" maxlength="50">
                        @error('txt_serie_activo')
                            <small class="txtError">*{{$message}}</small>
                        @enderror
                    </label>
                    <label for="txt_ubicacion_activo">
                        Ubicacion del activo: <br>
                        <input class="txt" type="text" name="txt_ubicacion_activo" id="txt_ubicacion_activo" value="{{$activo->ubicacion}}" maxlength="150">
                        @error('txt_ubicacion_activo')
                            <small class="txtError">*{{$message}}</small>
                        @enderror
                    </label>
                    <label for="txt_valor_activo">
                        Valor ₡ del activo: <br>
                        <input class="txt" type="text" name="txt_valor_activo" id="txt_valor_activo" value="{{$activo->valor}}" maxlength="30">
                        @error('txt_valor_activo')
                            <small class="txtError">*{{$message}}</small>
                        @enderror
                    </label>
                    <label for="txt_resp_activo">
                        Responsable del activo: <br>
                        <input class="txt" type="text" name="txt_resp_activo" id="txt_resp_activo" value="{{$activo->responsable}}" maxlength="100">
                        @error('txt_resp_activo')
                            <small class="txtError">*{{$message}}</small>
                        @enderror
                    </label>
                    <!--COMBOBOX para la INSTITUCION-->
                    <label for="cmb_institucion">
                    Institución: <br>
                        <select class="cmb" name="cmb_institucion" id="cmb_institucion">
                            @foreach ($instituciones ?? '' as $inst)
                                <option value='{{$inst->idInstitucion}}'
                                    @if ($activo->idInstitucion == $inst->idInstitucion) selected>
                                        {{$inst->nombre}}
                                    @endif
                                </option>
                            @endforeach
                        </select>
                        @error('cmb_institucion')
                            <small class="txtError">*{{$message}}</small>
                        @enderror
                    </label>
                    <!--COMBOBOX para el ESTADO DEL ACTIVO-->
                    <label for="cmb_estado">
                    Estado: <br>
                        <select class="cmb" name="cmb_estado" id="cmb_estado">
                            @foreach ($estado_activo ?? '' as $est_act)
                                <option value='{{$est_act->idEstado}}' 
                                @if($activo->idEstado == $est_act->idEstado) selected @endif>{{$est_act->estado}}</option>
                            @endforeach
                        </select>
                        @error('cmb_estado')
                            <small class="txtError">*{{$message}}</small>
                        @enderror
                    </label>
                    <label for="txt_observ_activo">
                        Observaciones: <br>
                        <textarea name="txt_observ_activo" id="txt_observ_activo" cols="25" rows="80" value="">{{$activo->observaciones}}</textarea>
                    </label>
                    @error('txt_observ_activo')
                        <small class="txtError">*{{$message}}</small>
                    @enderror
                    <div id="div_img">

                        <div id="btnsImg">
                            <button type="button" id="btn_cargarImg" class="btn_cargar" onclick="presionarInputFile()"><i class="far fa-images fa-2x"></i></button>
                            @if (!is_null($activo->fotoActivo)) <button type="button" id="deleteOriginal" class="btn_borrar" onclick="eliminarOriginal()"><i class="fas fa-eraser fa-2x"></i></button> @endif                
                        </div>                        
                        @if (!is_null($activo->fotoActivo))                
                            <img id="img_activo" name='img_activo' src='{{asset('storage/'.$activo->fotoActivo)}}' alt="">                 
                            <img name="imgOriginal" id="imgOriginal" style="visibility: hidden" src='{{asset('storage/'.$activo->fotoActivo)}}'>                           
                        @else 
                            <img id="img_activo" name='img_activo' src="{{asset('assets/rsc/pngs/activo_default.png')}}" alt="">
                        @endif
                        
                        <input type="hidden" id="tieneFoto" value="{{$activo->fotoActivo}}">
                        <input type="hidden" name="borrarOriginal" id="borrarOriginal" value="0">                      
                        <input type="file" accept="image/x-png, image/jpeg" name="file_imgActivo" id="file_imgActivo" onclick="cargarIMG()">   


                    </div>
                    <input type="hidden" name="codActivo" value="{{$activo->codActivo}}">
                </div>
                <div id="div_btns">
                    <input class="btn" type="submit" id="btn_guardar" value="Guardar">
                    <input class="btn" type="button" id="btn_cancelar" value="Cancelar" onclick="cancelar()">
                    <a href="{{route('activos')}}" id="a_cancelar"></a>
                </div>
            </form>
    </div>

@else 
    <script>window.location = "/";</script>
@endif

@endsection