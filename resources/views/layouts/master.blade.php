<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/master.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/fontAwesome/all.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/alertify.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/temasAlertify/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/customDataTables.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/DataTables/datatables.min.css')}}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/components/menu.min.css">
    
    <script type="text/javascript" src="{{asset('assets/DataTables/datatables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/fontAwesome/all.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/alertify.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/master.js')}}"></script>

    <title>@yield('title')</title>
    <!-- Yields para los js y css de las diferentes vistas-->
    @yield('css_js')    

</head>
<body>
@if (session('userdata') != null)            
    <!-- Codigo para la barra de usuario y el icono del sistema -->
    <div id="div_top">
        <div id="div_logo">
            <img src="{{asset('assets/rsc/imgs/fontSinfo.png')}}" alt="" onclick="inicio()">     
        </div>        
        <div class="div_seccionActiva">
            <p>@yield('seccionActiva')</p>            
        </div>
        <div id="div_nomUsuario" onclick="mostrarMenuUsuario()">            
            <p>{{session('userdata')['username']}}</p>       
        </div>        
    </div>    
    <div id="div_base">
        @include('alertas.alertas')
        @yield('content')        
        {{-- <i class="fas fa-compact-disc fa-spin fa-10x"></i> --}}    
    </div>
    <div id="div_menuUsuario" class="cerrado">
        <div id="div_infoUsuario">            
            <p>{{session('userdata')['realname']}}</p>            
            <div id="div_foto">
                @if (session('userdata')['userPic'] != null)
                    <img src="{{asset('storage/'.session('userdata')['userPic'])}}">
                @else
                    <img src="{{asset('assets/rsc/pngs/user_white.png')}}">
                @endif
            </div>  
        </div>
        <a href="{{route('editarUsuario', session('userdata')['userID'])}}">Editar</a>        
        <a href="{{route('salir')}}">Cerrar Sesión</a>
    </div>     
    <div id="div_menu" class="cerrado">
        <a id="a_inicio" href="{{route('inicio')}}">Inicio</a>
        @if (session('userdata')['role'] == 'A')            
            <a href="{{route('estudiantes')}}">Estudiantes</a> 
            <a href="{{route('activos')}}">Activos</a>
            <a href="{{route('instrumentos')}}">Instrumentos</a>        
            <a href="{{route('contratos')}}">Contratos de Prestamo</a>
            <a href="{{route('boletas')}}">Boletas de Salida</a>
            <a href="{{route('reportes')}}">Reportes</a>   
            <a href="{{route('registro_acciones')}}">Registro de Acciones</a>     
            <a href="{{route('usuarios')}}">Usuarios</a>
        @else <!-- A esto podran acceder los demas roles -->
            <a href="{{route('activos')}}">Activos</a>
            <a href="{{route('instrumentos')}}">Instrumentos</a>     
            <a href="{{route('contratos')}}">Contratos de Prestamo</a>
            <a href="{{route('boletas')}}">Boletas de Salida</a>
        @endif
        {{-- <a href="{{route('ayuda')}}">Ayuda</a> --}}
    </div>
    <div class="btn_menu" id="cerrado" onclick="mostrarMenu(this)">
        <div id="barra1"></div>
        <div id="barra2"></div>
        <div id="barra3"></div>
    </div>  
    @include('alertas.alertas')   
    <script>
        // Codigo para cambiar la ubicacion de todas las alertas mostradas con alertify
        alertify.set('notifier','position', 'top-right');
    </script> 
@else
    <script>window.location = "/";</script>
@endif          

<div id="div_loading">    
    <i class="fas fa-spinner fa-pulse fa-10x cargando"></i>
</div>

</body>
</html>