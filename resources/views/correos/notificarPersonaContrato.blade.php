@component('mail::message')

@component('mail::panel')
    <h1> Contrato EMSPZ-CTR-{{$contrato->idContrato}}</h1>
@endcomponent

<h3>Saludos, {{$contrato->nombreResponsable}}.</h3>
<p>A continución adjuntamos el detalle sobre su contrato. </p>

@component('mail::panel')

@endcomponent

Le informa,<br>
{{ config('app.name') }}

@endcomponent