@component('mail::message')

@component('mail::panel')
<h1>Recuperacion <small>de Contraseña</small></h1>
@endcomponent

{{ $usuario->persona->nombre }}, has solicitado 
la recuperacion de tu contraseña.

@component('mail::panel')
Tu contraseña es: <strong id="contraseña" hidden>{{ $usuario->contraseña }}</strong>
@endcomponent

Gracias,<br>
{{ config('app.name') }}

@endcomponent