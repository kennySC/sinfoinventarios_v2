@component('mail::message')

@component('mail::panel')
    <h1> Boleta de Salida EMSPZ-BLT-{{$boleta->idBoleta}} Finalizada</h1>
@endcomponent

<h3>Saludos, {{$boleta->nombreResponsable}}.</h3>
<p>Le informamos que su boleta de salida fue finalizada. En dicha boleta se encontraban: </p>

@component('mail::panel')
    <h3>ACTIVOS</h3>    
    @foreach($activos ?? "" as $act)
        <strong>{{$act->activo->nombre}}</strong><br>            
    @endforeach
    <p>Fecha correspondida de devolucion: <strong>{{$boleta->fechaDevolucion}}</strong></p>
@endcomponent

{{$boleta->fechaEntrega}} 

Le informa, <br>
{{ config('app.name') }}

@endcomponent
