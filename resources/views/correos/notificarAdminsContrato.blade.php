@component('mail::message')

@component('mail::panel')
    Saludos administrador o administradora de Sinfoinventarios, se le informa que se acaba de 
    <strong>{{$accion}}</strong> el contrato <strong>EMSPZ-CTR-{{$contrato->idContrato}}</strong>. 
@endcomponent
<br>
<br>
@component('mail::panel')

    <strong>Contrato del instrumento:</strong><br>
    {{$contrato->instrumento->tipoInstrumento->tipoInstrumento}} con número de inventario {{$contrato->instrumento->numeroInstrumento}} @if($contrato->instrumento->marca!=null), marca: {{$contrato->instrumento->marca}}@endif @if($contrato->instrumento->serie!=null ), número de serie: {{$contrato->instrumento->serie}}@endif @if($contrato->instrumento->observaciones!=null), {{$contrato->instrumento->observaciones}}@endif
@endcomponent
@component('mail::panel')
    <strong>Se le fue entregado al estudiante:</strong><br>
    {{$contrato->estudiante->nombreCompleto}}, con cédula de identidad número {{$contrato->estudiante->cedula}}
@endcomponent
@component('mail::panel')
    @if($contrato->cedulaEncargado!=null) 
    <strong>En calidad de padre de familia o encargado:</strong><br>
    {{$contrato->encargado->nombreCompleto}}, con cédula de identidad número {{$contrato->encargado->cedula}}
    @endif
@endcomponent
@component('mail::panel')
    <strong>En calidad de fiador:</strong> <br> 
    @if($contrato->cedulaEncargado!=null){{$contrato->encargado->nombreCompleto}}@endif{{$contrato->nombreCompletoFiador}}, con cédula de identidad número @if($contrato->cedulaEncargado!=null){{$contrato->encargado->cedula}}@endif{{$contrato->cedulaFiador}}.
@endcomponent
@component('mail::panel')
    <strong>El contrato tendrá inicio el:</strong><br>
    {{$contrato->fechaInicio}}
@endcomponent
@component('mail::panel')
    <strong>Finalizando el:</strong><br>
    {{$contrato->fechaFinalizacion}}
@endcomponent
<br>
<br>
Sin más, se despide<br>
{{ config('app.name') }}

@endcomponent