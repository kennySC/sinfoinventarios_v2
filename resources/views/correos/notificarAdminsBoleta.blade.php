@component('mail::message')

@component('mail::panel')
    Saludos administrador o administradora de Sinfoinventarios, se le informa que la boleta de salida <strong>EMSPZ-BLT-{{$boleta->idBoleta}}</strong>
    acaba de ser entregada. A continuación los detalles sobre dicha boleta.
@endcomponent

La boleta fue entregada a <strong>{{$boleta->nombreResponsable}}</strong>.
Las observaciones sobre dicha boleta son las siguientes: 
    <p>{{$boleta->observaciones}}</p>
<p>A continuación se detallan el o los activos en préstamo:</p>

@component('mail::panel')
    <h3>ACTIVOS</h3>    
    @foreach($activos ?? "" as $act)
        Código: <strong>{{$act->activo->codActivo}}</strong> Nombre: <strong>{{$act->activo->nombre}}</strong><br>            
    @endforeach
@endcomponent

<p>La fecha esperada de devolucion es para: <strong>{{$boleta->fechaDevolucion}}</strong></p>
<p>Esta boleta fue creada por el o la usuario/usuaria <strong>{{$boleta->usuario->nombreUsuario}}</strong>.</p>

{{$boleta->fechaEntrega}} 

Sin más, se despide<br>
{{ config('app.name') }}

@endcomponent
