@extends('layouts.master')

@section('title', 'Sinfoinventarios | Boletas de Salida')

@section('seccionActiva', 'Boletas de Salida')

@section('css_js')
    <link rel="stylesheet" href="{{asset('assets/css/boletas.css')}}">
    <script src="{{asset('assets/js/boletas.js')}}"></script>
@endsection

@section('content')

@if (session('userdata') != null)

    <div class="div_contenido">
        <div id="div_izq">
            <form id="form_filtrar" action="{{route('filtrarBoletas')}}" method="POST">
                @csrf
                <h1 id="header_filtrado">Opciones <small class="small_class">de filtrado</small></h1><br>
                <hr>
                <label id="lb_buscar" for="txt_cod_boleta">
                    Código de la boleta:
                    <h4>EMSPZ-BLT-<input type="text" name="txt_cod_boleta" id="txt_cod_boleta" class="txt_datos" maxlength="5"></h4>
                </label>
                <label id="lb_buscar" for="txt_responsable">
                    Responsable:
                    <input type="text" name="txt_responsable" id="txt_responsable" class="txt_datos" maxlength="100">
                </label>

                <div id="div_opciones">

                    <label id="lb_buscar" for="cmb_datos">Estado de la boleta:</label>
                    <select name="cmb_estado" id="cmb_estado" class="cmb_datos">
                        <option selected value="T">Todos</option>
                        <option value="gen">Generada</option>
                        <option value="act">Activa</option>
                        <option value="dev">Devuelta</option>
                        <option value="atr">Atrasada</option>
                    </select>
                
                    <div class="div_btnsfiltrar">
                        <input type="submit" class="boton" id="btn_filtrar" value="Filtrar">
                        <input type="button" class="boton" id="btn_limpiar" value="Limpiar" onclick="limpiarFiltros()">
                    </div>
                </div>
            </form>
            <div class="div_btns_agregar">
                    <!--Boton para agregar una nueva boleta de salida--> 
                    <!-- Consultamos si el usuario logeado es administrador o regular, de ser asi, la vista contara con el boton de crear una boleta de salida -->
                @if(session('userdata')['role'] != 'M')
                    <input type="button" id="btn_nuevaBoleta" class="boton" value="Crear boleta" onclick="irNuevaBoleta()">
                    <a id="a_nuevaBoleta" href="{{route('agregarBoleta')}}"></a>
                @endif
            </div>
        </div>

        <div id="div_der">
            @if ($boletas->count() <= 0)
                <h1 class='h1_sinResultado'> ¡Sin resultados para mostrar!</h1>
            @else 
                @foreach ($boletas ?? '' as $bol)
                    <div @switch($bol->estado)
                        @case('atr')
                            class="div_contenedorBoleta cb_atrasada"
                            @break
                        @case('dev')
                            class="div_contenedorBoleta cb_devuelta"
                            @break
                        @case('can')
                            class="div_contenedorBoleta cb_cancelada"
                            @break
                        @case('act')
                            class="div_contenedorBoleta cb_activa"
                            @break
                        @default
                            class="div_contenedorBoleta"
                            @break
                    @endswitch>
                        <h1 class="h1_boleta">{{$bol->nombreResponsable}}</h1>                        
                        <div class="div_info_boleta">
                            Código: 
                            <span>EMSPZ-BLT-{{$bol->idBoleta}}</span>
                            @if($bol->estado != 'gen')
                                Fecha de entrega: 
                                <span> {{date('d-m-Y', strtotime($bol->fechaEntrega))}} </span>
                            @endif
                            Fecha esperada de devolucion: 
                            <span> {{date('d-m-Y', strtotime($bol->fechaDevolucion))}} </span>
                            Estado:
                            @switch ($bol->estado)
                                @case('gen')
                                    <span> Generada </span>
                                    @break
                                @case('atr')
                                    <span class="atrasada"> Atrasada </span>
                                    @break
                                @case('act')
                                    <span class="activa"> Activa </span>
                                    @break
                                @case('dev')
                                    <span class="devuelta"> Devuelta </span>
                                    @break
                                @case('can')
                                    <span class="cancelada"> Cancelada </span>
                                    @break
                                <!-- Una boleta con estado: "entrega atrasada" quiere decir que ya la boleta fue devuelta/finalizada/cancelada, solo que fue despues de su fecha esperada de devolucion -->
                                <!-- Por lo tanto queda como entregada pero como una entrega atrasada -->
                                @case('eat')
                                    <span> Entrega Atrasada </span>
                                    @break
                            @endswitch
                        </div>                        
                        <div class="div_botones">
                            <!-- A continuacion declaramos los botones y los "a" necesarios para acceder a las rutas usando javascript  -->
                            <input id="{{$bol->idBoleta}}" type="button" class="btnDetalle" value="Ver detalles" onclick="ver_detalle(this)">
                        </div>     
                    </div>
                @endforeach
            @endif
        </div>
    </div>

@else 
    <script>window.location = "/";</script>
@endif

@endsection

