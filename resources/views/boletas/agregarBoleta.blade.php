@extends('layouts.master')

@section('title', 'Sinfoinventarios | Boletas de Salida')

@section('seccionActiva', 'Creando boleta de salida')

@section('css_js')
    <link rel="stylesheet" href="{{asset('assets/css/editarAgregarBoleta.css')}}">
    <script src="{{asset('assets/js/agregarBoleta.js')}}"></script>
@endsection

@section('content')

@if (session('userdata') != null)

    <div class="div_contenido">
        <form id="form_agregarboleta" action="{{route('guardarBoleta')}}" method ="POST"><!--route('actualizarBoleta')-->
            @csrf
            @method('post')
                <div id="div_datos">
                    <label for="txt_responsable">
                        Nombre de la persona responsable: <br>
                        <input class="txt" type="text" name="txt_responsable" id="txt_responsable" maxlength="100">
                        @error('txt_responsable')
                            <small class="txtError">*{{$message}}</small>
                        @enderror
                    </label>
                    <label for="txt_fechadevolucion">
                        Fecha esperada de devolucion: <br>
                        <input class="txt" type="date" name="txt_fechadevolucion" id="txt_fechadevolucion" onchange="verificarFecha()">
                        @error('txt_fechadevolucion')
                            <small class="txtError">*{{$message}}</small>
                        @enderror
                    </label>
                    <label for="txt_correoresponsable">
                        Correo de la persona responsable: <br>
                        <input class="txt" type="text" name="txt_correoresponsable" id="txt_correoresponsable" maxlength="100">
                        @error('txt_correoresponsable')
                            <small class="txtError">*{{$message}}</small>
                        @enderror
                    </label>
                    <label for="txt_observaciones">
                        Observaciones: <br>
                        <textarea class="txt" name="txt_observaciones" id="txt_observaciones" cols="25" rows="80" value=""></textarea>
                    </label>
                </div>
                <!-- <hr> -->
                <div id="div_btns">
                    <input class="btn btn_guardar" type="submit" id="btn_guardar" value="Guardar boleta">
                    <input class="btn" type="button" id="btn_cancelar" value="Volver" onclick="cancelar()">
                    <a href="{{route('boletas')}}" id="a_cancelar"></a>
            </div>
        </form>
        
        <!-- Boton para abrir ventana modal donde se va a mostrar todos los activos con opciones de agregar a la boleta -->
        <input id="btn_agregar_activo" type="button" class="btnAgregarActivo" value="Agregar" onclick="seleccionActivos(this)">
        <a id="a_editar_" href=""></a>
        <!-- Div del lado derecha de la pantalla donde va a mostrarse los activos respectivos de la boleta de salida -->
        <div id="div_activosBoleta" style="margin-right: 40px">
            
            <h1>Activos de la boleta </h1>
            <!-- Div contenedor dentro del cual iran los activos -->
            <div id="div_contActivos">
                <!-- Talvez la tabla tenga que ir dentro de un form  -->
                <table id="tabla_activos" style="width: 98%">
                    <thead>
                        <tr style="color: white">
                            <th>Código</th>
                            <th># Serie</th>
                            <th>Nombre</th>
                            <th>Opciones</th>
                        </tr>
                    </thead>
                    <tbody class="tbody-style">

                    </tbody>
                </table> 
            </div>

            <input class="btn" id="btn_limpiar" type="button" onclick="limpiarTabla()" value="Limpiar Tabla">
        </div>
    </div>

@else 
    <script>window.location = "/";</script>
@endif

@endsection