@extends('layouts.master')

@section('title', 'Sinfoinventarios | Boletas de Salida')

@section('seccionActiva', 'Detalle boleta de salida')

@section('css_js')
    <link rel="stylesheet" href="{{asset('assets/css/editarAgregarBoleta.css')}}">
    <script src="{{asset('assets/js/detalleBoleta.js')}}"></script>
@endsection

@section('content')

@if (session('userdata') != null)

    <div class="div_contenido">
        <div id="form_detalleboleta">
            @csrf
            <div id="div_datos">
                <label for="txt_codigo">
                    Código de la boleta: <br>
                    <input class="txt" type="text" name="txt_codigo" id="txt_codigo" value="EMSPZ-BLT-{{$boleta->idBoleta}}" disabled>
                </label>
                <label for="txt_responsable">
                    Nombre de la persona responsable: <br>
                    <input class="txt" type="text" name="txt_responsable" id="txt_responsable" value="{{$boleta->nombreResponsable}}" disabled>
                </label>
                <!-- Solamente las boletas que ya hayan sido activadas/entregadas pueden mostrar la fecha en que fueron entregadas -->
                @if($boleta->estado != 'gen')
                    <label for="txt_fechaentrega">
                        Fecha de entrega: <br>
                        <input class="txt" type="text" name="txt_fechaentrega" id="txt_fechaentrega" value="{{date('d / m / Y H:i:s', strtotime($boleta->fechaEntrega))}}" disabled>
                    </label>
                @endif
                <label for="txt_fechadevolucion">
                    Fecha esperada de devolucion: <br>
                    <input class="txt" type="text" name="txt_fechadevolucion" id="txt_fechadevolucion" value="{{date('d / m / Y', strtotime($boleta->fechaDevolucion))}}" disabled>
                </label>
                <!-- Unicamente si una boleta fue devuelta o cancelada va a mostrar el campo de la fecha en que fue devuelta/cancelada -->
                @if($boleta->estado == 'dev' || $boleta->estado == 'can')
                    <label for="txt_fechaRealDevolucion">
                        Fecha real de devolucion: <br>
                        <input class="txt" type="text" name="txt_fechaRealDevolucion" id="txt_fechaRealDevolucion" value="{{date('d / m / Y', strtotime($boleta->fechaRealDevolucion))}}" disabled>
                    </label>
                @endif
                <label for="txt_correoresponsable">
                    Correo de la persona responsable: <br>
                    <input class="txt" type="text" name="txt_correoresponsable" id="txt_correoresponsable" value="{{$boleta->emailResponsable}}" disabled>
                </label>
                <label for="cmb_estado">
                    Estado de la boleta: <br>
                    <select class="cmb" name="cmb_estado" id="cmb_estado" disabled>
                        <option value="gen" @if ($boleta->estado == 'gen')
                            selected
                        @endif>Generada</option>
                        <option value="atr" @if ($boleta->estado == 'atr')
                            selected
                        @endif>Atrasada</option>
                        <option value="act" @if($boleta->estado == 'act')
                            selected
                        @endif>Activa</option>
                        <option value="dev" @if ($boleta->estado == 'dev')
                            selected
                        @endif>Devuelta</option>
                        <option value="can" @if ($boleta->estado == 'can')
                            selected
                        @endif>Cancelada</option>
                    </select>
                    @error('cmb_estado')
                        <small class="txtError">*{{$message}}</small>
                    @enderror
                </label>
                <label for="txt_observaciones">
                    Observaciones: <br>
                    <textarea class="txt" name="txt_observaciones" id="txt_observaciones" cols="25" rows="80" 
                        @if($boleta->estado == 'can' || $boleta->estado == 'dev' || $boleta->estado == 'eat') disabled
                        @endif>{{$boleta->observaciones}}</textarea>
                </label>
            </div>
            
            <div id="div_btns">
                <!-- Validar dependiendo del estado que tenga la boleta, el boton puede variar, ser entregar, cancelar, o finalizar     -->
                @switch ($boleta->estado)
                    @case('gen')
                        <input  class="btn btn_guardar" type="button" id="{{$boleta->idBoleta}}" value="Entregar boleta" onclick="activarBoleta(this)">
                        <!-- <a id="a_activar_{{$boleta->idBoleta}}" href="{{route('activarBoleta', $boleta->idBoleta)}}"></a> -->
                    @break
                    @case('act')
                        <input  class="btn btn_guardar" type="button" id="{{$boleta->idBoleta}}" value="Finalizar boleta" onclick="finalizarBoleta(this)">
                        <a id="a_finalizar_{{$boleta->idBoleta}}" href="{{route('finalizarBoleta', $boleta->idBoleta)}}"></a>
                    @break
                    @case('atr')
                        <input class="btn btn_guardar" type="button" id="{{$boleta->idBoleta}}" value="Finalizar boleta" onclick="cancelarBoletaAtrasada(this)">
                        <a id="a_cancelar_{{$boleta->idBoleta}}" href="{{route('cancelarBoletaAtr', $boleta->idBoleta)}}"></a>
                    @break
                @endswitch
                <!-- Consultamos si el estado de la boleta es diferente a devuelta y a cancelada, es decir, se encuentra activa, generada o atrasada -->
                @if($boleta->estado != 'dev' && $boleta->estado != 'can' && $boleta->estado != 'atr' && $boleta->estado != 'eat')
                    <input type="button" class="btn btnCancelarBol" id="{{$boleta->idBoleta}}" name='{{$boleta->nombreResponsable}}' value="Cancelar Boleta" onclick="cancelarBoleta(this)">
                    <a id="a_cancelar_{{$boleta->idBoleta}}" href="{{route('cancelarBoleta', $boleta->idBoleta)}}"></a>                    
                @endif     
                @if($boleta->estado == 'act')
                    {{-- Boton para imprimir la boleta --}}
                    <button class="btn btn_guardar" id="{{$boleta->idBoleta}}" style="position: fixed; top: 90%; left: 50%; transform: translateX(-50%);" onclick="imprimirBoleta(this)">
                        <i class="fas fa-print"></i> Imprimir
                    </button>

                    <a hidden id="imprimirBoleta" href="{{ route('imprimirBoleta', $boleta->idBoleta) }}"></a>
                @endif
            </div>
        </div>

        <!-- Consultamos si la boleta se encuentra con estado activa, o que ya fue entregada, entonces se presenta la posibilidad de imprimir la boleta de salida -->
        @if($boleta->estado == 'act')
            <i class="fas fa-print"></i> <!--Boton para imprimir la boleta de salida-->
        @endif
        
        <!-- Div del lado derecha de la pantalla donde va a mostrarse los activos respectivos de la boleta de salida -->
        <div id="div_activosBoleta">
            
            <h1>Activos de la boleta EMSPZ-BLT-{{$boleta->idBoleta}}</h1>
            <!-- Div contenedor dentro del cual iran los activos -->
            <div id="div_contActivos">
                @if ($detalles->count() <= 0)
                    <h1 class='h1_sinResultado'>Sin Activos</h1>
                @else 
                <table id="tabla_activos" style="width: 98%">
                    <thead>
                        <tr style="color: white">
                            <th>Código</th>
                            <th># Serie</th>
                            <th>Nombre</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($detalles as $det)
                            <tr>
                               <td> {{$det->activo->codActivo}} </td> 
                               <td> {{$det->activo->serie}} </td> 
                               <td> {{$det->activo->nombre}} </td> 
                            </tr>
                        @endforeach                            
                    </tbody>
                </table> 
                @endif
            </div>
        </div>
        
        <div id="div_fondoOscuro">
                <div id="div_contMensaje">
                    
                </div>
            </div>
    </div>

    <input class="btn" type="button" id="btn_cancelar" style="position: fixed; top: 90%; left: 90%; z-index: 1;" value="Volver" onclick="irBoletas()">

@else 
    <script>window.location = "/";</script>
@endif
    
@endsection