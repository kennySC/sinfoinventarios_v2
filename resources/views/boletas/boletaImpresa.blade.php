<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/boletaImpresa.css">
</head>
<body>
    <div id="documento">
        <div id="top" dir="rtl">
                        
            <div >Escuela de Música Sinfónica de Pérez Zeledón</div>
            <div >Boleta: EMSPZ-BLT-{{$boleta->idBoleta}}</div>
        </div>
        <hr>
        <div id="info">
            <h3>
                Generada por:       <small>{{$boleta->usuario->nombreUsuario}}</small>
                <br><br>
                Responsable:        <small>{{$boleta->nombreResponsable}}</small>
                <br>
                Email:              <small>{{$boleta->emailResponsable}}</small>
                <br><br>
                Fecha Entrega:      <small>{{date('d-m-Y', strtotime($boleta->fechaEntrega))}}</small>
                <br>
                Fecha Devolucion:   <small>{{date('d-m-Y', strtotime($boleta->fechaDevolucion))}}</small>
                <br><br>
                Observaciones:<br>
                <small>{{$boleta->observaciones}}</small>
                <br><br>
                Cantidad Activos:   <small>{{$activos->count()}}</small>
            </h3>
        </div>
        <hr>
        <br><br><br>
        <h2>Lista de Activos solicitados</h2>
        <table id="tabla" style="width: 90%;" align="center" border="1">
            <thead>
                <tr>
                    <td align="center">Código Activo</td>
                    <td align="center"># Serie</td>
                    <td align="center">Nombre</td>
                </tr>
            </thead>        
            <tbody>
                @foreach ($activos as $act)
                    <tr>
                        <td>{{$act->codActivo}}</td>
                        <td>{{$act->serie}}</td>
                        <td>{{$act->nombre}}</td>
                    </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr></tr>
            </tfoot>
        </table>
        <br><br><br>
        <hr>
        <br>
        <br>
        <br>
        <h4>Firma del responsable: <br><br><br> __________________________________________</h4>    
    </div>
</body>
</html>