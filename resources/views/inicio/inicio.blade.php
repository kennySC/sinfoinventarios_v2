@extends('layouts.master')

@section('title', 'Inicio SinfoInventarios')

{{-- @section('seccionActiva', 'Inicio SinfoInventarios') --}}

@section('css_js')
    <link rel="stylesheet" href="{{asset('assets/css/inicio.css')}}">
    <script src="{{asset('assets/js/inicio.js')}}"></script>
@endsection

@section('content')
    
    <div class="div_contenido">      
        <h2>            
            @if (session('userdata')['gender'] == 'F')
                Bienvenida <br> <small>{{session('userdata')['realname']}}</small>
                @else
                Bienvenido <br> <small>{{session('userdata')['realname']}}</small>
            @endif
        </h2>
        
        {{-- Accesos directos de los --}}
        <div class="div_botones">
            <div class="div_fila">
                <button class="btn" id="btn_activos" onclick="accesoRapido(this)">
                    <i class="fas fa-clipboard-list fa-2x"></i> <small>Activos</small>
                </button>
                <button class="btn" id="btn_instrumentos" onclick="accesoRapido(this)">
                    <i class="fas fa-drum fa-2x"></i> <small>Instrumentos</small>
                </button>
            </div>
            <div class="div_fila">
                <button class="btn" id="btn_contratos" onclick="accesoRapido(this)">
                    <i class="fas fa-file-signature fa-2x"></i> <small>Contratos</small>
                </button>
                <button class="btn" id="btn_boletas" onclick="accesoRapido(this)">
                    <i class="fas fa-file-invoice fa-2x"></i> <small>Boletas</small>
                </button>
            </div>
        </div>    
        
        {{-- Los enlaces hidden --}}
        <a hidden id="a_activos" href="{{route('activos')}}"></a>
        <a hidden id="a_instrumentos" href="{{route('instrumentos')}}"></a>        
        <a hidden id="a_contratos" href="{{route('contratos')}}"></a>
        <a hidden id="a_boletas" href="{{route('boletas')}}"></a>

    </div>            

@endsection