@extends('layouts.master')

@section('title', 'Sinfoinventarios | Ayuda')

@section('seccionActiva', '¿Cómo le podemos ayudar?')

@section('css_js')
    <link rel="stylesheet" href="css/ayuda.css">
    <script src="js/ayuda.js"></script>
@endsection

@section('content')

    <div class="div_contenido">
        <div id="div_izq">
            <h1>Simon div izquierdo</h1>
            <h2>Auxilio</h2>
        </div>
        <div id="div_der">
            <h1>Simon div derecho</h1>
        </div>
    </div>

@endsection