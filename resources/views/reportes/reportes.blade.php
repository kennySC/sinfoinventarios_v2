@extends('layouts.master')

@section('title', 'Sinfoinventarios | Reportes')

@section('seccionActiva', 'Reportes')

@section('css_js')
    <link rel="stylesheet" href="{{asset('assets/css/reportes.css')}}">
    <script src="{{asset('assets/js/reportes.js')}}"></script>
@endsection

@section('content')

@if (session('userdata') != null)

    <div class="div_contenido">
        <div id="div_izq">
            <div class="div_form">                
                @csrf          
                <div class="div_titulo">
                    <h3 id="header">Generar <small class="small_class">reporte</small></h3><br>
                    <hr>
                </div>                      
                <div class="div_opciones">
                    <label class="label" for="cmb_datos">Institución: <br>
                        <select name="cmb_institucion" id="cmb_institucion" class="cmb_datos">
                            <option id="T" selected value="T">Todas</option>
                            @foreach ($instituciones ?? '' as $inst)
                                <option id="{{$inst->estdInstitucion}}" value="{{$inst->idInstitucion}}">{{$inst->nombre}}</option>
                            @endforeach
                        </select>
                    </label>
                    <label class="label" for="cmb_datos">Tipo: <br>
                            <select name="cmb_tipo_rep" id="cmb_tipo_rep" class="cmb_datos">                                
                                <option selected value="A"> Activos </option>
                                <option value="I"> Instrumentos </option>
                            </select>
                    </label>                
                </div>
                <div class="div_btn">
                    <input type="submit" class="btn" id="btn_generar" value="Generar">
                </div>                
            </div>
            
        </div>
        <div id="div_der">
            <div id="div_preview"> <!--Div donde se va a mostrar una vista previa del reporte generado-->  
                <table class="tabla" id="tabla_reporte">
                </table>
            </div>
        </div>
    </div>

@else 
    <script>window.location = "/";</script>
@endif

@endsection