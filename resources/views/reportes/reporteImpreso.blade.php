<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="/css/reporteImpreso.css">
    </head>
    <body>
        <div id="documento">
            <div id="top" dir="rtl">
                            
                <div >Escuela de Música Sinfónica de Pérez Zeledón</div>
                <div >Reporte: EMSPZ-REP-{{$date}}</div>
            </div>
            <hr>
            <div id="info">
                <h3>
                    Generado por:       <small>{{session('userdata')['realname']}}</small>
                    <br><br>
                    En la fecha:      <small>{{$date}}</small>
                    <br>
                    <br><br>
                    <!-- Consultamos si el tipo de reporte que se genero es diferente de un reporte de instrumentos -->
                    @if($tipo_reporte != 'I')
                        Cantidad Activos:   <small>{{$reporte->count()}}</small> <!--la variable "reporte" en este caso serian o puros activos o un mezclados con instrumentos--->
                    <!-- Si no es gallo, es gallina -->
                    @else 
                        Cantidad Instrumentos:   <small>{{$reporte->count()}}</small> <!--la variable "reporte" en este caso serian o puros instrumentos--->
                    @endif
                </h3>
            </div>
            <hr>
            <br><br><br>

            <!-- Poner aqui un if validando que tipo de reporte me llega para imprimir, de acuerdo a eso cambia la tabla, si de activos o instrumentos -->

            <h2>Lista de Activos </h2>
            <table id="tabla" style="width: 90%;" align="center" border="1">
                <thead>
                    <tr>
                        <td align="center">Código Activo</td>
                        <td align="center">Tipo</td>
                        <!-- Si el reporte es sobre todas las instituciones, mostramos la columna de institucion  -->
                        @if($tipo_reporte == 'T')
                            <td align="center">Institución</td>
                        @endif
                        <td align="center">Nombre</td>
                        <td align="center"># Serie</td>
                        <td align="center">₡ Valor</td>
                        <td align="center">Responsable</td>
                        <td align="center">Observaciones</td>
                        <td align="center">Estado</td>
                    </tr>
                </thead>        
                <tbody>
                    @foreach ($reporte as $act)
                        <tr>
                            <td>{{$act->codActivo}}</td>
                            <td>{{$act->tipo_activo->tipoActivo}}</td>
                            <!-- Si el reporte es sobre todas las instituciones, ponemos el nombre de cada una, si no, el campo no se muestra -->
                            @if($tipo_reporte == 'T')
                                <td>{{$act->institucion->nombre}}</td>
                            @endif
                            <td>{{$act->nombre}}</td>
                            <td>{{$act->serie}}</td>
                            <td>{{$act->valor}}</td>
                            <td>{{$act->responsable}}</td>
                            <td>{{$act->observaciones}}</td>
                            <td>{{$act->estado_activo->estado}}</td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr></tr>
                </tfoot>
            </table>
            <br><br><br>
        </div>
    </body>
</html>