@extends('layouts.master')

@section('title', 'Sinfoinventarios | Boletas de Salida')

@section('seccionActiva', 'Detalle Contrato de Prestamo')

@section('css_js')
    <link rel="stylesheet" href="{{asset('assets/css/editarAgregarContratos.css')}}">
    <script src="{{asset('assets/js/detalleContrato.js')}}"></script> 
@endsection

@section('content')

@if (session('userdata') != null)

    <div class="div_contenido">
    <form id="form_detalleContrato" action="{{route('actualizarContrato')}}" method ="post">
        
        
            @csrf
            @method('put')
            <div id="div_datos">
                <label for="txt_codigo">
                    Código del contrato: <br>
                    <input class="txt" type="text" name="txt_codigo" id="txt_codigo" value="EMSPZ-CTR-{{$contrato->idContrato}}" disabled>
                </label>

                <label for="txt_fecha_inicio">
                        Fecha de Inicio: <br>
                        <input class="txt" type="text" name="txt_fecha_inicio" id="txt_fecha_inicio" value="{{date('d / m / Y', strtotime($contrato->fechaInicio))}}" disabled>
                    </label>
                
                <label for="txt_fecha_finalizacion">
                    Fecha de finalización: <br>
                    <input class="txt" type="text" name="txt_fecha_finalizacion" id="txt_fecha_finalizacion" value="{{date('d / m / Y', strtotime($contrato->fechaFinalizacion))}}" disabled>
                </label>

                <label for="txt_presidente">
                    Presidente de la Asociación: <br>
                    <input class="txt" type="text" name="txt_presidente" id="txt_presidente" value=@if($contrato->presidente) "{{$contrato->presidente->nombreCompleto}}" @else "" @endif disabled>
                </label>

                <label for="txt_ced_estudiante">
                    Cédula del estudiante: <br>
                    <input class="txt" type="text" name="txt_ced_estudiante" id="txt_ced_estudiante" value="{{$contrato->estudiante->cedula}}" disabled>
                </label>               
                <label for="txt_estudiante">
                    Nombre del estudiante: <br>
                    <input class="txt" type="text" name="txt_estudiante" id="txt_estudiante" value="{{$contrato->estudiante->nombreCompleto}} " disabled>
                </label>
                <label for="txt_email_estudiante">
                    Email del estudiante: <br>
                    <input class="txt" type="text" name="txt_email_estudiante" id="txt_email_estudiante" value="{{$contrato->estudiante->email}}" disabled>
                </label>
                <label for="txt_tel_estudiante">
                    Télefono del estudiante: <br>
                    <input class="txt" type="text" name="txt_tel_estudiante" id="txt_tel_estudiante" value="{{$contrato->estudiante->telefono}}" disabled>
                </label> 


                <label for="txt_ced_responsable">
                    Cédula de la persona encargada: <br>
                    @if($contrato->cedulaEncargado != null)
                    <input class="txt" type="text" name="txt_ced_responsable" id="txt_ced_responsable" value="{{$contrato->encargado->cedula}}" disabled>
                    @else
                    <input class="txt" type="text" name="txt_ced_responsable" id="txt_ced_responsable" value="{{$contrato->cedulaFiador}}" disabled>
                    @endif
                </label>
                <label for="txt_responsable">
                    Nombre de la persona encargada: <br>
                    @if($contrato->cedulaEncargado != null)
                    <input class="txt" type="text" name="txt_responsable" id="txt_responsable" value="{{$contrato->encargado->nombreCompleto}} " disabled>
                    @else
                    <input class="txt" type="text" name="txt_responsable" id="txt_responsable" value="{{$contrato->nombreCompletoFiador}} " disabled>
                    @endif
                    
                </label>
                <label for="txt_email_estudiante">
                    Email de la persona encargada: <br>
                    @if($contrato->cedulaEncargado != null)
                    <input class="txt" type="text" name="txt_email_estudiante" id="txt_email_estudiante" value="{{$contrato->encargado->email}}" disabled>
                    @else
                    <input class="txt" type="text" name="txt_email_estudiante" id="txt_email_estudiante" value="{{$contrato->emailFiador}}" disabled>
                    @endif

                </label>
                <label for="txt_tel_estudiante">
                    Télefono de la persona encargada: <br>
                    @if($contrato->cedulaEncargado != null)
                    <input class="txt" type="text" name="txt_tel_estudiante" id="txt_tel_estudiante" value="{{$contrato->encargado->telefono}}" disabled>
                    @else
                    <input class="txt" type="text" name="txt_tel_estudiante" id="txt_tel_estudiante" value="{{$contrato->telefonoFiador}}" disabled>
                    @endif
                </label>                 

                <label for="cmb_estado">
                    Estado del Contrato: <br>
                    <select class="cmb" name="cmb_estado" id="cmb_estado" disabled>
                        <option value="A" @if ($contrato->estado == 'A')
                            Activo
                        @endif>Activo</option>
                        <option value="T" @if ($contrato->estado == 'T')
                            selected
                        @endif>Terminado</option>
                        <option value="C" @if($contrato->estado == 'C')
                            selected
                        @endif>Cancelado</option>
                    </select>
                    @error('cmb_estado')
                        <small class="txtError">*{{$message}}</small>
                    @enderror
                </label>
               <input type="hidden" name="idContrato" id="idContrato" value='{{$contrato->idContrato}}'>
               <input type="hidden" id="fecha_fin" value='{{$contrato->fechaFinalizacion}}'>
               <label for="txt_obs_instrumento">
                    Observaciones del Contrato: <br>
                    <input class="txt_area" type="text" name="txt_observaciones" id="txt_observaciones" value="{{$contrato->observaciones}}">
                </label>
                <div class="div_instrumento">
                    <label for="txt_cod_instrumento">
                        Código del instrumento: <br>
                        <input class="txt" type="text" name="txt_cod_instrumento" id="txt_cod_instrumento" value="{{$contrato->codInstrumento}}" disabled>
                    </label>
                    <label for="txt_nom_instrumento">
                        Nombre del instrumento: <br>
                        <input class="txt" type="text" name="txt_nom_instrumento" id="txt_nom_instrumento" value="{{$contrato->instrumento->nombre}}" disabled>
                    </label>
                    <label for="txt_num_instrumento">
                        Número del instrumento: <br>
                        <input class="txt" type="text" name="txt_num_instrumento" id="txt_num_instrumento" value="{{$contrato->instrumento->numeroInstrumento}}" disabled>
                    </label>
                    
                    <input type="button" id="btn_vermas" class="btnVerMas" value="Ver más" onclick="verMas({{$contrato->instrumento}})">
                </div>
            </div>
            <div id="div_btns">
                
                @if($contrato->estado == 'A' )
                <input type="button" class="btn" id="btn_TerminarContrato" value="Terminar Contrato" onclick="terminarContrato({{$contrato->idContrato}})">
                <input type="button" class="btn" id="btn_CancelarContrato" value="Cancelar Contrato" onclick="cancelarContrato({{$contrato->idContrato}})">
                
                <input type="button" class="btn" id="btn_descargar" value="Descargar Contrato" onclick="descargar({{$contrato->idContrato}})">
                <a id="a_descargar" href="{{route('descargarContrato', $contrato->idContrato)}}"></a>
                @endif

                <input class="btn" type="submit" id="btn_guardar" value="Guardar Observaciones">
                <input class="btn" type="button" id="btn_volver" value="Volver" onclick="cancelar()">
                <a href="{{route('contratos')}}" id="a_cancelar"></a>
                
            </div>
        </form>
            
    </div>

@else 
    <script>window.location = "/";</script>
@endif

@endsection