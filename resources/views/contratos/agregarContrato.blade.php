@extends('layouts.master')

@section('title', 'Sinfoinventarios | Contratos')

@section('seccionActiva', 'Nuevo Contrato de Prestamo')

@section('css_js')
    <link rel="stylesheet" href="{{asset('assets/css/editarAgregarContratos.css')}}">
    <script src="{{asset('assets/js/agregarContrato.js')}}"></script>
@endsection

@section('content')

@if (session('userdata') != null)

    <div class="div_contenido">
        <form id="form_agregarcontrato" action="{{route('guardarContrato')}}" method ="POST">
            @csrf
            @method('post')
                <div id="div_datos">
                
                <label for="txt_fechadeinicio">
                        Fecha de inicio: <br>
                        <input class="txt" type="date" name="txt_fechadeinicio" id="txt_fechadeinicio" onchange="verificarFechaInicio()" value="{{old('txt_fechadeinicio')}}">
                        @error('txt_fechadeinicio')
                            <small class="txtError">*{{$message}}</small>
                        @enderror
                        
                    </label>

                    <label for="txt_fechafinalizacion">
                        Fecha de finalizacion: <br>
                        <input class="txt" type="date" name="txt_fechafinalizacion" id="txt_fechafinalizacion" onchange="verificarFechaFin()" value="{{old('txt_fechafinalizacion')}}">
                        @error('txt_fechafinalizacion')
                            <small class="txtError">*{{$message}}</small>
                        @enderror
                    </label>

                <label for="txt_cod_instrumento">
                        Código del intrumento: <br>
                        <input class="txt" name="txt_cod_instrumento" id="txt_cod_instrumento" onkeyup= "buscarInstrumento()" maxlength="20" value="{{old('txt_cod_instrumento')}}">
                        @error('txt_cod_instrumento')
                            <small class="txtError">*{{$message}}</small>
                        @enderror
                </label>

                <label for="txt_nombre_activo">
                        Nombre del instrumento: <br>
                        <input class="txt" name="txt_nombre_activo" id="txt_nombre_activo" disabled>
                </label>

                <label for="txt_cedula_estudiante">
                        Cédula del estudiante: <br>
                        <input class="txt" name="txt_cedula_estudiante" id="txt_cedula_estudiante" onkeyup= "buscarEstudiante()" maxlength="15" value="{{old('txt_cedula_estudiante')}}">
                        @error('txt_cedula_estudiante')
                            <small class="txtError">*{{$message}}</small>
                        @enderror
                </label>

                <label for="txt_nombre_estudiante">
                        Nombre del estudiante: <br>
                        <input class="txt" name="txt_nombre_estudiante" id="txt_nombre_estudiante" disabled>
                </label>

                <label for="cmb_encargado_fiador">
                        Encargado o Fiador: <br>
                        <select name="cmb_encargado_fiador" id="cmb_encargado_fiador" class= 'cmb' onchange="mostrarDivFiador()">
                        
                        </select>
                </label>
                <div id = "div_datos_fiador" >
                    <label for="txt_cedula_fiador">
                            Cédula del fiador: <br>
                            <input class="txt" name="txt_cedula_fiador" id="txt_cedula_fiador" maxlength="15" value="{{old('txt_cedula_fiador')}}">
                        @error('txt_cedula_fiador')
                                <small class="txtError">*{{$message}}</small>
                        @enderror
                    </label>

                    <label for="txt_nombre_fiador">
                            Nombre completo del fiador: <br>
                            <input class="txt" name="txt_nombre_fiador" id="txt_nombre_fiador" maxlength="200" value="{{old('txt_nombre_fiador')}}">
                        @error('txt_nombre_fiador')
                                <small class="txtError">*{{$message}}</small>
                        @enderror
                    </label>
                    <label for="txt_email_fiador">
                            Email del fiador: <br>
                            <input class="txt" name="txt_email_fiador" id="txt_email_fiador" maxlength="150" value="{{old('txt_email_fiador')}}">
                            @error('txt_email_fiador')
                                <small class="txtError">*{{$message}}</small>
                            @enderror
                    </label>
                    <label for="txt_telefono_fiador">
                            Télefono del fiador: <br>
                            <input class="txt" name="txt_telefono_fiador" id="txt_telefono_fiador" maxlength="16" value="{{old('txt_telefono_fiador')}}">
                    </label>
                </div>

                </div>
                
                <div id="div_btns">
                    <input class="btn btn_guardar" type="submit" id="btn_guardar" value="Guardar contrato">
                    <input class="btn" type="button" id="btn_volver" value="Volver" onclick="cancelar()">
                    <a href="{{route('contratos')}}" id="a_cancelar"></a>
            </div>
        </form>
        </div>

@else 
    <script>window.location = "/";</script>
@endif
        
@endsection