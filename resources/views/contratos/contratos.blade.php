@extends('layouts.master')

@section('title', 'Sinfoinventarios | Contratos')

@section('seccionActiva', 'Contratos de Prestamo')

@section('css_js')
    <link rel="stylesheet" href="{{asset('assets/css/contratos.css')}}">
    <script src="{{asset('assets/js/contratos.js')}}"></script>
@endsection

@section('content')

@if (session('userdata') != null)
    <div class="div_contenido">
        <div id="div_izq">
        <form id="form_filtrar" action="{{route('filtrarContratos')}}" method="POST">
                @csrf
                <h1 id="header_filtrado">Opciones <small class="small_class">de filtrado</small></h1><br>
                <hr>
                <label id="lb_buscar" for="txt_cod_contrato">
                    Código del contrato:
                    
                    <h4>EMSPZ-CTR-<input type="text" name="txt_cod_contrato" id="txt_cod_contrato" class="txt_datos"></h4>
                </label>
                <label id="lb_buscar" for="txt_nom_estudiante">
                    Nombre del estudiante:
                    <input type="text" name="txt_nom_estudiante" id="txt_nom_estudiante" class="txt_datos">
                </label>
                <label id="lb_buscar" for="txt_ced_estudiante">
                    Cédula del estudiante:
                    <input type="text" name="txt_ced_estudiante" id="txt_ced_estudiante" class="txt_datos">
                </label>

                <div id="div_opciones">

                    <label id="lb_buscar" for="cmb_datos">Estado del contrato:</label>
                    <select name="cmb_estado" id="cmb_estado" class="cmb_datos">
                        <option selected value="To">Todos</option>
                        <option value="A">Activo</option>
                        <option value="T">Terminado</option>
                        <option value="C">Cancelado</option>
                    </select>
                
                    <div class="div_btnsfiltrar">
                        <input type="submit" class="boton" id="btn_filtrar" value="Filtrar">
                        <input type="button" class="boton" id="btn_limpiar" value="Limpiar" onclick="limpiarFiltros()">
                    </div>
                </div>
            </form>
            <div class="div_btns_agregar">
                @if(session('userdata')['role'] == 'A')
                    <input type="button" id="btn_nuevoContrato" class="boton" value="Crear Contrato" onclick="irNuevoContrato()">
                    <a id="a_nuevoContrato" href="{{route('agregarContrato')}}"></a>
                @endif
            </div>
        </div>
        <div id="div_der">
        @if ($contratos->count() <= 0)
                <h1 class='h1_sinResultado'> ¡Sin resultados para mostrar!</h1>
            @else 
                @foreach ($contratos ?? '' as $con)
                    <div @switch($con->estado)                        
                        @case('C')
                            class="div_contenedorContrato cc_cancelado"
                            @break
                        @case('T')
                            class="div_contenedorContrato cc_terminado"
                            @break
                        @case('R')
                            class='div_contenedorContrato cc_atrasado'
                            @break
                        @default
                            class="div_contenedorContrato cc_activo"
                    @endswitch>
                        <h1 class="h1_boleta">{{$con->estudiante->nombreCompleto}}</h1>                            
                            <div class="div_info_contrato">
                                Código: 
                                <span>EMSPZ-CTR-{{$con->idContrato}}</span>

                                Estudiante: 
                                <span> {{$con->estudiante->nombreCompleto}} </span>
                                @if($con->encargado!= null)
                                Encargado: 
                                <span> {{$con->encargado->nombreCompleto}} </span>
                                @else
                                Fiador: 
                                <span> {{$con->nombreCompletoFiador}} </span>
                                <br>
                                @endif
                                Fecha de inicio: 
                                <span> {{$con->fechaInicio}} </span>
                                
                                Fecha de finalización: 
                                <span> {{$con->fechaFinalizacion}} </span>
                                Estado:
                                @switch ($con->estado)
                                    @case('A')
                                        <span class="activo">  Activo </span>
                                        @break
                                    @case('R')
                                        <span class="atrasado"> Terminado </span>
                                        @break
                                    @case('T')
                                        <span class="terminado"> Terminado </span>
                                        @break
                                    @case('C')
                                        <span class="cancelado"> Cancelado </span>
                                        @break
                                    
                                @endswitch
                            </div>                            
                            <div class="div_botones">
                                
                                <input id="{{$con->idContrato}}" type="button" class="btnDetalle" value="Ver detalles" onclick="ver_detalle(this)">
                                <a id="a_detalle_{{$con->idContrato}}" href="{{route('detalleContrato', $con->idContrato)}}"></a>
                            </div>     
                        </div>
                    @endforeach
                @endif
        </div>
    </div>

@else 
    <script>window.location = "/";</script>
@endif

@endsection