@extends('layouts.master')

@section('title', 'Sinfoinventarios | Usuarios')

@section('css_js')
    <link rel="stylesheet" href="{{asset('assets/css/usuarios.css')}}">
    <script src="{{asset('assets/js/usuarios.js')}}"></script>
@endsection

@section('seccionActiva', 'Usuarios')

@section('content')

    @if (session('userdata') != null)

        @if(session('userdata')['role'] != 'A')
            <script>window.location = "/";</script>
        @endif

        <div class="div_contenido">
            <div id="div_izq">        
                <form id="form_filtrar" action="{{route('filtrarUsuarios')}}" method="POST">
                    @csrf
                    <h1>Opciones <small class="small_class">de filtrado</small></h1>
                    <hr class="hr_class">
                    <label id="lb_buscar" for="txt_buscar">
                        Nombre Real:
                        <input type="text" name="txt_buscarNom" id="txt_buscarNom">
                    </label>
                    <div id="div_opciones">
                        Rol de Acceso <br>
                        <select name="cmb_rol" id="cmb_rol" class="cmb_datos">
                            <option selected value="T">Todos</option>
                            <option value="A">Administrador</option>
                            <option value="R">Regular</option>
                            <option value="M">Mantenimiento</option>                            
                        </select>
                        Estado  <br>
                        <select name="cmb_estado" id="cmb_estado" class="cmb_datos">
                            <option selected value="T">Todos</option>
                            <option value="A">Activo</option>
                            <option value="I">Dado de Baja</option>                           
                        </select>
                    </div>                
                    <div class="div_btnsfiltrar">
                        <input type="submit" class="boton" id="btn_filtrar" value="Filtrar">
                        <input type="button" class="boton" id="btn_limpiar" value="Limpiar" onclick="limpiarFiltros()">
                    </div>
                </form>
                <div class="div_btns_agregar">
                    <input type="button" id="btn_presidente" class="btn" value="Editar Presidente" onclick="editarPresidente()">
                    <input type="button" id="btn_nuevoUs" class="btn" value="Agregar nuevo usuario" onclick="irNuevoUsuario()">
                    <a id="a_nuevoUsuario" href="{{route('agregarUsuario')}}"></a>
                </div>
                
            </div>
            <div id="div_der">
                
                <div id="div_usuarios">
                    @if ($usuarios->count() <= 0)
                        <h1 class='h1_sinResultado'> ¡Sin resultados para mostrar!</h1>
                    @else                            

                        @foreach ($usuarios ?? '' as $us)
                            <div @if ($us->estado == 'A') class="div_contenedorUsuario"
                                @else class="div_contenedorUsuario baja"
                                @endif>
                                <h1>{{$us->nombreUsuario}}</h1>
                                <div class="div_textUsuario">
                                    <div class="div_fotoUsuario">
                                        @if (!is_null($us->fotoUsuario))                         
                                            <img class="img_fotoUs" src="{{asset('storage/'. $us->fotoUsuario)}}" alt="">
                                        @else
                                            <img class="img_fotoUs" src="{{asset('assets/rsc/pngs/user_white.png')}}" alt="">
                                        @endif
                                    </div>
                                    <div class="div_infoUsuario">                            
                                        <span>
                                            Nombre: <small> {{$us->persona->nombreCompleto }} </small>
                                        </span>
                                        <br>
                                        
                                        <span>
                                            Ced: <small>{{substr($us->persona->cedula, 0, 1)}} {{substr($us->persona->cedula, 1, 4)}} {{substr($us->persona->cedula, 5, 4)}}</small>
                                        </span>
                                        <br>
                                        
                                        <span>
                                            Rol de usuario: 
                                            <small>
                                                @switch($us->rolAcceso)
                                                @case('A')
                                                    Administrador
                                                    @break
                                                @case('R')
                                                    Regular
                                                    @break
                                                @case('M')
                                                    Mantenimiento
                                                    @break
                                                @endswitch
                                            </small>
                                        </span>
                                        <br>                
                                        <span>
                                            Estado: 
                                            {{-- <small> --}}
                                                @switch($us->estado)
                                                @case('A')
                                                    <small class="small_activo">Activo</small>
                                                    @break
                                                @case('I')
                                                    <small class="small_dadoBaja">Dado de Baja</small>
                                                    @break
                                                @endswitch
                                            {{-- </small> --}}
                                        </span>
                                    </div>
                                </div>
                                <div class="div_botones">
                                    <!-- A continuacion declaramos los botones y los "a" necesarios para acceder a las rutas usando javascript  -->
                                    <input id="{{$us->idUsuario}}" type="button" class="btnEditar" value="Editar" onclick="editar(this)">
                                    <a id="a_editar_{{$us->idUsuario}}" href="{{route('editarUsuario', $us->idUsuario)}}"></a>
                                    @if ($us->estado == 'A')
                                        <input type="button" class="btnBaja" id="{{$us->idUsuario}}" name='{{$us->nombreUsuario}}' value="Dar de Baja" onclick="confirmarBaja(this)">
                                    @else
                                        <input type="button" class="btnActivar" id="{{$us->idUsuario}}" name='{{$us->nombreUsuario}}' value="Activar" onclick="activarUsuario(this)">
                                    @endif                        
                                </div>
                            </div>
                            
                        @endforeach

                    @endif
                </div>
                <div id="div_paginacion">
                    {{!! $usuarios->links() !!}}
                </div>

            </div>
        </div>
    @else 
        <script>window.location = "/";</script>
    @endif
    <script>
        
    </script>
@endsection