@extends('layouts.master')

@section('title', 'Agregar Usuario')

@section('seccionActiva', 'Agregar Usuario')

@section('css_js')
    <link rel="stylesheet" href="{{asset('assets/css/editarAgregarUsuario.css')}}">
    <script src="{{asset('assets/js/agregarUsuario.js')}}"></script>
@endsection

@section('content')

@if (session('userdata') != null)

<div class="div_contenido">
    <form id="form_usuario" action="{{route('guardarUsuario')}}" method="post" enctype="multipart/form-data">
        @csrf
        <div id="div_txts">
            <label for="txt_ced">
                Cedula: <br>    
                <input class="txt" type="text" name="txt_ced" id="txt_ced" minlength="9" maxlength="9" value="{{old('txt_ced')}}"><br>
                @error('txt_ced')
                    <small class="txtError">*{{$message}}</small>
                @enderror
            </label>
            <label for="txt_nombre">
                Nombre: <br>    
                <input class="txt" type="text" name="txt_nombre" id="txt_nombre" maxlength="45" value="{{old('txt_nombre')}}"><br>
                @error('txt_nombre')
                    <small class="txtError">*{{$message}}</small>
                @enderror
            </label>
            
            <label for="txt_nombreUS">
                Nombre de Usuario: <br>
                <input class="txt" type="text" name="txt_nombreUs"  maxlength="45" id="txt_nombreUs" value="{{old('txt_nombreUs')}}"><br>
                @error('txt_nombreUs')
                    <small class="txtError">*{{$message}}</small>
                @enderror
            </label>
            <label for="txt_password">
                Contraseña: <br>        
                <div id="div_pass">
                    <input class="txt" type="password" style="width: 90%" name="txt_password" id="txt_password" value="">                                                    

                    <button onclick="mostrarPassword()" id="btn_show" type="button" class="btn_pass"><i class="fas fa-eye"></i></button>              
                    <br>                
                    @error('txt_password')
                        <small class="txtError">*{{$message}}</small>
                    @enderror
                </div>
            </label>

            <label for="txt_email">
                Correo Electronico: <br>
                <input class="txt" type="email" name="txt_email" id="txt_email" value="{{old('txt_email')}}">
                <br>                
                @error('txt_email')
                    <small class="txtError">*{{$message}}</small>
                @enderror
            </label>
            <label for="cmb_rol">
                Rol de Usuario: <br>
                <select name="cmb_rol" id="cmb_rol" class="combobox">
                    <option value="A">Administrador</option>
                    <option value="R">Regular</option>
                    <option value="M">Mantenimiento</option>
                </select>
            </label>                
            <input type="hidden" name="idUsuario">
        </div>
        <div id="div_img">
            <div id="btnsImg">
                <button type="button" id="btn_cargarImg" class="btn_cargar" onclick="presionarInputFile()"><i class="far fa-images fa-2x"></i></button>                
            </div>
            <img id="img_usuario" name='img_usuario' src="{{asset('assets/rsc/pngs/user_white.png')}}" alt="">                    
            <input type="file" accept="image/x-png, image/jpeg" name="file_imgUsuario" id="file_imgUsuario" onclick="cargarIMG()">
        </div>
        <div id="div_btns">
            <input class="btn" type="submit" id="btn_guardar" value="Guardar">
            <input class="btn" type="button" id="btn_cancelar" value="Cancelar" onclick="cancelar()">
            <a href="{{route('usuarios')}}" id="a_cancelar"></a>
        </div>
    </form>
</div>
@else 
    <script>window.location = "/";</script>
@endif


@endsection