@extends('layouts.master')

@section('title', 'Editar Usuario')

@section('seccionActiva', 'Editar Usuario')

@section('css_js')
    <link rel="stylesheet" href="{{asset('assets/css/editarAgregarUsuario.css')}}">
    <script src="{{asset('assets/js/editarUsuario.js')}}"></script>
@endsection

@section('content')

@if (session('userdata') != null)

<div class="div_contenido">    
    <form id="form_usuario" action="{{route('actualizarUsuario')}}" method="post" enctype="multipart/form-data">
        @csrf
        @method('put')
        <div id="div_txts">            
            <label for="txt_ced">
                Cedula: <br>    
                <input class="txt" type="text" name="txt_ced" id="txt_ced" minlength="9" maxlength="9" readonly value="{{$usuario->persona->cedula}}">
                <br>
                @error('txt_ced')
                    <small class="txtError">*{{$message}}</small>
                @enderror
            </label>
            <label for="txt_nombre">
                Nombre: <br>    
                <input class="txt" type="text" name="txt_nombre" id="txt_nombre" value="{{$usuario->persona->nombreCompleto}}">
                <br>
                @error('txt_nombre')
                    <small class="txtError">*{{$message}}</small>
                @enderror
            </label>
            <label for="txt_nombreUS">
                Nombre de Usuario: <br>
                <input class="txt" type="text" name="txt_nombreUs" id="txt_nombreUs" value="{{$usuario->nombreUsuario}}">
                <br>                
                @error('txt_nombreUs')
                    <small class="txtError">*{{$message}}</small>
                @enderror
            </label>

            <label for="txt_password">
                Contraseña: <br>     
                <div id="div_pass">
                @if ($usuario->idUsuario == session('userdata')['userID']) 
                    <input class="txt" type="password" style="width: 90%;" 
                        @if ($usuario->idUsuario != session('userdata')['userID']) readonly @endif 
                        name="txt_password" id="txt_password" value="{{ $usuario->contraseña }}">                                     

                    <button onclick="mostrarPassword()" id="btn_show" type="button" class="btn_pass"><i class="fas fa-eye"></i></button>              
                    <br>                
                    @error('txt_password')
                        <small class="txtError">*{{$message}}</small>
                    @enderror
                @else
                    <input class="txt" type="text" 
                    @if ($usuario->idUsuario != session('userdata')['userID']) readonly @endif 
                    value="!Usted no puede editar esta contraseña¡">                
                @endif
                </div>   
            </label>

            <label for="txt_email">
                Correo Electronico: <br>
                <input class="txt" type="email" name="txt_email" id="txt_email" value="{{$usuario->persona->email}}">
                <br>                
                @error('txt_email')
                    <small class="txtError">*{{$message}}</small>
                @enderror
            </label>
            <label for="cmb_rol">
                Rol de Usuario: <br>
                <select name="cmb_rol" id="cmb_rol" class="combobox">
                    <option value="A" @if ($usuario->rolAcceso == 'A')
                        selected
                    @endif>Administrador</option>
                    <option value="R" @if ($usuario->rolAcceso == 'R')
                        selected
                    @endif>Regular</option>
                    <option value="M" @if ($usuario->rolAcceso == 'M')
                        selected
                    @endif>Mantenimiento</option>
                </select>
                @error('cmb_rol')
                    <small class="txtError">*{{$message}}</small>
                @enderror
            </label>      
            <input type="hidden" name="idUsuario" value="{{$usuario->idUsuario}}">
        </div>
        <div id="div_img">            

            <div id="btnsImg">
                <button type="button" id="btn_cargarImg" class="btn_cargar" onclick="presionarInputFile()"><i class="far fa-images fa-2x"></i></button>
                @if (!is_null($usuario->fotoUsuario)) <button type="button" id="deleteOriginal" class="btn_borrar" onclick="eliminarOriginal()"><i class="fas fa-eraser fa-2x"></i></button> @endif                
            </div>
            @if (!is_null($usuario->fotoUsuario))                
                <img id="img_usuario" name='img_usuario' src='{{asset('storage/'.$usuario->fotoUsuario)}}' alt="">                 
                <img name="imgOriginal" id="imgOriginal" style="visibility: hidden" src='{{asset('storage/'.$usuario->fotoUsuario)}}'>                           
            @else 
                <img id="img_usuario" name='img_usuario' src="{{asset('assets/rsc/pngs/user_white.png')}}" alt="">                                    
            @endif
            
            <input type="hidden" id="tieneFoto" value="{{$usuario->fotoUsuario}}">
            <input type="hidden" name="borrarOriginal" id="borrarOriginal" value="0">                      
            <input type="file" accept="image/x-png, image/jpeg" name="file_imgUsuario" id="file_imgUsuario" onclick="cargarIMG()">               
        </div>
        <div id="div_btns">
            <input class="btn" type="submit" id="btn_guardar" value="Guardar">
            <input class="btn" type="button" id="btn_cancelar" value="Cancelar" onclick="cancelar()">
            <a href="{{route('usuarios')}}" id="a_cancelar"></a>
        </div>
    </form>       
</div>

@else 
    <script>window.location = "/";</script>
@endif

@endsection