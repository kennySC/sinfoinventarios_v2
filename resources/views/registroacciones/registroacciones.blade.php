@extends('layouts.master')

@section('title', 'Sinfoinventarios | Registro de Acciones')

@section('seccionActiva', 'Registro de Acciones')

@section('css_js')
    <link rel="stylesheet" href="{{asset('assets/css/registroacciones.css')}}">
    <script src="{{asset('assets/js/registroacciones.js')}}"></script>
@endsection

@section('content')

@if (session('userdata') != null)

<div class="div_contenido">

    <div class="div_control">
        @csrf          
        <div class="div_titulo">
            <h3 id="header">Generar <small class="small_class">reporte</small></h3><br>
            <hr>
        </div>    
        <div class="ck_horas">
            <label class="lb_rb" for="rb_fechas">
                <input type="checkbox" name="" id="rb_fechas"> ¿Usar fechas?                
            </label>
        </div>
        <div class="div_fechas txt_hidden" id="div_fechas">            
            <label class="labels" for="dp_incio">
                Fecha Inicio
                <input type="date" class="txt " name="dp_inicio" id="dp_inicio">
            </label>
            <label class="labels" for="dp_final">
                Fecha Final
                <input disabled type="date" class="txt " name="dp_final" id="dp_final">
            </label>
        </div>
        <div class="div_btn">
            <input type="submit" class="btn" id="btn_generar" value="Generar">
        </div>   
    </div>
    
    <div id="div_tabla">
        <table class="tabla" id="tabla_registro" style="width: 98%">
            <thead>
                <tr style="color: white">                    
                    <th>Tipo de Acción</th>
                    <th>Acción sobre</th>
                    <th>Usuario</th>
                    <th>Fecha del Acción</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table> 
    </div>

</div>

@else 
    <script>window.location = "/";</script>
@endif

@endsection