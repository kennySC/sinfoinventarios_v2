<link rel="stylesheet" href="{{asset('assets/css/toastr.min.css')}}">
<script src="{{asset('assets/js/toastr.min.js')}}"></script>


<!-- Codigo para los mensajes de ERROR -->
@if (Session::get('error')) 
    <script type="text/javascript">        
        alertify.set('notifier','position', 'top-right');
        alertify.error("{{ Session::get('error') }}");
    </script>
@endif


<!-- Codigo para los mensajes de SUCCESS -->
@if (Session::get('success')) 
    <script type="text/javascript">        
        alertify.set('notifier','position', 'top-right');
        alertify.success("{{ Session::get('success') }}");
    </script>
@endif


<!-- Codigo para los mensajes de INFO -->
@if (Session::get('info')) 
    <script type="text/javascript">        
        alertify.set('notifier','position', 'top-right');
        alertify.notify("{{ Session::get('info') }}");
    </script>
@endif


<!-- Codigo para los mensajes de WARNING -->
@if (Session::get('warning')) 
    <script type="text/javascript">        
        alertify.set('notifier','position', 'top-right');
        alertify.warning("{{ Session::get('warning') }}");
    </script>
@endif