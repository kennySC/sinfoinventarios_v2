<?php
/** 
  * Semantic UI
  * Includes previous and next buttons
  * @example $pages->links('pagination-advanced', ['paginator' => $pages])
  * @example @include('pagination-advanced', ['paginator' => $pages])
  *
  * @link https://semantic-ui.com/collections/menu.html#inverted Inverted styles
  * @see <div class="ui pagination inverted blue menu"> Inverted blue menu
**/
?>
@if ($paginator->lastPage() > 1)
    <div class="ui inverted menu">
        <a href="{{ $paginator->previousPageUrl() }}" class="{{ ($paginator->currentPage() == 1) ? ' disabled' : 'pagination-a' }} item">
            ←
        </a>
        @if ($paginator->currentPage() > 4)
            <a href="{{ $paginator->url(1) }}" class="item pagination-a">
                {{ 1 }}
            </a>            
        @endif  
        @if ($paginator->currentPage() > 5)
            <a aria-disabled="true" class="item disable">...</a>
        @endif                
        @for ($i = $paginator->currentPage()-3; $i <= $paginator->currentPage()+3; $i++)
            @if ($i > 0 && $i <= $paginator->lastPage())
                <a href="{{ $paginator->url($i) }}" class="{{ ($paginator->currentPage() == $i) ? ' active' : '' }} item pagination-a">
                    {{ $i }}
                </a>
            @endif
        @endfor
        @if ($paginator->currentPage() < $paginator->lastPage() - 4)
            <a aria-disabled="true" class="item disable">...</a>
        @endif  
        @if ($paginator->currentPage() < $paginator->lastPage() - 3)
            <a href="{{ $paginator->url($paginator->lastPage()) }}" class="item pagination-a">
                {{ $paginator->lastPage() }}
            </a>
        @endif
        <a href="{{ $paginator->nextPageUrl() }}" class="{{ ($paginator->currentPage() == $paginator->lastPage()) ? ' disabled' : 'pagination-a' }} item">
            →
        </a>
    </div>
@endif