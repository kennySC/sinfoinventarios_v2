<?php
/** 
  * Semantic UI
  * Includes previous and next buttons
  * @example $pages->links('pagination-advanced', ['paginator' => $pages])
  * @example @include('pagination-advanced', ['paginator' => $pages])
  *
  * @link https://semantic-ui.com/collections/menu.html#inverted Inverted styles
  * @see <div class="ui pagination inverted blue menu"> Inverted blue menu
**/
?>
@if ($paginator->hasPages())
<div class="ui inverted menu">
    
    <a href="{{ $paginator->url(1) }}" class="pagination-a item">
        <i class="fas fa-angle-double-left"></i>
    </a>

    <a href="{{ $paginator->previousPageUrl() }}" class="pagination-a item">
        <i class="fas fa-angle-left"></i>
    </a>
    
    {{-- pagina tal de tantas --}}
    <span class="item">
        {{ $paginator->currentPage() }} de {{ $paginator->lastPage() }}
    </span>

    <a href="{{ $paginator->nextPageUrl() }}" class="pagination-a item">
        <i class="fas fa-angle-right"></i>
    </a>

    <a href="{{ $paginator->url($paginator->lastPage()) }}" class="pagination-a item">
        <i class="fas fa-angle-double-right"></i>
    </a>
    
</div>
@endif