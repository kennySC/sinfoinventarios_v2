@extends('layouts.master')

@section('title', 'Sinfoinventarios | Recepción de Instrumentos')

@section('seccionActiva', 'Recepción de Instrumentos')

@section('css_js')
    <link rel="stylesheet" href="css/instrumentos.css">
    <script src="js/instrumentos.js"></script>
@endsection

@section('content')

    <div class="div_contenido">
        <div id="div_izq">
            <h1>Simon div izquierdo pase el instrumento aqui lo recibo</h1>
        </div>
        <div id="div_der">
            <h1>Simon div derecho</h1>
        </div>
    </div>

@endsection