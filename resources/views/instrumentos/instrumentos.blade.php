@extends('layouts.master')

@section('title', 'Sinfoinventarios | Instrumentos')

@section('seccionActiva', 'Instrumentos')

@section('css_js')
    <link rel="stylesheet" href="{{asset('assets/css/instrumentos.css')}}">
    <script src="{{asset('assets/js/instrumentos.js')}}"></script>
@endsection

@section('content')


@if (session('userdata') != null)

    <div class="div_contenido">
    
        <div id="div_izq">
            
            <form id="form_filtrar" action="{{route('filtrarInstrumentos')}}" method="POST">
            @csrf
                <h1 id="header_filtrado">Opciones <small class="small_class">de filtrado</small></h1><br>
                <hr>
                <label id="lb_buscar" for="txt_cod_instrumento">
                    Código del Instrumento:
                    <input type="text" name="txt_cod_instrumento" id="txt_cod_instrumento" class="txt_datos" >
                </label>    

                <div id="div_opciones">
                    <label id="lb_buscar" for="cmb_institucion_instrumentos">
                        Institucción:
                        <select name="cmb_institucion_instrumentos" id="cmb_institucion_instrumentos" class= 'cmb_datos'>
                            <option value='T'>Todos</option>
                            @foreach ($institucion ?? '' as $in)
                                <option value='{{$in->idInstitucion}}'>{{$in->nombre}}</option>
                            @endforeach
                        </select> 
                    </label>

                    <label id="lb_buscar" for="cmb_tipo_instrumentos">
                        Tipo de Instrumento:
                        <select name="cmb_tipo_instrumentos" id="cmb_tipo_instrumentos" class= 'cmb_datos'>
                        <option value='T'>Todos</option>
                            @foreach ($tipo ?? '' as $ti)
                                <option value='{{$ti->codTipoInstrumento}}'>{{$ti->tipoInstrumento}}</option>
                            @endforeach
                        </select> 
                    </label>
                   

                    <label id="lb_buscar" for="cmb_datos">
                        Estado:
                        <select name="cmb_estado" id="cmb_estado" class="cmb_datos">
                            <option selected value="T">Todos</option>
                                @foreach ($estado ?? '' as $est)
                            <option value='{{$est->idEstado}}'>{{$est->estado}}</option>
                                @endforeach
                        </select>
                    </label>
                </div>
                <div class="div_btnsfiltrar">
                    <input type="submit" class="boton" id="btn_filtrar" value="Filtrar">
                    <input type="button" class="boton" id="btn_limpiar" value="Limpiar" onclick="limpiarFiltros()">
                </div>
            </form>

            @if(session('userdata')['role'] == 'A')
                <input type="button" id="btn_nuevoAct" class="boton" value="Agregar nuevo instrumento" onclick="irNuevoInstrumento()">
                <a id="a_nuevoInstrumento" href="{{route('agregarInstrumento')}}"></a>
            @endif
        </div>
        
        <div id="div_der">
            <div id="div_instrumentos">
            @if ($instrumentos->count() <= 0)
                <h1 class='h1_sinResultado'> ¡Sin resultados para mostrar!</h1>
            @else 
                @foreach ($instrumentos ?? '' as $in)
                    <div @if ($in->estado->estado  != 'Dado de baja') class="div_contenedorActivo"
                        @else class="div_contenedorActivo dadoBaja"
                        @endif> 
                        <h1>{{$in->nombre}}</h1>
                        <div class="div_datos_instrumento">                                
                            
                            <div class="div_foto_instrumento">                                 
                                @if ($in->fotoInstrumento != null)
                                    <img class="img_fotoIns" src="{{asset('storage/'.$in->fotoInstrumento)}}" alt="">
                                @else
                                    <img class="img_fotoIns" src="{{asset('assets/rsc/pngs/activo_default.png')}}" alt="">
                                @endif                                
                            </div> 

                            <div class="div_info_instrumento">
                                <div class="rowInfo">                                        
                                    <p>Código Placa: <span id="placa"> {{$in->codInstrumento}} </span></p>
                                    <p>Código Interno: <span> {{$in->tipoInstrumento->codTipoInstrumento . '-' . $in->numeroInstrumento}} </span></p>
                                    <p>Institución: <span> {{$in->institucion->estdInstitucion}} </span></p>
                                </div>
                                <div class="rowInfo">                                                                                                                     
                                    <p>Responsable: <span> {{$in->responsable}} </span></p>
                                    <p>Estado: <span> {{$in->estado->estado}} </span></p>
                                </div>  
                                <div class="rowInfo">                                                                                                                     
                                    <input type="button" id="{{$in->codInstrumento}}" class="btnVerMas" value="Ver más" onclick="verMas({{$in}})">
                                </div>                                                                                       
                            </div>                            

                        </div>
                        
                        <div class="div_botones">                            
                            @if(session('userdata')['role'] == 'A')
                                <input id="{{$in->codInstrumento}}" type="button" class="btnEditar" value="Editar" onclick="editarInstrumento(this)">
                                <a id="a_editar_{{$in->codInstrumento}}" href="{{route('editarInstrumento', $in->codInstrumento)}}"></a>
                                @if ($in->estado->estado !='Dado de baja') 
                                    <input type="button" class="btnBaja" id="{{$in->codInstrumento}}" name='{{$in->nombre}}' value="Dar de Baja" onclick="confirmarBaja(this)">
                                @else 
                                    <input type="button" class="btnActivar" id="{{$in->codInstrumento}}" name='{{$in->nombre}}' value="Activar" onclick="activarInstrumento(this)">
                                    <a id="a_activar_{{$in->codInstrumento}}" href="{{route('activarInstrumento', $in->codInstrumento)}}"></a>
                                @endif
                            @endif    
                        </div>

                    </div> 
                @endforeach
            @endif
        </div>
                   
        <div id="div_paginacion">
            {{ $instrumentos->links() }}
        </div>        
    </div>

@else 
    <script>window.location = "/";</script>
@endif

@endsection