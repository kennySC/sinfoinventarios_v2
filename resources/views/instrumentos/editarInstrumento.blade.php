@extends('layouts.master')

@section('title', 'Editar Instrumento')

@section('seccionActiva', 'Editar Instrumentos')

@section('css_js')
    <link rel="stylesheet" href="{{asset('assets/css/editarAgregarInstrumento.css')}}">
    <script src="{{asset('assets/js/editarInstrumento.js')}}"></script>
@endsection

@section('content')

@if (session('userdata') != null)

    <div class="div_contenido">
            <form id="form_activo" action="{{route('actualizarInstrumento')}}" method ="post" enctype="multipart/form-data">
                @csrf
                @method('put')
                <div id="div_datos">
                    <label for="txt_codigo">
                        Código del instrumento: <br>
                        <input class="txt" type="text" name="txt_codigo" id="txt_codigo" value="{{$instrumento->codInstrumento}}">
                        @error('txt_codigo')
                            <br><small class="txtError">*{{$message}}</small>
                        @enderror
                    </label>
                    <label  for="tipoIsnt">
                        Tipo de Instrumento:
                        <div id="tipoIsnt">
                            <select class="txt" name="cmb_tipo" id="cmb_tipo" class= 'cmb_datos'>
                                @foreach ($tipo ?? '' as $ti)
                                    <option value='{{$ti->codTipoInstrumento}}'
                                        @if($instrumento->codTipoInstrumento == $ti->codTipoInstrumento) selected @endif>{{$ti->tipoInstrumento}}</option>
                                @endforeach
                            </select>                             
                            <input readonly type="text" class="txt" name="txt_numero" id="txt_numero" value="{{$instrumento->numeroInstrumento}}">
                        </div>
                        @error('cmb_tipo')
                            <small class="txtError">*{{$message}}</small>
                        @enderror
                        @error('txt_numero')
                            <small class="txtError">*{{$message}}</small>
                        @enderror
                    </label>
                    <label for="txt_nombre">
                        Nombre del instrumento: <br>
                        <input class="txt" type="text" name="txt_nombre" id="txt_nombre" value="{{$instrumento->nombre}}">
                        @error('txt_nombre')
                            <br><small class="txtError">*{{$message}}</small>
                        @enderror
                    </label>         
                    <label for="txt_marca">
                        Marca del instrumento: <br>
                        <input class="txt" type="text" name="txt_marca" id="txt_marca" value="{{$instrumento->marca}}">
                        @error('txt_marca')
                            <br><small class="txtError">*{{$message}}</small>
                        @enderror
                    </label>  
                    <label for="txt_modelo">
                        Modelo del instrumento: <br>
                        <input class="txt" type="text" name="txt_modelo" id="txt_modelo" value="{{$instrumento->modelo}}">
                        @error('txt_modelo')
                            <br><small class="txtError">*{{$message}}</small>
                        @enderror
                    </label>                                        
                    <label for="txt_serie">
                        # Serie del instrumento: <br>
                        <input class="txt" type="text" name="txt_serie" id="txt_serie" value="{{$instrumento->serie}}">
                        @error('txt_serie')
                            <br><small class="txtError">*{{$message}}</small>
                        @enderror
                    </label>
                    <label for="txt_valor">
                        Valor ₡ del instrumento: <br>
                        <input class="txt" type="text" name="txt_valor" id="txt_valor" value="{{$instrumento->valor}}">
                        @error('txt_valor')
                            <br><small class="txtError">*{{$message}}</small>
                        @enderror
                    </label>
                    <label for="txt_ubicacion">
                        Ubicación del instrumento: <br>
                        <input class="txt" type="text" name="txt_ubicacion" id="txt_ubicacion" value="{{$instrumento->ubicacion}}">
                        @error('txt_ubicacion')
                            <br><small class="txtError">*{{$message}}</small>
                        @enderror
                    </label>
                    <label for="txt_responsable">
                        Responsable del instrumento: <br>
                        <input class="txt" type="text" name="txt_responsable" id="txt_responsable" value="{{$instrumento->responsable}}">
                        @error('txt_responsable')
                            <br><small class="txtError">*{{$message}}</small>
                        @enderror
                    </label>                                             
                    <label  for="cmb_institucion" >
                        Institución:<br>
                        <select class="txt" name="cmb_institucion" id="cmb_institucion" class= 'cmb_datos'>
                            @foreach ($institucion ?? '' as $in)
                                <option value='{{$in->idInstitucion}}'
                                    @if($instrumento->idInstitucion == $in->idInstitucion) selected @endif>{{$in->nombre}}</option>
                            @endforeach
                        </select> 
                        @error('cmb_institucion')
                            <br><small class="txtError">*{{$message}}</small>
                        @enderror
                    </label>

                    <label for="cmb_estado">
                        Estado: <br>
                        <select class="txt" name="cmb_estado" id="cmb_estado">
                                @foreach ($estado_activo ?? '' as $est_act)
                                    <option value='{{$est_act->idEstado}}'
                                        @if($instrumento->idEstado == $est_act->idEstado) selected @endif>{{$est_act->estado}}</option>
                                @endforeach
                        </select>                        
                        @error('cmb_estado')
                            <br><small class="txtError">*{{$message}}</small>
                        @enderror
                    </label>

                    <label for="txt_observaciones">
                        Observaciones sobre el activo: <br>
                        <textarea class="txt_area" type="text" cols="30" rows="8" name="txt_observaciones" id="txt_observaciones" value="{{$instrumento->observacion}}"></textarea>
                    </label>
                    @error('txt_observaciones')
                        <br><small class="txtError">*{{$message}}</small>   
                    @enderror                    
                    <input type="hidden" name="txt_codOriginal" id="txt_codOriginal" value="{{$instrumento->codInstrumento}}">

                    <div id="div_img">

                        <div id="btnsImg">
                            <button type="button" id="btn_cargarImg" class="btn_cargar" onclick="presionarInputFile()"><i class="far fa-images fa-2x"></i></button>
                            @if (!is_null($instrumento->fotoInstrumento)) <button type="button" id="deleteOriginal" class="btn_borrar" onclick="eliminarOriginal()"><i class="fas fa-eraser fa-2x"></i></button> @endif                
                        </div>                        
                        @if (!is_null($instrumento->fotoInstrumento))                
                            <img id="img_instrumento" name='img_instrumento' src='{{asset('storage/'.$instrumento->fotoInstrumento)}}' alt="">                 
                            <img name="imgOriginal" id="imgOriginal" style="visibility: hidden" src='{{asset('storage/'.$instrumento->fotoInstrumento)}}'>                           
                        @else 
                            <img id="img_instrumento" name='img_instrumento' src="{{asset('assets/rsc/pngs/activo_default.png')}}" alt="">                                    
                        @endif
                        
                        <input type="hidden" id="tieneFoto" value="{{$instrumento->fotoInstrumento}}">
                        <input type="hidden" name="borrarOriginal" id="borrarOriginal" value="0">                      
                        <input type="file" accept="image/x-png, image/jpeg" name="file_imgInstrumento" id="file_imgInstrumento" onclick="cargarIMG()">   


                    </div>

                </div>
                <div id="div_btns">
                    <input class="btn" type="submit" id="btn_guardar" value="Guardar">
                    <input class="btn" type="button" id="btn_cancelar" value="Cancelar" onclick="cancelar()">
                    <a href="{{route('instrumentos')}}" id="a_cancelar"></a>
                    
                </div>
            </form>
    </div>

@else 
    <script>window.location = "/";</script>
@endif

@endsection