
showPass = false;

// funcion para cargar el DOM y los metodos
window.addEventListener('load', cargarFunciones, false);

function cargarFunciones() {
    // Seteamos un filtro que solo permite insertar numeros en el campo de cedula
    // filtroDeInput(document.getElementById("txt_ced"), function(value) { return /^\d*$/.test(value); });
    // Seteamos un filtro para que los campos de nombres y apellidos no puedan ingresar numeros
    // filtroDeInput(document.getElementById("txt_nombre"), function(value) { return /^[a-zA-Z\b]+$/.test(value); });
    validarCedula(document.getElementById("txt_ced"));
    // validarLetras(document.getElementById("txt_nombre"));
}


// Funcion para cancelar el añadir o editar un usuario
function cancelar() {
    document.getElementById('a_cancelar').click();
}

function presionarInputFile() {
    document.getElementById('file_imgUsuario').click();
}


function validarCedula (cedula) {
    ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
        cedula.addEventListener(event, function() {
            if (/^[a-zA-Z\b]+$/.test(this.value)) {
                valor = String(this.value);
                this.value = valor.slice(0, -1);
            }
        });
    });
}

function validarLetras (caompoTexto) {
    ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
        caompoTexto.addEventListener(event, function() {
            if (!/^[a-zA-Z\b]+$/.test(this.value)) {
                valor = String(this.value);
                this.value = valor.slice(0, -1);
            }
        });
    });
}

function borrarIMG() {
    document.getElementById('img_usuario').src = '../rsc/pngs/user_white.png';
    document.getElementById('file_imgUsuario').value = '';
    var btn = document.getElementById('btn_cargarImg');
    btn.className = 'btn_cargar';
    btn.innerHTML = '<i class="far fa-images fa-2x"></i>';
    btn.onclick = presionarInputFile;
    document.getElementById('cambios_img').value = '1';
}

function cargarIMG() {    
    this.onchange = e => {        
        if(e.target.files[0]){            
            imagenCargada = new Image();                    
            imagenCargada.onload = function () {                                
                document.getElementById('img_usuario').src = this.src;
                var btn = document.getElementById('btn_cargarImg');
                btn.className = 'btn_borrar';
                btn.innerHTML = '<i class="fas fa-trash-alt fa-2x"></i>';
                btn.onclick = borrarIMG;
            }
            imagenCargada.src = URL.createObjectURL(e.target.files[0]);
        }                         
    };
}

function mostrarPassword() {
    var x = document.getElementById('txt_password');
    if (x.type === "password") {
        x.type = "text";
        document.getElementById('btn_show').innerHTML = '<i class="fas fa-eye-slash">';
    } else {
        x.type = "password";
        document.getElementById('btn_show').innerHTML = '<i class="fas fa-eye">';        
    }
}

// Metodo que copia la pasword de un txt a otro de los que pueden mostrar la contraseña
function copiarPassword(entrada) {
    if (entrada.id == 'txt_password') {
        document.getElementById('txt_verPass').value = entrada.value;
    } else {
        document.getElementById('txt_password').value = entrada.value;
    }
}