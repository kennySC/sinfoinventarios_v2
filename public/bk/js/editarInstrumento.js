var tipoOriginal;
var numOriginal;
tieneFoto = false;
originalEliminada = false;


window.addEventListener('load', function () {
    
    document.getElementById('cmb_tipo').onchange = calcularUltimoInstrumento;

    tipoOriginal = document.getElementById('cmb_tipo').value;
    numOriginal = document.getElementById('txt_numero').value;

    if (document.getElementById('tieneFoto').value != '') {
        tieneFoto = true;
    }  

});

function calcularUltimoInstrumento() {
    var cod = document.getElementById('cmb_tipo').value;

    if (cod == tipoOriginal) {
        document.getElementById('txt_numero').value = numOriginal;
    } else {
        $.ajax({
            url: "/instrumentos/ultimoPorTipo/" + cod,
            type:'GET',       
            success: function(data) {      
                console.log(data);
                if (data < 10) {
                    document.getElementById('txt_numero').value = '00' + data;
                } else if (data >= 10 && data < 100) {  
                    document.getElementById('txt_numero').value = '0' + data;
                } else {
                    document.getElementById('txt_numero').value = data;
                }     
            },   
            error: function(data) {
                console.error(data);
            }
        });
    }

}

// Metodo especifico para el boton de cancelar en la ventana de agregar o editar un activo
function cancelar() {
    document.getElementById('a_cancelar').click();
}

// Metodo para bueno, presionar el input file xd
function presionarInputFile() {
    document.getElementById('file_imgInstrumento').click();
}

// Metodo para limpiar la foto, o remover la foto que se haya seleccionado
function borrarIMG() {
    if (tieneFoto && originalEliminada) {
        document.getElementById('img_instrumento').src = '/../rsc/pngs/activo_default.png';
    } else if (tieneFoto) {
        document.getElementById('img_instrumento').src = document.getElementById('imgOriginal').src;
        document.getElementById('deleteOriginal').style.visibility = 'visible';
    } else {
        document.getElementById('img_instrumento').src = '/../rsc/pngs/activo_default.png';
    }
    
    document.getElementById('file_imgInstrumento').value = '';
    var btn = document.getElementById('btn_cargarImg');
    btn.className = 'btn_cargar';
    btn.innerHTML = '<i class="far fa-images fa-2x"></i>';
    btn.onclick = presionarInputFile;
} 

// Metodo para cargar la imagen
function cargarIMG() {
    this.onchange = e => {        
        if(e.target.files[0]){            
            imagenCargada = new Image();                    
            imagenCargada.onload = function () {                                
                document.getElementById('img_instrumento').src = this.src;
                var btn = document.getElementById('btn_cargarImg');
                btn.className = 'btn_borrar';
                btn.innerHTML = '<i class="fas fa-trash-alt fa-2x"></i>';
                btn.onclick = borrarIMG;
                if (tieneFoto) {
                    document.getElementById('deleteOriginal').style.visibility = 'hidden';
                }                
            }
            imagenCargada.src = URL.createObjectURL(e.target.files[0]);
        }                         
    };
}

function eliminarOriginal () {
    document.getElementById('img_instrumento').src = '/../rsc/pngs/activo_default.png';
    document.getElementById('file_imgInstrumento').value = '';
    originalEliminada = true;
    document.getElementById('deleteOriginal').style.visibility = 'hidden';
    document.getElementById('borrarOriginal').value = 1;
}