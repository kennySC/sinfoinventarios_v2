
var datosTabla;

window.addEventListener('load', function() {

    tabla = $('#tabla_activos').DataTable({
        data: datosTabla,
        columns: [
            { data: 'codActivo' },
            { data: 'serie' },
            { data: 'nombre' },
        ],
        "bFilter": false,
        "bLengthChange": false,
        "bPaginate": false,
        "bInfo" : false,
        "language": {
            "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningún dato disponible en esta tabla",
            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":    "Último",
                "sNext":    "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });
}, false);

// Metodo especifico para el boton de activar boleta en la ventana del detalle de la boleta
function activarBoleta(boton) {        
    var id = boton.id;

    var divLoading = document.getElementById('div_loading');    

    alertify.confirm('Activación boleta',
                '¿Desea entregar la boleta?', 
                function(){ 
                    divLoading.style.visibility = 'visible';
                    $.ajax({
                        url: "/boletas/detalle/activar/" + id,
                        type:'GET',
                        success: function (data) {
                            console.log(data);
                            
                            divLoading.style.visibility = 'hidden';

                            if(data) {                                                                  
                                alertify.alert('Boleta Entregada Correctamente', function () {
                                    location.reload();
                                    console.log(data);
                                }).setHeader('Exito!').set('movable', false);                              
                            }
                        },
                        error: function (data) {
                            divLoading.style.visibility = 'hidden';
                            alertify.error('¡Hubo un error al entregar la boleta!');
                            console.log(data);                            
                        }
                    });                    
                },                 
                function(){ 
                    alertify.error('Movimiento cancelado');
                }).set('movable', false);
}  

// Metodo especifico para el boton de activar boleta en la ventana del detalle de la boleta
function finalizarBoleta(boton) {
    var id = boton.id;
    
    var divLoading = document.getElementById('div_loading');  

    alertify.confirm('Finalización boleta',
                '¿De verdad desea dar por finalizada la boleta?', 
                function(){ 

                    divLoading.style.visibility = 'visible';

                    $.ajax({
                        url: "/boletas/detalle/finalizar/" + id,
                        type:'GET',
                        success: function (data) {
                            
                            divLoading.style.visibility = 'hidden';
                            
                            if (data) {                                                                                                     
                                alertify.alert('Boleta Finalizada Correctamente.', function () {
                                    location.reload();                            
                                }).setHeader('Exito!').set('movable', false);
                            }
                        },
                        error: function (data) {
                            
                            divLoading.style.visibility = 'hidden';

                            alertify.error('¡Hubo un error al finalizar la boleta!');
                        }
                    });                    
                },                 
                function(){ 

                    divLoading.style.visibility = 'hidden';
                    
                    alertify.error('Movimiento cancelado');
                });
}

// Metodo para cancelar una boleta de salida
function cancelarBoleta(boton) {
    var id = boton.id;
    // var nombreBoleta = boton.name;    
    alertify.confirm('Cancelación boleta',
                '¿Desea dar por cancelada la boleta?', 
                function(){ 
                    $.ajax({
                        url: "/boletas/detalle/cancelar/" + id,
                        type:'GET',
                        success: function (data) {                            
                            if (data) {
                                alertify.alert('La boleta fue cancelada correctamente.', function () {
                                    location.reload();                            
                                }).setHeader('Exito!').set('movable', false);
                            }
                        },
                        error: function (error) {
                            alertify.error('Error', '¡La boleta no pudo ser cancelada!');
                            console.log(error);
                        }
                    });                    
                },                 
                function(){ 
                    alertify.error('Movimiento cancelado');
                });
}

// Metodo para cancelar una boleta de salida que se encuentra con el estado de atrasada
function cancelarBoletaAtrasada(boton) {
    var id = boton.id;
    // var nombreBoleta = boton.name;    
    alertify.confirm('Cancelación boleta',
                '¿Desea dar por entregada la boleta?', 
                function(){ 
                    $.ajax({
                        url: "/boletas/detalle/cancelaratrasada/" + id,
                        type:'GET',
                        success: function (data) {
                            if (data) {
                                alertify.alert('La boleta fue cancelada correctamente.', function () {
                                    location.reload();                            
                                }).setHeader('Exito!').set('movable', false);
                            }
                        },
                        error: function (error) {
                            alertify.error('¡La boleta no pudo ser cancelada!');
                            console.log(error);
                        }
                    });                    
                },                 
                function(){ 
                    alertify.error('Movimiento cancelado');
                });
}

function irBoletas() {
    window.location.href = '/boletas';
}

// Codigo para imprimir la boleta de salida que se esta observando
function imprimirBoleta (boton) {
    document.getElementById('imprimirBoleta').click();
}