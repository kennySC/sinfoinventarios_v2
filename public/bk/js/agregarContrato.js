window.addEventListener('load', funcionCargar, false);

function funcionCargar() { 

    // Codigo para bloquear las fechas que no se deberian poder escoger en los date pickers
    var date = new Date();
    date.setHours(date.getHours() - 6);
    var today = date.toISOString().split('T')[0];    

    document.getElementById('txt_fechadeinicio').setAttribute('max', today);
    document.getElementById('txt_fechafinalizacion').setAttribute('min', today);

    // DESMUTEAR ESTE CODIGO PARA BLOQUEAR FECHAS ANTERIORES AL DIA ACTUAL EN LA FECHA DE INICIO DE CONTRATO
    // date.setDate(date.getDate() - 1);
    // var yesterday = date.toISOString().split('T')[0];
    // document.getElementById('txt_fechadeinicio').setAttribute('min', yesterday);

    cargarFecha();    

}

function cancelar() {
    document.getElementById('a_cancelar').click();
}

function cargarFecha(){
    var fecha = new Date(); 
      var dd = fecha.getDate();
      var mm = fecha.getMonth()+1; //January is 0!
      var yyyy = fecha.getFullYear();

      if(dd<10) {
          dd = '0'+dd
      } 

      if(mm<10) {
          mm = '0'+mm
      } 

      fecha = yyyy + '-' + mm + '-' + dd;

      console.log(fecha);
      document.getElementById('txt_fechadeinicio').value = fecha;
}
    
function buscarEstudiante () {    
    
    var _token = $("input[name='_token']").val();
    var txt_cedula_estudiante = $('#txt_cedula_estudiante').val();

    txt_cedula_estudiante = txt_cedula_estudiante.replace(/ /g,'');

    console.log(txt_cedula_estudiante);

    if(document.getElementById('txt_cedula_estudiante').value == ''){
        
        document.getElementById('txt_nombre_estudiante').value = '';
        document.getElementById("cmb_encargado_fiador").innerHTML ="";
        mostrarDivFiador();

    }else{

        $.ajax({
            url: "/contratos/agregar/buscarEstudiante",
            type:'POST',
            data: { _token:_token, txt_cedula_estudiante:txt_cedula_estudiante},
            success: function(data) {      
                
                console.log(data);
                
                if(!data){ 
                    document.getElementById('txt_nombre_estudiante').value = "No se ha encontrado un estudiante con esta cédula";  
                    document.getElementById("txt_nombre_estudiante").style.color = "red";
                    document.getElementById("cmb_encargado_fiador").innerHTML ="";
                    mostrarDivFiador();
                    
                    
                }else{
                    document.getElementById("txt_nombre_estudiante").style.color = "white";
                    document.getElementById('txt_nombre_estudiante').value = data.nombreCompleto;  
                }
                if(data.encargados)
                cargarEncargados(data);

            },   
            error: function(data) {
                console.error(data);
            }
        });
    }
}

function cargarEncargados(estudiante){
    if(document.getElementById("txt_nombre_estudiante").style.color!="red"){
    document.getElementById("cmb_encargado_fiador").innerHTML ="<option value='F'>Agregar fiador</option>";
    }
    estudiante.encargados.forEach(encargado => {
        document.getElementById("cmb_encargado_fiador").innerHTML = document.getElementById("cmb_encargado_fiador").innerHTML + "<option value='"+encargado.cedulaEncargado+"'>"+encargado.persona.nombreCompleto+"</option>";
    });
    mostrarDivFiador();

}

function buscarInstrumento () {    
    
    
    var _token = $("input[name='_token']").val();
    var txt_cod_instrumento = $('#txt_cod_instrumento').val();    

    console.log(txt_cod_instrumento);
    if(document.getElementById('txt_cod_instrumento').value == ''){
        
        document.getElementById('txt_cod_instrumento').value = '';
        

    }else{

        $.ajax({
            url: "/contratos/agregar/buscarInstrumento",
            type:'POST',
            data: { _token:_token, txt_cod_instrumento:txt_cod_instrumento},
            success: function(data) {      
                
                if(!data){ 
                    document.getElementById('txt_nombre_activo').value = "No se ha encontrado un instrumento con este código";  
                    document.getElementById("txt_nombre_activo").style.color = "red";
                    
                }else{
                    
                    document.getElementById("txt_nombre_activo").style.color = "white";
                    document.getElementById('txt_nombre_activo').value = data.nombre; 
                    
                }

            },   
            error: function(data) {
                console.error(data);
            }
        });
    }
}

function mostrarDivFiador () { 
    var encargado_fiador = $('#cmb_encargado_fiador').val();
    
    if(encargado_fiador == 'F'){
        document.getElementById("div_datos_fiador").style.width = "auto";
        document.getElementById("div_datos_fiador").style.height = "auto";
        document.getElementById("div_datos_fiador").style.visibility = "visible";
    }else{
        
        document.getElementById("div_datos_fiador").style.width = "0px";
        document.getElementById("div_datos_fiador").style.height = "0px";
        document.getElementById("div_datos_fiador").style.visibility = "hidden";
    }
    
}
// Metodo para verificar que la fecha esperada de devolucion que se este ingresando no sea una fecha anterior a la presente
function verificarFechaInicio(){
    var fecha_input = document.getElementById('txt_fechadeinicio').value; // Fecha input es el string que queda en el input cuando se selecciona la fecha con el datepicker
    var fecha_inicio = new Date(fecha_input); // Creamos la variable fecha para que sea un objeto de tipo Date
    var fecha_actual = new Date();
    // Validamos
    try {
        if(fecha_inicio < fecha_actual){
            alertify.error("¡La fecha de inicio no puede haber pasado!");
        }
    } catch (error) {
        console.log(error)
    }
}


// Metodo para verificar que la fecha esperada de devolucion que se este ingresando no sea una fecha anterior a la presente
function verificarFechaFin(){
    // var fecha_input_ini = document.getElementById('txt_fechadeinicio').value; 
    // var fecha_inicio = new Date(fecha_input_ini); 
    // var fecha_input_fin = document.getElementById('txt_fechafinalizacion').value; 
    // var fecha_final = new Date(fecha_input_fin); 

    
    
    // try {
    //     if(fecha_inicio >= fecha_final){
    //         alertify.error("¡La fecha de inicio tiene que ser mayor que la fecha de finalización!");
    //     }
    // } catch (error) {
    //     console.log(error)
    // }
}