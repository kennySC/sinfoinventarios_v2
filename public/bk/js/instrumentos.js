pagina = 1;
filtrando = false;

window.addEventListener('load', funcionCargar, false);

function funcionCargar () {

    document.getElementById('btn_filtrar').addEventListener('click', function(e) {
        e.preventDefault();
        filtrar();
    }, false);

    $(document).on('click', '.pagination-a', function(event){
        event.preventDefault(); 
        pagina = $(this).attr('href').split('page=')[1];
        if (filtrando) {
            filtrar();
        } else {
            paginarAjax();
        }
    });
}

function paginarAjax() {
    var _token = $("input[name=_token]").val();
    $.ajax({
        url:"/instrumentos",
        method:"get",
        data:{_token:_token, page:pagina},
        success:function(data) {
            refrescarItems(data);
        }
    });
}

function filtrar () {   
    var _token = $("input[name='_token']").val();
    var txt_cod_instrumento = $('#txt_cod_instrumento').val();
    var cmb_institucion_instrumentos = $('#cmb_institucion_instrumentos').val();
    var cmb_tipo_instrumentos = $('#cmb_tipo_instrumentos').val();
    var cmb_estado = $('#cmb_estado').val();

    console.log(_token);
    $.ajax({
        url: "/instrumentos/filtrar",
        type:'POST',
        data: { _token:_token, 
            txt_cod_instrumento:txt_cod_instrumento, 
                cmb_institucion_instrumentos:cmb_institucion_instrumentos,
                cmb_tipo_instrumentos:cmb_tipo_instrumentos,
                cmb_estado:cmb_estado,
                page:pagina },
        success: function(data) {   
            refrescarItems(data);            
            filtrando = true;
        },   
        error: function(data) {
            console.log(data.responseJSON.message);
            console.error(data);
        }
    });
}


function refrescarItems(data) { 
   document.getElementById('div_der').innerHTML = '<i class="fas fa-spinner fa-pulse fa-10x cargando"></i>';    
    setTimeout(() => {        
        var temphtml = document.createElement('div'); //div temporal
        temphtml.innerHTML = data;
        document.getElementById('div_der').innerHTML = temphtml.querySelector("#div_der").innerHTML;
    }, 500);    
}

function limpiarFiltros() {
    $("#txt_cod_instrumento").val('');
    $("#cmb_institucion_instrumentos").val('T');
    $("#cmb_tipo_instrumentos").val('T');
    $("#cmb_estado").val('T');
    filtrar();
    filtrando = false;
    pagina = 1;
}

//  Metodo para cancelar la dada de baja
function confirmarBaja(boton) {
    var id = boton.id;
    var nombreInstrumento= boton.name;    

    alertify.confirm('Dar de baja a ' + nombreInstrumento,
                '¿Esta seguro que desea dar de baja al instrumento?', 
                function(){ 
                    $.ajax({
                        url: '/instrumentos/darBaja/'+id,
                        success: function (data) {
                            refrescarItems(data);
                            alertify.success('El instrumento '+nombreInstrumento + ' fue dado de baja exitosamente.');
                            console.log(data);
                        },
                        error: function (data) {
                            alertify.error('¡Ha ocurrido un error al dar de baja al instrumento!');
                            console.error(data);
                        }
                    });                    
                },                 
                function(){ 
                    alertify.warning('Movimiento cancelado');
                }).set('movable', false);
}

// Metodo para activar un activo que se encontraba dado de baja
function activarInstrumento(boton) {
    var id = boton.id;
    var nombreInstrumento= boton.name;    
   
    alertify.confirm('Activar ' + nombreInstrumento,
                '¿Esta seguro que desea activar el instrumento?', 
                function(){ 
                    $.ajax({
                        url: '/instrumentos/activar/'+id,
                        success: function (data) {
                            refrescarItems(data);
                            alertify.success('El instrumento '+ nombreInstrumento + ' fue activado con éxito.');
                        },
                        error: function (data) {
                            alertify.error('¡Ha ocurrido un error al activar el instrumento!');
                            console.error(data);
                        }
                    });                    
                },                 
                function(){ 
                    alertify.warning('Movimiento cancelado');
                }).set('movable', false);
}

// Metodo para pasar a la vista de editar un activo
function editarInstrumento(boton) { // Recibe el boton al que se le dio click para obtener el id del activo por editar
    var id = boton.id;
    // Usamos el id obtenido para buscar al tag "a" especifico que edita a ese activo 
    document.getElementById('a_editar_' + id).click(); //  y le damos click
}

//Metodo para pasar a la vista de agregar un nuevo activo
function irNuevoInstrumento() {
    document.getElementById('a_nuevoInstrumento').click();
}



function verMas(inst) {      
    console.log(inst);
    alertify.prompt()            
            .set('movable', false)
            .setHeader(inst.nombre + '  |  ' + inst.codTipoInstrumento + '-' + inst.numeroInstrumento)
            .set('frameless', true)
            .setContent(
                '<div id="form_activo">' +
                    '<div id="div_datos">' +
                        '<div class="columna">' +
                            '<label for="txt_codigo">' +
                                'Código del inst: <br>' +
                                '<input readonly class="txt" type="text" name="txt_codigo" id="txt_codigo" value="' + inst.codInstrumento + '">' +
                            '</label>' +
                            '<label  for="tipoIsnt">' +
                                'Tipo de Instrumento:' +
                                '<div id="tipoIsnt">' +
                                    '<input readonly type="text" class="txt" name="cmb_tipo" id="cmb_tipo" class="cmb_datos" value="' + inst.tipo_instrumento.tipoInstrumento + '">' +
                                    '<input readonly type="text" class="txt" name="txt_numero" id="txt_numero" value="' + inst.numeroInstrumento + '">' +
                                '</div>' +
                            '</label>' +    
                            '<label for="txt_nombre">' +
                                'Nombre del instrumento: <br>' +
                                '<input readonly class="txt" type="text" name="txt_nombre" id="txt_nombre" value="' + inst.nombre + '">' +
                            '</label>' +
                            '<label for="txt_marca">' +
                                'Marca del instrumento: <br>' +
                                '<input readonly class="txt" type="text" name="txt_marca" id="txt_marca" value="' + inst.marca + '">' +
                            '</label>  ' +
                            '<label for="txt_modelo">' +
                                'Modelo del instrumento: <br>' +
                                '<input readonly class="txt" type="text" name="txt_modelo" id="txt_modelo" value="' + inst.modelo + '">' +
                            '</label>' +
                        '</div>' +
                        '<div class="columna">' +
                            '<label for="txt_serie">' +
                                '# Serie del instrumento: <br>' +
                                '<input readonly class="txt" type="text" name="txt_serie" id="txt_serie" value="' + inst.serie + '">' +
                            '</label>' +
                            '<label for="txt_valor">' +
                                'Valor ₡ del instrumento: <br>' +
                                '<input readonly class="txt" type="text" name="txt_valor" id="txt_valor" value="' + inst.valor + '">' +
                            '</label>' +
                            '<label for="txt_ubicacion">' +
                                'Ubicación del instrumento: <br>' +
                                '<input readonly class="txt" type="text" name="txt_ubicacion" id="txt_ubicacion" value="' + inst.ubicacion + '">' +
                            '</label>' +
                            '<label for="txt_responsable">' +
                                'Responsable del instrumento: <br>' +
                                '<input readonly class="txt" type="text" name="txt_responsable" id="txt_responsable" value="' + inst.responsable + '">' +
                            '</label>' +
                        '</div>' +
                        '<div class="columna">' +
                            '<label  for="cmb_institucion">' +
                                'Institución:<br>' +
                                '<input readonly type="text" class="txt" name="cmb_institucion" id="cmb_institucion" class="cmb_datos" value="' + inst.institucion.nombre + '">' +
                            '</label>' +
                
                            '<label for="cmb_estado">' +
                                'Estado: <br>' +
                                '<input readonly type="text" class="txt" name="cmb_estado" id="cmb_estado" value="' + inst.estado.estado + '">' +
                            '</label>' +
                
                            '<label for="txt_observaciones">' +
                                'Observaciones sobre el activo: <br>' +
                                '<textarea readonly class="txt_area" type="text" cols="30" rows="8" name="txt_observaciones" id="txt_observaciones">' + inst.observaciones + '</textarea>' +
                            '</label>' +
                        '</div>        ' +
                    '</div>' +
                '</div>'
            )
            .set('resizable',true).resizeTo('75%', '70%').show();
}