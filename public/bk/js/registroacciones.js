var tabla;
var datosTabla = [];

var dp_inicio;
var dp_final;

var generar = false;

window.addEventListener('load', function () {

    dp_final = document.getElementById("dp_final");
    dp_inicio = document.getElementById("dp_inicio");

    // bloqueamos las fechas
    var date = new Date();
    date.setHours(date.getHours() - 6);
    var today = date.toISOString().split('T')[0];

    dp_inicio.setAttribute('max', today);     
    dp_final.setAttribute('max', today);        

    // Metodo onclick del boton generar
    document.getElementById('btn_generar').addEventListener('click', function(e) {
        e.preventDefault();
        generar_reporte();        
    }, false);

    // Metodo para el onchage del checkbox
    document.getElementById('rb_fechas').addEventListener('change', function (e) {        
        if (document.getElementById('rb_fechas').checked) {
            document.getElementById('div_fechas').classList.remove('txt_hidden');
        } else {
            document.getElementById('div_fechas').classList.add('txt_hidden');
        }
    }, false); 

    // Metodo onchange del input date "dp_inicio" para activar la fecha de inicio
    dp_inicio.addEventListener('change', function (e) {
        generar = false;
        dp_final.value = '';
        dp_final.setAttribute('min', dp_inicio.value);      
        dp_final.disabled = false;
    }, false);

    dp_final.addEventListener('change', function (e) {
        generar = true;
    })        

    tabla = $('#tabla_registro').DataTable({
        data: datosTabla,
        columns: [
            { data: 'tipo_accion.tipoAccion' },
            { data: 'idReferencial' },            
            { data: 'usuario.nombreUsuario' },
            { data: 'fechaAccion' },
        ],
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excelHtml5',               
            },
            {
                extend: 'pdfHtml5',
                pageSize: 'LEGAL',
                title: 'Registro de acciones Sinfoinventarios'
            },
            {
                text: 'Limpiar',
                action: function ( e, dt, node, config ) {
                    limpiarTabla();
                }
            }
        ], 
        "bFilter": true,
        "bLengthChange": false,
        "bPaginate": false,
        "bInfo" : false,
        "language": {
            "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningún dato disponible en esta tabla",
            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":    "Último",
                "sNext":    "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });

}, false);


function generar_reporte() {
    if ( (document.getElementById('rb_fechas').checked && ( dp_inicio.value != '' && dp_final.value != '' )) || !document.getElementById('rb_fechas').checked ) {
        
        var _token = $("input[name='_token']").val();
        var fecha_inicio = $('#dp_inicio').val();
        var fecha_final = $('#dp_final').val();

        
        $.ajax({
            url: "/registroAcciones/obtenerRegistro",
            type:'POST',
            data: { _token:_token, dp_inicio:fecha_inicio, dp_final:fecha_final, usar_rango:document.getElementById('rb_fechas').checked},
            success: function(data) {              
                datosTabla = data;

                tabla.clear();

                datosTabla.forEach(dt => {

                    tabla.row.add(dt);
                });

                tabla.draw();
            },   
            error: function(data) {
                console.error(data);
            }
        });        

    } else {
        alertify.error('Debe elegir un rango de fechas para generar el reporte!');
    }
}


function limpiarTabla() {
    tabla.clear();
    tabla.draw();
    datosTabla = [];
}