window.addEventListener('load', funcionCargar, false);


function funcionCargar() {
    
    var fecha_final = document.getElementById('fecha_fin').value;     

    var fecha = new Date().toISOString().split('T')[0];

    console.log(fecha_final);
    console.log(fecha);    


    if(fecha_final <= fecha){        
        document.getElementById('btn_TerminarContrato').style.visibility = "visible";        
    }
  

}
    
function cancelar() {
    document.getElementById('a_cancelar').click();
}

function descargar(id) {
     
        $.ajax({
            url: "/contratos/descargar/" + id,
            type:'GET',
            success: function (data) {                            
                if (data) {
                  
                    var filename = "EMSPZ-CTR-"+id;
                    


                    var blob = new Blob(['\ufeff', data],{
                        type: 'application/msword'
                    });

                    var url = 'data:application/vnd.ms-word;charset=utf-8,' + encodeURIComponent(data)

                    filename = filename?filename+'.doc': 'document.doc';

                    var downloadLink = document.createElement("a");

                    document.body.appendChild(downloadLink);

                    if(navigator.msSaveOrOpenBlob){
                        navigator.msSaveOrOpenBlob(blob, filename);
                    }else{
                        downloadLink.href = url;

                        downloadLink.download = filename;

                        downloadLink.click();
                    }

                    document.body.removeChild(downloadLink);
                    
                }
            },
            error: function (error) {
                alertify.error('Error', '¡El contrato no pudo ser descargado!');
                console.log(error);
            }
        }); 
        

}

function cancelarContrato(id) {    

    var divLoading = document.getElementById('div_loading');  

    alertify.confirm('Cancelación de contrato',
                '¿Desea dar por cancelado el contrato?', 
                function(){ 

                    divLoading.style.visibility = 'visible';

                    $.ajax({
                        url: "/contratos/detalle/cancelar/" + id,
                        type:'GET',
                        success: function (data) {          
                            
                            divLoading.style.visibility = 'hidden';

                            if (data) {
                                alertify.alert('El contrato fue cancelado correctamente.', function () {
                                    location.reload();                            
                                });
                            }
                        },
                        error: function (error) {

                            divLoading.style.visibility = 'hidden';
                            
                            alertify.error('Error', '¡El contrato no pudo ser cancelado!');
                            console.log(error);
                        }
                    });                    
                },                 
                function(){ 
                    alertify.error('Movimiento cancelado');
                });
}

function terminarContrato(id) {    

    var divLoading = document.getElementById('div_loading');  

    alertify.confirm('Cancelación de contrato',
                '¿Desea dar por terminado el contrato?', 
                function(){ 
                    
                    divLoading.style.visibility = 'visible';

                    $.ajax({
                        url: "/contratos/detalle/terminar/" + id,
                        type:'GET',
                        success: function (data) {                            

                            divLoading.style.visibility = 'hidden';

                            if (data) {
                                alertify.alert('El contrato fue terminado correctamente.', function () {
                                    location.reload();                            
                                });
                            }
                        },
                        error: function (error) {

                            divLoading.style.visibility = 'hidden';

                            alertify.error('Error', '¡El contrato no pudo ser terminado!');
                            console.log(error);
                        }
                    });                    
                },                 
                function(){ 
                    alertify.error('Movimiento cancelado');
                });
}



function verMas(inst) {      
    console.log(inst);
    alertify.prompt()            
            .set('movable', false)
            .setHeader(inst.nombre + '  |  ' + inst.codTipoInstrumento + '-' + inst.numeroInstrumento)
            .set('frameless', true)
            .setContent(
                '<div id="form_activo">' +
                    '<div id="div_datos_inst">' +
                        '<div class="columna">' +
                            '<label for="txt_codigo">' +
                                'Código del inst: <br>' +
                                '<input readonly class="txt" type="text" name="txt_codigo" id="txt_codigo" value="' + inst.codInstrumento + '">' +
                            '</label>' +
                            '<label  for="tipoIsnt">' +
                                'Tipo de Instrumento:' +
                                '<div id="tipoIsnt">' +
                                    '<input readonly type="text" class="txt" name="cmb_tipo" id="cmb_tipo" class="cmb_datos" value="' + inst.tipo_instrumento.tipoInstrumento + '">' +
                                    '<input readonly type="text" class="txt" name="txt_numero" id="txt_numero" value="' + inst.numeroInstrumento + '">' +
                                '</div>' +
                            '</label>' +    
                            '<label for="txt_nombre">' +
                                'Nombre del instrumento: <br>' +
                                '<input readonly class="txt" type="text" name="txt_nombre" id="txt_nombre" value="' + inst.nombre + '">' +
                            '</label>' +
                            '<label for="txt_marca">' +
                                'Marca del instrumento: <br>' +
                                '<input readonly class="txt" type="text" name="txt_marca" id="txt_marca" value="' + inst.marca + '">' +
                            '</label>  ' +
                            '<label for="txt_modelo">' +
                                'Modelo del instrumento: <br>' +
                                '<input readonly class="txt" type="text" name="txt_modelo" id="txt_modelo" value="' + inst.modelo + '">' +
                            '</label>' +
                        '</div>' +
                        '<div class="columna">' +
                            '<label for="txt_serie">' +
                                '# Serie del instrumento: <br>' +
                                '<input readonly class="txt" type="text" name="txt_serie" id="txt_serie" value="' + inst.serie + '">' +
                            '</label>' +
                            '<label for="txt_valor">' +
                                'Valor ₡ del instrumento: <br>' +
                                '<input readonly class="txt" type="text" name="txt_valor" id="txt_valor" value="' + inst.valor + '">' +
                            '</label>' +
                            '<label for="txt_ubicacion">' +
                                'Ubicación del instrumento: <br>' +
                                '<input readonly class="txt" type="text" name="txt_ubicacion" id="txt_ubicacion" value="' + inst.ubicacion + '">' +
                            '</label>' +
                            '<label for="txt_responsable">' +
                                'Responsable del instrumento: <br>' +
                                '<input readonly class="txt" type="text" name="txt_responsable" id="txt_responsable" value="' + inst.responsable + '">' +
                            '</label>' +
                        '</div>' +
                        '<div class="columna">' +
                            '<label  for="cmb_institucion">' +
                                'Institución:<br>' +
                                '<input readonly type="text" class="txt" name="cmb_institucion" id="cmb_institucion" class="cmb_datos" value="' + inst.institucion.nombre + '">' +
                            '</label>' +
                
                            '<label for="cmb_estado">' +
                                'Estado: <br>' +
                                '<input readonly type="text" class="txt" name="cmb_estado" id="cmb_estado" value="' + inst.estado.estado + '">' +
                            '</label>' +
                
                            '<label for="txt_observaciones">' +
                                'Observaciones sobre el activo: <br>' +
                                '<textarea readonly class="txt_area" type="text" cols="30" rows="8" name="txt_observaciones" id="txt_observaciones">' + inst.observaciones + '</textarea>' +
                            '</label>' +
                        '</div>        ' +
                    '</div>' +
                '</div>'
            )
            .set('resizable',true).resizeTo('75%', '70%').show();
}