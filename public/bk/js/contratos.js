
window.addEventListener('load', funcionCargar, false);

function funcionCargar () {
    document.getElementById('btn_filtrar').addEventListener('click', function(e) {
        e.preventDefault();
        filtrar();
    }, false);
}

function filtrar () {    
    
    var _token = $("input[name='_token']").val();
    var txt_nom_estudiante = $('#txt_nom_estudiante').val();
    var txt_ced_estudiante = $('#txt_ced_estudiante').val();
    txt_ced_estudiante = txt_ced_estudiante.replace(/ /g,'');
    var txt_nom_encargado = $('#txt_nom_encargado').val();
    var txt_ced_encargado = $('#txt_ced_encargado').val();
    var txt_cod_contrato = $('#txt_cod_contrato').val();
    var cmb_estado = $('#cmb_estado').val();

    console.log(txt_ced_estudiante);

    $.ajax({
        url: "/contratos/filtrar",
        type:'POST',
        data: { _token:_token, txt_cod_contrato:txt_cod_contrato, txt_nom_estudiante:txt_nom_estudiante,txt_ced_estudiante:txt_ced_estudiante,txt_nom_encargado:txt_nom_encargado,txt_ced_encargado:txt_ced_encargado,cmb_estado:cmb_estado},
        success: function(data) {                    
            refrescarItems(data);
        },   
        error: function(data) {
            console.error(data);
        }
    });
}

function refrescarItems(data) { 
    document.getElementById('div_der').innerHTML = '<i class="fas fa-spinner fa-pulse fa-10x cargando"></i>';    
    setTimeout(() => {        
        var temphtml = document.createElement('div'); //div temporal
        temphtml.innerHTML = data;
        document.getElementById('div_der').innerHTML = temphtml.querySelector("#div_der").innerHTML;
    }, 500);    
}

function limpiarFiltros() {
    $('#txt_nom_estudiante').val('');
    $('#txt_ced_estudiante').val('');
    $('#txt_nom_encargado').val('');
    $('#txt_ced_encargado').val('');
    $('#txt_cod_contrato').val('');
    $("#cmb_estado").val('To');
    filtrar();
}
function irNuevoContrato() {
    document.getElementById('a_nuevoContrato').click();
}

function ver_detalle(boton) {
    var id = boton.id;
    document.getElementById('a_detalle_' + id).click();
}