// Metodo especifico para el boton de cancelar en la ventana de agregar o editar un activo
function cancelar() {
    document.getElementById('a_cancelar').click();
}

// Metodo para bueno, presionar el input file xd
function presionarInputFile() {
    document.getElementById('file_imgActivo').click();
}

// Metodo para limpiar la foto, o remover la foto que se haya seleccionado
function borrarIMG() {
    document.getElementById('img_activo').src = '../rsc/pngs/activo_default.png';
    document.getElementById('file_imgActivo').value = '';
    var btn = document.getElementById('btn_cargarImg');
    btn.className = 'btn_cargar';
    btn.innerHTML = '<i class="far fa-images fa-2x"></i>';
    btn.onclick = presionarInputFile;
    document.getElementById('cambios_img').value = '1';
}

// Metodo para cargar la imagen
function cargarIMG() {
    this.onchange = e => {        
        if(e.target.files[0]){            
            imagenCargada = new Image();                    
            imagenCargada.onload = function () {                                
                document.getElementById('img_activo').src = this.src;
                var btn = document.getElementById('btn_cargarImg');
                btn.className = 'btn_borrar';
                btn.innerHTML = '<i class="fas fa-trash-alt fa-2x"></i>';
                btn.onclick = borrarIMG;
                document.getElementById('cambios_img').value = '1';
            }
            imagenCargada.src = URL.createObjectURL(e.target.files[0]);
        }                         
    };
}