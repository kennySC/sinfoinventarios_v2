
var tabla;
var datosTabla = [];
var tipoReporte;
var Institucion;
var fecha;
var tituloReporte;

var tabla_activos = 
    '<thead id="header_tabla">'+
        '<tr style="color: white">'+
            '<td>Código</td>'+
            '<td>Institucion</td>'+
            '<td>Nombre</td>'+
            '<td>Marca</td>'+
            '<td>Modelo</td>'+
            '<td>Serie</td>'+
            '<td>Valor</td>'+
            '<td>Responsable</td>'+
            '<td>Ubicación</td>'+
            '<td>Observaciones</td>'+
            '<td>Estado</td>'+
        '</tr>'+
    '</thead>'+
    '<tbody id="body_tabla">'+
    '</tbody>';

var tabla_instrumentos = 
    '<thead id="header_tabla">'+
        '<tr style="color: white">'+
            '<td>Código</td>'+
            '<td>Institución</td>'+
            '<td>Tipo</td>'+
            '<td>Número</td>'+
            '<td>Nombre</td>'+
            '<td>Marca</td>'+
            '<td>Modelo</td>'+
            '<td>Serie</td>'+
            '<td>Valor</td>'+
            '<td>Responsable</td>'+
            '<td>Ubicación</td>'+
            '<td>Observaciones</td>'+
            '<td>Estado</td>'+
        '</tr>'+
    '</thead>'+
    '<tbody id="body_tabla">'+
    '</tbody>';

var columnas_activos = [
                            { data: 'codActivo' },
                            { data: 'institucion.nombre' },
                            { data: 'nombre' },
                            { data: 'marca' },
                            { data: 'modelo' },
                            { data: 'serie' },
                            { data: 'valor' },
                            { data: 'responsable' },
                            { data: 'ubicacion' },
                            { data: 'observaciones' },
                            { data: 'estado_activo.estado' }                            
                        ];

var columnas_instrumentos = [
                                { data: 'codInstrumento' },
                                { data: 'institucion.nombre' },
                                { data: 'tipo_instrumento.tipoInstrumento' },
                                { data: 'numeroInstrumento' },
                                { data: 'nombre' },
                                { data: 'marca' },
                                { data: 'modelo' },
                                { data: 'serie' },
                                { data: 'valor' },
                                { data: 'responsable' },
                                { data: 'ubicacion' },
                                { data: 'observaciones' },
                                { data: 'estado.estado' }
                            ];

window.addEventListener('load', funcionCargar, false);

// Metodo para añadirle un listener al boton de generar reporte
function funcionCargar(){



     // Le seteamos el metodo click al boton de generar y evitamos que accione el form, y asi utilizaremos los datos del form
    document.getElementById('btn_generar').addEventListener('click', function(e) {
        e.preventDefault();
        generar_reporte();
    }, false);
}

function generar_reporte(){
    
    // limpiamos el preview para asi poder trabajar completamente limpios
    document.getElementById('div_preview').innerHTML = '<table class="tabla" id="tabla_reporte"></table>';

    var _token = $("input[name='_token']").val();
    var cmb_institucion = $('#cmb_institucion').val();
    var cmb_tipo_rep = $('#cmb_tipo_rep').val();

    var columnas; // Variable para ser llamada dentro del datatable de acuerdo al tipo de reporte que se quiera generar
    if(cmb_tipo_rep == 'A'){ // Consultamos si el tipo de reporte es sobre activos o de todos
        columnas = columnas_activos;        
    }
    else { // Si no, es un reporte sobre instrumentos
        columnas = columnas_instrumentos;        
    };

    var e = document.getElementById("cmb_tipo_rep");
    tipoReporte = e.options[e.selectedIndex].text;        

    var e = document.getElementById("cmb_institucion");
    Institucion = e.options[e.selectedIndex].id;    

    if (Institucion == 'T') {
        Institucion = 'Todas';
    }

    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();

    fecha = dd + '-' + mm + '-' + yyyy;

    tituloReporte = 'SinfoPZ - Reporte ' + tipoReporte + ' ' + Institucion + ' ' + fecha;

    $.ajax({
        url: "/reportes/generar",
        type:'POST',
        data: { _token:_token, cmb_institucion:cmb_institucion, cmb_tipo_rep:cmb_tipo_rep},
        success: function(data) {              
            
            //Validar con esto cmb_tipo_rep = $('#cmb_tipo_rep').val(); el valor del tipo de reporte que se quiere hacer
            if (cmb_tipo_rep == 'A') {                
                // Si el tipo de reporte es para ACTIVOS llamamos la tabla de activos
                document.getElementById('tabla_reporte').innerHTML = tabla_activos;                 
            } else {
                // Si el tipo de reporte es para INSTRUMENTOS llamamos la tabla de instrumentos
                document.getElementById('tabla_reporte').innerHTML = tabla_instrumentos;                 
            }

            console.log(data);
            
            $('#tabla_reporte').DataTable({                
                data: data,                
                columns: columnas,
                dom: 'Bfrtip',                
                buttons: [
                    {
                        extend: 'excelHtml5',                        
                        filename: tituloReporte,
                        messageTop: tituloReporte                        
                    },
                    {
                        extend: 'pdfHtml5',
                        orientation: 'landscape',
                        pageSize: 'LEGAL',
                        title: 'Reportes Sinfoinventarios',
                        filename: tituloReporte,
                        messageTop: tituloReporte
                    },
                    {
                        text: 'Limpiar',
                        action: function ( e, dt, node, config ) {
                            limpiarTabla();
                        }
                    }
                ],                                   
                "bLengthChange": false,
                "bPaginate": false,
                "bInfo" : false,
                "autoWidth": false,
                "language": {
                    "sProcessing":    "Procesando...",
                    "sLengthMenu":    "Mostrar _MENU_ registros",
                    "sZeroRecords":   "No se encontraron resultados",
                    "sEmptyTable":    "Ningún dato disponible en esta tabla",
                    "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":   "",
                    "sSearch":        "Buscar:",
                    "sUrl":           "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":    "Último",
                        "sNext":    "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            });
            // document.getElementById('cmbInstAux').value = cmb_institucion;
            // document.getElementById('cmbTipoAux').value = cmb_tipo_rep;
        },   
        error: function(data) {
            console.error(data);
        }
    });

}


//Metodo para borrar la tabla con el reporte generado, en caso de desear generar otro
function limpiarTabla () {
    if(document.getElementById('div_preview').innerHTML != " "){
        alertify.confirm('Limpiar Datos', '¿De verdad desea limpiar la tabla?', 
        function(){ 
            document.getElementById('div_preview').innerHTML = '<table class="tabla" id="tabla_reporte"></table>';
        },
        function(){ 
            
        }).set('movable', false);
    }
}

function descargar () {

}