window.addEventListener('load', cargar, false);


function cargar() {
    $(document).on('click', '.pagination-a', function(event){
        event.preventDefault(); 
        pagina = $(this).attr('href').split('page=')[1];
        if (filtrando) {
            filtrar();
        } else {
            paginarAjax();
        }
    });
}

function mostrarMenuUsuario() {
    var menu = document.getElementById("div_menuUsuario");
    if (menu.className == "cerrado") {
        menu.style.animation = "abrirMenuUsuario 500ms both";
        menu.className = "abierto";
    } else if(menu.className == 'abierto') {
        menu.style.animation = "cerrarMenuUsuario 500ms both";
        menu.className = "cerrado";
    }
}

function mostrarMenu(x) {    
    x.classList.toggle("cambiar");
    if(x.id == 'cerrado'){
        x.style.animation = "abrirBoton 500ms both";
        x.id = 'abierto';
    } else {
        x.style.animation = "cerrarBoton 500ms both";
        x.id = 'cerrado';
    }        
    var menu = document.getElementById("div_menu");
    if(menu.className == 'cerrado') {
        menu.style.animation = "abrirMenu 500ms both";
        menu.className = 'abierto';
    } else {
        menu.style.animation = "cerrarMenu 500ms both";
        menu.className = 'cerrado';
    }
}

function inicio() {
    document.getElementById('a_inicio').click();
}