pagina = 1;
filtrando = false;

var estudiante;

window.addEventListener('load', funcionCargar, false);

function funcionCargar () {
    // Le seteamos el metodo click al boton de filtrar y evitamos que accione el form, y asi utilizaremos los datos
    // del form para filtrar la informacion
    document.getElementById('btn_filtrar').addEventListener('click', function(e) {
        e.preventDefault();
        filtrar();
    }, false);

    $(document).on('click', '.pagination-a', function(event){
        event.preventDefault(); 
        pagina = $(this).attr('href').split('page=')[1];
        if (filtrando) {
            filtrar();
        } else {
            paginarAjax();
        }
    });

}

function paginarAjax() {
    var _token = $("input[name=_token]").val();
    $.ajax({
        url:"/usuarios",
        method:"get",
        data:{_token:_token, page:pagina},
        success:function(data) {
            refrescarItems(data);
        }
    });
}

//  Metodo para acceder a la ruta de editar a cada usuario a travez de los botones de editar
function editar(boton) { // Resive el boton al que se le dio click para obtener el id del usuario por editar
    var id = boton.id;
    // Usamos el id obtenido para buscar al tag "a" especifico que edita a ese usuario
    //  y le damos click
    document.getElementById('a_editar_' + id).click();
}

// Metodo para mostrar la pantalla de agregar usuario
function irNuevoUsuario() {
    document.getElementById('a_nuevoUsuario').click();
}

// Metodo que limpia los filtros de los usuarios
function limpiarFiltros() {
    $("#txt_buscarNom").val('');
    $("#cmb_rol").val('T');
    $("#cmb_estado").val('T');
    filtrar();
    filtrando = false;
    pagina = 1;
}

//  ------- Metodos que funcionan con AJAX y JQuery ------- 

// Metodo que mustra una confirmacion para eliminar el usuario seleccinado
function confirmarBaja(boton) {
    var id = boton.id;
    var nombreUsuario = boton.name;    
    alertify.confirm('Dar de baja a ' + nombreUsuario,
                '¿Esta seguro que sea dar de baja al usuario?', 
                function(){ 
                    $.ajax({
                        url: '/usuarios/darBaja/'+id,
                        success: function (data) {
                            refrescarItems(data);
                            alertify.success(nombreUsuario + ' Dado de baja');
                        },
                        error: function (data) {
                            alertify.error('¡No se puede dar de baja a un usuario conectado!');
                        }
                    });                    
                },                 
                function(){ 
                    alertify.error('Cancelado');
                }).set('movable', false); 
}

//  Metodo para activar un usuario dado de baja
function activarUsuario(boton) {
    var id = boton.id;
    var nombreUsuario = boton.name;    
    alertify.confirm('Activar ' + nombreUsuario,
                '¿Esta seguro que activar al usuario?', 
                function(){ 
                    $.ajax({
                        url: '/usuarios/activar/'+id,
                        success: function (data) {
                            refrescarItems(data);
                            alertify.success(nombreUsuario + ' Activado');
                        },
                        error: function (data) {
                            alertify.error(data);
                        }
                    });                    
                },                 
                function(){ 
                    alertify.error('Cancelado')
                });
}

// Metodo para filtrar los usuarios de manera asincronica
function filtrar () {    
    
    var _token = $("input[name='_token']").val();
    var txt_buscarNom = $('#txt_buscarNom').val();
    var cmb_rol = $('#cmb_rol').val();
    var cmb_estado = $('#cmb_estado').val();

    $.ajax({
        url: "/usuarios/filtrar",
        type:'POST',
        data: { _token:_token, txt_buscarNom:txt_buscarNom, cmb_rol:cmb_rol, cmb_estado:cmb_estado, page:pagina },
        success: function(data) {      
            refrescarItems(data);
            filtrando = true;
        },   
        error: function(data) {
            console.error(data);
        }
    });
}

//  Metodo que refresca a los usuarios en caso de haber cambiado, esencial para filtrar, dar de baja y activar usuarios
function refrescarItems(data) { 
    document.getElementById('div_der').innerHTML = '<i class="fas fa-spinner fa-pulse fa-10x cargando"></i>';    
    setTimeout(() => {        
        var temphtml = document.createElement('div'); //div temporal
        temphtml.innerHTML = data;
        document.getElementById('div_der').innerHTML = temphtml.querySelector("#div_der").innerHTML;
    }, 500);    
}

function editarPresidente() {

    $.ajax({
        url: "/usuarios/getPresidente",
        type:'GET',
        success: function(data) {      
            console.log(data);
            alertify.prompt()
                .set('movable',false)
                .setHeader('Editar Presidente')                
                .set('labels', {ok:'Guardar!', cancel:'Cancelar!'})
                .setContent(
                        '<form id="form_presidente">' +                    
                            '<label class="lb_infoPre" for="txt_cedulaPre">' +
                                'Cedula:' +
                                '<input type="text" class="txt_infoPre" name="txt_cedulaPre" id="txt_cedulaPre" value="' + data.cedula + '">' +
                            '</label>' +
                            '<label class="lb_infoPre" for="txt_nombrePre">' +
                                'Nombre Completo:' +
                                '<input type="text" class="txt_infoPre" name="txt_nombrePre" id="txt_nombrePre" value="' + data.nombreCompleto + '">' +
                            '</label>' +
                            '<label class="lb_infoPre" for="txt_emailPre">' +
                                'Email:' +
                                '<input type="text" class="txt_infoPre" name="txt_emailPre" id="txt_emailPre" value="' + data.email + '">' +
                            '</label>' +
                            '<label class="lb_infoPre" for="txt_telefonoPre">' +
                                'Telefono:' +
                                '<input type="text" class="txt_infoPre" name="txt_telefonoPre" id="txt_telefonoPre" value="' + data.telefono + '">' +
                            '</label>' +
                        '</form>'
                    )
                .set('resizable', true).resizeTo('50%', '70%')
                .set('overflow', false)
                .set('onok', savePresidente)    
                .set('padding',false)           
                .show();     
        },   
        error: function(data) {
            console.error(data);
        }
    });    


    function savePresidente() {
        var _token = $("input[name='_token']").val();
        var cedulaPre = document.getElementById('txt_cedulaPre').value;
        var nombrePre = document.getElementById('txt_nombrePre').value;

        $.ajax({
            url: "/usuarios/guardarPresidente",
            type:'POST',
            data: { _token:_token, nombrePre:nombrePre, cedulaPre:cedulaPre },
            success: function(data) {                      
                alertify.success('Presidente editado correctamente!');
            },   
            error: function(data) {                
                alertify.error('Error al editar el presidente!');
            }
        });
    }

}