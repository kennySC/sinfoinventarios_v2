
var datosTabla = [];
var tabla;

window.addEventListener('load', function () {

    // Codigo para bloquear las fechas anteriores a el dia actual en la fecha esperada de devolucion
    var date = new Date();
    date.setHours(date.getHours() - 6);
    var today = date.toISOString().split('T')[0];    

    document.getElementById('txt_fechadevolucion').setAttribute('min', today);

    tabla = $('#tabla_activos').DataTable({
        data: datosTabla,
        columns: [
            { data: 'codActivo' },
            { data: 'serie' },
            { data: 'nombre' },
            { data: null },
        ],
        "columnDefs": [ {
            "targets": 3,
            "data": null,
            "defaultContent": "<button class='btn-eliminar'  onclick='quitarActivo(this)'><i class='fas fa-trash-alt'></i></button>"
        }],
        "bLengthChange": false,
        "bPaginate": false,
        "bInfo" : false,
        "language": {
            "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningún dato disponible en esta tabla",
            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":    "Último",
                "sNext":    "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });

    document.getElementById('btn_guardar').addEventListener('click', function(e) {
        e.preventDefault();
        guardarBoleta();
    }, false);

}, false);

// Al quitar un elemento de la tabla debemos quitarlo de la lista que sirve como source data
function quitarActivo(boton) {
    var data = tabla.row( $(boton).parents('tr') ).data();
    alertify.confirm('Desea quitar el activo ' + data.nombre +  '?', function () {
        tabla.row( $(boton).parents('tr') ).remove().draw();
        datosTabla.forEach(dt => {
            if (dt.codActivo == data.codActivo) {
                datosTabla.pop(dt);
            }
        });
    });
}

function limpiarTabla () {
    alertify.confirm('Limpiar Tabla',
        '¿Desea limpiar la tabla de activos?\nDeberá agregar los activos nuevamente' , 
    function () {
        tabla.clear();
        tabla.draw();
        datosTabla = [];
    },
    function(){ 
        alertify.warning('Movimiento cancelado');
    }).set('movable', false);
    
}

// Metodo especifico para el boton de cancelar en la ventana del detalle de la boleta
function cancelar() {
    document.getElementById('a_cancelar').click();
}


function seleccionActivos() {
    alertify.prompt()
            .set({
                onclosing: function () { // Cuando se cierra la ventana modal
                    
                    var numFilas = $('#lista_activos').DataTable().rows( { selected: true } ).count();                                        

                    repetidos = false;           

                    for (i = 0; i < numFilas; i++) {
                        var activo = $('#lista_activos').DataTable().rows( { selected: true } ).data()[i];
                        
                    // Preguntamos si el activo que se quiere añadir, ya existe
                        if (validarActivosAgregados(activo)){
                            datosTabla.push(activo);                        
                        } else {
                            repetidos = true;
                        }

                    }
                    
                    tabla.clear();  // Limpiamos la tabla
                    datosTabla.forEach(dt => {                        
                        tabla.row.add(dt); // Reagreamos todos los objetos
                    });                    
                    
                    tabla.draw(); 

                    if (repetidos) {
                        alertify.warning('Uno o mas activos ya estaban agregados y han sido omitidos!');
                    }
                },               
            })
            .set('movable', false)
            .setHeader('Escoje los Activos')
            .setContent(
                '<div id="div_tabla">' +
                    '<table class="tabla" id="lista_activos">' +
                        '<thead id="header_tabla">'+
                            '<tr>'+
                                '<td>Codigo</td>'+
                                '<td>Serie</td>'+
                                '<td>Nombre</td>'+
                            '</tr>'+
                        '</thead>'+
                        '<tbody id="body_tabla">'+
                        '</tbody>'+
                    '</table>' +
                '</div>'
            )
            .show();
            $.ajax({
                url: '/activos/listaActivos',
                type: 'GET',
                success: function (data) {
                    $('#lista_activos').DataTable({                
                        data: data,
                        columns: [
                            { data: 'codActivo' },
                            { data: 'serie' },
                            { data: 'nombre' },
                        ],
                        select: {
                            style: 'multi'
                        },
                        "bInfo" : false,
                        "language": {
                            "sProcessing":    "Procesando...",
                            "sLengthMenu":    "Mostrar _MENU_ registros",
                            "sZeroRecords":   "No se encontraron resultados",
                            "sEmptyTable":    "Ningún dato disponible en esta tabla",
                            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                            "sInfoPostFix":   "",
                            "sSearch":        "Buscar:",
                            "sUrl":           "",
                            "sInfoThousands":  ",",
                            "sLoadingRecords": "Cargando...",
                            "oPaginate": {
                                "sFirst":    "Primero",
                                "sLast":    "Último",
                                "sNext":    "Siguiente",
                                "sPrevious": "Anterior"
                            },
                            "oAria": {
                                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                            }
                        }
                    });
                },
                error: function (data) {
                    alertify.error(data);
                }
            });
             
}

function validarActivosAgregados (activo) {    
    for (i = 0; i < datosTabla.length; i++) {
        dt = datosTabla[i];
        if (dt.codActivo == activo.codActivo) {
            return false;
        }
    }
    return true;
}


function guardarBoleta () {

    var divLoading = document.getElementById('div_loading');

    divLoading.style.visibility = 'visible';

    var _token = $("input[name='_token']").val();
    var txt_responsable = $('#txt_responsable').val();
    var txt_fechadevolucion = $('#txt_fechadevolucion').val();
    var txt_correoresponsable = $('#txt_correoresponsable').val();
    var txt_observaciones = $('#txt_observaciones').val();

    $.ajax({
        url: '/boletas/agregar/guardar',
        type: 'POST',
        data: { _token:_token, txt_responsable:txt_responsable,
                txt_fechadevolucion:txt_fechadevolucion, 
                txt_correoresponsable:txt_correoresponsable, 
                txt_observaciones:txt_observaciones,
                activos:datosTabla },

        success: function (data) {
            divLoading.style.visibility = 'hidden';
            alertify.confirm('Boleta creada correctamente', '¿Desea ver el detalle de la boleta? \n De lo contrario podra crear una nueva boleta',
                function () { // Aceptar
                    location.href = '/boletas/detalle/' + data;
                },
                function () { // Cancelar
                    $('#txt_responsable').val('');
                    $('#txt_fechadevolucion').val('');
                    $('#txt_correoresponsable').val('');
                    $('#txt_observaciones').val('');
                    datosTabla = [];
                    tabla.clear();
                    tabla.draw();
                    location.href = '/boletas';
                }
            ).set('movable', false);
        },
        error: function (error) {
            console.log(error.responseJSON.message);
            alertify.error('Error al guardar la boleta');
        }
    });
    
}

// Metodo para verificar que la fecha esperada de devolucion que se este ingresando no sea una fecha anterior a la presente
function verificarFecha(){

}