

window.addEventListener('load', function() {


    document.getElementById('btn_recover').addEventListener('click', recuperarContrasena, false);


}, false);



function recuperarContrasena() {
    alertify.prompt( 'Recuperar contraseña', '¡Ingrese su nombre de usuario para recuperar su contraseña!', '', 
                function(evt, value) {                    
                    $.ajax({
                        url: '/recoverPass/'+value,
                        success: function (data) {                            
                            switch (data) {
                                case 'success':
                                    alertify.success('Se ha enviado un correo de recuperacion');
                                    break;                            
                                default:
                                    alertify.warning('El nombre de usuario ingresado es incorrecto');
                                    break;
                            }
                        },
                        error: function (error) {
                            console.error(error.responseJSON.message);
                            alertify.error('Ocurrio un error al recuperar su contraseña');
                        }
                    });           

                }, 
                function() {                    
                });
}