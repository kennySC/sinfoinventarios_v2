originalEliminada = false;
showPass = false;
tieneFoto = false;

// funcion para cargar el DOM y los metodos
window.addEventListener('load', cargarFunciones, false);

function cargarFunciones() {
    // Seteamos un filtro que solo permite insertar numeros en el campo de cedula
    // filtroDeInput(document.getElementById("txt_ced"), function(value) { return /^\d*$/.test(value); });
    // Seteamos un filtro para que los campos de nombres y apellidos no puedan ingresar numeros
    // filtroDeInput(document.getElementById("txt_nombre"), function(value) { return /^[a-zA-Z\b]+$/.test(value); });
    validarCedula(document.getElementById("txt_ced"));
    // validarLetras(document.getElementById("txt_nombre"));
    // validarLetras(document.getElementById("txt_nombre2"));
    // validarLetras(document.getElementById("txt_apellido"));
    // validarLetras(document.getElementById("txt_apellido2"));    

    if (document.getElementById('tieneFoto').value != '') {
        tieneFoto = true;
    }

}


// Funcion para cancelar el añadir o editar un usuario
function cancelar() {
    document.getElementById('a_cancelar').click();
}

function presionarInputFile() {
    document.getElementById('file_imgUsuario').click();
}


function validarCedula (cedula) {
    ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
        cedula.addEventListener(event, function() {
            if (/^[a-zA-Z\b]+$/.test(this.value)) {
                valor = String(this.value);
                this.value = valor.slice(0, -1);
            }
        });
    });
}

function validarLetras (caompoTexto) {
    ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
        caompoTexto.addEventListener(event, function() {
            if (!/^[a-zA-Z\b]+$/.test(this.value)) {
                valor = String(this.value);
                this.value = valor.slice(0, -1);
            }
        });
    });
}

function borrarIMG() {
    if (tieneFoto && originalEliminada) {
        document.getElementById('img_usuario').src = '/assets/rsc/pngs/user_white.png';
    } else if (tieneFoto) {
        document.getElementById('img_usuario').src = document.getElementById('imgOriginal').src;
        document.getElementById('deleteOriginal').style.visibility = 'visible';
    } else {
        document.getElementById('img_usuario').src = '/assets/rsc/pngs/user_white.png';
    }
    console.log(document.getElementById('img_usuario').src);
    document.getElementById('file_imgUsuario').value = '';
    var btn = document.getElementById('btn_cargarImg');
    btn.className = 'btn_cargar';
    btn.innerHTML = '<i class="far fa-images fa-2x"></i>';
    btn.onclick = presionarInputFile;    
}

function cargarIMG() {    
    this.onchange = e => {        
        if(e.target.files[0]){            
            imagenCargada = new Image();                    
            imagenCargada.onload = function () {                                
                document.getElementById('img_usuario').src = this.src;
                var btn = document.getElementById('btn_cargarImg');
                btn.className = 'btn_borrar';
                btn.innerHTML = '<i class="fas fa-trash-alt fa-2x"></i>';
                btn.onclick = borrarIMG;
                if (tieneFoto) {
                    document.getElementById('deleteOriginal').style.visibility = 'hidden';
                }
            }
            imagenCargada.src = URL.createObjectURL(e.target.files[0]);
        }                         
    };
}

function eliminarOriginal () {
    document.getElementById('img_usuario').src = '/assets/rsc/pngs/user_white.png';
    document.getElementById('file_imgUsuario').value = '';
    originalEliminada = true;
    document.getElementById('deleteOriginal').style.visibility = 'hidden';
    document.getElementById('borrarOriginal').value = 1;
}

function mostrarPassword() {
    var x = document.getElementById('txt_password');
    if (x.type === "password") {
        x.type = "text";
        document.getElementById('btn_show').innerHTML = '<i class="fas fa-eye-slash">';
    } else {
        x.type = "password";
        document.getElementById('btn_show').innerHTML = '<i class="fas fa-eye">';        
    }
}