pagina = 1;
filtrando = false;

window.addEventListener('load', funcionCargar, false);

function funcionCargar () {
    // Le seteamos el metodo click al boton de filtrar y evitamos que accione el form, y asi utilizaremos los datos
    // del form para filtrar la informacion
    document.getElementById('btn_filtrar').addEventListener('click', function(e) {
        e.preventDefault();
        filtrar();
    }, false);

    $(document).on('click', '.pagination-a', function(event){
        event.preventDefault(); 
        pagina = $(this).attr('href').split('page=')[1];
        if (filtrando) {
            filtrar();
        } else {
            paginarAjax();
        }
    });
}

function paginarAjax() {
    var _token = $("input[name=_token]").val();
    $.ajax({
        url:"/estudiantes",
        method:"get",
        data:{_token:_token, page:pagina},
        success:function(data) {
            refrescarItems(data);
        }
    });
}


// Metodo que limpia los filtros de los usuarios
function limpiarFiltros() {
    $("#txt_nombre").val('');
    $("#txt_cedula").val('');
    filtrar();
    filtrando = false;
    pagina = 1;
}

// Metodo para filtrar los usuarios de manera asincronica
function filtrar () {    
    
    var _token = $("input[name='_token']").val();
    var txt_nombre = $('#txt_nombre').val();
    var txt_cedula = $('#txt_cedula').val();

    txt_cedula = txt_cedula.replace(/ /g,'');
    console.log(txt_cedula);

    $.ajax({
        url: "/estudiantes/filtrar",
        type:'POST',
        data: { _token:_token, txt_nombre:txt_nombre, txt_cedula:txt_cedula, page:pagina },
        success: function(data) {  
            refrescarItems(data);
            filtrando = true;
        },   
        error: function(data) {
            console.error(data);
        }
    });
}

//  Metodo que refresca a los usuarios en caso de haber cambiado, esencial para filtrar, dar de baja y activar usuarios
function refrescarItems(data) { 
    document.getElementById('div_der').innerHTML = '<i class="fas fa-spinner fa-pulse fa-10x cargando"></i>';    
    setTimeout(() => {        
        var temphtml = document.createElement('div'); //div temporal
        temphtml.innerHTML = data;
        document.getElementById('div_der').innerHTML = temphtml.querySelector("#div_der").innerHTML;
    }, 500);    
}

// function acualizarEstudiantes() {
//     document.getElementById('acualizarEstudiantes').click();
// }

function actualizarEstudiantes() {
    alertify.confirm('Actualizar Lista de Estudiantes',                
                'Este proceso dura algunos minutos, y deberá esperar hasta que sea completado.<br>' + 
                '¿Esta seguro que desea actualizar la lista de estudiantes?\n', 
                function () {
                    ajaxActualizarLista();
                },                 
                function(){}).set('movable', false)
                                .set('modal', true)
                                .set('labels', {ok:'Aceptar', cancel:'Cancelar'});
}

function ajaxActualizarLista(){ 
                            
    var divLoading = document.getElementById('div_loading');    
    divLoading.style.visibility = 'visible';

    $.ajax({
        url: '/estudiantes/actualizar',
        success: function (data) {
            if(data) {
                alertify.alert('Exito!', 'Estudiantes actualizados correctamente', function(){ location.href = '/estudiantes'; })
                    .set('movable', false)
                    .set('modal', true)
                    .set('label', 'Aceptar');
            }                            
        },
        error: function (error) {
            console.log(error);
            alertify.alert('Mala suerte!', 'Ha ocurrido un error <br> ¿Desea intentar nuevamente?', 
                            function() { ajaxActualizarLista(); }, 
                            function() {
                                window.location.reload();                            
                            })
                .set('movable', false)
                .set('modal', true)
                .set('label', {ok:'Aceptar!', cancel:'Cancelar!'});            
                
        }
    });             
}