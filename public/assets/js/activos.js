pagina = 1;
filtrando = false;

window.addEventListener('load', funcionCargar, false);

function funcionCargar () {
    // Le seteamos el metodo click al boton de filtrar y evitamos que accione el form, y asi utilizaremos los datos
    // del form para filtrar la informacion
    document.getElementById('btn_filtrar').addEventListener('click', function(e) {
        e.preventDefault();
        filtrar();
    }, false);

    $(document).on('click', '.pagination-a', function(event){
        event.preventDefault(); 
        pagina = $(this).attr('href').split('page=')[1];
        if (filtrando) {
            filtrar();
        } else {
            paginarAjax();
        }
    });
}

function paginarAjax() {
    var _token = $("input[name=_token]").val();
    $.ajax({
        url:"/activos",
        method:"get",
        data:{_token:_token, page:pagina},
        success:function(data) {
            refrescarItems(data);
        }
    });
}

function filtrar () {    
    
    var _token = $("input[name='_token']").val();
    var txt_nom_activo = $('#txt_nom_activo').val();
    var txt_cod_activo = $('#txt_cod_activo').val();
    var cmb_institucion = $('#cmb_institucion').val();
    var cmb_estado = $('#cmb_estado').val();
    
    // console.log(_token);
    
    $.ajax({
        url: "/activos/filtrar",
        type:'POST',
        data: { _token:_token, txt_nom_activo:txt_nom_activo, txt_cod_activo:txt_cod_activo, cmb_institucion:cmb_institucion, cmb_estado:cmb_estado, page:pagina},
        success: function(data) {      
            // console.log(data);      
            refrescarItems(data);
            filtrando = true;
        },   
        error: function(data) {
            console.error(data);
        }
    });
}


// Metodo que mustra una confirmacion para eliminar el usuario seleccinado
function confirmarBaja(boton) {
    var id = boton.id;
    var nombreActivo = boton.name;    
    alertify.confirm('Dar ' + nombreActivo + ' de baja',
                '¿De verdad desea dar de baja el activo: '  + nombreActivo+ ' ?', 
                function(){ 
                    $.ajax({
                        url: '/activos/darBaja/'+id,
                        success: function (data) {
                            refrescarItems(data);
                            alertify.success('El activo ' + nombreActivo + ' fue dado de baja exitosamente.');
                        },
                        error: function (data) {
                            alertify.error('¡El activo no pudo ser dado de baja!');
                        }
                    });                    
                },                 
                function(){ 
                    alertify.warning('Movimiento cancelado');
                }).set('movable', false);
}

// Metodo para activar un activo que se encontraba dado de baja
function activarActivo(boton) {
    var id = boton.id;
    var nombreActivo = boton.name;    
    alertify.confirm('Activar ' +  nombreActivo,
                '¿De verdad desea activar el activo: ' +  nombreActivo +' ?', 
                function(){ 
                    $.ajax({
                        url: '/activos/activar/'+id,
                        success: function (data) {
                            refrescarItems(data);
                            // alertify.success('El activo '+ nombreActivo + ' fue activado con éxito.');
                        },
                        error: function (data) {
                            alertify.error(data);
                        }
                    });                    
                },                 
                function(){ 
                    alertify.warning('Movimiento cancelado');
                }).set('movable', false); 
}

// Metodo para pasar a la vista de editar un activo
function editarActivo(boton) { // Recibe el boton al que se le dio click para obtener el id del activo por editar
    var id = boton.id;
    // Usamos el id obtenido para buscar al tag "a" especifico que edita a ese activo 
    document.getElementById('a_editar_' + id).click(); //  y le damos click
}

//Metodo para pasar a la vista de agregar un nuevo activo
function irNuevoActivo() {
    document.getElementById('a_nuevoActivo').click();
}

// Metodo especifico para el boton de cancelar en la ventana de agregar o editar un activo
function cancelar() {
    document.getElementById('a_cancelar').click();
}

// Metodo para bueno, presionar el input file xd
function presionarInputFile() {
    document.getElementById('file_imgActivo').click();
}

// Metodo para limpiar la foto, o remover la foto que se haya seleccionado
function borrarIMG() {
    document.getElementById('img_activo').src = '../rsc/pngs/activo_default.png';
    document.getElementById('file_imgActivo').value = '';
    var btn = document.getElementById('btn_cargarImg');
    btn.className = 'btn_cargar';
    btn.innerHTML = '<i class="far fa-images fa-2x"></i>';
    btn.onclick = presionarInputFile;
    document.getElementById('cambios_img').value = '1';
}

// Metodo para cargar la imagen
function cargarIMG() {
    this.onchange = e => {        
        if(e.target.files[0]){            
            imagenCargada = new Image();                    
            imagenCargada.onload = function () {                                
                document.getElementById('img_activo').src = this.src;
                var btn = document.getElementById('btn_cargarImg');
                btn.className = 'btn_borrar';
                btn.innerHTML = '<i class="fas fa-trash-alt fa-2x"></i>';
                btn.onclick = borrarIMG;
                document.getElementById('cambios_img').value = '1';
            }
            imagenCargada.src = URL.createObjectURL(e.target.files[0]);
        }                         
    };
}

//  Metodo que refresca a los usuarios en caso de haber cambiado, esencial para filtrar, dar de baja y activar usuarios
function refrescarItems(data) { 
    document.getElementById('div_der').innerHTML = '<i class="fas fa-spinner fa-pulse fa-10x cargando"></i>';    
    setTimeout(() => {        
        var temphtml = document.createElement('div'); //div temporal
        temphtml.innerHTML = data;
        document.getElementById('div_der').innerHTML = temphtml.querySelector("#div_der").innerHTML;
    }, 500);    
}

function limpiarFiltros() {
    $("#txt_nom_activo").val('');
    $("#txt_cod_activo").val('');
    $("#cmb_institucion").val('T');
    $("#cmb_tipo").val('T');
    $("#cmb_estado").val('T');

    filtrar ();

    filtrando = false;
    pagina = 1;
}

function verMas(act) {
    console.log(act);
    alertify.prompt()
        .set('movable',false)
        .setHeader(act.codActivo + ' | ' + act.nombre)
        .set('frameless', true)
        .setContent(
            '<div id="form_activo">' +
                    '<div id="div_datos">' +
                        '<div class="columna">' +
                            '<label for="txt_cod_activo">' +
                                'Código del activo: <br>' +
                                '<input readonly class="txt" type="text" name="txt_cod_activo" id="txt_cod_activo" value="' + act.codActivo + '">' +
                            '</label>' +    
                            '<label for="txt_nom_activo">' +
                                'Nombre del activo: <br>' +
                                '<input readonly class="txt" type="text" name="txt_nom_activo" id="txt_nom_activo" value="' + act.nombre + '">' +
                            '</label>' +
                            '<label for="txt_marca_activo">' +
                                'Marca del activo: <br>' +
                                '<input readonly class="txt" type="text" name="txt_marca_activo" id="txt_marca_activo" value="' + act.marca + '">' +
                            '</label>  ' +
                            '<label for="txt_modelo_activo">' +
                                'Modelo del activo: <br>' +
                                '<input readonly class="txt" type="text" name="txt_modelo_activo" id="txt_modelo_activo" value="' + act.modelo + '">' +
                            '</label>' +
                        '</div>' +
                        '<div class="columna">' +
                            '<label for="txt_serie_activo">' +
                                '# Serie del activo: <br>' +
                                '<input readonly class="txt" type="text" name="txt_serie_activo" id="txt_serie_activo" value="' + act.serie + '">' +
                            '</label>' +
                            '<label for="txt_valor_activo">' +
                                'Valor ₡ del activo: <br>' +
                                '<input readonly class="txt" type="text" name="txt_valor_activo" id="txt_valor_activo" value="' + act.valor + '">' +
                            '</label>' +
                            '<label for="txt_ubicacion_activo">' +
                                'Ubicación del activo: <br>' +
                                '<input readonly class="txt" type="text" name="txt_ubicacion_activo" id="txt_ubicacion_activo" value="' + act.ubicacion + '">' +
                            '</label>' +
                            '<label for="txt_resp_activo">' +
                                'Responsable del activo: <br>' +
                                '<input readonly class="txt" type="text" name="txt_resp_activo" id="txt_resp_activo" value="' + act.responsable + '">' +
                            '</label>' +
                        '</div>' +
                        '<div class="columna">' +
                            '<label  for="cmb_institucion">' +
                                'Institución:<br>' +
                                '<input readonly type="text" class="txt" name="cmb_institucion" id="cmb_institucion" class="cmb_datos" value="' + act.institucion.nombre + '">' +
                            '</label>' +
                
                            '<label for="cmb_estado">' +
                                'Estado: <br>' +
                                '<input readonly type="text" class="txt" name="cmb_estado" id="cmb_estado" value="' + act.estado_activo.estado + '">' +
                            '</label>' +
                
                            '<label for="txt_observ_activo">' +
                                'Observaciones sobre el activo: <br>' +
                                '<textarea readonly class="txt_area" type="text" cols="30" rows="8" name="txt_observ_activo" id="txt_observ_activo" value="' + act.observaciones + '"></textarea>' +
                            '</label>' +
                        '</div>        ' +
                    '</div>' +
                '</div>'
            )
            .set('resizable',true).resizeTo('75%', '70%').show();
}