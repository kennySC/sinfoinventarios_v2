pagina = 1;
filtrando = false;

window.addEventListener('load', funcionCargar, false);

function funcionCargar () {
    // Le seteamos el metodo click al boton de filtrar y evitamos que accione el form, y asi utilizaremos los datos
    // del form para filtrar la informacion
    try {
        document.getElementById('btn_filtrar').addEventListener('click', function(e) {
            e.preventDefault();
            filtrar();
        }, false);
    } catch (error) { }
}

function filtrar () {    
    
    var _token = $("input[name='_token']").val();
    var txt_cod_boleta = $('#txt_cod_boleta').val();
    var txt_responsable = $('#txt_responsable').val();
    var cmb_estado = $('#cmb_estado').val();

    console.log(_token);

    $.ajax({
        url: "/boletas/filtrar",
        type:'POST',
        data: { _token:_token, txt_cod_boleta:txt_cod_boleta, txt_responsable:txt_responsable, cmb_estado:cmb_estado},
        success: function(data) {      
            console.log(data);      
            refrescarItems(data);
            filtrando = true;
        },   
        error: function(data) {
            console.error(data);
        }
    });
}

//  Metodo que refresca a los usuarios en caso de haber cambiado, esencial para filtrar, dar de baja y activar usuarios
function refrescarItems(data) { 
    document.getElementById('div_der').innerHTML = '<i class="fas fa-spinner fa-pulse fa-10x cargando"></i>';    
    setTimeout(() => {        
        var temphtml = document.createElement('div'); //div temporal
        temphtml.innerHTML = data;
        document.getElementById('div_der').innerHTML = temphtml.querySelector("#div_der").innerHTML;
    }, 500);    
}

function limpiarFiltros() {
    $("#txt_cod_boleta").val('');
    $("#txt_responsable").val('');
    $("#cmb_estado").val('T');

    filtrar ();

    filtrando = false;
    pagina = 1;
}

//Metodo para pasar a la vista de agregar un nuevo activo
function irNuevaBoleta() {
    window.location.href = '/boletas/agregar';
}

// Metodo para pasar a la vista donde se mostraran a fondo los detalles de una boleta de salida
function ver_detalle(boton) {
    var id = boton.id;
    window.location.href = '/boletas/detalle/' + id;
}